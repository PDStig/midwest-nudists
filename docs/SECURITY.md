# Composr CMS Security policy #

Our security policy is covered in full in a section of [Problem and feedback reports, and development policies](https://composr.app/docs/tut-software-feedback.htm). This page also includes our maintenance policies for older versions.

Basically we appreciate those who report security issues, and respect their right to do full disclosures. We ask though that disclosure is made responsibly, so Composr CMS users have time to upgrade before problems are widely reported.

Issues will be fixed within a month. Whether they will or will not be disclosed directly by the Composr CMS developers depends on context (basically what we think is best for users based on the situation).

To report an issue, submit to [the tracker](https://composr.app/tracker), with 'Severity' marked as 'Security-hole' and 'View Status' as 'private'.
