# Contributing to Composr CMS #

Information about ways you can contribute to the Composr CMS project is on our [contributions page](https://composr.app/contributions.htm).

## Contributing code ##

If you'd like to contribute code to the project, make a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) on [the GitLab repository](https://gitlab.com/composr-foundation/composr).
Make merge requests for a fairly recent version of the branch you intend them to be released to so that merging will be easy.
Restrict any particular merge request to handling one thing. This does require discipline (probably lots of different branches in your fork), but it is important.

If contributing code then please run the automated tests before making your merge request, to reduce the chance of you having accidentally broken something. Do that by opening `http://yourbaseurl/_tests` in your browser and using the UI there.

It is good if you can create your own automated tests, but it is not mandatory.

Please try to follow the [coding standards](https://composr.app/docs/codebook-standards.htm).

There is a lot of information about Composr coding in the [Code Book](https://composr.app/docs/codebook.htm).

## Making suggestions ##

This is covered under [support](SUPPORT.md).
