{+START,SET,manual_approval}
	[semihtml]
		<p>New members must be manually approved by staff before they can log in. You will be notified by e-mail when/if your account has been approved.</p>
		<p>Approval is largely based on what you write in the field 'Describe your interest / involvement in the nudist community'. This field requires at minimum 128 characters. This is to help prevent unsolicited or spam registrations.</p>
	[/semihtml]
{+END}

{+START,SET,blacklist_text}
	[semihtml]
		<p>Some e-mail providers may block emails from Midwest Nudists. Sometimes there is nothing we can do about that. If you register and do not get an email to verify your address within one hour (be sure to check your spam), please contact us by going in the menu under Help > Contact Us, and select "I joined but did not receive a verification e-mail" (do not email support@midwestnudists.org as your incoming email might also get blocked).</p>
		
		<p>The following e-mail domains have been known to block e-mails to/from us; please use an e-mail from a domain not on this list if possible:</p>
		
		{!mwn_misc:CNS_JOIN_EMAIL_BLACKLIST_DOMAINS}
	[/semihtml]
{+END}

{$COMCODE,[box="Spam prevention / manual account approval"]{$GET,manual_approval}[/box]}
{$COMCODE,[box="Known issues with e-mail domains"]{$GET,blacklist_text}[/box]}