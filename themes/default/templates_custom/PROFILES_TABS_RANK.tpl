{$REQUIRE_CSS,stepper}

<div class="stepper-wrapper">
	{+START,LOOP,RANKS}
		{+START,IF,{$EQ,{SIMPLE_STEPPER},0}}
			<div class="stepper-item{+START,IF,{$GT,{RANK_STATUS},1}} completed{+END}{+START,IF,{$EQ,{RANK_STATUS},1}} active{+END}">
				<div class="step-counter">{RANK_THRESHOLD*}</div>
				<div class="step-name">{RANK_NAME*}</div>
			</div>
		{+END}
		{+START,IF,{$EQ,{SIMPLE_STEPPER},1}}
			<div class="stepper-item simple{+START,IF,{$GT,{RANK_STATUS},1}} completed{+END}{+START,IF,{$EQ,{RANK_STATUS},1}} active{+END}">
				<div class="step-counter" title="{RANK_NAME*} ({RANK_THRESHOLD*})"> </div>
			</div>
		{+END}
	{+END}
</div>

<p>Your current rank is <strong>{CURRENT_RANK*}</strong>. You need <strong>{NUM_POINTS_ADVANCE*}</strong> more life-time points (points you ever earned) to advance to the next rank.</p>

<hr>
								
<p>Here are a list of rank-based privileges you have unlocked or have yet to unlock, and any rank-based restrictions in place:</p>

<p>✅Unlocked themes: {UNLOCKED_THEMES*}</p>
{+START,IF,{$EQ,{HAS_DAILY_UPLOAD_QUOTA},0}}<p>✅Daily upload quota (for attachments)*: <strong>Unlimited!</strong></p>{+END}
{+START,IF,{$EQ,{HAS_DAILY_UPLOAD_QUOTA},1}}{+START,IF,{$GT,{_DAILY_UPLOAD_QUOTA},0}}<p>✅Daily upload quota (for attachments)*: <strong>{DAILY_UPLOAD_QUOTA*} MB</strong></p>{+END}{+END}
{+START,IF,{$GT,{_MAX_ATTACHMENTS},0}}<p>✅Maximum attachments allowed per post*: <strong>{MAX_ATTACHMENTS*}</strong></p>{+END}
{+START,IF,{$GT,{_POST_LENGTH},0}}<p>✅Maximum post length in topics: <strong>{POST_LENGTH*} characters</strong></p>{+END}
{+START,IF,{$GT,{_SIGNATURE_LENGTH},0}}<p>✅Maximum signature length: <strong>{SIGNATURE_LENGTH*} characters</strong> (including HTML/Comcode tags)</p>{+END}
{+START,IF,{$EQ,{CAN_UPLOAD_AVATARS},1}}<p>✅Maximum avatar dimensions*: <strong>{MAXIMUM_AVATAR_DIMENSIONS*}</strong> (if you upload a bigger one, it will be resized)</p>{+END}
{+START,IF,{$EQ,{INFINITE_PERSONAL_GALLERY_ENTRIES},0}}{+START,IF,{$GT,{_PERSONAL_GALLERY_ENTRIES_IMAGES},0}}<p>✅Maximum images allowed in your personal gallery*: <strong>{PERSONAL_GALLERY_ENTRIES_IMAGES*}</strong></p>{+END}{+END}
{+START,IF,{$EQ,{INFINITE_PERSONAL_GALLERY_ENTRIES},0}}{+START,IF,{$GT,{_PERSONAL_GALLERY_ENTRIES_VIDEOS},0}}<p>✅Maximum videos/multimedia allowed in your personal gallery*: <strong>{PERSONAL_GALLERY_ENTRIES_VIDEOS*}</strong></p>{+END}{+END}
{+START,IF,{$EQ,{INFINITE_PERSONAL_GALLERY_ENTRIES},1}}<p>✅Maximum images/videos/multimedia allowed in your personal gallery*: <strong>Unlimited!</strong></p>{+END}
{+START,IF,{$GT,{_GIFT_POINTS},0}}<p>✅Gift points made available to you (flat number based on rank; you may have used some of these already)**: <strong>{GIFT_POINTS*}</strong></p>{+END}
{+START,IF,{$GT,{_GIFT_POINTS_PER_DAY},0}}<p>✅Gift points made available to you for each day since you registered**: <strong>{GIFT_POINTS_PER_DAY*}</strong></p>{+END}
								
{+START,LOOP,UNLOCKED_PRIVILEGES}
	<p>✅Unlocked privilege: "{PRIVILEGE*}" (scope: {SCOPE*})</p>
{+END}
								
{+START,IF,{$GT,{_FLOOD_CONTROL_SUBMIT},0}}<p>❌Must wait <strong>{FLOOD_CONTROL_SUBMIT*} seconds</strong> between loading a form and submitting it</p>{+END}
{+START,IF,{$GT,{_FLOOD_CONTROL_ACCESS},0}}<p>❌Must wait <strong>{FLOOD_CONTROL_ACCESS*} seconds</strong> between each page load</p>{+END}
							
<p>🔒Themes not yet unlocked: {LOCKED_THEMES*}</p>
{+START,IF,{$EQ,{HAS_DAILY_UPLOAD_QUOTA},1}}{+START,IF,{$EQ,{_DAILY_UPLOAD_QUOTA},0}}<p>🔒Ability to upload attachments* not yet unlocked</p>{+END}{+END}
{+START,IF,{$EQ,{_MAX_ATTACHMENTS},0}}<p>🔒Ability to add attachments to posts* not yet unlocked</p>{+END}
{+START,IF,{$EQ,{_POST_LENGTH},0}}<p>🔒Ability to make posts in the forums not yet unlocked</p>{+END}
{+START,IF,{$EQ,{_SIGNATURE_LENGTH},0}}<p>🔒Ability to use signatures not yet unlocked</p>{+END}
{+START,IF,{$EQ,{INFINITE_PERSONAL_GALLERY_ENTRIES},0}}{+START,IF,{$EQ,{_PERSONAL_GALLERY_ENTRIES_IMAGES},0}}{+START,IF,{$EQ,{_PERSONAL_GALLERY_ENTRIES_VIDEOS},0}}<p>🔒Ability to use your own personal gallery* not yet unlocked</p>{+END}{+END}{+END}
{+START,IF,{$AND,{$EQ,{_GIFT_POINTS},0},{$EQ,{_GIFT_POINTS_PER_DAY},0}}}<p>🔒Ability to use gift points** not yet unlocked</p>{+END}

{+START,LOOP,LOCKED_PRIVILEGES}
	<p>🔒Privilege "{PRIVILEGE*}" (scope: {SCOPE*}) not yet unlocked</p>
{+END}
								
<p>*If you have not yet verified your age and identity, then file uploading privileges will always be listed as not yet unlocked. <a href="{$PAGE_LINK*,site:verification}">Click here to verify</a>.</p>
<p>**Gift points are free, non-spendable points you can give to other members (which become spendable points for them). It is a way to reward others without deducting from your own points balance.</p>