{$REQUIRE_CSS,stepper}

<div class="stepper-wrapper">
		{+START,IF,{$EQ,{STANDING},1}}<div class="stepper-item active">{+END}
		{+START,IF,{$NEQ,{STANDING},1}}<div class="stepper-item">{+END}
		<div class="step-counter"{+START,IF,{$EQ,{STANDING},1}} style="background: #FF3D00; color: #FFFFFF;"{+END}>‼️</div>
		<div class="step-name">Danger</div>
	</div>
		{+START,IF,{$EQ,{STANDING},2}}<div class="stepper-item active">{+END}
		{+START,IF,{$NEQ,{STANDING},2}}<div class="stepper-item">{+END}
		<div class="step-counter"{+START,IF,{$EQ,{STANDING},2}} style="background: #FF9100; color: #000000;"{+END}>🚫</div>
		<div class="step-name">Probation</div>
	</div>
		{+START,IF,{$EQ,{STANDING},3}}<div class="stepper-item active">{+END}
		{+START,IF,{$NEQ,{STANDING},3}}<div class="stepper-item">{+END}
		<div class="step-counter"{+START,IF,{$EQ,{STANDING},3}} style="background: #FFC400; color: #000000;"{+END}>🔒</div>
		<div class="step-name">Restricted</div>
	</div>
		{+START,IF,{$EQ,{STANDING},4}}<div class="stepper-item active">{+END}
		{+START,IF,{$NEQ,{STANDING},4}}<div class="stepper-item">{+END}
		<div class="step-counter"{+START,IF,{$EQ,{STANDING},4}} style="background: #FFEA00; color: #000000;"{+END}>☢️</div>
		<div class="step-name">At risk</div>
	</div>
		{+START,IF,{$EQ,{STANDING},5}}<div class="stepper-item active">{+END}
		{+START,IF,{$NEQ,{STANDING},5}}<div class="stepper-item">{+END}
		<div class="step-counter"{+START,IF,{$EQ,{STANDING},5}} style="background: #C6FF00; color: #FFFFFF;"{+END}>⚠️</div>
		<div class="step-name">Caution</div>
	</div>
		{+START,IF,{$EQ,{STANDING},6}}<div class="stepper-item active">{+END}
		{+START,IF,{$NEQ,{STANDING},6}}<div class="stepper-item">{+END}
		<div class="step-counter"{+START,IF,{$EQ,{STANDING},6}} style="background: #00E676; color: #FFFFFF;"{+END}>✅</div>
		<div class="step-name">Good</div>
	</div>
</div>
						
<p>{!DESCRIPTION_ACCOUNT_STANDING,{$PAGE_LINK*,:rules},{$PAGE_LINK*,site:catalogues:entry:52}}</p>
<hr>
<p>{SUMMARY*}</p>

{+START,IF,{$EQ,{STANDING},1}}
	{+START,IF,{$GT,{PREVIOUSLY_BANNED},0}}<p>‼️{!ACCOUNT_STANDING_PREVIOUSLY_BANNED_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{SPAM_SYNDICATION},0}}<p>‼️{!ACCOUNT_STANDING_SPAM_SYNDICATION_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{BAD_KARMA},249}}<p>‼️{!ACCOUNT_STANDING_1_BAD_KARMA_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{FORMAL_WARNINGS},5}}<p>‼️{!ACCOUNT_STANDING_1_WARNINGS_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{PROBATION_TOTAL},13}}<p>‼️{!ACCOUNT_STANDING_1_PROBATION_TOTAL_FOOTNOTE}</p>{+END}
{+END}

{$,These we always want to show regardless of standing}
{+START,IF,{$GT,{_PROBATION_REMAINING},0}}<p>🚫{!ACCOUNT_STANDING_2_PROBATION_REMAINING_FOOTNOTE,{PROBATION_REMAINING*}}</p>{+END}
{+START,LOOP,SILENCE_FORUMS_NOW}
	<p>🔒{!ACCOUNT_STANDING_SILENCE_FORUM_FOOTNOTE,{FORUM_NAME*},{TIME_LEFT*}}</p>
{+END}
{+START,LOOP,SILENCE_TOPICS_NOW}
	<p>🔒{!ACCOUNT_STANDING_SILENCE_TOPIC_FOOTNOTE,{TOPIC_NAME*},{FORUM_NAME*},{TIME_LEFT*}}</p>
{+END}
{+START,LOOP,PRIVILEGES_REVOKED_NOW}
	<p>🔒{!ACCOUNT_STANDING_PRIVILEGE_REVOKED_FOOTNOTE,{PRIVILEGE*},{MODULE_THE_NAME*},{CATEGORY_NAME*},{THE_PAGE*},{TIME_LEFT*}}</p>
{+END}
{$,TODO access revoke}
{+START,IF,{$LT,{STANDING},5}}<p>☢️{!ACCOUNT_STANDING_4_FOOTNOTE*}</p>{+END}
						
{+START,IF,{$EQ,{STANDING},4}}
	{+START,IF,{$GT,{BAD_KARMA},99}}<p>☢️{!ACCOUNT_STANDING_4_BAD_KARMA_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{FORMAL_WARNINGS},2}}<p>☢️{!ACCOUNT_STANDING_4_WARNINGS_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{PROBATION_TOTAL},0}}<p>☢️{!ACCOUNT_STANDING_4_PROBATION_TOTAL_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{PRIVILEGES_REVOKED_TOTAL},0}}<p>☢️{!ACCOUNT_STANDING_4_PRIVILEGES_REVOKED_TOTAL_FOOTNOTE}</p>{+END}
	{+START,IF,{$GT,{ACCESS_REVOKED_TOTAL},0}}<p>☢️{!ACCOUNT_STANDING_4_ACCESS_REVOKED_TOTAL_FOOTNOTE}</p>{+END}
{+END}

{+START,IF,{$LT,{STANDING},6}}<p>⚠️{!ACCOUNT_STANDING_5_FOOTNOTE*}</p>{+END}

<hr>