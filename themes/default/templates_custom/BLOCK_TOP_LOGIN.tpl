{$REQUIRE_JAVASCRIPT,checking}

{+START,IF,{$NOR,{$GET,login_screen},{$MATCH_KEY_MATCH,_WILD:login}}}
	<div data-tpl="blockTopLogin">
		<form title="{!_LOGIN}" action="{LOGIN_URL*}" method="post" class="form-inline top-login" autocomplete="on">
			<input type="hidden" name="_active_login" value="1" />
			<ul class="horizontal-links with-icons block-top-login-links">
				<li class="li-login"><a data-open-as-overlay="{}" rel="nofollow" href="{FULL_LOGIN_URL*}" title="{!_LOGIN}">{+START,INCLUDE,ICON}NAME=menu/site_meta/user_actions/login{+END} {!_LOGIN} {!OR} {!JOIN}</a></li>
			</ul>
		</form>
	</div>
{+END}
