<h3>{!WEATHER_REPORT}{+START,IF_PASSED,LOCATION}: {LOCATION*}{+END}</h3>	

{+START,IF_NON_EMPTY,{CURRENT_CONDITIONS}}
	{+START,LOOP,CURRENT_CONDITIONS}
		<div class="float-surrounder">
			<h4>{!CURRENT_CONDITIONS}</h4>
			<small>{!CURRENT_CONDITIONS_TIMESTAMP,{TIMESTAMP*}}</small>

			{+START,IF_PASSED,ICON_URL}
				<img class="right" src="{$ENSURE_PROTOCOL_SUITABILITY*,{ICON_URL}}" alt="" title="" />
			{+END}

			<ul>
				{+START,LOOP,VALUES}
					<li>{_loop_var*}</li>
				{+END}
			</ul>
		</div>
	{+END}
{+END}

{+START,IF_NON_EMPTY,{FORECASTS}}
	{+START,LOOP,FORECASTS}
		<div class="float-surrounder">
			<h4>{FORECAST_DAY*}</h4>
			<small>{!FORECAST_TIMESTAMP,{TIMESTAMP_START*},{TIMESTAMP_END*}}</small>

			{+START,IF_PASSED,ICON_URL}
				<img class="right" src="{$ENSURE_PROTOCOL_SUITABILITY*,{ICON_URL}}" alt="" title="" />
			{+END}

			<ul>
				{+START,LOOP,VALUES}
					<li>{_loop_var*}</li>
				{+END}
			</ul>
		</div>
	{+END}
{+END}