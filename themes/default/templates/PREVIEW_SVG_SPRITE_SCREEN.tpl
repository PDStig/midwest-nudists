<div class="preview-svg-sprite-screen">
	{TITLE}

	<h3>{!PREVIEWING_SPRITE,{SPRITE_PATH*}}</h3>

	<div class="preview-svg-sprite-screen-icons">
		{ICONS}
	</div>
</div>
