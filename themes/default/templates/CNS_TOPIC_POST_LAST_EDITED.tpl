<p class="edited" role="note">
	<img alt="" width="9" height="6" src="{$IMG*,edited}" />
	<span>{!LAST_EDIT_BY,<time datetime="{$FROM_TIMESTAMP*,Y-m-d\TH:i:s\Z,{LAST_EDIT_DATE_RAW}}">{LAST_EDIT_DATE*}</time>,<a href="{LAST_EDIT_PROFILE_URL*}">{$DISPLAYED_USERNAME*,{LAST_EDIT_USERNAME}}</a>}</span>
</p>
