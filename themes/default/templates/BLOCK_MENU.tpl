{+START,IF_NON_EMPTY,{CONTENT}}
	<section class="box box---block-menu-{TYPE*}"><div class="box-inner">
		<h3>{TITLE}</h3>

		{CONTENT}
	</div></section>
{+END}
