<div class="table-cell-nowrap vertical-alignment">
	{+START,INCLUDE,ICON}
		NAME=admin/edit
		ICON_SIZE=14
	{+END}
	<a href="{EDIT_URL*}"><strong>{!EDIT}</strong></a>
</div>
&nbsp;
<div class="table-cell-nowrap vertical-alignment">
	{+START,INCLUDE,ICON}
		NAME=admin/install
		ICON_SIZE=14
	{+END}
	<a href="{CLONE_URL*}">{!CLONE}</a>
</div>
<div class="table-cell-nowrap vertical-alignment">
	{+START,INCLUDE,ICON}
		NAME=buttons/proceed
		ICON_SIZE=14
	{+END}
	<a href="{VIEW_URL*}">{!VIEW}</a>
</div>
