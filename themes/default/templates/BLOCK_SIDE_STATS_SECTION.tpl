{+START,IF_NON_EMPTY,{CONTENT}}
	<ul class="compact-list">
		<li>
			<p class="lonely-label">{SECTION*}:</p>
			<dl class="compact-list associated-details">
				{CONTENT}
			</dl>
		</li>
	</ul>
{+END}
