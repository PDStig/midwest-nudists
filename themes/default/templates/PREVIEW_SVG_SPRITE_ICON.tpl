<div class="preview-svg-sprite-icon">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon" role="presentation" width="48" height="48"><use xlink:href="{SPRITE_URL*}#{SYMBOL_ID*}" /></svg>
	<p class="icon-name">#{SYMBOL_ID*}</p>
</div>
