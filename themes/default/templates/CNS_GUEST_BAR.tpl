{$REQUIRE_JAVASCRIPT,core_cns}

<section id="tray-{!MEMBER|}" data-tpl="cnsGuestBar" data-toggleable-tray="{ save: true }" class="box box--cns-guest-bar cns-information-bar-outer">
	<div class="box-inner">
		<h2 class="toggleable-tray-title">
			<a class="toggleable-tray-button js-tray-onclick-toggle-tray inline-desktop" href="#!" title="{!CONTRACT}">{+START,INCLUDE,ICON}
				NAME=trays/contract
				ICON_SIZE=24
			{+END}</a>

			<a class="toggleable-tray-button js-tray-onclick-toggle-tray" href="#!">{!_LOGIN}{+START,IF,{$ADDON_INSTALLED,search}}{+START,IF,{$ADDON_INSTALLED,search}}{+START,IF,{$HAS_ACTUAL_PAGE_ACCESS,search}} / {!SEARCH}{+END}{+END}{+END}</a>
		</h2>

		<div class="toggleable-tray js-tray-content">
			<div class="cns-information-bar clearfix">
				<div class="cns-guest-column cns-guest-column-a">
					<form title="{!_LOGIN}" class="inline" action="{LOGIN_URL*}" method="post" autocomplete="on">
						<input type="hidden" name="_active_login" value="1" />

						<div>
							<div class="accessibility-hidden"><label for="member-bar-login-username">{$LOGIN_LABEL}</label></div>
							<div class="accessibility-hidden"><label for="member-bar-s-password">{!PASSWORD}</label></div>
							<input size="15" type="text" placeholder="{!USERNAME}" id="member-bar-login-username" class="form-control" name="username" autocomplete="username" />
							<input size="15" type="password" placeholder="{!PASSWORD}" name="password" autocomplete="current-password" id="member-bar-s-password" class="form-control" />

							<span class="inline-lined-up">
								{+START,IF,{$EQ,{$CONFIG_OPTION,remember_me_behaviour},default_off,default_on}}
									<span class="cns-remember-me">
										<label for="remember">{!REMEMBER_ME}</label>
										<input type="checkbox" id="remember" name="remember" value="1"{+START,IF,{$EQ,{$CONFIG_OPTION,remember_me_behaviour},default_on}} checked="checked"{+END} class="{+START,IF,{$EQ,{$CONFIG_OPTION,remember_me_behaviour},default_off}}js-click-checkbox-remember-me-confirm{+END}" />
									</span>
								{+END}
								{+START,IF,{$EQ,{$CONFIG_OPTION,remember_me_behaviour},always_on}}
									<input type="hidden" name="remember" value="1" />
								{+END}

								<button class="btn btn-primary btn-sm btn-scri menu--site-meta--user-actions--login js-check-field-login-username" type="submit">{+START,INCLUDE,ICON}NAME=menu/site_meta/user_actions/login{+END} <span>{!_LOGIN}</span></button>
							</span>
						</div>
					</form>

					<ul class="horizontal-links associated-links-block-group">
						<li><a href="{JOIN_URL*}">{!_JOIN}</a></li>
						<li><a data-open-as-overlay="{}" rel="nofollow" href="{FULL_LOGIN_URL*}" title="{!MORE}: {!_LOGIN}">{!MORE}</a></li>
					</ul>

					{$GET,guest_bar_supplemental}
				</div>
				{+START,IF,{$ADDON_INSTALLED,search}}{+START,IF,{$HAS_ACTUAL_PAGE_ACCESS,search}}
					<div class="cns-guest-column cns-guest-column-c">
						{+START,INCLUDE,MEMBER_BAR_SEARCH}{+END}
					</div>
				{+END}{+END}

				<nav class="cns-guest-column cns-member-column-d">
					{$,<p class="cns-member-column-title">{!VIEW}:</p>}
					<ul class="actions-list">
						<li>{+START,INCLUDE,ICON}NAME=buttons/proceed2{+END} <a data-open-as-overlay="{}" href="{NEW_POSTS_URL*}">{!POSTS_SINCE}</a></li>
						<li>{+START,INCLUDE,ICON}NAME=buttons/proceed2{+END} <a data-open-as-overlay="{}" href="{UNANSWERED_TOPICS_URL*}">{!UNANSWERED_TOPICS}</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>
