{$REQUIRE_JAVASCRIPT,shopping}

<div data-tpl="ecomShoppingItemRemoveField">
	<label for="remove_{TYPE_CODE*}" class="accessibility-hidden">{!REMOVE}:</label>
	<input class="js-click-unfade-cart-update-button" type="checkbox" name="remove_{TYPE_CODE*}" id="remove_{TYPE_CODE*}" value="1" />
</div>
