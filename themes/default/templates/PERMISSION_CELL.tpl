<td data-click-forward="input[type='checkbox']">
	<div class="accessibility-hidden"><label for="{NAME*}">{HUMAN*}</label></div>
	<input data-cms-tooltip="{ contents: '{HUMAN;/=}', triggers: 'hover focus', width: '20%' }" alt="{HUMAN*}" type="checkbox" id="{NAME*}" name="{NAME*}"{+START,IF,{CHECKED}} checked="checked"{+END} value="1" />
</td>
