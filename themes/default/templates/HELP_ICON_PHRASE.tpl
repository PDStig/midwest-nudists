<span class="vertical-alignment" data-cms-rich-tooltip="{haveLinks: {$?,{$IN_STR,{TOOLTIP},<a},true,false}}" title="{TOOLTIP=}">
	<span class="help-icon">
		{+START,INCLUDE,ICON}
			NAME=help
			ICON_SIZE=24
		{+END}
	</span>
	<span>{LABEL*}</span>
</span>
