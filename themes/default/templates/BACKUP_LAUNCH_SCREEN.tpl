{$REQUIRE_JAVASCRIPT,backup}

<div data-tpl="backupLaunchScreen">
	{TITLE}

	<h2>{!PREVIOUS}</h2>

	{RESULTS}

	<p>{TEXT*}</p>

	<h2>{!NEW}</h2>

	{FORM}
</div>
