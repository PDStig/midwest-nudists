{$REQUIRE_JAVASCRIPT,setupwizard}

<div data-tpl="setupWizard7">
	{FORM}
	<div class="box box---setupwizard-7-screen">
		<div class="box-inner">
			<h2>{!PREVIEW}</h2>

			<div id="rules-set">
				<div id="preview-box-balanced" style="display: block">
					{BALANCED}
				</div>
				<div id="preview-box-corporate" style="display: none">
					{CORPORATE}
				</div>
				<div id="preview-box-liberal" style="display: none">
					{LIBERAL}
				</div>
			</div>
		</div>
	</div>
</div>
