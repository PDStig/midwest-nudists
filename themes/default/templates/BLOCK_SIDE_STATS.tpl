<section class="box box---block-side-stats"><div class="box-inner">
	<h3>{!STATISTICS}</h3>

	{CONTENT}
	{+START,IF_EMPTY,{CONTENT}}
		<p class="nothing-here">{!NONE}</p>
	{+END}
</div></section>
