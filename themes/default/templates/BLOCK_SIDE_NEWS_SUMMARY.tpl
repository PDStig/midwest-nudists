<div class="box box---block-side-news-summary"><div class="box-inner">
	<p class="tiny-paragraph">
		<a title="{$STRIP_TAGS,{NEWS_TITLE}}" href="{FULL_URL*}">{$TRUNCATE_LEFT,{NEWS_TITLE},30,0,1}</a>
	</p>

	<p class="tiny-paragraph associated-details">
		{!BY_SIMPLE,{AUTHOR*}}
	</p>

	<p class="tiny-paragraph associated-details">
		{!POSTED_TIME_SIMPLE,{DATE*}}
	</p>
</div></div>
