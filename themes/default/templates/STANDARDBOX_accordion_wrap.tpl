<div data-toggleable-tray="{ accordion: true }">
	{+START,IF_NON_EMPTY,{TITLE}}
		<h3>
			{TITLE}
		</h3>
	{+END}

	{CONTENT}
</div>
