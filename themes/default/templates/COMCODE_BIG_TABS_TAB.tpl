<div aria-labelledby="{PASS_ID|*}-{$GET%,big_tab_sets}-btgoto-{NAME|*}" role="tabpanel" class="comcode-big-tab" id="{PASS_ID|*}-{$GET%,big_tab_sets}-section-{NAME|*}" style="display: {$?,{DEFAULT},block,none}">
	<div class="clearfix spaced-list">
		{CONTENT}
	</div>
</div>
