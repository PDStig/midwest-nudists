<div class="long-warning">
	<p class="installer-warning">
		{MESSAGE}
	</p>
	<ul>
		{+START,LOOP,FILES}
			<li><kbd>{_loop_var}</kbd></li>
		{+END}
	</ul>
</div>
