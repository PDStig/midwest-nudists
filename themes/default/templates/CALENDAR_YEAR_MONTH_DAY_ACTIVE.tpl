<td class="calendar-active calendar-year-month-day{+START,IF,{CURRENT}} calendar-current{+END}">
	<a href="{DAY_URL*}" title="{DAY*}: {DATE*}: {EVENTS_AND_PRIORITY_LANG}">{DAY*}</a>
	<span class="calendar-year-month-day-num-events">{COUNT*}</span>
</td>
