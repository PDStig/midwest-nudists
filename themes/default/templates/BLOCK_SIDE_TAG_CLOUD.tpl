<section class="box box---block-side-tag-cloud"><div class="box-inner">
	<h3>{TITLE*}</h3>

	{+START,LOOP,TAGS}
		<a rel="tag" href="{LINK*}" style="font-size: {EM*}em">{TAG*}</a>
	{+END}
</div></section>
