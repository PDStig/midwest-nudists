<div class="box box-logowizard-2">
	<div class="box-inner">
		<h2>{LOGO_TYPE*} ({LOGO_OUTPUT_THEME_IMAGE*})</h2>

		<div class="logowizard-preview">
			<img alt="{!LOGO}" src="{$FIND_SCRIPT*,logowizard}?name={NAME&*}&amp;font={FONT&*}&amp;colour={COLOUR&*}&amp;logo_type={LOGO_TYPE&*}&amp;logo_theme_image={LOGO_THEME_IMAGE&*}&amp;background_theme_image={BACKGROUND_THEME_IMAGE&*}&amp;theme={THEME&*}{$KEEP*}" />
		</div>
	</div>
</div>
