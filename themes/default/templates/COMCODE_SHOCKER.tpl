{$SET,RAND_ID_SHOCKER,rand{$RAND}}

{$REQUIRE_JAVASCRIPT,core_rich_media}
{$REQUIRE_JAVASCRIPT,pulse}

<div class="shocker" data-tpl="comcodeShocker" data-tpl-params="{+START,PARAMS_JSON,RAND_ID_SHOCKER,PARTS,TIME,MAX_COLOR,MIN_COLOR}{_*}{+END}">
	<div class="shocker-left" id="comcodeshocker{$GET,RAND_ID_SHOCKER}-left"></div>
	<div class="shocker-right" id="comcodeshocker{$GET,RAND_ID_SHOCKER}-right"></div>
</div>
