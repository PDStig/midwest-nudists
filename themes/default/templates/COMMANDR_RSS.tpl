<div class="webstandards-checker-off">
	{+START,LOOP,CONTENT}
		<p>
			<a rel="external" title="{NEWS_TITLE*} {!LINK_NEW_WINDOW}" target="_blank" href="{FULL_URL*}">{$TRUNCATE_LEFT,{NEWS_TITLE},30,0,1}</a> <span class="associated-details">({DATE*})</span>
		</p>

		<blockquote>
			<p>{SUMMARY*}</p>
		</blockquote>
	{+END}
</div>
