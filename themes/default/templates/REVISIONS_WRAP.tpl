<a id="revisions"></a>

{+START,IF_NON_EMPTY,{RESULTS}}
	<div class="box box---revisions-wrap"><div class="box-inner">
		<h2>{!REVISIONS}</h2>

		{RESULTS}
	</div></div>
{+END}
