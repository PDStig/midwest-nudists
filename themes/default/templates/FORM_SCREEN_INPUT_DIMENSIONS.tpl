<div class="input-dimensions">
	<input maxlength="6" data-prevent-input="[^\-\d{$BACKSLASH}{$DECIMAL_POINT*}]" tabindex="{TABINDEX*}" class="form-control input-integer{REQUIRED*}" type="number" id="{NAME_WIDTH*}" name="{NAME_WIDTH*}" value="{DEFAULT_WIDTH*}" />
	&times;
	<label for="{NAME_HEIGHT*}" class="accessibility-hidden">{!HEIGHT}</label>
	<input maxlength="6" data-prevent-input="[^\-\d{$BACKSLASH}{$DECIMAL_POINT*}]" tabindex="{TABINDEX*}" class="form-control input-integer{REQUIRED*}" type="number" id="{NAME_HEIGHT*}" name="{NAME_HEIGHT*}" value="{DEFAULT_HEIGHT*}" />
</div>
