<!-- CC-error -->
{+START,INCLUDE,RED_ALERT}
	ROLE=error
	TEXT={!CCP_ERROR,{LINE*},{MESSAGE*}}
{+END}

<h2>{!SOURCE}</h2>

<code>
	{SOURCE*}
</code>
