{TITLE}

{MESSAGES}

<hr class="spaced-rule" />

<p class="back-button">
	<a href="#!" data-cms-btn-go-back="1" title="{!NEXT_ITEM_BACK}">{+START,INCLUDE,ICON}
		NAME=admin/back
		ICON_SIZE=48
	{+END}</a>
</p>
