{+START,IF_NON_EMPTY,{SIGNATURE}}
	<div class="cns-current-sig-wrap"><div class="cns-current-sig">
		<section class="box box---cns-edit-signature-tab"><div class="box-inner">
			<h3>{!SIGNATURE_NOW}</h3>

			<div class="cns-member-signature">{SIGNATURE}</div>
		</div></section>
	</div></div>
{+END}

<div class="box cns-sig-requirements"><div class="box-inner help-jumpout">
	<p>
		{!_MAX_SIG_LENGTH_COMCODE,{SIZE*}}
	</p>
</div></div>
