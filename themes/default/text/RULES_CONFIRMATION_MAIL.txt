{!RULES_CONFIRMATION_MAIL_HEADER,{$SITE_NAME*},{AGREEMENT_DATE_TIME*}}

---

{!RULES_CONFIRMATION_MAIL_HEADER_RULES}
[html]{RULES}[/html]

{+START,IF_NON_EMPTY,{PRIVACY_POLICY}}
---

{!RULES_CONFIRMATION_MAIL_HEADER_PRIVACY_POLICY}
[html]{PRIVACY_POLICY}[/html]

{+END}
---

{!RULES_CONFIRMATION_MAIL_HEADER_DECLARATIONS}
[list]
	{+START,LOOP,DECLARATIONS}
		[*]{_loop_var@}
	{+END}
[/list]

---

{!RULES_CONFIRMATION_MAIL_FOOTER}
