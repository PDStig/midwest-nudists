<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    mwn_misc
 */

i_solemnly_declare(I_UNDERSTAND_SQL_INJECTION | I_UNDERSTAND_XSS | I_UNDERSTAND_PATH_INJECTION);

$extra_support_inform = [];
$extra_support_notice = [];
$extra_support_warn = [];
$extra_brief_details = [];

if (is_guest()) {
    $extra_brief_details['email'] = [
        'label' => 'Email address',
        'description' => 'As you are not logged in, please provide your e-mail address at which we may contact you.',
        'type' => 'email',
        'default' => '',
        'options' => '',
        'required' => true,
    ];

    $type = get_param_string('type', 'start');

    $types_requiring_login = [
        'staff',
        'other_media',
    ];

    if (in_array($type, $types_requiring_login)) {
        access_denied('NOT_AS_GUEST');
    }
}

require_code('decision_tree');
require_code('mwn_verification');

$decision_tree = [
    'start' => [
        'title' => 'Contact Midwest Nudists',
        'text' => 'How may we help you? By proceeding through this wizard, a support ticket may be created between you and the Midwest Nudists staff depending on your selection.' . "\n\n" . 'You may also contact us directly at support@midwestnudists.org. Please use the e-mail address associated with your Midwest Nudists account if/when possible.',
        'previous' => '',
        'questions' => [
            'support_type' => [
                'label' => 'Support Type',
                'description' => 'Please select the inquiry which best matches your reason for contacting us.',
                'type' => 'list',
                'default' => 'I need help or have a question (select this if none of the other options apply).',
                'default_list' => [
                    'I would like to report a site issue, bug, or error.',
                    'I would like to report something that is against the rules / Terms of Use.',
                    'I would like to report something on the site which is inaccurate or contains a typo.',
                    'I would like to provide feedback or a suggestion (potentially for voting by other members).',
                    'I would like to get verified so I can upload media.',
                    'I want to upload media that contains people other than myself.',
                    'I need help or have a question (select this if none of the other options apply).',
                    'I would like to appeal discipline that was issued against me.',
                    'I cannot access my account.',
                    'I joined but did not receive a verification e-mail.',
                    'I would like to download or purge my personal data.',
                    'I am interested in contributing content to the site.',
                    'I am interested in helping operate the site (becoming a staff member).',
                    'I am a manager of a nudist club/venue and would like access to manage my listing in the directory.',
                    'I have a big nudist event I would like you to mass-promote to Midwest Nudist members.',
                    'I would like you to build a form for use with my club/venue/event.',
                    'I would like to set up a newsletter for my club/venue through Midwest Nudists.',
                    'I would like a basic sub-website via Midwest Nudists for a nudist function/club/venue.',
                    'I would like PDStig, LLC to build or maintain a standalone website for my club/venue',
                ],
                'options' => 'widget=radio',
                'required' => true,
            ],
        ],
        'next' => [
            ['support_type', 'I would like to report a site issue, bug, or error.', 'issue'],
            ['support_type', 'I would like to report something that is against the rules / Terms of Use.', 'tos'],
            ['support_type', 'I would like to report something on the site which is inaccurate or contains a typo.', 'typo'],
            ['support_type', 'I would like to provide feedback or a suggestion (potentially for voting by other members).', 'feedback'],
            ['support_type', 'I would like to get verified so I can upload media.', 'verification'],
            ['support_type', 'I want to upload media that contains people other than myself.', 'other_media'],
            ['support_type', 'I need help or have a question (select this if none of the other options apply).', 'other'],
            ['support_type', 'I would like to appeal discipline that was issued against me.', 'appeal'],
            ['support_type', 'I cannot access my account.', 'account'],
            ['support_type', 'I joined but did not receive a verification e-mail.', 'join_email'],
            ['support_type', 'I would like to download or purge my personal data.', 'data'],
            ['support_type', 'I am interested in contributing content to the site.', 'contribute'],
            ['support_type', 'I am interested in helping operate the site (becoming a staff member).', 'staff'],
            ['support_type', 'I am a manager of a nudist club/venue and would like access to manage my listing in the directory.', 'directory'],
            ['support_type', 'I have a big nudist event I would like you to mass-promote to Midwest Nudist members.', 'event'],
            ['support_type', 'I would like you to build a form for use with my club/venue/event.', 'form'],
            ['support_type', 'I would like to set up a newsletter for my club/venue through Midwest Nudists.', 'newsletter'],
            ['support_type', 'I would like a basic sub-website via Midwest Nudists for a nudist function/club/venue.', 'basic_site'],
            ['support_type', 'I would like PDStig, LLC to build or maintain a standalone website for my club/venue', 'site'],
        ],
    ],

    'issue' => [
        'title' => 'Report a site issue',
        'text' => 'Thank you for taking the time to report an issue. Here are a few questions so we can gather information about the issue, bug, or error you encountered. You may be awarded points as a thank-you for making aware site issues.',
        'previous' => 'start',
        'questions' => [
            'title' => [
                'label' => 'Concise sentence of the issue',
                'description' => 'Describe the issue you are having in one concise sentence. This will be used as the title of the support ticket.',
                'type' => 'short_text',
                'default' => '',
                'required' => true,
            ],
            'description' => [
                'label' => 'Description of issue / error messages',
                'description' => 'Please describe the issue, error, or bug you encountered on the site. Please also paste any error messages you encountered here if applicable.',
                'type' => 'long_text',
                'default' => '',
                'required' => true,
            ],
            'reproduction' => [
                'label' => 'What were you trying to do?',
                'description' => 'Describe the steps you took which led to you encountering the issue. What buttons did you click? Did you fill out any forms? etc.',
                'type' => 'long_text',
                'default' => '',
                'required' => true,
            ],
            'url' => [
                'label' => 'URL / page',
                'description' => 'If possible, please provide the URL where you encountered the issue.',
                'type' => 'url',
                'default' => '',
                'required' => false,
            ],
            'screenshots' => [
                'label' => 'Screenshots',
                'description' => 'Optionally upload any screenshots / pictures of the issue or error messages.',
                'type' => 'picture_multi',
                'default' => '',
                'required' => false,
            ],
        ] + $extra_brief_details,
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Site issue, bug, or error'], get_module_zone('tickets')),
    ],

    'tos' => [
        'title' => 'Report a Terms of Use / Rules violation',
        'text' => 'Thank you for taking the time to keep our community safe and clean. You will now be asked a few questions about what you are reporting. You may be awarded points for making us aware of this violation.',
        'previous' => 'start',
        'notice' => [
            'Please use the report links instead of this wizard when possible. When viewing the content which violates the rules, click the Report This link, or the Report button (for forum posts), to report it. That way, we will receive a hard-copy of the content in its present state for better investigation (especially if it is later deleted or modified).'
        ],
        'questions' => [
                'url' => [
                    'label' => 'Violating URL / page',
                    'description' => 'Please specify the direct URL to the content in violation of the rules.',
                    'type' => 'url',
                    'default' => '',
                    'required' => true,
                ],
                'screenshots' => [
                    'label' => 'Screenshots of violation',
                    'description' => 'Please include screenshots of the violating content in case the content is changed or removed before we investigate. The screenshots must not be altered in any way except to crop out irrelevant portions of the page.',
                    'type' => 'picture_multi',
                    'default' => '',
                    'required' => true,
                ],
                'title' => [
                    'label' => 'Concise explanation of violation',
                    'description' => 'Concisely explain in one sentence why this content is in violation of our rules. This will be used as the title of the support ticket.',
                    'type' => 'short_text',
                    'default' => '',
                    'required' => true,
                ],
                'explanation' => [
                    'label' => 'Further explanation',
                    'description' => 'Please explain further why you believe this content is in violation of our rules / Terms of Use. Ideally reference the Terms or rules being violated.',
                    'type' => 'long_text',
                    'default' => '',
                    'required' => false,
                ],
            ] + $extra_brief_details,
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Rule / Terms of Use violation'], get_module_zone('tickets')),
    ],

    'typo' => [
        'title' => 'Report a typo or something that is inaccurate',
        'text' => 'Thank you for helping us keep our content up-to-date. You will now be asked a few questions about what you are reporting. You may be awarded points for making us aware of this inaccuracy or typo.',
        'previous' => 'start',
        'questions' => [
                'title' => [
                    'label' => 'Concise explanation of inaccuracy',
                    'description' => 'Concisely explain in one sentence the typo or inaccuracy. This will be used as the title of the support ticket.',
                    'type' => 'short_text',
                    'default' => '',
                    'required' => true,
                ],
                'url' => [
                    'label' => 'URL / page',
                    'description' => 'Please specify the URL to the content containing the inaccuracy or typo.',
                    'type' => 'url',
                    'default' => '',
                    'required' => true,
                ],
                'explanation' => [
                    'label' => 'Explanation and correction (with sources)',
                    'description' => 'Please explain what is inaccurate or needs fixed on the provided URL / page. Please also include the correction, and a reference/source to sustain that your correction is accurate (not needed for spelling / grammar typos).',
                    'type' => 'long_text',
                    'default' => '',
                    'required' => true,
                ],
                'screenshots' => [
                    'label' => 'Screenshots of inaccuracies',
                    'description' => 'Optionally provide screenshots of the inaccuracies. You may draw circles / arrows on the images to indicate the problems.',
                    'type' => 'picture_multi',
                    'default' => '',
                    'required' => false,
                ],
            ] + $extra_brief_details,
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Inaccuracy or Typo'], get_module_zone('tickets')),
    ],

    'feedback' => [
        'title' => 'Provide us feedback or a suggestion',
        'text' => 'Thank you for helping us improve Midwest Nudists! You will now be asked a few questions about your feedback or suggestion. If your feedback or suggestion is something that could realistically be implemented, it will be posted by staff in the [url="Issues / Voting forum"]https://midwestnudists.org/forum/pg/forumview/browse/suggestions-voting[/url] so members may vote on whether or not it should get implemented. By submitting this form, you agree staff will anonymously share what you fill in this form on said forum topic for voting and discussion.',
        'previous' => 'start',
        'questions' => [
                'title' => [
                    'label' => 'Concise explanation of feedback',
                    'description' => 'Concisely explain in one sentence your feedback or suggestion. This will be used as the title of the support ticket.',
                    'type' => 'short_text',
                    'default' => '',
                    'required' => true,
                ],
                'explanation' => [
                    'label' => 'Description of feedback or suggestion',
                    'description' => 'Please constructively provide your feedback or suggestion. For example, simply telling us something is bad is not helpful for us; tell us why it is bad and include suggestions on how we can improve on it.',
                    'type' => 'long_text',
                    'default' => '',
                    'required' => true,
                ],
                'benefits' => [
                    'label' => 'Benefits to the Community',
                    'description' => 'Please explain how your feedback or suggestion will benefit the overall Midwest Nudists community, in your perspective.',
                    'type' => 'long_text',
                    'default' => '',
                    'required' => true,
                ],
                'implications' => [
                    'label' => 'Challenges or Implications',
                    'description' => 'Please include any challenges or implications you believe may need to be considered for this suggestion and what could be done to address them.',
                    'type' => 'long_text',
                    'default' => '',
                    'required' => true,
                ],
                'url' => [
                    'label' => 'Relevant URLs / pages',
                    'description' => 'Specify the URLs / pages relevant to your feedback / suggestion if applicable.',
                    'type' => 'url_multi',
                    'default' => '',
                    'required' => false,
                ],
                'screenshots' => [
                    'label' => 'Screenshots',
                    'description' => 'Optionally provide screenshots relevant to your feedback or suggestion. You may draw circles / arrows on the images where applicable.',
                    'type' => 'picture_multi',
                    'default' => '',
                    'required' => false,
                ],
            ] + $extra_brief_details,
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Feedback or Suggestion'], get_module_zone('tickets')),
    ],

    'verification' => [
        'title' => 'Verification to upload media',
        'text' => 'Click next to be taken to the form to verify your age / identity. By verifying your age and identity, you will be allowed to upload media / attachments / custom avatars until your verification expires (verification can always be renewed). You will need an official, valid government-issued ID which has a photo of you and your date of birth, a paper, a pen, and your camera. Additional instructions will be provided on the form when you proceed. All verifications are kept on-site; we do not use any third-party services to verify members.',
        'previous' => 'start',
        'next' => build_url(['page' => 'verification'], get_module_zone('verification')),
    ],

    'other_media' => [
        'title' => 'Request to upload media of others',
        'text' => 'Thank you for being considerate of the privacy and security of others by following our consent protocol. You are required to fill out the [page="site:downloads:entry:1"]Midwest Nudists Media Release Form[/page] when uploading media to Midwest Nudists containing other people if anyone can potentially be personally identified from the media. If you want to upload media which only contains yourself, then you do not have to fill out this form (but you must verify your age and identity if you have not already done so). Please carefully read the description / help of each field before filling it out as there are very specific requirements.',
        'notice' => [
            'You must have verified your age and identity before you may request to upload media of others. If you have not done this yet, please go back and select the appropriate option.',
        ],
        'warn' => [
            'Remember that you may not, under any circumstances, upload media containing anyone under the age of 18 years regardless of their consent or state of dress. U.S. law may interpret any media containing minors partially or fully nude as child pornography even if it is non-sexual. We also do not want to take chances allowing media of clothed minors on a nudist website. Therefore, doing this is a bannable offense, and we will prosecute you to the maximum extent of the law.'
        ],
        'previous' => 'start',
        'questions' => [
            'title' => [
                'label' => 'Concise explanation of media',
                'description' => 'Concisely describe in one sentence the media you wish to upload to Midwest Nudists. This will be used as the title of the support ticket.',
                'type' => 'short_text',
                'default' => '',
                'required' => true,
            ],
            'media_files' => [
                'label' => 'Media files',
                'description' => 'Please upload all media files relevant to this request (you can also put them in an archive like a ZIP file and upload the ZIP file). They will be kept private between you and staff via the Support Ticket. If/when the request is approved, you will only be allowed to post the files which you uploaded on this form. You will be required to make a new request if there are other files which you want to upload of others.',
                'type' => 'upload_multi',
                'default' => '',
                'required' => true,
            ],
            'proof' => [
                'label' => 'Completed release form(s)',
                'description' => 'Please upload completed Midwest Nudists release form(s) for this request. We will only accept the official Midwest Nudists release form. A download link is provided up above this form.',
                'type' => 'upload_multi',
                'default' => '',
                'required' => true,
            ],
            'emails' => [
                'label' => 'Email addresses of parties',
                'description' => 'To ensure accuracy (in case we cannot read the e-mail addresses written on the releases), please type the email addresses of every party, one per box (a new box will appear as you start typing).',
                'type' => 'short_text_multi',
                'default' => '',
                'required' => true,
            ],
        ],
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Request to upload media of others'], get_module_zone('tickets')),
    ],

    'other' => [
        'title' => 'Other request',
        'text' => 'By clicking next, you will be directed to create a support ticket with your question / inquiry. Please describe your inquiry in detail so that we may better assist you.',
        'previous' => 'start',
        'next' => build_url(['page' => 'tickets', 'type' => 'ticket', 'ticket_type' => 'Other'], get_module_zone('tickets')),
    ],

    'appeal' => [
        'title' => 'Appeal a discipline',
        'text' => 'To appeal a discipline, please reply to the Private Topic which was sent to you. Or, please send an email to support@midwestnudists.org from the email address associated with your account (and include the warning which you are appealing). Provide your reasoning why you are appealing: you did not violate the indicated rules, the discipline does not justify the violations (e.g. too harsh or not constructive), or the staff abused their power (discrimination, personal bias, or attempted to compound multiple rule violations on one single action).',
        'previous' => 'start',
    ],

    'account' => [
        'title' => 'Cannot access my account',
        'text' => 'We apologize for the inconvenience you are having. First, use the Forgot Username or Password link on the login form and try resetting your password or recovering your username. If you are unable to do this, you can also send an email to support@midwestnudists.org from the email address associated with your account. We will ask a couple questions to verify it is really you and not a hacker who gained access to your email. If you are unable to do either one, then unfortunately to protect your own security, we cannot recover your account. We recommend instead going back and requesting to purge your data; we can possibly wipe your account and data so you can create a new one. You will need to provide at minimum your username (or member ID) and email address. Assuming the username and email address provided match up, we will then email that address. And if we get no response for one week, we will delete the account and purge all its data. Note that you might not be able to use the same username, email address, or IP address for your new account.',
        'previous' => 'start',
    ],

    'join_email' => [
        'title' => 'Did not receive verification e-mail after joining',
        'text' => 'We are sorry that you did not receive an email like you should have. Please allow up to one hour first before filling out this form. If after one hour you did not get the email, and you checked your spam, fill out this form to let us know how you want us to proceed.',
        'previous' => 'start',
        'questions' => [
                'title' => [
                    'label' => 'Username',
                    'description' => 'Provide the username of the new account you registered. This will become the title of your Support Ticket.',
                    'type' => 'short_text',
                    'default' => '',
                    'required' => true,
                ],
                'email' => [
                    'label' => 'Email address',
                    'description' => 'Please provide the email address you used to register your account.',
                    'type' => 'email',
                    'default' => '',
                    'options' => '',
                    'required' => true,
                ],
                'action' => [
                    'label' => 'Action to take',
                    'description' => 'What would you like us to do?',
                    'type' => 'list',
                    'default' => 'Try a different email address for my account.',
                    'default_list' => [
                        'Try a different email address for my account',
                        'Use a @midwestnudists.org email address for emails from the site'
                    ],
                    'options' => 'widget=radio',
                    'required' => true,
                ],
            ],
        'next' => [
            ['action', 'Try a different email address for my account', 'join_email_change'],
            ['action', 'Use a @midwestnudists.org email address for emails from the site', 'join_email_new'],
        ],
    ],

    'join_email_change' => [
        'title' => 'Did not receive verification e-mail after joining - Change email',
        'text' => 'Please provide for us a new email address. Staff will send you a new verification at this address. Once you confirm, your member account will be verified and its email address changed to the one you provided.',
        'notice' => [
            'You must have verified your age and identity before you may request to upload media of others. If you have not done this yet, please go back and select the appropriate option.',
        ],
        'expects_parameters' => [
            'title',
            'email',
            'action',
        ],
        'previous' => 'join_email',
        'questions' => [
            'new_email' => [
                'label' => 'New e-mail address',
                'description' => 'Provide a new email address for us to try.',
                'type' => 'email',
                'default' => '',
                'required' => true,
            ],
        ],
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Did not receive verification e-mail'], get_module_zone('tickets')),
    ],

    'join_email_new' => [
        'title' => 'Did not receive verification e-mail after joining - Make @midwestnudists.org email address',
        'text' => 'We can make you an @midwestnudists.org email address for use specifically to receive site emails and then associate it with your account. Note that you will only get 100 MB mailbox space, and you will not be allowed to send emails out from this address. It is solely so you can receive and access site emails. Your email account will be username@midwestnudists.org. We will also provide you instructions and credentials for accessing the account.',
        'warn' => [
            'Using a @midwestnudists.org email should be a last resort. You will not be able to email anyone, including support@midwestnudists.org, from this email. Therefore you will not be able to recover your account if you lose access to it.',
        ],
        'expects_parameters' => [
            'title',
            'email',
            'action',
        ],
        'previous' => 'join_email',
        'questions' => [
            'new_email' => [
                'label' => 'Set up a @midwestnudists.org email',
                'description' => 'By ticking this box, you confirm you want us to set up a @midwestnudists.org email address for the sole purpose of receiving site emails.',
                'type' => 'tick',
                'default' => '',
                'required' => true,
            ],
        ],
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Did not receive verification e-mail'], get_module_zone('tickets')),
    ],

    'data' => [
        'title' => 'Request to download / purge personal data',
        'text' => 'Please fill out as many criteria as you can to get started. The more criteria you fill, the more personal data we can match for you.',
        'inform' => [
            'Did you know?: You have basic and quick functionality to download or purge your personal data from your member profile without the need for staff interaction. On your profile, go to Edit > Personal Data. If you need more fine-grained control, or you used up your limit, please use this wizard and staff will assist you.',
            ['lost_access', '1', 'If you are using this form to request the purging / deletion of an account to which you lost access, and you are unable to email us at support@midwestnudists.org from the associated email address, you can use this form to have the account purged and deleted. You must provide at minimum its username or member ID, and its email address. We will email the address, and if no response is received within one week, we will delete the account and purge all its data. You will then be able to re-register, but you might not be able to use the same email address, username, or IP address as the purged account.'],
        ],
        'previous' => 'start',
        'questions' => [
                'title' => [
                    'label' => 'Concise explanation of data to purge / download',
                    'description' => 'Concisely describe in one sentence what data you want to download or purge. This will be used as the title of the support ticket.',
                    'type' => 'short_text',
                    'default' => '',
                    'required' => true,
                ],
                'lost_access' => [
                    'label' => 'I lost access to the associated account',
                    'description' => 'Tick this box if you are making this request because you lost access to your account and cannot recover it (e.g. forgot username / password does not work, or you lost access to the email associated with the account).',
                    'type' => 'tick',
                    'default' => '0',
                    'required' => true,
                ],
                'member_id' => [
                    'label' => 'Member ID',
                    'description' => 'If you know what your member ID number is/was, please provide it here. These are never shared, so once you delete your account, no one will ever have the same ID.',
                    'type' => 'integer',
                    'default' => '',
                    'required' => false,
                ],
                'username' => [
                    'label' => 'Username',
                    'description' => 'Please provide a current or past username if you can. Note that if someone else is now using your past username, then we cannot match personal data against it anymore without one of the other criteria.',
                    'type' => 'short_text',
                    'default' => '',
                    'required' => false,
                ],
                'email' => [
                    'label' => 'Email address',
                    'description' => 'Please provide an email address that was associated with your data. This is required if you are not logged in as we will contact you at this address.',
                    'type' => 'email',
                    'default' => '',
                    'required' => is_guest(),
                ],
                'ip_address' => [
                    'label' => 'IP address',
                    'description' => 'Please provide an IP address that you used with your account if you can. Note that if other people also use our site from the same IP, then you will need to specify another criteria as well so we can better match your data.',
                    'type' => 'ip_address',
                    'default' => '',
                    'required' => false,
                ],
                'other' => [
                    'label' => 'Other criteria',
                    'description' => 'If there is any other criteria we should search for, please specify them here.',
                    'type' => 'long_text',
                    'default' => '',
                    'required' => false,
                ],
            ],
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Request to download / purge personal data'], get_module_zone('tickets')),
    ],

    'contribute' => [
        'title' => 'Interest in contribution',
        'text' => 'Wonderful! We love receiving contributions from others for new or additional site content. And we will reward you with points should the content go up on the site. Click next to create a Support Ticket and let us know what contributions you would like to make.',
        'previous' => 'start',
        'next' => build_url(['page' => 'tickets', 'type' => 'ticket', 'ticket_type' => 'Interest in contributing content'], get_module_zone('tickets')),
    ],

    'staff' => [
        'title' => 'Request to help operate the site / be a staff member',
        'text' => 'Wonderful! Midwest Nudists will benefit from having experienced individuals help run and maintain the website. There are various ways you can volunteer your time: helping run the site on the technical end, reviewing and updating content, promoting the site, moderation / enforcement of the rules, and putting on events. Prospective staff members need to be voted in by the community. So when you fill out this form, a current staff member will make a posting and the community can vote you in (or not). It will be up to you to promote yourself and your talents so the community knows they can trust you have their best interests. You will receive more information about this in the support ticket.',
        'inform' => [
            'You are more likely to be voted in as a staff member if you have already been here a while, actively contributed, have a clean disciplinary record, and are a well-respected and experienced participant in the nudist community. Also, while not required, you will be more likely to be voted in if you verified your age and identity.',
            'Staff are volunteers; Midwest Nudists does not have the resources to employ / pay staff. However, because of this, staff have more flexibility with how much they can/want to contribute and when.'
        ],
        'warn' => [
            'Staff members are especially held to a high standard with following the rules / Terms of Use. As such, members cannot be staff if they ever earned probation. Members who earned a warning in the last 90 days must wait until 90 days have passed since the most recent warning before applying for staff. And staff members are immediately removed from their position if they earn a warning, but may re-apply after going 90 days without earning any warnings (unless they earn/earned probation). Exceptions may be made for one-time trivial violations which caused little to no harm to the community.',
        ],
        'previous' => 'start',
        'questions' => [
            'title' => [
                'label' => 'Concise explanation of request',
                'description' => 'In one sentence, tell us about your desire to be a staff member. This will be used as the title of the support ticket.',
                'type' => 'short_text',
                'default' => '',
                'required' => true,
            ],
            'hours' => [
                'label' => 'Hours per week commitment',
                'description' => 'About how many hours per week do you wish to contribute to Midwest Nudists (at minimum)? Decimal values are permitted.',
                'type' => 'float',
                'default' => '',
                'required' => true,
            ],
            'contributions' => [
                'label' => 'Desired contribution(s)',
                'description' => 'How would you like to contribute to the site as a staff member? Blank fields are provided if you have your own ideas.',
                'type' => 'list_multi',
                'default' => '',
                'default_list' => [
                    'I would like to help operate the site on the technical level',
                    'I would like to help promote the site',
                    'I would like to help populate, update, and review the site content',
                    'I would like to help moderate the site and enforce the rules',
                    'I would like to help plan and run events for the community',
                    'I would like to be involved in making decisions for the site / community',
                    'I would like to act as a legal adviser for the site',
                    'I would like to help ensure the site maintains core nudist values and ethics',
                ],
                'options' => 'widget=vertical_checkboxes,custom_values=multiple',
                'required' => true,
            ],
            'experience' => [
                'label' => 'Relevant experience',
                'description' => 'Please provide relevant skills and experience in regards to your chosen contributions. We especially would like to see experience within the nudist community or within being staff on other sites.',
                'type' => 'long_trans',
                'default' => '',
                'required' => true,
            ],
            'resume' => [
                'label' => 'Resume',
                'description' => 'Optionally, you can upload a resume here to further showcase your experience and skills.',
                'type' => 'upload',
                'default' => '',
                'required' => false,
            ],
            'consent' => [
                'label' => 'Consent to share on forum (see description)',
                'description' => 'Please consent, by ticking this box, to Midwest Nudists posting your details from this wizard in a forum topic so members may review your application and vote on whether or not to elect you as a staff member.',
                'type' => 'tick',
                'default' => '0',
                'options' => '',
                'required' => true,
            ],
        ],
        'needs_captcha' => ((addon_installed('captcha')) && (get_option('captcha_on_feedback') == '1') && (use_captcha())),
        'next' => build_url(['page' => 'tickets', 'type' => 'post', 'ticket_type' => 'Request to become a staff member'], get_module_zone('tickets')),
    ],

    'directory' => [
        'title' => 'Request access to manage a nudist directory entry',
        'previous' => 'start',
        'text' => 'Certainly! We can grant you ownership to the nudist directory entry pertaining to your club/venue. However, for security, this cannot be done through this wizard. Please email us at support@midwestnudists.org from an email address listed on the official website of the venue/club (we must be able to tell you are actually one of the managers of the club/venue). In your email, include the title or URL to the nudist directory entry you want to manage, and include your Midwest Nudists username. We can then grant you full edit / ownership access on the directory entry. Be aware once you are granted ownership, it will become your responsibility to keep the entry up to date (Midwest Nudists will no longer check it themselves).',
    ],

    // TODO
    'event' => [
        'title' => 'Request to have a big event mass-promoted',
        'previous' => 'start',
        'text' => 'We would love to promote your event. At this time, we have not yet set up a facility for mass-promotion, but we are working on it. For now, you can add the event to the public calendar of Midwest Nudists. In the future, you will be able to purchase a mass-promotion of an event in the points store using your points or a financial contribution.',
    ],

    // TODO
    'form' => [
        'title' => 'Request a form to be made for venue/event/club use',
        'previous' => 'start',
        'text' => 'This is coming soon. It will be an item available for purchase in the points store or for a financial contribution to Midwest Nudists.',
    ],

    // TODO
    'newsletter' => [
        'title' => 'Request for Midwest Nudists to host a newsletter for your club/venue',
        'previous' => 'start',
        'text' => 'This is coming soon. It will be an item available for purchase in the points store or for a financial contribution to Midwest Nudists.',
    ],

    // TODO
    'basic_site' => [
        'title' => 'Request for a sub-site on Midwest Nudists for your function/club/venue',
        'previous' => 'start',
        'text' => 'This is coming soon! The software running Midwest Nudists is not yet stable enough to support this feature. We will announce in a newsletter / news post when this feature is ready.',
    ],

    'site' => [
        'title' => 'Request for PDStig, LLC to build or maintain a full site for my club/venue',
        'previous' => 'start',
        'next' => 'https://pdstig.me',
        'text' => 'PDStig, LLC, the company hosting and maintaining Midwest Nudists, is willing to help any individual, club, venue, or business, nudist or not, to build or maintain a website. PDStig, LLC will kick back 30% of its profits from clients running nudist sites to a nudist cause or organization of their choice. To see a list of services and prices, and to get started, click next to be redirected to the PDStig, LLC site.',
    ],
];

$ob = new DecisionTree($decision_tree, 'start');
$tpl = $ob->run();
$tpl->evaluate_echo();
