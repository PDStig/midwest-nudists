[semihtml]
{+START,IF,{$NOT,{$CONFIG_OPTION,single_public_zone}}}
[surround="hero-section"]
	[title]Welcome to {$SITE_NAME*}[/title]

	[block failsafe="1"]main_greeting[/block]
	[surround="hero-button"][page="site:home"]{$SITE_NAME*}[/page][/surround]
[/surround]
{+END}

{+START,IF,{$CONFIG_OPTION,single_public_zone}}
[title]{$SITE_NAME*}[/title]

[block="3" failsafe="1"]main_greeting[/block]

[block]main_multi_content[/block]

[block failsafe="1"]main_leader_board[/block]

[block failsafe="1"]main_quotes[/block]

[block]main_comcode_page_children[/block]
{+END}
[/semihtml]
