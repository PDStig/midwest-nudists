[strings]
HEALTH_CHECK=Health Check
DOC_HEALTH_CHECK=The Health Check can run large numbers of automated checks to see if your website is running optimally. It can catch things you might easily have missed, and guide you through manual checks also.
MODULE_TRANS_NAME_admin_health_check=Health Check

CONFIG_CATEGORY_HEALTH_CHECK=Health Check options
CONFIG_CATEGORY_DESCRIPTION__HEALTH_CHECK=Options related to the Health Check system. Many of these options are configurable activation thresholds for what qualifies as a problem.

PERFORMANCE=Performance
AUTOMATIC_CHECKS=Automatic checks
CONFIG_GROUP_DEFAULT_DESCRIP_FLOOD_CONTROL=These options will only work if rate-limiting is enabled and configured. These options don't define additional rate-limits, they just define when to flag up high rates as a health problem to be investigated.

HC_IS_TEST_SITE=Test site
CONFIG_OPTION_hc_is_test_site=Whether this is a test site. Test sites have different (mainly fewer) tests run than a live site.
CONFIG_OPTION_hc_is_test_site_VALUE_-1=Automatic: test site if site is closed
CONFIG_OPTION_hc_is_test_site_VALUE_0=No
CONFIG_OPTION_hc_is_test_site_VALUE_1=Yes
HC_SCAN_PAGE_LINKS=Targets to scan
CONFIG_OPTION_hc_scan_page_links=A list of page-links or URLs to scan for various problems on. List your most important or most failure-prone pages. &ldquo;<kbd>:</kbd>&rdquo; is the home page. You may use the [tt]*[/tt] or [tt]?[/tt] wildcards in the zone or page sections of the page-link. Most checks that run on individual page respect this option; the main exception are checks that can reliably work fast across many pages.
HC_DISK_SPACE_THRESHOLD=Minimum free disk space
CONFIG_OPTION_hc_disk_space_threshold=The minimum amount of free disk space in Megabytes.

HC_ERRORLOG_DAY_FLOOD_THRESHOLD=Tolerable logged errors per day
CONFIG_OPTION_hc_error_log_day_flood_threshold=The minimum number of errors per day in the error log before it's considered a serious issue.
HC_PAGE_SIZE_THRESHOLD=Maximum web page size
CONFIG_OPTION_hc_page_size_threshold=The maximum size of a web page in Kilobytes.

HC_WEBSTANDARDS_SAFELIST=Ignored Web Standards errors
CONFIG_OPTION_hc_webstandards_safelist=This option specifies web standards checker errors that should be ignored. Each line is a substring of an error message to ignore.

HC_ADMIN_STALE_THRESHOLD=Stale administrator threshold
CONFIG_OPTION_hc_admin_stale_threshold=The minimum time in days before an administrator account is considered stale.
HC_GOOGLE_SAFE_BROWSING_API_KEY=Google Safe Browsing API enabled
CONFIG_OPTION_hc_google_safe_browsing_api_enabled=Whether to run checks with the <a title="Google Safe Browsing (this link will open in a new window)" target="_blank" href="https://developers.google.com/safe-browsing/v4/get-started">Google Safe Browsing</a> API. If you enable this then your configured Google API Key must have this service enabled on it.

HC_CRON_THRESHOLD=Maximum system scheduler time
CONFIG_OPTION_hc_cron_threshold=The maximum reasonable time in seconds for the system scheduler to take to run.
HC_CPU_PCT_THRESHOLD=Maximum CPU load
CONFIG_OPTION_hc_cpu_pct_threshold=The maximum CPU load (as a percentage).
HC_CPU_SCORE_THRESHOLD=Minimum CPU performance
CONFIG_OPTION_hc_cpu_score_threshold=This is a score revealing how many MD5 uniqid operations your server can perform per second (this can fluctuate wildly depending on current load). Most hosting-industry-supplied servers do not perform that well as they are either made with low-cost hardware, recycled old hardware, or are actually virtual machines. Set the minimum expected score you would like here before the health check fails.
HC_IO_PCT_THRESHOLD=Maximum I/O load
CONFIG_OPTION_hc_io_pct_threshold=The maximum I/O load (as a percentage).
HC_PAGE_SPEED_THRESHOLD=Maximum page load time
CONFIG_OPTION_hc_page_speed_threshold=The maximum time in seconds after which a page request is considered slow. The default threshold is quite high because we may have stale caches or load spiking; we're looking for major issues, not testing our overall optimisation.
HC_PROCESS_HANG_THRESHOLD=Maximum process run-time
CONFIG_OPTION_hc_process_hang_threshold=The maximum time in minutes after which one of the monitored processes is considered to have hung.
HC_PROCESSES_TO_MONITOR=Processes to monitor
CONFIG_OPTION_hc_processes_to_monitor=A <a title="Regular Expressions (this link will open in a new window)" target="_blank" href="http://php.net/manual/en/reference.pcre.pattern.syntax.php">regular expression</a> showing which process names to hold to a maximum run-time.
HC_RAM_THRESHOLD=Minimum free RAM
CONFIG_OPTION_hc_ram_threshold=The minimum amount of free RAM in Megabytes.
HC_TRANSFER_LATENCY_THRESHOLD=Maximum transfer latency
CONFIG_OPTION_hc_transfer_latency_threshold=The maximum download latency in seconds (measured as time downloading Google home page).
HC_TRANSFER_SPEED_THRESHOLD=Minimum transfer speed
CONFIG_OPTION_hc_transfer_speed_threshold=The minimum transfer speed in Megabits per second (not Megabytes). This is upload speed from the server perspective and download speed from the visitor perspective.
HC_UPTIME_THRESHOLD=Maximum uptime value
CONFIG_OPTION_hc_uptime_threshold=The maximum value of the system 'uptime'.
HC_IO_MBS=Minimum IO read-speed
CONFIG_OPTION_hc_io_mbs=The minimum number of MB/s that can be read per second off the disk.

HC_DATABASE_THRESHOLD=Maximum database size
CONFIG_OPTION_hc_database_threshold=The maximum database size in Megabytes. If you have a database size limit, set this to around 80% of the limit to leave room for growth and for non-cleaned deleted rows.

HC_REQUESTS_PER_SECOND_THRESHOLD=Maximum requests per second for a visitor
CONFIG_OPTION_hc_requests_per_second_threshold=The maximum requests per second for a visitor before it is considered a problem. The problem being a likely <abbr title="Denial Of Service">DOS</abbr> attack.
HC_REQUESTS_WINDOW_SIZE=Request window size for a visitor
CONFIG_OPTION_hc_requests_window_size=The number of requests within the rate-count window to calculate a &ldquo;Requests per second for a visitor&rdquo; value with. If there are fewer recent requests from the visitor than this then no calculation will be performed and no check done.
HC_COMPOUND_REQUESTS_PER_SECOND_THRESHOLD=Maximum requests per second in total
CONFIG_OPTION_hc_compound_requests_per_second_threshold=The maximum requests per second for all visitors together before it is considered a problem. The problem being a likely <abbr title="Denial Of Service">DDOS</abbr> attack, or just an indication we need more system resources.
HC_COMPOUND_REQUESTS_WINDOW_SIZE=Request window size in total
CONFIG_OPTION_hc_compound_requests_window_size=The number of requests within the rate-count window to calculate a &ldquo;Requests per second in total&rdquo; value with. If there are fewer recent requests across all visitors than this then no calculation will be performed and no check done.

HC_MAIL_WAIT_TIME=Wait time
CONFIG_OPTION_hc_mail_wait_time=The maximum number of seconds to wait after sending a test e-mail before assuming it has not been sent.

HC_CRON_SECTIONS_TO_RUN=Check sections to run
CONFIG_OPTION_hc_cron_sections_to_run=Which Health Check sections to run automatically. This also sets the default selection for a manual Health Check.
HC_CRON_REGULARITY=Automatic check regularity
CONFIG_OPTION_hc_cron_regularity=The number of minutes between automatic health checks.
HC_CRON_NOTIFY_REGARDLESS=Send full reports
CONFIG_OPTION_hc_cron_notify_regardless=Send notification reports of automatic health checks even if everything passes. This is useful if you worry the site may break badly and you would not notice, as the health check system itself would have gone down. You would calendar yourself to regularly check that you receive these notifications.

HC_VERSION_CHECK=Version check
CONFIG_OPTION_hc_version_check=What kind of Health Check version checking to do. 
CONFIG_OPTION_hc_version_check_VALUE_deprecated=Version is not deprecated
CONFIG_OPTION_hc_version_check_VALUE_uptodate=Version is fully up-to-date

HEALTH_CHECK_SUBJECT_fail=Health Check failed
HEALTH_CHECK_SUBJECT_misc=Health Check results
HEALTH_CHECK_BODY=Health Check gave the following results...\n\n[html]{1}[/html]

NO_HEALTH_ISSUES_FOUND=No issues were found.
SHOW_FAILS=Show failed checks
SHOW_PASSES=Show passed checks
SHOW_SKIPS=Show skipped checks
SHOW_MANUAL_CHECKS=Show manual checks

REASONS=Reasons
CHECK_FAILED=Failed
CHECK_PASSED=Passed
CHECK_SKIPPED=Skipped
CHECK_MANUAL=Manual
PASSES=Total Passes
FAILS=Total Fails
HELP_THRESHOLDS=If a check shows as passed then the message is what would have showed if it failed. Any numbers involved would be higher/lower for a truly failed check.

CMD_HEALTH_CHECK_HELP=Run Health Checks on system.
CMD_HEALTH_CHECK_HELP_PARAM_0=The Health Check section code to run (e.g. testSpellingContent).
CMD_HEALTH_CHECK_HELP_V=Show verbose output.

CMD_HEALTH_CHECK_PAGES_HELP=Run Health Checks on pages.
CMD_HEALTH_CHECK_PAGES_HELP_PARAM_0=The page-links to run checks on (e.g. :start).
CMD_HEALTH_CHECK_PAGES_HELP_V=Show verbose output.

LEVEL_1_HEADERS_PROBLEM=You have not defined any level 1 headings in {1}, which will impinge on your [acronym="Search Engine Optimisation"]SEO[/acronym], breadcrumbs, and web accessibility. You can easily make one in the [acronym="What You See Is What You Get"]WYSIWYG[/acronym] editor (write some text, and select &lsquo;Heading 1&rsquo; from the format dropdown list), or via the Comcode 'title' tag. Advanced users can hide visually-undesired titles using [acronym="Cascading Style Sheets"]CSS[/acronym].
LEVEL_2_HEADERS_PROBLEM=You have not defined any level 2 headings in {1}, but you seem to have simulated them using custom styling, which will impinge on your web accessibility. You can easily make them in the [acronym="What You See Is What You Get"]WYSIWYG[/acronym] editor (select &lsquo;Heading 2&rsquo; from the format dropdown list), or with Comcode like [tt][title="2"]...[/title][/tt].
PAGE_INTEGRITY_PROBLEM=Page appears broken, missing closing HTML tag in {1}
PAGE_SIZE_PROBLEM=Page is very large @ {1} in {2}
HTTPS_EMBED_PROBLEM=Embedding HTTP resources on HTTPS page: [tt]{1}[/tt] in {2}
HTTPS_LINKING_PROBLEM=Linking to a local HTTP page on all-HTTPS site: [tt]{1}[/tt] in {2}
BROKEN_LINK_PROBLEM=Broken link: [tt]{1}[/tt], '{2}' code.
BROKEN_LINK_PROBLEM__401=Broken link: [tt]{1}[/tt], '{2}' code (unauthorized; authorization or logging in to that site is required to view the page)
BROKEN_LINK_PROBLEM__403=Broken link: [tt]{1}[/tt], '{2}' code (forbidden; access to the page/resource is denied)
BROKEN_LINK_PROBLEM__404=Broken link: [tt]{1}[/tt], '{2}' code (not found; double-check that the link is correct)
BROKEN_LINK_PROBLEM__408=Broken link: [tt]{1}[/tt], '{2}' code (request timed out)
BROKEN_LINK_PROBLEM__410=Broken link: [tt]{1}[/tt], '{2}' code (gone; the page has been removed)
BROKEN_LINK_PROBLEM__429=Broken link: [tt]{1}[/tt], '{2}' code (too many requests; we have been rate-limited by this site)
BROKEN_LINK_PROBLEM__500=Broken link: [tt]{1}[/tt], '{2}' code (internal error; there is a problem with this site or its server)
BROKEN_LINK_PROBLEM__503=Broken link: [tt]{1}[/tt], '{2}' code (service unavailable; the site might be down for maintenance)
BROKEN_LINK_PROBLEM__curl_error_1=Broken link: [tt]{1}[/tt], '{2}' code (cURL unsupported protocol)
BROKEN_LINK_PROBLEM__curl_error_6=Broken link: [tt]{1}[/tt], '{2}' code (cURL could not resolve host; double-check that the link is correct)
BROKEN_LINK_PROBLEM__curl_error_7=Broken link: [tt]{1}[/tt], '{2}' code (cURL could not connect to host or proxy; check proxy settings in the software and that the link is correct)
BROKEN_LINK_PROBLEM__curl_error_8=Broken link: [tt]{1}[/tt], '{2}' code (cURL could not parse server reply)
BROKEN_LINK_PROBLEM__curl_error_23=Error processing link: [tt]{1}[/tt], '{2}' code (cURL could not write the contents of the response to file)
BROKEN_LINK_PROBLEM__curl_error_28=Broken link: [tt]{1}[/tt], '{2}' code (cURL operation timed out)
BROKEN_LINK_PROBLEM__curl_error_33=Error processing link: [tt]{1}[/tt], '{2}' code (server does not accept byte ranges)
BROKEN_LINK_PROBLEM__curl_error_47=Broken link: [tt]{1}[/tt], '{2}' code (too many redirects)
BROKEN_LINK_PROBLEM__curl_error_61=Broken link: [tt]{1}[/tt], '{2}' code (unrecognised content encoding from server)
INCOMPLETE_CONTENT_PROBLEM=Found a suspicious "{1}" in {2}
LOCAL_LINKING_PROBLEM=Found links to a local URL in {1}
FORM_ELEMENT_PROBLEM=Form element(s) found outside of a form in {1}
WEB_STANDARDS_PROBLEM=Web standards errors were detected in {1} ({2})
SPELLING_PROBLEM=Misspelling: {1} in {2}
FORM_ACTION_PROBLEM=Has no form action defined for web POST form
FORM_ACTION_RELATIVE_PROBLEM=Form action is not absolute (i.e. fragile)
FORM_ACTION_ERROR_HANDLING_PROBLEM=Should get 400 response, indicating only issue is missing POST parameter(s), [tt]{1}[/tt] (got [tt]{2}[/tt])
DOUBLE_SPACE_PROBLEM=Accidental double spacing using nbsp entity
TRAILING_SPACE_PROBLEM=Accidental white-space at end of a line or a page due to nbsp
DEAD_LINE_PROBLEM=Lines that are just white-space
INCONSISTENT_PERIOD_PROBLEM=Inconsistent use of full-stops at the end of list items
SEQUENTIAL_FONT_SIZE_PROBLEM=Sequential font size setting for the same size
NESTED_FONT_SIZE_PROBLEM=Nested font size setting for the same size
FAKE_HEADING_PROBLEM=Fake heading
POOR_WYSIWYG_PROBLEM=Poorly-converted WYSIWYG markup
FAKE_TABLES_PROBLEM=Non-semantic table
CRON_UPGRADE_PENDING=The system scheduler is currently disabled because an upgrade is pending. Please upgrade by running [tt]yourbaseurl/upgrader.php[/tt] or by going to the Admin Dashboard.
CRON_NOT_RUNNING=The system scheduler is not running or has not run for over 5 hours. Please check the [url="Configuration Tutorial"]{1}[/url] to learn how to set up the scheduler.
WRONG_JOIN_MODULE_PERMISSIONS=The test account does not have access to the join module. This implies members might not be able to re-agree to the rules (Terms of Service) if you require acceptance and you change the declarations (effectively locking them out of their account). You should fix this immediately under the Admin Zone > Security > Permissions tree editor (also check Match-key page restrictions if the addon is installed). The only group that can safely be denied is guests (if you want to disallow account registrations).

HC_PAGE_DOWNLOAD_ERROR=Could not download "{1}" page from website

DAYS_TO_KEEP__HEALTH_CHECK_LOG=Days to keep Health Check log
CONFIG_OPTION_days_to_keep__health_check_log=The number of days to keep records in the <kbd>health_check.log</kbd> file. Depending on what is there, you may need to keep it short to preserve disk space or meet a GDPR-style privacy policy. Leave blank to keep records forever, barring manual intervention.

CHECKLIST_HEALTH_CHECK=Run/schedule Health Checks
