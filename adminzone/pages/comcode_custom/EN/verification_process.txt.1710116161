﻿[semihtml]<h1 class="screen-title" itemprop="name"><a id="title--VerificationProcess"></a> Validating Age / Verification entries</h1>

<p>This page will detail the process which should be followed to verify a member. Verification allows members to upload media of themselves, including nudist media, to Midwest Nudists.</p>

<p><strong>Entries with their photos can be viewed at&nbsp;<a href="{$PAGE_LINK*,site:catalogues:category:18}">{$PAGE_LINK*,site:catalogues:category:18}</a></strong>&nbsp;</p>

<p>Here are a couple quick disclaimers before we begin:</p>

<ul>
	<li>Staff are&nbsp;<strong>not</strong>&nbsp;exempt from the verification requirement and must go through the process like any other member if they wish to publish media of themselves.</li>
	<li>Staff&nbsp;<strong>must not</strong>&nbsp;verify themselves; another staff member must validate their verification entry.</li>
</ul>

<h2>Assessing the Photo</h2>

<p>When validating the entry, make sure the Verification Photo&nbsp;contains all required information&nbsp;<strong>in one photo</strong>. Members are not allowed to upload multiple verification photos nor are they allowed to edit together multiple photos into one. If they did any of that, request for them to re-take the photo with all required information visible in a single photo.</p>

<p>Make sure their face in real life is clearly visible and not obstructed. If it is, they must re-take the photo.</p>

<p>Make sure all required information in their government ID is visible, and that you can determine the ID is real and not forged by looking at its security features. If either are not true, they must re-take their photo. Required info:</p>

<ul>
	<li>Photo ID, and the photo matches their IRL face</li>
	<li>Date of Birth, and it matches the DOB on their profile, and they are at least 18 years old.</li>
	<li>Date the ID was issued / goes into effect, and it is not in the future.</li>
	<li>Date the ID expires, and it is not in the past.</li>
	<li>Logos&nbsp;/ holograms proving the ID is legitimate.</li>
</ul>

<p>Make sure their piece of paper is&nbsp;<strong>hand-written</strong>&nbsp;and legible, and it contains all required information. If not, they must re-take their photo. Required info:</p>

<ul>
	<li>The member&#39;s username</li>
	<li>The &quot;Identification Code&quot; on the form</li>
</ul>

<h2>Handling Bad Submissions&nbsp;</h2>

<p>If you are rejecting a submission,&nbsp;<strong>delete it&nbsp;</strong>by checking the &quot;Delete&quot; box before saving. Then, send a Private Topic to the member who submitted the request and explain why their submission was rejected. Tell them they must go through the verification process again to get verified (they must do the entire verification with a new Identification Code; there are no &quot;partial verifications&quot;).</p>

<h2>Verifying the Member</h2>

<p><strong>Follow these instructions carefully</strong>&nbsp;to verify a submission if the submission is acceptable;&nbsp;<strong>DO NOT</strong>&nbsp;simply click &quot;Save&quot; to validate it:</p>

<p>1) Under &quot;Privacy settings&quot;, set &quot;Visible to&quot; to &quot;Myself Only&quot;.</p>

<p>2) Under &quot;Periodic Content Reviews&quot;, set &quot;Next review date&quot; to the date their ID expires. Set the &quot;Review date action&quot; to &quot;Delete content&quot;. This will cause their validation submission to automatically expire / delete on that date.</p>

<p>3) Make sure &quot;Validated&quot; is On, and Save.</p>
That is it. So long as an entry exists for that member and it is validated, they will be able to upload media and files. If you later delete or invalidate the entry, then the member will no longer be able to upload media or files.[/semihtml]