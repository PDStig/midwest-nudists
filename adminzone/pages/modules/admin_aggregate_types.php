<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    aggregate_types
 */

require_code('crud_module');

/**
 * Module page class.
 */
class Module_admin_aggregate_types extends Standard_crud_module
{
    protected $lang_type = 'AGGREGATE_TYPE_INSTANCE';
    protected $select_name = 'LABEL';
    protected $menu_label = 'AGGREGATE_TYPES';
    protected $orderer = 'aggregate_label';
    protected $table = 'aggregate_type_instances';
    protected $do_preview = null;

    protected $add_one_label = null;
    protected $edit_this_label = null;
    protected $edit_one_label = null;
    protected $donext_entry_content_type = 'aggregate_type_instance';
    protected $donext_category_content_type = null;

    /**
     * Find details of the module.
     *
     * @return ?array Map of module info (null: module is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Chris Graham';
        $info['organisation'] = 'Composr';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 2;
        $info['update_require_upgrade'] = true;
        $info['locked'] = false;
        $info['min_cms_version'] = 11.0;
        $info['addon'] = 'aggregate_types';
        return $info;
    }

    /**
     * Uninstall the module.
     */
    public function uninstall()
    {
        $GLOBALS['SITE_DB']->drop_table_if_exists('aggregate_type_instances');
    }

    /**
     * Install the module.
     *
     * @param  ?integer $upgrade_from What version we're upgrading from (null: new install)
     * @param  ?integer $upgrade_from_hack What hack version we're upgrading from (null: new-install/not-upgrading-from-a-hacked-version)
     */
    public function install(?int $upgrade_from = null, ?int $upgrade_from_hack = null)
    {
        if ($upgrade_from === null) {
            $GLOBALS['SITE_DB']->create_table('aggregate_type_instances', [
                'id' => '*AUTO',
                'aggregate_label' => 'SHORT_TEXT',
                'aggregate_type' => 'ID_TEXT',
                'other_parameters' => 'SERIAL',
                'add_time' => 'TIME',
                'edit_time' => '?TIME',
            ]);
            $GLOBALS['SITE_DB']->create_index('aggregate_type_instances', 'aggregate_lookup', ['aggregate_label'/*, 'aggregate_type' key would be too long*/]);
        }

        if (($upgrade_from !== null) && ($upgrade_from < 2)) { // LEGACY
            $GLOBALS['SITE_DB']->alter_table_field('aggregate_type_instances', 'other_parameters', 'SERIAL');
        }
    }

    /**
     * Find entry-points available within this module.
     *
     * @param  boolean $check_perms Whether to check permissions
     * @param  ?MEMBER $member_id The member to check permissions as (null: current user)
     * @param  boolean $support_crosslinks Whether to allow cross links to other modules (identifiable via a full-page-link rather than a screen-name)
     * @param  boolean $be_deferential Whether to avoid any entry-point (or even return null to disable the page in the Sitemap) if we know another module, or page_group, is going to link to that entry-point. Note that "!" and "browse" entry points are automatically merged with container page nodes (likely called by page-groupings) as appropriate.
     * @return ?array A map of entry points (screen-name=>language-code/string or screen-name=>[language-code/string, icon-theme-image]) (null: disabled)
     */
    public function get_entry_points(bool $check_perms = true, ?int $member_id = null, bool $support_crosslinks = true, bool $be_deferential = false) : ?array
    {
        if (!addon_installed('aggregate_types')) {
            return null;
        }

        $ret = [
            'browse' => ['AGGREGATE_TYPES', 'menu/adminzone/structure/aggregate_types'],
        ];
        $ret += parent::get_entry_points();
        if (has_privilege(get_member(), 'assume_any_member')) {
            $ret['xml'] = ['EDIT_AGGREGATE_TYPES', 'admin/xml'];
        }
        $ret['sync'] = ['SYNCHRONISE_AGGREGATE_TYPES', 'admin/sync'];
        return $ret;
    }

    public $title;

    /**
     * Module pre-run function. Allows us to know metadata for <head> before we start streaming output.
     *
     * @param  boolean $top_level Whether this is running at the top level, prior to having sub-objects called
     * @param  ?ID_TEXT $type The screen type to consider for metadata purposes (null: read from environment)
     * @return ?Tempcode Tempcode indicating some kind of exceptional output (null: none)
     */
    public function pre_run(bool $top_level = true, ?string $type = null) : ?object
    {
        $error_msg = new Tempcode();
        if (!addon_installed__messaged('aggregate_types', $error_msg)) {
            return $error_msg;
        }

        if (!addon_installed('commandr')) {
            warn_exit(do_lang_tempcode('MISSING_ADDON', escape_html('commandr')));
        }
        if (!addon_installed('import')) {
            warn_exit(do_lang_tempcode('MISSING_ADDON', escape_html('import')));
        }

        if ($type === null) {
            $type = get_param_string('type', 'browse');
        }

        require_lang('aggregate_types');

        set_helper_panel_tutorial('tut_aggregate_types');
        set_helper_panel_text(comcode_lang_string('DOC_AGGREGATE_TYPES'));

        if ($type == 'xml' || $type == '_xml') {
            $this->title = get_screen_title('EDIT_AGGREGATE_TYPES');
        }

        if ($type == 'sync') {
            inform_non_canonical_parameter('sync_type', false);

            $this->title = get_screen_title('SYNCHRONISE_AGGREGATE_TYPES');
        }

        if ($type == '_sync') {
            $this->title = get_screen_title('SYNCHRONISE_AGGREGATE_TYPES');
        }

        return parent::pre_run($top_level);
    }

    /**
     * Standard crud_module run_start.
     *
     * @param  ID_TEXT $type The type of module execution
     * @return Tempcode The output of the run
     */
    public function run_start(string $type) : object
    {
        require_code('aggregate_types');

        $this->add_one_label = do_lang_tempcode('ADD_AGGREGATE_TYPE_INSTANCE');
        $this->edit_this_label = do_lang_tempcode('EDIT_THIS_AGGREGATE_TYPE_INSTANCE');
        $this->edit_one_label = do_lang_tempcode('EDIT_AGGREGATE_TYPE_INSTANCE');

        if ($type == 'browse') {
            return $this->browse();
        }

        if ($type == 'xml') {
            return $this->xml();
        }
        if ($type == '_xml') {
            return $this->_xml();
        }
        if ($type == 'sync') {
            return $this->sync();
        }
        if ($type == '_sync') {
            return $this->_sync();
        }

        return new Tempcode();
    }

    /**
     * The do-next manager for before setup management.
     *
     * @return Tempcode The UI
     */
    public function browse() : object
    {
        require_code('templates_donext');
        return do_next_manager(
            get_screen_title('AGGREGATE_TYPES'),
            comcode_lang_string('DOC_AGGREGATE_TYPES'),
            [
                ['admin/add', ['_SELF', ['type' => 'add'], '_SELF'], do_lang('ADD_AGGREGATE_TYPE_INSTANCE')],
                ['admin/edit', ['_SELF', ['type' => 'edit'], '_SELF'], do_lang('EDIT_AGGREGATE_TYPE_INSTANCE')],
                ['admin/xml', ['_SELF', ['type' => 'xml'], '_SELF'], do_lang('EDIT_AGGREGATE_TYPES')],
                ['admin/sync', ['_SELF', ['type' => 'sync'], '_SELF'], do_lang('SYNCHRONISE_AGGREGATE_TYPES')],
            ],
            do_lang('AGGREGATE_TYPES')
        );
    }

    /**
     * Get Tempcode for an adding form.
     *
     * @return mixed Either Tempcode; or a tuple of: (fields, hidden-fields[, delete-fields][, edit-text][, whether all delete fields are specified][, posting form text, more fields][, parsed WYSIWYG editable text])
     */
    public function get_form_fields_for_add()
    {
        return $this->get_form_fields();
    }

    /**
     * Get Tempcode for a forum grouping template adding/editing form.
     *
     * @param  ID_TEXT $aggregate_type The aggregate type (blank: ask first)
     * @param  SHORT_TEXT $aggregate_label The label for the instance
     * @param  array $other_parameters Other parameters
     * @return mixed Either Tempcode; or a tuple: the input fields, hidden fields, delete fields
     */
    public function get_form_fields(string $aggregate_type = '', string $aggregate_label = '', array $other_parameters = [])
    {
        if ($aggregate_type == '') {
            $aggregate_type = get_param_string('aggregate_type', '');

            if ($aggregate_type == '') {
                $fields = new Tempcode();
                $list = new Tempcode();
                $types = parse_aggregate_xml();
                foreach (array_keys($types) as $type) {
                    $list->attach(form_input_list_entry($type, false, titleify($type)));
                }
                $fields->attach(form_input_list(do_lang_tempcode('AGGREGATE_TYPE'), '', 'aggregate_type', $list, null, true, true));
                $submit_name = do_lang_tempcode('PROCEED');
                $url = get_self_url();
                return do_template('FORM_SCREEN', [
                    '_GUID' => '8bd97d858f2ab1dc885a7453b3dd781c',
                    'TITLE' => $this->title,
                    'SKIP_WEBSTANDARDS' => true,
                    'HIDDEN' => '',
                    'GET' => true,
                    'URL' => $url,
                    'FIELDS' => $fields,
                    'TEXT' => '',
                    'SUBMIT_ICON' => 'buttons/proceed',
                    'SUBMIT_NAME' => $submit_name,
                ]);
            }
        }

        $fields = new Tempcode();
        $hidden = new Tempcode();

        $fields->attach(form_input_line(do_lang_tempcode('LABEL'), do_lang_tempcode('DESCRIPTION_LABEL'), 'aggregate_label', $aggregate_label, true));

        $parameters = find_aggregate_type_parameters($aggregate_type);
        foreach ($parameters as $parameter) {
            if ($parameter != 'label') {
                $required = true;

                $default = array_key_exists($parameter, $other_parameters) ? $other_parameters[$parameter] : '';
                $fields->attach(form_input_line(titleify($parameter), '', $parameter, $default, $required));
            }
        }

        $hidden->attach(form_input_hidden('aggregate_type', $aggregate_type));

        $delete_fields = new Tempcode();
        if ($GLOBALS['FORUM_DRIVER']->is_super_admin(get_member())) {
            $delete_fields->attach(form_input_tick(do_lang_tempcode('DELETE_AGGREGATE_MATCHES'), do_lang_tempcode('DESCRIPTION_DELETE_AGGREGATE_MATCHES'), 'delete_matches', false));
        }

        return [$fields, $hidden, $delete_fields];
    }

    /**
     * Standard crud_module table function.
     *
     * @param  array $url_map Details to go to build_url for link to the next screen
     * @return array A quartet: The choose table, Whether re-ordering is supported from this screen, Search URL, Archive URL
     */
    public function create_selection_list_choose_table(array $url_map) : array
    {
        require_code('templates_results_table');

        $current_ordering = get_param_string('sort', 'aggregate_label ASC', INPUT_FILTER_GET_COMPLEX);
        if (strpos($current_ordering, ' ') === false) {
            warn_exit(do_lang_tempcode('INTERNAL_ERROR', escape_html('e82ce40f74535b8c91cdd72a63e83a7d')));
        }
        $sortables = [
            'aggregate_label' => do_lang_tempcode('LABEL'),
            'aggregate_type' => do_lang_tempcode('TYPE'),
            'add_time' => do_lang_tempcode('TIME'),
        ];
        list($sql_sort, $sort_order, $sortable) = process_sorting_params('aggregate_type_instance', $current_ordering);

        $header_row = results_header_row([
            do_lang_tempcode('LABEL'),
            do_lang_tempcode('TYPE'),
            do_lang_tempcode('TIME'),
            do_lang_tempcode('ACTIONS'),
        ], $sortables, 'sort', $sortable . ' ' . $sort_order);

        $result_entries = new Tempcode();

        list($rows, $max_rows) = $this->get_entry_rows(false, $sql_sort);
        foreach ($rows as $row) {
            $edit_url = build_url($url_map + ['id' => $row['id']], '_SELF');

            $result_entries->attach(results_entry([$row['aggregate_label'], $row['aggregate_type'], get_timezoned_date_time($row['add_time']), protect_from_escaping(hyperlink($edit_url, do_lang_tempcode('EDIT'), false, false, do_lang('EDIT') . ' #' . strval($row['id'])))], true));
        }

        $search_url = null;
        $archive_url = null;

        return [results_table(do_lang($this->menu_label), get_param_integer('start', 0), 'start', get_param_integer('max', 20), 'max', $max_rows, $header_row, $result_entries, $sortables, $sortable, $sort_order), false, $search_url, $archive_url];
    }

    /**
     * Standard crud_module edit form filler.
     *
     * @param  ID_TEXT $_id The entry being edited
     * @return mixed Either Tempcode; or a tuple of: (fields, hidden-fields[, delete-fields][, edit-text][, whether all delete fields are specified][, posting form text, more fields][, parsed WYSIWYG editable text])
     */
    public function fill_in_edit_form(string $_id)
    {
        $id = intval($_id);

        $m = $GLOBALS['SITE_DB']->query_select('aggregate_type_instances', ['*'], ['id' => $id], '', 1);
        if (!array_key_exists(0, $m)) {
            warn_exit(do_lang_tempcode('MISSING_RESOURCE'));
        }
        $r = $m[0];

        return $this->get_form_fields($r['aggregate_type'], $r['aggregate_label'], unserialize($r['other_parameters']));
    }

    /**
     * Read in parameters for adding/editing.
     *
     * @return array Parameters
     */
    public function _read_in_parameters() : array
    {
        $aggregate_label = post_param_string('aggregate_label');
        $aggregate_type = post_param_string('aggregate_type');
        $other_parameters = [];
        $parameters = find_aggregate_type_parameters($aggregate_type);
        foreach ($parameters as $parameter) {
            if ($parameter != 'label') {
                $other_parameters[$parameter] = post_param_string($parameter, '');
            }
        }
        return [$aggregate_label, $aggregate_type, $other_parameters];
    }

    /**
     * Standard crud_module add actualiser.
     *
     * @return array A pair: The entry added, description about usage
     */
    public function add_actualisation() : array
    {
        list($aggregate_label, $aggregate_type, $other_parameters) = $this->_read_in_parameters();
        $id = add_aggregate_type_instance($aggregate_label, $aggregate_type, $other_parameters);
        return [strval($id), null];
    }

    /**
     * Standard crud_module edit actualiser.
     *
     * @param  ID_TEXT $id The entry being edited
     * @return ?Tempcode Description about usage (null: none)
     */
    public function edit_actualisation(string $id) : ?object
    {
        list($aggregate_label, $aggregate_type, $other_parameters) = $this->_read_in_parameters();
        edit_aggregate_type_instance(intval($id), $aggregate_label, $aggregate_type, $other_parameters);
        return null;
    }

    /**
     * Standard crud_module delete actualiser.
     *
     * @param  ID_TEXT $id The entry being deleted
     */
    public function delete_actualisation(string $id)
    {
        $delete_matches = false;
        if ($GLOBALS['FORUM_DRIVER']->is_super_admin(get_member())) {
            $delete_matches = (post_param_integer('delete_matches', 0) == 1);
        }
        delete_aggregate_type_instance(intval($id), $delete_matches);
    }

    /**
     * The UI to edit the aggregate_types XML file.
     *
     * @return Tempcode The UI
     */
    public function xml() : object
    {
        check_privilege('assume_any_member'); // Could be used to set up dangerous new stuff

        parse_aggregate_xml(true);

        $post_url = build_url(['page' => '_SELF', 'type' => '_xml'], '_SELF');

        return do_template('XML_CONFIG_SCREEN', [
            '_GUID' => '2303459e94b959d2edf8444188bbeea9',
            'TITLE' => $this->title,
            'POST_URL' => $post_url,
            'XML' => file_exists(get_custom_file_base() . '/data_custom/xml_config/aggregate_types.xml') ? cms_file_get_contents_safe(get_custom_file_base() . '/data_custom/xml_config/aggregate_types.xml') : cms_file_get_contents_safe(get_file_base() . '/data/xml_config/aggregate_types.xml', FILE_READ_LOCK | FILE_READ_BOM),
        ]);
    }

    /**
     * The UI actualiser edit the aggregate_types XML file.
     *
     * @return Tempcode The UI
     */
    public function _xml() : object
    {
        check_privilege('assume_any_member'); // Could be used to set up dangerous new stuff

        require_code('files');
        $path = get_custom_file_base() . '/data_custom/xml_config/aggregate_types.xml';
        $xml = post_param_string('xml');
        cms_file_put_contents_safe($path, $xml, FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE | FILE_WRITE_BOM);

        log_it('EDIT_AGGREGATE_TYPES');

        parse_aggregate_xml(true);

        return inform_screen($this->title, do_lang_tempcode('SUCCESS'));
    }

    /**
     * The UI to start a synchronisation of aggregate content type instances.
     *
     * @return Tempcode The UI
     */
    public function sync() : object
    {
        $_type = get_param_string('sync_type', '');

        $fields = new Tempcode();

        $list = new Tempcode();
        $types = parse_aggregate_xml();
        foreach (array_keys($types) as $type) {
            $list->attach(form_input_list_entry($type, $_type == $type, titleify($type)));
        }
        $fields->attach(form_input_multi_list(do_lang_tempcode('AGGREGATE_TYPE'), '', 'aggregate_type', $list, null, 15, true));

        $submit_name = do_lang_tempcode('PROCEED');

        $url = build_url(['page' => '_SELF', 'type' => '_sync'], '_SELF');

        return do_template('FORM_SCREEN', [
            '_GUID' => '823999c74834fc34a51a6a63cdafeab5',
            'TITLE' => $this->title,
            'SKIP_WEBSTANDARDS' => true,
            'HIDDEN' => '',
            'URL' => $url,
            'FIELDS' => $fields,
            'TEXT' => do_lang_tempcode('SELECT_AGGREGATE_TYPES_FOR_SYNC'),
            'SUBMIT_ICON' => 'admin/sync',
            'SUBMIT_NAME' => $submit_name,
        ]);
    }

    /**
     * The actualiser to start a synchronisation of aggregate content type instances.
     *
     * @return Tempcode The UI
     */
    public function _sync() : object
    {
        if (!isset($_POST['aggregate_type'])) {
            warn_exit(do_lang_tempcode('NO_PARAMETER_SENT', escape_html('aggregate_type')));
        }

        $types = $_POST['aggregate_type'];
        foreach ($types as $type) {
            resync_all_aggregate_type_instances($type);
        }

        return inform_screen($this->title, do_lang_tempcode('SUCCESS'));
    }
}
