<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_configuration
 */

/**
 * Module page class.
 */
class Module_admin_config
{
    /**
     * Find details of the module.
     *
     * @return ?array Map of module info (null: module is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Chris Graham';
        $info['organisation'] = 'Composr';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 15;
        $info['locked'] = false;
        $info['min_cms_version'] = 11.0;
        $info['addon'] = 'core_configuration';
        return $info;
    }

    /**
     * Find entry-points available within this module.
     *
     * @param  boolean $check_perms Whether to check permissions
     * @param  ?MEMBER $member_id The member to check permissions as (null: current user)
     * @param  boolean $support_crosslinks Whether to allow cross links to other modules (identifiable via a full-page-link rather than a screen-name)
     * @param  boolean $be_deferential Whether to avoid any entry-point (or even return null to disable the page in the Sitemap) if we know another module, or page_group, is going to link to that entry-point. Note that "!" and "browse" entry points are automatically merged with container page nodes (likely called by page-groupings) as appropriate.
     * @return ?array A map of entry points (screen-name=>language-code/string or screen-name=>[language-code/string, icon-theme-image]) (null: disabled)
     */
    public function get_entry_points(bool $check_perms = true, ?int $member_id = null, bool $support_crosslinks = true, bool $be_deferential = false) : ?array
    {
        $ret = [
            'browse' => ['CONFIGURATION', 'menu/adminzone/setup/config/config'],
        ];

        $ret['base'] = ['BASE_CONFIGURATION', 'menu/adminzone/setup/config/base_config'];

        if (!$be_deferential) {
            if (addon_installed('xml_fields')) {
                $ret['xml_fields'] = ['FIELD_FILTERS', 'menu/adminzone/setup/xml_fields'];
            }

            if (addon_installed('breadcrumbs')) {
                $ret['xml_breadcrumbs'] = ['BREADCRUMB_OVERRIDES', 'menu/adminzone/structure/breadcrumbs'];
            }

            if (addon_installed('securitylogging')) {
                $ret['advanced_banning'] = ['ADVANCED_BANNING', 'menu/adminzone/security/ip_ban'];
            }

            if (get_value('brand_base_url') === null) {
                $ret['upgrader'] = ['UPGRADER_UPGRADER_TITLE', 'menu/adminzone/tools/upgrade'];
            }

            if (addon_installed('syndication')) {
                $ret['backend'] = ['FEEDS', 'links/rss'];
            }

            if (addon_installed('code_editor')) {
                $ret['code_editor'] = ['CODE_EDITOR', 'menu/adminzone/tools/code_editor'];
            }
        }

        return $ret;
    }

    public $title;
    public $category;

    /**
     * Module pre-run function. Allows us to know metadata for <head> before we start streaming output.
     *
     * @return ?Tempcode Tempcode indicating some kind of exceptional output (null: none)
     */
    public function pre_run() : ?object
    {
        require_code('form_templates'); // Needs to run high so that the anti-click-hacking header is sent

        require_code('input_filter_2');
        if (get_value('disable_modsecurity_workaround') !== '1') {
            modsecurity_workaround_enable();
        }

        require_all_lang();

        require_javascript('core_configuration');
        require_javascript('core_form_interfaces');

        $type = get_param_string('type', 'browse');

        require_lang('config');

        if ($type == 'browse') {
            set_helper_panel_tutorial('tut_adv_configuration');
            set_helper_panel_text(comcode_lang_string('DOC_CONFIGURATION'));

            $this->title = get_screen_title('CONFIGURATION');
        }

        if ($type == 'category') {
            /*Actually let's save the space  set_helper_panel_tutorial('tut_adv_configuration');*/

            $category = get_param_string('id');

            $test = do_lang('CONFIG_CATEGORY_' . $category, null, null, null, null, false);
            if ($test === null) {
                attach_message(do_lang_tempcode('CAT_NOT_FOUND', escape_html($category), 'OPTION_CATEGORY'), 'warn');

                $this->title = get_screen_title('CONFIGURATION');
            } else {
                breadcrumb_set_parents([['_SELF:_SELF:browse', do_lang_tempcode('CONFIGURATION')]]);
                breadcrumb_set_self(do_lang_tempcode('CONFIG_CATEGORY_' . $category));

                $this->title = get_screen_title(do_lang_tempcode('CONFIG_CATEGORY_' . $category), false);
            }

            $this->category = $category;
        }

        if ($type == 'set') {
            $category = get_param_string('id', 'MAIN');

            $test = do_lang('CONFIG_CATEGORY_' . $category, null, null, null, null, false);
            if ($test === null) {
                $this->title = get_screen_title('CONFIGURATION');
            } else {
                $this->title = get_screen_title(do_lang_tempcode('CONFIG_CATEGORY_' . $category), false);
            }
        }

        if ($type == 'base') {
            $this->title = get_screen_title('CONFIGURATION');
        }

        if ($type == 'upgrader') {
            $this->title = get_screen_title('UPGRADER_UPGRADER_TITLE');
        }

        if ($type == 'backend') {
            $this->title = get_screen_title('FEEDS');
        }

        if ($type == 'code_editor') {
            $this->title = get_screen_title('CODE_EDITOR');
        }

        if ($type == 'xml_fields') {
            set_helper_panel_tutorial('tut_fields_filter');
            set_helper_panel_text(comcode_lang_string('DOC_FIELD_FILTERS'));

            $this->title = get_screen_title('FIELD_FILTERS');
        }

        if ($type == '_xml_fields') {
            $this->title = get_screen_title('FIELD_FILTERS');

            breadcrumb_set_parents([['_SEARCH:admin_config:xml_fields', do_lang_tempcode('FIELD_FILTERS')]]);
            breadcrumb_set_self(do_lang_tempcode('DONE'));
        }

        if ($type == 'xml_breadcrumbs') {
            set_helper_panel_tutorial('tut_structure');
            set_helper_panel_text(comcode_lang_string('DOC_BREADCRUMB_OVERRIDES'));

            $this->title = get_screen_title('BREADCRUMB_OVERRIDES');
        }

        if ($type == '_xml_breadcrumbs') {
            $this->title = get_screen_title('BREADCRUMB_OVERRIDES');

            breadcrumb_set_parents([['_SEARCH:admin_config:xml_breadcrumbs', do_lang_tempcode('BREADCRUMB_OVERRIDES')]]);
            breadcrumb_set_self(do_lang_tempcode('DONE'));
        }

        if ($type == 'advanced_banning') {
            set_helper_panel_tutorial('tut_censor');
            set_helper_panel_text(comcode_lang_string('DOC_ADVANCED_BANNING'));

            $this->title = get_screen_title('ADVANCED_BANNING');
        }

        if ($type == '_advanced_banning') {
            $this->title = get_screen_title('ADVANCED_BANNING');
        }

        return null;
    }

    /**
     * Execute the module.
     *
     * @return Tempcode The result of execution
     */
    public function run() : object
    {
        require_code('config2');

        $type = get_param_string('type', 'browse');

        if ($type == 'browse') {
            return $this->config_choose(); // List of categories
        }
        if ($type == 'category') {
            return $this->config_category(); // Category editing UI
        }
        if ($type == 'set') {
            return $this->config_set(); // Category editing actualiser
        }

        if (addon_installed('xml_fields')) {
            if ($type == 'xml_fields') {
                return $this->xml_fields();
            }
            if ($type == '_xml_fields') {
                return $this->_xml_fields();
            }
        }
        if (addon_installed('breadcrumbs')) {
            if ($type == 'xml_breadcrumbs') {
                return $this->xml_breadcrumbs();
            }
            if ($type == '_xml_breadcrumbs') {
                return $this->_xml_breadcrumbs();
            }
        }
        if (addon_installed('securitylogging')) {
            if ($type == 'advanced_banning') {
                return $this->advanced_banning();
            }
            if ($type == '_advanced_banning') {
                return $this->_advanced_banning();
            }
        }

        if ($type == 'base') {
            return $this->base();
        }
        if (get_value('brand_base_url') === null) {
            if ($type == 'upgrader') {
                return $this->upgrader();
            }
        }
        if (addon_installed('syndication')) {
            if ($type == 'backend') {
                return $this->backend();
            }
        }
        if (addon_installed('code_editor')) {
            if ($type == 'code_editor') {
                return $this->code_editor();
            }
        }

        return new Tempcode();
    }

    /**
     * The UI to choose what configuration page to edit.
     *
     * @return Tempcode The UI
     */
    public function config_choose() : object
    {
        $pre = new Tempcode();

        // Import?
        if ($GLOBALS['DEV_MODE']) {
            $path = get_file_base() . '/_tests/assets/keys.csv';
            if (is_file($path)) {
                require_code('files_spreadsheets_read');
                $sheet_reader = spreadsheet_open_read($path);
                $import_map = [];
                $list_options = [];
                while (($row = $sheet_reader->read_row()) !== false) {
                    $option_name = $row['Option'];
                    $option_value = $row['Value'];
                    $option_type = $row['Type'];

                    switch ($option_type) {
                        case 'option':
                            if ((is_file(get_file_base() . '/sources/hooks/systems/config/' . $option_name . '.php')) || (is_file(get_file_base() . '/sources_custom/hooks/systems/config/' . $option_name . '.php'))) {
                                require_code('hooks/systems/config/' . filter_naughty_harsh($option_name));
                                $ob = object_factory('Hook_config_' . filter_naughty_harsh($option_name), true);
                                if ($ob !== null) {
                                    $details = $ob->get_details();

                                    $option_title = do_lang($details['group']) . ': ' . do_lang($details['human_name']) . ' [' . escape_html($option_name) . ']' . ' {' . escape_html($option_type) . '}';

                                    $import_map[] = $row;
                                    $option_selected = ((get_option($option_name) == '') || (get_option($option_name) == get_default_option($option_name))) && ($option_value != '') && ($option_value != get_default_option($option_name));

                                    $list_options[$option_title] = [$option_type, $option_name, $option_selected];
                                }
                            }
                            break;

                        case 'hidden':
                        case 'hidden_elective':
                            $option_title = escape_html($option_name) . ' {' . escape_html($option_type) . '}';

                            $import_map[] = $row;
                            $option_selected = cms_empty_safe(get_value($option_name, null, $option_type == 'hidden_elective')) && ($option_value != '');

                            $list_options[$option_title] = [$option_type, $option_name, $option_selected];

                            break;

                        case 'return':
                            break;
                    }
                }
                $sheet_reader->close();

                ksort($list_options, SORT_NATURAL | SORT_FLAG_CASE);

                $import_list = new Tempcode();
                foreach ($list_options as $option_title => $list_option_bits) {
                    list($option_type, $option_name, $option_selected) = $list_option_bits;

                    $import_list->attach(form_input_list_entry($option_type . '/' . $option_name, $option_selected, protect_from_escaping($option_title)));
                }

                if (!empty($import_map)) {
                    // UI
                    $fields = form_input_multi_list('Keys', '', 'import_keys', $import_list, null, 15);
                    $form = do_template('FORM', [
                        '_GUID' => '1a4c9009961ea4218b19ff7bce2935ce',
                        'TEXT' => protect_from_escaping('You are in developer mode and have a <kbd>keys.csv</kbd> file with keys that you can import into the configuration.'),
                        'HIDDEN' => '',
                        'FIELDS' => $fields,
                        'SUBMIT_ICON' => 'admin/import',
                        'SUBMIT_NAME' => do_lang_tempcode('IMPORT'),
                        'URL' => get_self_url(),
                        'SKIP_REQUIRED' => true,
                    ]);
                    $pre->attach(put_in_standard_box($form, make_string_tempcode('Development: Key import'), 'default', '', 'tray_closed'));

                    // Save
                    if (!empty($_POST['import_keys'])) {
                        foreach ($import_map as $row) {
                            $option_type = $row['Type'];
                            $option_name = $row['Option'];
                            $option_value = $row['Value'];

                            if (in_array($option_type . '/' . $option_name, $_POST['import_keys'])) {
                                switch ($option_type) {
                                    case 'option':
                                        set_option($option_name, $option_value);
                                        break;

                                    case 'hidden':
                                        set_value($option_name, $option_value);
                                        break;

                                    case 'hidden_elective':
                                        set_value($option_name, $option_value, true);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        $pre->attach(paragraph(do_lang_tempcode('CHOOSE_A_CONFIG_CATEGORY')));

        // Find all categories
        $hooks = find_all_hook_obs('systems', 'config', 'Hook_config_');
        $categories = [];
        foreach ($hooks as $ob) {
            $details = $ob->get_details();
            if (($GLOBALS['CURRENT_SHARE_USER'] === null) || ($details['shared_hosting_restricted'] == 0)) {
                if ($ob->get_default() !== null) {
                    $category = $details['category'];
                    if (!isset($categories[$category])) {
                        $categories[$category] = 0;
                    }
                    $categories[$category]++;
                }
            }
        }

        // Show all categories
        $categories_tpl = new Tempcode();
        cms_mb_ksort($categories, SORT_NATURAL | SORT_FLAG_CASE);
        foreach ($categories as $category => $option_count) {
            // Some are skipped
            if (get_forum_type() != 'cns') {
                if ($category == 'USERS') {
                    continue;
                }
                if ($category == 'FORUMS') {
                    continue;
                }
            }
            if (has_no_forum()) {
                if ($category == 'FORUMS') {
                    continue;
                }
            }

            // Put together details...

            $url = build_url(['page' => '_SELF', 'type' => 'category', 'id' => $category], '_SELF');

            $_category_name = do_lang('CONFIG_CATEGORY_' . $category, null, null, null, null, false);
            if ($_category_name === null) {
                attach_message(do_lang_tempcode('CAT_NOT_FOUND', escape_html($category), 'OPTION_CATEGORY'), 'warn');

                $category_name = make_string_tempcode($category);

                $description = new Tempcode();
            } else {
                $category_name = do_lang_tempcode('CONFIG_CATEGORY_' . $category);

                $description = do_lang_tempcode('CONFIG_CATEGORY_DESCRIPTION__' . $category);
            }

            $count = do_lang_tempcode('CATEGORY_SUBORDINATE_2', escape_html(integer_format($option_count)));

            $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
                '_GUID' => '6ba2b09432d06e7502c71e7aac2d3527',
                'COUNT' => $count,
                'NAME' => $category_name,
                'TITLE' => '',
                'DESCRIPTION' => $description,
                'URL' => $url,
            ]));
        }

        $categories_tpl->attach(do_template('COMCODE_SUBTITLE', [
            '_GUID' => '4b641b9cecb15576b36b1b857eb0f624',
            'TITLE' => do_lang('DEEPER_CONFIGURATION'),
            'LEVEL' => '2',
        ]));

        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '6fde99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'base'], '_SELF'),
            'NAME' => do_lang_tempcode('BASE_CONFIGURATION'),
            'DESCRIPTION' => do_lang_tempcode('DOC_BASE_CONFIGURATION'),
        ]));

        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '7fde99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'xml_fields'], '_SELF'),
            'NAME' => do_lang_tempcode('FIELD_FILTERS'),
            'DESCRIPTION' => do_lang_tempcode('DOC_FIELD_FILTERS'),
        ]));
        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '8fde99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'xml_breadcrumbs'], '_SELF'),
            'NAME' => do_lang_tempcode('BREADCRUMB_OVERRIDES'),
            'DESCRIPTION' => do_lang_tempcode('DOC_BREADCRUMB_OVERRIDES'),
        ]));
        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '123e99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'advanced_banning'], '_SELF'),
            'NAME' => do_lang_tempcode('ADVANCED_BANNING'),
            'DESCRIPTION' => do_lang_tempcode('DOC_ADVANCED_BANNING'),
        ]));
        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '9fde99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'upgrader'], '_SELF'),
            'NAME' => do_lang_tempcode('UPGRADER_UPGRADER_TITLE'),
            'DESCRIPTION' => do_lang_tempcode('UPGRADER_UPGRADER_INTRO'),
        ]));
        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '0fde99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'backend'], '_SELF'),
            'NAME' => do_lang_tempcode('_FEEDS'),
            'DESCRIPTION' => comcode_to_tempcode(do_lang('OPML_INDEX_DESCRIPTION')),
        ]));
        $categories_tpl->attach(do_template('INDEX_SCREEN_FANCIER_ENTRY', [
            '_GUID' => '1fde99ae81367fb7405e94b6731a7d9a',
            'COUNT' => null,
            'TITLE' => '',
            'URL' => build_url(['page' => '_SELF', 'type' => 'code_editor'], '_SELF'),
            'NAME' => do_lang_tempcode('CODE_EDITOR'),
            'DESCRIPTION' => do_lang_tempcode('DOC_CODE_EDITOR'),
        ]));

        // Wrapper
        return do_template('INDEX_SCREEN_FANCIER_SCREEN', [
            '_GUID' => 'c8fdb2b481625d58b0b228c897fda72f',
            'TITLE' => $this->title,
            'PRE' => $pre,
            'CONTENT' => $categories_tpl,
            'POST' => '',
        ]);
    }

    /**
     * The UI to edit a configuration page.
     *
     * @return Tempcode The UI
     */
    public function config_category() : object
    {
        require_javascript('checking');

        // Load up some basic details
        $category = $this->category;
        $post_url = build_url(['page' => '_SELF', 'type' => 'set', 'id' => $category, 'redirect' => protect_url_parameter(get_param_string('redirect', null, INPUT_FILTER_URL_INTERNAL))], '_SELF');
        $category_description = do_lang_tempcode('CONFIG_CATEGORY_DESCRIPTION__' . $category);

        // Find all options in category
        $hooks = find_all_hook_obs('systems', 'config', 'Hook_config_');
        $options = [];
        foreach ($hooks as $hook => $ob) {
            $details = $ob->get_details();
            if (($GLOBALS['CURRENT_SHARE_USER'] === null) || ($details['shared_hosting_restricted'] == 0)) {
                if ($category == $details['category']) {
                    if ($ob->get_default() !== null) {
                        if (!isset($details['order_in_category_group'])) {
                            $details['order_in_category_group'] = 100;
                        }
                        $details['ob'] = $ob;
                        $details['name'] = $hook;
                        $options[$details['group']][$hook] = $details;
                    }
                }
            }
        }

        require_code('files');
        $upload_max_filesize = (ini_get('upload_max_filesize') == '0') ? do_lang('NA') : clean_file_size(php_return_bytes(ini_get('upload_max_filesize')));
        $post_max_size = (ini_get('post_max_size') == '0') ? do_lang('NA') : clean_file_size(php_return_bytes(ini_get('post_max_size')));

        // Sort the groups
        $all_known_groups = [];
        foreach (array_keys($options) as $group) {
            $_group = do_lang($group);

            $_group = cms_mb_strtolower(trim(cms_preg_replace_safe('#(&.*;)|[^\w\s]#U', '', strip_tags($_group))));
            if ((isset($all_known_groups[$_group])) && ($all_known_groups[$_group] != $group)) {
                $_group = 'std_' . $group; // If cat names translate to same things or are in non-latin characters like Cyrillic
            }

            $all_known_groups[$_group] = $group;
        }

        cms_mb_ksort($all_known_groups, SORT_NATURAL | SORT_FLAG_CASE);

        $general_key = cms_mb_strtolower(trim(cms_preg_replace_safe('#(&.*;)|[^\w\s]#U', '', do_lang('GENERAL'))));
        if (isset($all_known_groups[$general_key])) { // General goes first
            $temp = $all_known_groups[$general_key];
            unset($all_known_groups[$general_key]);
            array_unshift($all_known_groups, $temp);
        }

        $advanced_key = cms_mb_strtolower(trim(cms_preg_replace_safe('#(&.*;)|[^\w\s]#U', '', do_lang('ADVANCED'))));
        if (isset($all_known_groups[$advanced_key])) { // Advanced goes last
            $temp = $all_known_groups[$advanced_key];
            unset($all_known_groups[$advanced_key]);
            $all_known_groups[$advanced_key] = $temp;
        }

        // Render option groups
        $groups_arr = [];
        $_groups = [];
        foreach ($all_known_groups as $group_codename) {
            if (!isset($options[$group_codename])) {
                continue;
            }

            $options_in_group = $options[$group_codename];

            $all_orders_default = true;
            foreach ($options_in_group as $name => $details) {
                if ($details['order_in_category_group'] != 100) {
                    $all_orders_default = false;
                }
                $options_in_group[$name]['human_name_trans'] = do_lang($details['human_name']);
            }
            if ($all_orders_default) {
                sort_maps_by($options_in_group, 'human_name_trans', false, true);
            } else {
                sort_maps_by($options_in_group, 'order_in_category_group');
            }

            $out = '';
            foreach ($options_in_group as $name => $details) {
                $out .= static_evaluate_tempcode(build_config_inputter($name, $details));
            }

            // Render group
            $group_title = do_lang_tempcode($group_codename);
            $_group_description = do_lang('CONFIG_GROUP_DESCRIP_' . $group_codename, escape_html($post_max_size), escape_html($upload_max_filesize), escape_html(get_base_url()), null, false);
            if ($_group_description === null) {
                $group_description = new Tempcode();
            } else {
                $group_description = do_lang_tempcode('CONFIG_GROUP_DESCRIP_' . $group_codename, escape_html($post_max_size), escape_html($upload_max_filesize), escape_html(get_base_url()));
            }
            $groups_arr[] = ['GROUP_DESCRIPTION' => $group_description, 'GROUP_NAME' => $group_codename, 'GROUP' => $out, 'GROUP_TITLE' => $group_title];
            $_groups[$group_codename] = $group_title;
        }

        list($warning_details, $ping_url) = handle_conflict_resolution();

        // Render
        return do_template('CONFIG_CATEGORY_SCREEN', [
            '_GUID' => 'd01b28b71c38bbb52b6aaf877c7f7b0e',
            'CATEGORY_DESCRIPTION' => $category_description,
            '_GROUPS' => $_groups,
            'PING_URL' => $ping_url,
            'WARNING_DETAILS' => $warning_details,
            'TITLE' => $this->title,
            'URL' => $post_url,
            'GROUPS' => $groups_arr,
            'SUBMIT_ICON' => 'buttons/save',
            'SUBMIT_NAME' => do_lang_tempcode('SAVE'),
        ]);
    }

    /**
     * The actualiser to edit a configuration page.
     *
     * @return Tempcode The UI
     */
    public function config_set() : object
    {
        require_code('input_filter_2');
        rescue_shortened_post_request();

        require_code('caches3');

        global $CONFIG_OPTIONS_CACHE;

        $category = get_param_string('id', 'MAIN');

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            warn_exit(do_lang_tempcode('INTERNAL_ERROR', escape_html('2da66c7e8db4514eb34d606c398e17ec')), false, false, 400);
        }

        // Find all options in category
        $hooks = find_all_hook_obs('systems', 'config', 'Hook_config_');
        $options = [];
        foreach ($hooks as $hook => $ob) {
            $details = $ob->get_details();
            if ($category == $details['category']) {
                if (($GLOBALS['CURRENT_SHARE_USER'] === null) || ($details['shared_hosting_restricted'] == 0)) {
                    if ($ob->get_default() !== null) {
                        $details['ob'] = $ob;
                        $options[$hook] = $details;
                    }
                }
            }
        }

        // Go through all options on the page, saving
        foreach ($options as $name => $details) {
            $value = get_submitted_config_value($name, $details);

            // If the option was changed
            $old_value = get_option($name);
            if (($old_value != $value) || (!isset($CONFIG_OPTIONS_CACHE[$name]['c_set'])) || ($CONFIG_OPTIONS_CACHE[$name]['c_set'] == 0)) {
                $ob = $details['ob'];

                // Run pre-save code where it exists
                if (($ob !== null) && (method_exists($ob, 'presave_handler'))) {
                    $okay_to_save = $ob->presave_handler($value, $old_value);
                } else {
                    $okay_to_save = true;
                }

                // Save
                if ($okay_to_save) {
                    set_option($name, $value, 1, $ob);
                }
            }
        }

        // Clear some caching
        erase_comcode_page_cache();
        erase_block_cache();
        //persistent_cache_delete('OPTIONS');  Done by set_option / erase_persistent_cache
        erase_persistent_cache();
        regenerate_trusted_sites_cache();
        erase_cached_templates(false, null, TEMPLATE_DECACHE_WITH_CONFIG);

        // Show it worked / Refresh
        $redirect = get_param_string('redirect', '', INPUT_FILTER_URL_INTERNAL);
        if ($redirect == '') {
            $url = build_url(['page' => '_SELF', 'type' => 'browse'], '_SELF'); // , 'type' => 'category', 'id' => $category
        } else {
            $url = make_string_tempcode($redirect);
        }
        return redirect_screen($this->title, $url, do_lang_tempcode('SUCCESS'));
    }

    /**
     * Redirect to the config_editor script.
     *
     * @return Tempcode The UI
     */
    public function base() : object
    {
        $keep = symbol_tempcode('KEEP', ['1']);
        $url = get_base_url() . '/config_editor.php' . $keep->evaluate();
        return redirect_screen($this->title, $url);
    }

    /**
     * Redirect to the upgrader script.
     *
     * @return Tempcode The UI
     */
    public function upgrader() : object
    {
        $keep = symbol_tempcode('KEEP', ['1']);
        $url = get_base_url() . '/upgrader.php' . $keep->evaluate();
        return redirect_screen($this->title, $url);
    }

    /**
     * Redirect to the backend script.
     *
     * @return Tempcode The UI
     */
    public function backend() : object
    {
        $keep = symbol_tempcode('KEEP', ['1']);
        $url = get_base_url() . '/backend.php' . $keep->evaluate();
        return redirect_screen($this->title, $url);
    }

    /**
     * Redirect to the code_editor script.
     *
     * @return Tempcode The UI
     */
    public function code_editor() : object
    {
        $keep = symbol_tempcode('KEEP', ['1']);
        $url = get_base_url() . '/code_editor.php' . $keep->evaluate();
        return redirect_screen($this->title, $url);
    }

    /**
     * The UI to edit the fields XML file.
     *
     * @return Tempcode The UI
     */
    public function xml_fields() : object
    {
        $post_url = build_url(['page' => '_SELF', 'type' => '_xml_fields'], '_SELF');

        return do_template('XML_CONFIG_SCREEN', [
            '_GUID' => 'cc21f921ecbdbdf83e1e28d2b3f75a3a',
            'TITLE' => $this->title,
            'POST_URL' => $post_url,
            'XML' => file_exists(get_custom_file_base() . '/data_custom/xml_config/fields.xml') ? cms_file_get_contents_safe(get_custom_file_base() . '/data_custom/xml_config/fields.xml') : cms_file_get_contents_safe(get_file_base() . '/data/xml_config/fields.xml', FILE_READ_LOCK | FILE_READ_BOM),
        ]);
    }

    /**
     * The UI actualiser edit the fields XML file.
     *
     * @return Tempcode The UI
     */
    public function _xml_fields() : object
    {
        require_code('files');
        $full_path = get_custom_file_base() . '/data_custom/xml_config/fields.xml';
        $xml = post_param_string('xml');
        cms_file_put_contents_safe($full_path, $xml, FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE | FILE_WRITE_BOM);

        log_it('FIELD_FILTERS');

        return inform_screen($this->title, do_lang_tempcode('SUCCESS'));
    }

    /**
     * The UI to edit the breadcrumbs XML file.
     *
     * @return Tempcode The UI
     */
    public function xml_breadcrumbs() : object
    {
        $post_url = build_url(['page' => '_SELF', 'type' => '_xml_breadcrumbs'], '_SELF');

        return do_template('XML_CONFIG_SCREEN', [
            '_GUID' => '456f56149832d459bce72ca63a1578b9',
            'TITLE' => $this->title,
            'POST_URL' => $post_url,
            'XML' => file_exists(get_custom_file_base() . '/data_custom/xml_config/breadcrumbs.xml') ? cms_file_get_contents_safe(get_custom_file_base() . '/data_custom/xml_config/breadcrumbs.xml') : cms_file_get_contents_safe(get_file_base() . '/data/xml_config/breadcrumbs.xml', FILE_READ_LOCK | FILE_READ_BOM),
        ]);
    }

    /**
     * The UI actualiser edit the breadcrumbs XML file.
     *
     * @return Tempcode The UI
     */
    public function _xml_breadcrumbs() : object
    {
        require_code('files');
        $full_path = get_custom_file_base() . '/data_custom/xml_config/breadcrumbs.xml';
        $xml = post_param_string('xml');
        cms_file_put_contents_safe($full_path, $xml, FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE | FILE_WRITE_BOM);

        log_it('BREADCRUMB_OVERRIDES');

        return inform_screen($this->title, do_lang_tempcode('SUCCESS'));
    }

    /**
     * The UI to edit the advanced banning XML file.
     *
     * @return Tempcode The UI
     */
    public function advanced_banning() : object
    {
        $post_url = build_url(['page' => '_SELF', 'type' => '_advanced_banning'], '_SELF');

        return do_template('XML_CONFIG_SCREEN', [
            '_GUID' => '123f56149832d459bce72ca63a1578b9',
            'TITLE' => $this->title,
            'POST_URL' => $post_url,
            'XML' => file_exists(get_custom_file_base() . '/data_custom/xml_config/advanced_banning.xml') ? cms_file_get_contents_safe(get_custom_file_base() . '/data_custom/xml_config/advanced_banning.xml') : cms_file_get_contents_safe(get_file_base() . '/data/xml_config/advanced_banning.xml', FILE_READ_LOCK | FILE_READ_BOM),
        ]);
    }

    /**
     * The UI actualiser edit the advanced banning XML file.
     *
     * @return Tempcode The UI
     */
    public function _advanced_banning() : object
    {
        require_code('files');
        $full_path = get_custom_file_base() . '/data_custom/xml_config/advanced_banning.xml';
        $xml = post_param_string('xml');
        cms_file_put_contents_safe($full_path, $xml, FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE | FILE_WRITE_BOM);

        log_it('ADVANCED_BANNING');

        return inform_screen($this->title, do_lang_tempcode('SUCCESS'));
    }
}
