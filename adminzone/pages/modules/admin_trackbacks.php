<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_feedback_features
 */

/**
 * Module page class.
 */
class Module_admin_trackbacks
{
    /**
     * Find details of the module.
     *
     * @return ?array Map of module info (null: module is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Chris Graham';
        $info['organisation'] = 'Composr';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 2;
        $info['locked'] = false;
        $info['min_cms_version'] = 11.0;
        $info['addon'] = 'core_feedback_features';
        return $info;
    }

    /**
     * Find entry-points available within this module.
     *
     * @param  boolean $check_perms Whether to check permissions
     * @param  ?MEMBER $member_id The member to check permissions as (null: current user)
     * @param  boolean $support_crosslinks Whether to allow cross links to other modules (identifiable via a full-page-link rather than a screen-name)
     * @param  boolean $be_deferential Whether to avoid any entry-point (or even return null to disable the page in the Sitemap) if we know another module, or page_group, is going to link to that entry-point. Note that "!" and "browse" entry points are automatically merged with container page nodes (likely called by page-groupings) as appropriate.
     * @return ?array A map of entry points (screen-name=>language-code/string or screen-name=>[language-code/string, icon-theme-image]) (null: disabled)
     */
    public function get_entry_points(bool $check_perms = true, ?int $member_id = null, bool $support_crosslinks = true, bool $be_deferential = false) : ?array
    {
        if ($check_perms) {
            if ((get_option('is_on_trackbacks') == '0') || ($GLOBALS['SITE_DB']->query_select_value('trackbacks', 'COUNT(*)') == 0)) {
                return null;
            }
        }

        return [
            'browse' => ['MANAGE_TRACKBACKS', 'menu/adminzone/audit/trackbacks'],
        ];
    }

    public $title;

    /**
     * Module pre-run function. Allows us to know metadata for <head> before we start streaming output.
     *
     * @return ?Tempcode Tempcode indicating some kind of exceptional output (null: none)
     */
    public function pre_run() : ?object
    {
        $type = get_param_string('type', 'browse');

        require_lang('trackbacks');

        set_helper_panel_text(comcode_lang_string('DOC_TRACKBACKS'));

        if ($type == 'browse') {
            $this->title = get_screen_title('MANAGE_TRACKBACKS');
        }

        if ($type == 'delete') {
            $this->title = get_screen_title('DELETE_TRACKBACKS');
        }

        return null;
    }

    /**
     * Execute the module.
     *
     * @return Tempcode The result of execution
     */
    public function run() : object
    {
        $type = get_param_string('type', 'browse');

        if ($type == 'browse') {
            return $this->choose();
        }
        if ($type == 'delete') {
            return $this->delete_trackbacks();
        }

        return new Tempcode();
    }

    /**
     * The UI to delete trackbacks.
     *
     * @return Tempcode The UI
     */
    public function choose() : object
    {
        $trackback_rows = $GLOBALS['SITE_DB']->query_select('trackbacks', ['*'], [], 'ORDER BY trackback_time DESC,id DESC', 1000);

        $trackbacks = '';
        foreach ($trackback_rows as $value) {
            $trackbacks .= static_evaluate_tempcode(do_template('TRACKBACK', [
                '_GUID' => 'eb005ff4cf387e4c18cbc862c38555e3',
                'ID' => strval($value['id']),
                '_DATE' => strval($value['trackback_time']),
                'DATE' => get_timezoned_date_time($value['trackback_time']),
                'URL' => $value['trackback_url'],
                'TITLE' => $value['trackback_title'],
                'EXCERPT' => $value['trackback_excerpt'],
                'NAME' => $value['trackback_name'],
            ]));
        }

        return do_template('TRACKBACK_DELETE_SCREEN', ['_GUID' => '51f7e4c1976bcaf120758d2c86771289', 'TITLE' => $this->title, 'TRACKBACKS' => $trackbacks, 'LOTS' => count($trackback_rows) == 1000]);
    }

    /**
     * The actualiser to delete trackbacks.
     *
     * @return Tempcode The UI
     */
    public function delete_trackbacks() : object
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            warn_exit(do_lang_tempcode('IMPROPERLY_FILLED_IN'), false, false, 400);
        }

        foreach ($_POST as $key => $val) {
            if (!is_string($val)) {
                continue;
            }

            if ((is_string($key)) && (substr($key, 0, 10) == 'trackback_')) {
                $id = intval(substr($key, 10));
                switch ($val) {
                    case '2':
                        if (addon_installed('securitylogging')) {
                            $trackback_ip_address = $GLOBALS['SITE_DB']->query_select_value_if_there('trackbacks', 'trackback_ip_address', ['id' => $id]);
                            if ($trackback_ip_address === null) {
                                break;
                            }
                            require_code('failure');
                            require_code('failure_spammers');
                            add_ip_ban($trackback_ip_address, do_lang('TRACKBACK_SPAM'));
                            syndicate_spammer_report($trackback_ip_address, '', '', do_lang('TRACKBACK_SPAM'), false);
                        }
                        // no break
                    case '1':
                        $GLOBALS['SITE_DB']->query_delete('trackbacks', ['id' => $id], '', 1);
                        break;
                    // (zero is do nothing)
                }
            }
        }

        log_it('DELETE_TRACKBACKS');

        // Show it worked / Refresh
        $text = do_lang_tempcode('SUCCESS');
        $url = get_param_string('redirect', '', INPUT_FILTER_URL_INTERNAL);
        if ($url == '') {
            $_url = build_url(['page' => '_SELF', 'type' => 'browse'], '_SELF');
            $url = $_url->evaluate();
        }
        return redirect_screen($this->title, $url, $text);
    }
}
