<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/*EXTRA FUNCTIONS: get_included_files*/

/**
 * API to do content translation programatically.
 * This function is intended for programmers, writing upgrade scripts for a custom site (dev>staging>live).
 *
 * @param  LANGUAGE_NAME $lang_from From language
 * @param  LANGUAGE_NAME $lang_to To language
 * @param  array $content_lang_string_changes Mapping of strings to change (from => to); if 'to' is null it should be the same as 'from'
 * @param  boolean $test_run Whether this is only a test run, don't change anything
 * @param  boolean $allow_multiple_matches Allow multiple matches of source strings (false if we'll require exactly one match)
 * @return array A List of error message strings, problems that happened
 */
function content_lang_string_translation(string $lang_from, string $lang_to, array $content_lang_string_changes, bool $test_run = false, bool $allow_multiple_matches = true) : array
{
    if (!multi_lang_content()) {
        warn_exit('multi_lang_content must be enabled');
    }

    // Checks...

    $errors = [];

    foreach (array_keys($content_lang_string_changes) as $from) {
        $rows = $GLOBALS['SITE_DB']->query_select('translate', ['id'], ['language' => $lang_from, 'text_original' => $from]);

        if (empty($rows)) {
            $errors[] = 'No strings for \'' . $from . '\'';
            unset($content_lang_string_changes[$from]);
        }

        if ((!$allow_multiple_matches) && (count($rows) > 1)) {
            $errors[] = 'Multiple strings for \'' . $from . '\'';
            unset($content_lang_string_changes[$from]);
        }
    }

    if ($test_run) {
        return $errors;
    }

    // Make changes...

    foreach ($content_lang_string_changes as $from => $to) {
        if ($to === null) {
            $to = $from;
        }

        $rows = $GLOBALS['SITE_DB']->query_select('translate', ['id', 'importance_level', 'source_user'], ['language' => $lang_from, 'text_original' => $from]);
        foreach ($rows as $row) {
            // Delete any existing translation
            $GLOBALS['SITE_DB']->query_delete('translate', ['language' => $lang_to, 'id' => $row['id']]);

            // Insert new translation
            $GLOBALS['SITE_DB']->query_insert('translate', [
                'id' => $row['id'],
                'language' => $lang_to,
                'importance_level' => $row['importance_level'],
                'text_original' => $to,
                'text_parsed' => '',
                'broken' => 0,
                'source_user' => $row['source_user'],
            ]);
        }
    }

    return $errors;
}

/**
 * API to do language string translation programatically.
 * This function is intended for programmers, writing upgrade scripts for a custom site (dev>staging>live).
 *
 * @param  LANGUAGE_NAME $lang_from From language
 * @param  LANGUAGE_NAME $lang_to To language
 * @param  array $lang_string_changes A mapping of language file to a mapping of strings to change (from => to); if 'to' is null it should be the same as 'from'
 * @param  boolean $test_run Whether this is only a test run, don't change anything
 * @param  boolean $allow_multiple_matches Allow multiple matches of source strings (false if we'll require exactly one match)
 * @return array A List of error message strings, problems that happened
 */
function lang_string_translation(string $lang_from, string $lang_to, array $lang_string_changes, bool $test_run = false, bool $allow_multiple_matches = false) : array
{
    require_code('files2');

    // Checks...

    $errors = [];

    require_code('lang_compile');

    foreach ($lang_string_changes as $lang_file => $_lang_string_changes) {
        $lang_file_map_from = array_merge(get_lang_file_map($lang_from, $lang_file, true, true), get_lang_file_map($lang_from, $lang_file, false, true));
        $_lang_file_map_from = array_count_values($lang_file_map_from);

        foreach (array_keys($_lang_string_changes) as $from) {
            if (!array_key_exists($from, $_lang_file_map_from)) {
                $errors[] = 'No strings for \'' . $from . '\'';
                unset($_lang_string_changes[$from]);
            }

            if ((!$allow_multiple_matches) && ($_lang_file_map_from[$from] > 1)) {
                $errors[] = 'Multiple strings for \'' . $from . '\'';
                unset($_lang_string_changes[$from]);
            }
        }
    }

    if ($test_run) {
        return $errors;
    }

    // Make changes...

    $lang_to_dir = get_custom_file_base() . '/lang_custom/' . $lang_to;
    if (!file_exists($lang_to_dir)) {
        require_code('files2');
        make_missing_directory($lang_to_dir);
    }

    foreach ($lang_string_changes as $lang_file => $_lang_string_changes) {
        $lang_file_map_from = array_merge(get_lang_file_map($lang_from, $lang_file, true, true), get_lang_file_map($lang_from, $lang_file, false, true));
        $lang_file_map_to = array_merge(get_lang_file_map($lang_to, $lang_file, true, true), get_lang_file_map($lang_to, $lang_file, false, true));

        foreach ($_lang_string_changes as $from => $to) {
            if ($to === null) {
                $to = $from;
            }

            foreach ($lang_file_map_from as $key => $val) {
                if ($val == $from) {
                    $lang_file_map_to[$key] = $to;
                }
            }
        }

        $out = '[strings]' . "\n";
        foreach ($lang_file_map_to as $key => $val) {
            $out .= $key . '=' . $val . "\n";
        }
        cms_file_put_contents_safe($lang_to_dir . '/' . $lang_file . '.ini', $out, FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE | FILE_WRITE_BOM);
    }

    return $errors;
}

/**
 * Edit a language string direct from something saved into the code.
 *
 * @param  ID_TEXT $codename The language string codename
 * @param  ?LANGUAGE_NAME $lang The language to use (null: user's language)
 */
function inline_language_editing(string &$codename, ?string $lang)
{
    global $LANGS_REQUESTED, $LANGUAGE_STRINGS_CACHE;

    $pos = strpos($codename, ':');
    if ($pos !== false) {
        $lang_file = substr($codename, 0, $pos);
        $codename = substr($codename, $pos + 1);
    } else {
        // Find loaded file with smallest levenshtein distance to current page
        $best = null;
        $best_for = 'global';
        foreach (array_keys($LANGS_REQUESTED) as $possible) {
            $dist = levenshtein(get_page_name(), $possible);
            if (($best === null) || ($best > $dist)) {
                $best = $dist;
                $best_for = $possible;
            }
        }

        $lang_file = $best_for;
    }

    // Where to save to (preference to saving to original files)
    $save_path = get_file_base() . '/lang/' . fallback_lang() . '/' . $lang_file . '.ini';
    if (!is_file($save_path)) {
        $save_path = get_file_base() . '/lang_custom/' . fallback_lang() . '/' . $lang_file . '.ini';
    }
    if (!is_file($save_path)) {
        $save_path = get_custom_file_base() . '/lang_custom/' . fallback_lang() . '/' . $lang_file . '.ini';
    }

    if (!file_exists(dirname($save_path))) {
        require_code('files2');
        make_missing_directory(dirname($save_path));
    }

    require_code('files');

    // Tack language strings onto this file
    list($codename, $value) = explode('=', $codename, 2);
    if (!is_file($save_path)) {
        $myfile = cms_fopen_text_write($save_path, true, 'ab');
        fwrite($myfile, "[strings]\n");

        $has_terminating_line = true;
        $write_needed = true;
    } else {
        $c = cms_file_get_contents_safe($save_path, FILE_READ_LOCK | FILE_READ_UNIXIFIED_TEXT | FILE_READ_BOM);
        $has_terminating_line = (substr($c, -1) == "\n");
        $write_needed = (strpos($c, "\n" . $codename . '=' . $value . "\n") === false);

        $myfile = cms_fopen_text_write($save_path, true, 'ab');
    }
    if ($write_needed) {
        if (!$has_terminating_line) {
            fwrite($myfile, "\n");
        }
        fwrite($myfile, $codename . '=' . $value . "\n");
    }
    flock($myfile, LOCK_UN);
    fclose($myfile);
    sync_file($save_path);
    fix_permissions($save_path);

    // Fake-load the string
    $LANGUAGE_STRINGS_CACHE[$lang][$codename] = $value;

    // Go through all required files, doing a string replace if needed
    $included_files = get_included_files();
    foreach ($included_files as $inc) {
        $orig_contents = cms_file_get_contents_safe($inc, FILE_READ_LOCK | FILE_READ_UNIXIFIED_TEXT | FILE_READ_BOM);
        $contents = str_replace("'" . $codename . '=' . $value . "'", "'" . $codename . "'", $orig_contents);
        if ($orig_contents != $contents) {
            require_code('files');
            cms_file_put_contents_safe($inc, $contents, FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE | FILE_WRITE_BOM);
        }
    }
}

/**
 * Get a list of languages files for the given language. ONLY those that are overridden.
 *
 * @param  ?LANGUAGE_NAME $lang The language (null: uses the current language)
 * @return array The language files (a map between language file name -and- lang or lang_custom)
 */
function get_lang_files(?string $lang = null) : array
{
    require_code('files');

    if ($lang === null) {
        $lang = get_site_default_lang();
    }

    $_dir = @opendir(get_file_base() . '/lang/' . $lang);
    $_lang_files = [];
    if ($_dir !== false) {
        while (false !== ($file = readdir($_dir))) {
            if (($file[0] != '.') && (substr($file, -4) == '.ini')/* && (!should_ignore_file(get_file_base().'/lang/'.$lang.'/'.$file,0,0))*/) {
                $file = substr($file, 0, strlen($file) - 4);
                $_lang_files[$file] = 'lang';
            }
        }
        closedir($_dir);
    }
    $_dir = @opendir(get_custom_file_base() . '/lang_custom/' . $lang);
    if ($_dir !== false) {
        while (false !== ($file = readdir($_dir))) {
            if (($file[0] != '.') && (substr($file, -4) == '.ini')/* && (!should_ignore_file(get_custom_file_base().'/lang_custom/'.$lang.'/'.$file,0,0))*/) {
                $file = substr($file, 0, strlen($file) - 4);
                $_lang_files[$file] = 'lang_custom';
            }
        }
        closedir($_dir);
    }
    if (get_file_base() != get_custom_file_base()) {
        $_dir = @opendir(get_file_base() . '/lang_custom/' . $lang);
        if ($_dir !== false) {
            while (false !== ($file = readdir($_dir))) {
                if (($file != '.') && ($file != '..') && (substr($file, -4) == '.ini') && (!should_ignore_file(get_file_base() . '/lang_custom/' . $lang . '/' . $file))) {
                    $file = substr($file, 0, strlen($file) - 4);
                    $_lang_files[$file] = 'lang_custom';
                }
            }
            closedir($_dir);
        }
    }

    return $_lang_files;
}

/**
 * Search the database to find human-readable names for content language string IDs.
 *
 * @param  array $lang_ids The content language string IDs (array of AUTO_LINK)
 * @return array Human readable names (List of string against same IDs in input array or null for orphan strings)
 */
function find_lang_content_names(array $lang_ids) : array
{
    require_code('content');

    static $langidfields = null;
    if ($langidfields === null) {
        $query = 'SELECT m_name,m_table,m_type FROM ' . $GLOBALS['SITE_DB']->get_table_prefix() . 'db_meta';
        $all_fields = $GLOBALS['SITE_DB']->query($query);

        $langidfields = [];
        foreach ($all_fields as $f) {
            if (strpos($f['m_type'], '_TRANS') !== false) {
                $langidfields[] = ['m_name' => $f['m_name'], 'm_table' => $f['m_table'], 'key' => null];
            }
        }
        foreach ($langidfields as $i => $l) {
            foreach ($all_fields as $f) {
                if (($l['m_table'] == $f['m_table']) && (substr($f['m_type'], 0, 1) == '*') && ($l['key'] === null)) {
                    $langidfields[$i]['key'] = $f['m_name'];
                }
            }
        }
    }

    $ret = [];

    foreach ($langidfields as $field) {
        $db = get_db_for($field['m_table']);
        if ($db === null) {
            continue; // None forum driver
        }
        $or_list = '';
        foreach ($lang_ids as $lang_id) {
            if (!isset($ret[$lang_id])) { // Try and lookup anything not found yet
                if ($or_list != '') {
                    $or_list .= ' OR ';
                }
                $or_list .= ($field['m_table'] == 'config') ? db_string_equal_to($field['m_name'], strval($lang_id)) : ($field['m_name'] . '=' . strval($lang_id));
            }
        }
        if ($or_list != '') {
            $info = null;
            $cma_hook = convert_cms_type_codes('table', $field['m_table'], 'content_type');
            if ($cma_hook !== null) {
                $ob = get_content_object($cma_hook);
                if ($ob !== null) {
                    $info = $ob->info();
                }
            }

            $fields_to_select = [$field['m_name']];
            if ($field['key'] !== null) {
                $fields_to_select[] = $field['key'];
            }
            if ($info !== null) {
                append_content_select_for_fields($fields_to_select, $info, ['id', 'title']);
            }
            $test = list_to_map($field['m_name'], $db->query('SELECT ' . implode(',', $fields_to_select) . ' FROM ' . $db->get_table_prefix() . $field['m_table'] . ' WHERE ' . $or_list));
            foreach ($lang_ids as $lang_id) {
                if (array_key_exists($lang_id, $test)) {
                    if ($info !== null) {
                        $ret[$lang_id] = $field['m_table'] . ' \ ' . $ob->get_title($test[$lang_id]) . ' \ ' . $field['m_name'];
                    } else {
                        $ret[$lang_id] = $field['m_table'] . ' \ ' . (is_integer($test[$lang_id][$field['key']]) ? strval($test[$lang_id][$field['key']]) : $test[$lang_id][$field['key']]) . ' \ ' . $field['m_name'];
                    }
                } else {
                    if (!array_key_exists($lang_id, $ret)) {
                        $ret[$lang_id] = null;
                    }
                }
            }
        }
    }

    return $ret;
}

/**
 * Get a nice formatted HTML listed language file selector for the given language.
 *
 * @param  ?LANGUAGE_NAME $lang The language (null: default to the current language)
 * @param  ?string $default_lang_file The language file to select by default (null: none)
 * @return Tempcode The language file selector
 */
function create_selection_list_lang_files(?string $lang = null, ?string $default_lang_file = null) : object
{
    $_lang_files = get_lang_files(($lang === null) ? get_site_default_lang() : $lang);

    ksort($_lang_files);

    require_lang('lang');
    require_code('lang_compile');

    $lang_files = new Tempcode();
    foreach (array_keys($_lang_files) as $lang_file) {
        if ($lang !== null) {
            $base_map = get_lang_file_map(fallback_lang(), $lang_file, true);
            $criticise_map = get_lang_file_map($lang, $lang_file);

            $num_translated = 0;
            $num_english = count($base_map);

            foreach ($base_map as $key => $val) {
                if (array_key_exists($key, $criticise_map)) {
                    $num_translated++;
                }
            }

            $lang_files->attach(form_input_list_entry($lang_file, $lang_file === $default_lang_file, do_lang_tempcode('TRANSLATION_PROGRESS', escape_html($lang_file), escape_html(integer_format($num_translated)), escape_html(integer_format($num_english)))));
        } else {
            $lang_files->attach(form_input_list_entry($lang_file, $lang_file === $default_lang_file, $lang_file));
        }
    }

    return $lang_files;
}

/**
 * Get the full name of a language. e.g. 'EN' would become 'English'.
 *
 * @param  LANGUAGE_NAME $code The language
 * @return string The full name of the language
 */
function lookup_language_full_name(string $code) : string
{
    if ($code == 'EN') {
        return 'English'; // Optimisation
    }

    $langs_map = get_langs_map();
    return isset($langs_map[$code]) ? $langs_map[$code] : $code;
}

/**
 * Get the map between language codenames and language full names.
 *
 * @return array The map
 */
function get_langs_map() : array
{
    static $langs_map = null;

    if ($langs_map === null) {
        $langs_map = persistent_cache_get('LANGS_MAP_CACHE');
    }
    if ($langs_map === null) {
        require_code('files');
        $map_file_a = get_file_base() . '/lang/langs.ini';
        $map_file_b = get_custom_file_base() . '/lang_custom/langs.ini';
        if (!is_file($map_file_b)) {
            $map_file_b = $map_file_a;
        }
        $langs_map = cms_parse_ini_file_fast($map_file_b);

        persistent_cache_set('LANGS_MAP_CACHE', $langs_map);
    }

    return $langs_map;
}
