<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Hook class.
 */
class Hook_cns_auth_converge
{
    /**
     * Try and authenticate for our password compatibility scheme.
     *
     * @param  SHORT_TEXT $username The member username
     * @param  MEMBER $member_id The member ID
     * @param  string $password_raw The raw password
     * @param  array $row Row of Conversr account
     * @return ?Tempcode Error message (null: none)
     */
    public function auth(string $username, int $member_id, string $password_raw, array $row) : ?object
    {
        if (!hash_equals(md5(md5($row['m_pass_salt']) . $password_raw), $row['m_pass_hash_salted'])) {
            return do_lang_tempcode((get_option('login_error_secrecy') == '1') ? 'MEMBER_INVALID_LOGIN' : 'MEMBER_BAD_PASSWORD');
        }

        return null;
    }
}
