<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    search
 */

/**
 * Hook class.
 */
class Hook_cleanup_fast_custom_index
{
    /**
     * Find details about this cleanup hook.
     *
     * @return ?array Map of cleanup hook info (null: hook is disabled)
     */
    public function info() : ?array
    {
        if (!addon_installed('search')) {
            return null;
        }

        require_lang('search');

        $info = [];
        $info['title'] = do_lang_tempcode('FAST_CUSTOM_INDEX');
        $info['description'] = do_lang_tempcode('DESCRIPTION_CLEAR_FAST_CUSTOM_INDEX');
        $info['type'] = 'cache';

        return $info;
    }

    /**
     * Run the cleanup hook action.
     *
     * @return Tempcode Results
     */
    public function run() : object
    {
        require_code('database_search');

        $hooks = find_all_hook_obs('modules', 'search', 'Hook_search_');
        foreach ($hooks as $hook => $ob) {
            if (method_exists($ob, 'empty_index')) {
                $ob->empty_index();
            }
        }

        return new Tempcode();
    }
}
