<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    stats_block
 */

/**
 * Hook class.
 */
class Hook_addon_registry_stats_block
{
    /**
     * Get a list of file permissions to set.
     *
     * @param  boolean $runtime Whether to include wildcards represented runtime-created chmoddable files
     * @return array File permissions to set
     */
    public function get_chmod_array(bool $runtime = false) : array
    {
        return [];
    }

    /**
     * Get the current version of this addon (usually software major, software minor, addon build).
     * Put the comment "// addon_version_auto_update" to the right of the return if you want release tools to automatically update this according to software version and find_addon_effective_mtime.
     *
     * @return SHORT_TEXT Version number
     */
    public function get_version() : string
    {
        return '11'; // addon_version_auto_update 57e523847d702b47f8e5ae4f47825e89
    }

    /**
     * Get the minimum required version of the website software needed to use this addon.
     *
     * @return float Minimum required website software version
     */
    public function get_min_cms_version() : float
    {
        return 11.0;
    }

    /**
     * Get the maximum compatible version of the website software to use this addon.
     *
     * @return ?float Maximum compatible website software version (null: no maximum version currently)
     */
    public function get_max_cms_version() : ?float
    {
        return 11.9;
    }

    /**
     * Get the addon category.
     *
     * @return string The category
     */
    public function get_category() : string
    {
        return 'Information Display';
    }

    /**
     * Get the description of the addon.
     *
     * @return string Description of the addon
     */
    public function get_description() : string
    {
        return 'A block to show a selection of your website statistics to your visitors.';
    }

    /**
     * Get a list of tutorials that apply to this addon.
     *
     * @return array List of tutorials
     */
    public function get_applicable_tutorials() : array
    {
        return [
            'tut_statistics',
        ];
    }

    /**
     * Get a mapping of dependency types.
     *
     * @return array A structure specifying dependency information
     */
    public function get_dependencies() : array
    {
        return [
            'requires' => [],
            'recommends' => [],
            'conflicts_with' => [],
        ];
    }

    /**
     * Explicitly say which icon should be used.
     *
     * @return URLPATH Icon
     */
    public function get_default_icon() : string
    {
        return 'themes/default/images/icons/admin/component.svg';
    }

    /**
     * Get a list of files that belong to this addon.
     *
     * @return array List of files
     */
    public function get_file_list() : array
    {
        return [
            'sources/blocks/side_stats.php',
            'sources/hooks/blocks/side_stats/.htaccess',
            'sources/hooks/blocks/side_stats/forum.php',
            'sources/hooks/blocks/side_stats/index.html',
            'sources/hooks/modules/admin_setupwizard/stats_block.php',
            'sources/hooks/systems/addon_registry/stats_block.php',
            'sources/hooks/systems/config/activity_show_stats_count_page_views_this_month.php',
            'sources/hooks/systems/config/activity_show_stats_count_page_views_this_week.php',
            'sources/hooks/systems/config/activity_show_stats_count_page_views_today.php',
            'sources/hooks/systems/config/activity_show_stats_count_users_online.php',
            'sources/hooks/systems/config/activity_show_stats_count_users_online_forum.php',
            'sources/hooks/systems/config/activity_show_stats_count_users_online_record.php',
            'sources/hooks/systems/config/forum_show_stats_count_members.php',
            'sources/hooks/systems/config/forum_show_stats_count_members_active_this_month.php',
            'sources/hooks/systems/config/forum_show_stats_count_members_active_this_week.php',
            'sources/hooks/systems/config/forum_show_stats_count_members_active_today.php',
            'sources/hooks/systems/config/forum_show_stats_count_members_new_this_month.php',
            'sources/hooks/systems/config/forum_show_stats_count_members_new_this_week.php',
            'sources/hooks/systems/config/forum_show_stats_count_members_new_today.php',
            'sources/hooks/systems/config/forum_show_stats_count_posts.php',
            'sources/hooks/systems/config/forum_show_stats_count_posts_today.php',
            'sources/hooks/systems/config/forum_show_stats_count_topics.php',
            'sources_custom/hooks/blocks/side_stats/.htaccess',
            'sources_custom/hooks/blocks/side_stats/index.html',
            'themes/default/templates/BLOCK_SIDE_STATS.tpl',
            'themes/default/templates/BLOCK_SIDE_STATS_SECTION.tpl',
            'themes/default/templates/BLOCK_SIDE_STATS_SUBLINE.tpl',
        ];
    }

    /**
     * Get mapping between template names and the method of this class that can render a preview of them.
     *
     * @return array The mapping
     */
    public function tpl_previews() : array
    {
        return [
            'templates/BLOCK_SIDE_STATS_SUBLINE.tpl' => 'block_side_stats',
            'templates/BLOCK_SIDE_STATS_SECTION.tpl' => 'block_side_stats',
            'templates/BLOCK_SIDE_STATS.tpl' => 'block_side_stats',
        ];
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__block_side_stats() : object
    {
        $full_tpl = new Tempcode();
        $bits = new Tempcode();
        foreach (placeholder_array() as $v) {
            $bits->attach(do_lorem_template('BLOCK_SIDE_STATS_SUBLINE', [
                'KEY' => lorem_phrase(),
                'RAW_VALUE' => placeholder_number(),
                'VALUE' => placeholder_number(),
            ]));
        }
        $full_tpl->attach(do_lorem_template('BLOCK_SIDE_STATS_SECTION', [
            'SECTION' => lorem_phrase(),
            'CONTENT' => $bits,
        ]));

        return lorem_globalise(do_lorem_template('BLOCK_SIDE_STATS', [
            'BLOCK_ID' => lorem_word(),
            'CONTENT' => $full_tpl,
        ]), null, '', true);
    }
}
