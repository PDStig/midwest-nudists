<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    health_check
 */

/**
 * Hook class.
 */
class Hook_addon_registry_health_check
{
    /**
     * Get a list of file permissions to set.
     *
     * @param  boolean $runtime Whether to include wildcards represented runtime-created chmoddable files
     * @return array File permissions to set
     */
    public function get_chmod_array(bool $runtime = false) : array
    {
        return [];
    }

    /**
     * Get the current version of this addon (usually software major, software minor, addon build).
     * Put the comment "// addon_version_auto_update" to the right of the return if you want release tools to automatically update this according to software version and find_addon_effective_mtime.
     *
     * @return SHORT_TEXT Version number
     */
    public function get_version() : string
    {
        return '11.0.1'; // addon_version_auto_update 729a50ed229c0db5da41568c9650b9f7
    }

    /**
     * Get the minimum required version of the website software needed to use this addon.
     *
     * @return float Minimum required website software version
     */
    public function get_min_cms_version() : float
    {
        return 11.0;
    }

    /**
     * Get the maximum compatible version of the website software to use this addon.
     *
     * @return ?float Maximum compatible website software version (null: no maximum version currently)
     */
    public function get_max_cms_version() : ?float
    {
        return 11.9;
    }

    /**
     * Get the addon category.
     *
     * @return string The category
     */
    public function get_category() : string
    {
        return 'Admin Utilities';
    }

    /**
     * Get the addon author.
     *
     * @return string The author
     */
    public function get_author() : string
    {
        return 'Chris Graham';
    }

    /**
     * Find other authors.
     *
     * @return array A list of co-authors that should be attributed
     */
    public function get_copyright_attribution() : array
    {
        return [];
    }

    /**
     * Get the addon licence (one-line summary only).
     *
     * @return string The licence
     */
    public function get_licence() : string
    {
        return 'Licensed on the same terms as ' . brand_name();
    }

    /**
     * Get the description of the addon.
     *
     * @return string Description of the addon
     */
    public function get_description() : string
    {
        return 'The Health Check addon automatically finds problems on your website and server.';
    }

    /**
     * Get a list of tutorials that apply to this addon.
     *
     * @return array List of tutorials
     */
    public function get_applicable_tutorials() : array
    {
        return [
            'tut_website_health',
        ];
    }

    /**
     * Get a mapping of dependency types.
     *
     * @return array A structure specifying dependency information
     */
    public function get_dependencies() : array
    {
        return [
            'requires' => [],
            'recommends' => [],
            'conflicts_with' => []
        ];
    }

    /**
     * Explicitly say which icon should be used.
     *
     * @return URLPATH Icon
     */
    public function get_default_icon() : string
    {
        return 'themes/default/images/icons/menu/adminzone/tools/health_check.svg';
    }

    /**
     * Get a list of files that belong to this addon.
     *
     * @return array List of files
     */
    public function get_file_list() : array
    {
        return [
            'adminzone/pages/modules/admin_health_check.php',
            'data/health_check.php',
            'data/xml_config/page_errors.xml',
            'lang/EN/health_check.ini',
            'sources/health_check.php',
            'sources/hooks/blocks/main_staff_checklist/health_check.php',
            'sources/hooks/systems/actionlog/health_check.php',
            'sources/hooks/systems/addon_registry/health_check.php',
            'sources/hooks/systems/commandr_commands/health_check.php',
            'sources/hooks/systems/commandr_commands/health_check_pages.php',
            'sources/hooks/systems/config/days_to_keep__health_check_log.php',
            'sources/hooks/systems/config/hc_admin_stale_threshold.php',
            'sources/hooks/systems/config/hc_compound_requests_per_second_threshold.php',
            'sources/hooks/systems/config/hc_compound_requests_window_size.php',
            'sources/hooks/systems/config/hc_cpu_pct_threshold.php',
            'sources/hooks/systems/config/hc_cpu_score_threshold.php',
            'sources/hooks/systems/config/hc_cron_notify_regardless.php',
            'sources/hooks/systems/config/hc_cron_regularity.php',
            'sources/hooks/systems/config/hc_cron_sections_to_run.php',
            'sources/hooks/systems/config/hc_cron_threshold.php',
            'sources/hooks/systems/config/hc_database_threshold.php',
            'sources/hooks/systems/config/hc_disk_space_threshold.php',
            'sources/hooks/systems/config/hc_error_log_day_flood_threshold.php',
            'sources/hooks/systems/config/hc_google_safe_browsing_api_enabled.php',
            'sources/hooks/systems/config/hc_io_mbs.php',
            'sources/hooks/systems/config/hc_io_pct_threshold.php',
            'sources/hooks/systems/config/hc_is_test_site.php',
            'sources/hooks/systems/config/hc_mail_wait_time.php',
            'sources/hooks/systems/config/hc_page_size_threshold.php',
            'sources/hooks/systems/config/hc_page_speed_threshold.php',
            'sources/hooks/systems/config/hc_process_hang_threshold.php',
            'sources/hooks/systems/config/hc_processes_to_monitor.php',
            'sources/hooks/systems/config/hc_ram_threshold.php',
            'sources/hooks/systems/config/hc_requests_per_second_threshold.php',
            'sources/hooks/systems/config/hc_requests_window_size.php',
            'sources/hooks/systems/config/hc_scan_page_links.php',
            'sources/hooks/systems/config/hc_transfer_latency_threshold.php',
            'sources/hooks/systems/config/hc_transfer_speed_threshold.php',
            'sources/hooks/systems/config/hc_uptime_threshold.php',
            'sources/hooks/systems/config/hc_version_check.php',
            'sources/hooks/systems/config/hc_webstandards_safelist.php',
            'sources/hooks/systems/config_categories/health_check.php',
            'sources/hooks/systems/cron/health_check.php',
            'sources/hooks/systems/health_checks/.htaccess',
            'sources/hooks/systems/health_checks/apis.php',
            'sources/hooks/systems/health_checks/cron.php',
            'sources/hooks/systems/health_checks/domains.php',
            'sources/hooks/systems/health_checks/email.php',
            'sources/hooks/systems/health_checks/email_newsletter.php',
            'sources/hooks/systems/health_checks/index.html',
            'sources/hooks/systems/health_checks/install_env.php',
            'sources/hooks/systems/health_checks/install_env_php_ext.php',
            'sources/hooks/systems/health_checks/install_env_php_lock_down.php',
            'sources/hooks/systems/health_checks/integrity.php',
            'sources/hooks/systems/health_checks/marketing.php',
            'sources/hooks/systems/health_checks/marketing_seo.php',
            'sources/hooks/systems/health_checks/marketing_seo_robotstxt.php',
            'sources/hooks/systems/health_checks/mistakes_build.php',
            'sources/hooks/systems/health_checks/mistakes_deploy.php',
            'sources/hooks/systems/health_checks/mistakes_user_ux.php',
            'sources/hooks/systems/health_checks/network.php',
            'sources/hooks/systems/health_checks/performance.php',
            'sources/hooks/systems/health_checks/performance_bloat.php',
            'sources/hooks/systems/health_checks/performance_server.php',
            'sources/hooks/systems/health_checks/security.php',
            'sources/hooks/systems/health_checks/security_hackattack.php',
            'sources/hooks/systems/health_checks/security_ssl.php',
            'sources/hooks/systems/health_checks/stability.php',
            'sources/hooks/systems/health_checks/upkeep.php',
            'sources/hooks/systems/health_checks/upkeep_backups.php',
            'sources/hooks/systems/logs/health_check.php',
            'sources/hooks/systems/page_groupings/health_check.php',
            'sources_custom/hooks/systems/health_checks/.htaccess',
            'sources_custom/hooks/systems/health_checks/index.html',
            'themes/default/images/icons/menu/adminzone/tools/health_check.svg',
            'themes/default/images/icons_monochrome/menu/adminzone/tools/health_check.svg',
            'themes/default/templates/HEALTH_CHECK_RESULTS.tpl',
            'themes/default/templates/HEALTH_CHECK_SCREEN.tpl',
        ];
    }

    /**
     * Get mapping between template names and the method of this class that can render a preview of them.
     *
     * @return array The mapping
     */
    public function tpl_previews() : array
    {
        return [
            'templates/HEALTH_CHECK_RESULTS.tpl' => 'health_check_screen',
            'templates/HEALTH_CHECK_SCREEN.tpl' => 'health_check_screen',
        ];
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__health_check_screen() : object
    {
        $categories = [
            lorem_phrase() => [
                'SECTIONS' => [
                    lorem_phrase() . ' 1' => [
                        'RESULTS' => [
                            [
                                'RESULT' => 'PASS',
                                'MESSAGE' => lorem_sentence_html(),
                            ],
                            [
                                'RESULT' => 'MANUAL',
                                'MESSAGE' => lorem_sentence_html(),
                            ],
                        ],
                        'NUM_FAILS' => placeholder_number(),
                        'NUM_PASSES' => placeholder_number(),
                        'NUM_SKIPPED' => placeholder_number(),
                        'NUM_MANUAL' => placeholder_number(),
                        '_NUM_FAILS' => '1',
                        '_NUM_PASSES' => '1',
                        '_NUM_SKIPPED' => '1',
                        '_NUM_MANUAL' => '1',
                    ],
                    lorem_phrase() . ' 2' => [
                        'RESULTS' => [
                            [
                                'RESULT' => 'FAIL',
                                'MESSAGE' => lorem_sentence_html(),
                            ],
                        ],
                        'NUM_FAILS' => placeholder_number(),
                        'NUM_PASSES' => placeholder_number(),
                        'NUM_SKIPPED' => placeholder_number(),
                        'NUM_MANUAL' => placeholder_number(),
                        '_NUM_FAILS' => '1',
                        '_NUM_PASSES' => '1',
                        '_NUM_SKIPPED' => '1',
                        '_NUM_MANUAL' => '1',
                    ],
                    lorem_phrase() . ' 3' => [
                        'RESULTS' => [
                            [
                                'RESULT' => 'SKIP',
                                'MESSAGE' => lorem_sentence_html(),
                            ],
                        ],
                        'NUM_FAILS' => placeholder_number(),
                        'NUM_PASSES' => placeholder_number(),
                        'NUM_SKIPPED' => placeholder_number(),
                        'NUM_MANUAL' => placeholder_number(),
                        '_NUM_FAILS' => '1',
                        '_NUM_PASSES' => '1',
                        '_NUM_SKIPPED' => '1',
                        '_NUM_MANUAL' => '1',
                    ],
                ],
            ],
        ];
        $results = do_lorem_template('HEALTH_CHECK_RESULTS', ['CATEGORIES' => $categories]);

        return lorem_globalise(do_lorem_template('HEALTH_CHECK_SCREEN', [
            'TITLE' => lorem_screen_title(),
            'SECTIONS' => placeholder_options(),
            'SHOW_PASSES' => true,
            'SHOW_SKIPS' => true,
            'SHOW_MANUAL_CHECKS' => true,
            'RESULTS' => $results,
        ]), null, '', true);
    }
}
