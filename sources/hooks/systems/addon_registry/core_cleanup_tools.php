<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cleanup_tools
 */

/**
 * Hook class.
 */
class Hook_addon_registry_core_cleanup_tools
{
    /**
     * Get a list of file permissions to set.
     *
     * @param  boolean $runtime Whether to include wildcards represented runtime-created chmoddable files
     * @return array File permissions to set
     */
    public function get_chmod_array(bool $runtime = false) : array
    {
        return [];
    }

    /**
     * Get the current version of this addon (usually software major, software minor, addon build).
     * Put the comment "// addon_version_auto_update" to the right of the return if you want release tools to automatically update this according to software version and find_addon_effective_mtime.
     *
     * @return SHORT_TEXT Version number
     */
    public function get_version() : string
    {
        return '11.0.1'; // addon_version_auto_update b45731bf8bcdcce88a34fcd807d21865
    }

    /**
     * Get the minimum required version of the website software needed to use this addon.
     *
     * @return float Minimum required website software version
     */
    public function get_min_cms_version() : float
    {
        return 11.0;
    }

    /**
     * Get the maximum compatible version of the website software to use this addon.
     *
     * @return ?float Maximum compatible website software version (null: no maximum version currently)
     */
    public function get_max_cms_version() : ?float
    {
        return 11.9;
    }

    /**
     * Get the addon category.
     *
     * @return string The category
     */
    public function get_category() : string
    {
        return 'Admin Utilities';
    }

    /**
     * Get the description of the addon.
     *
     * @return string Description of the addon
     */
    public function get_description() : string
    {
        return 'Behind-the-scenes maintenance tasks.';
    }

    /**
     * Get a list of tutorials that apply to this addon.
     *
     * @return array List of tutorials
     */
    public function get_applicable_tutorials() : array
    {
        return [
            'tut_website_health',
        ];
    }

    /**
     * Get a mapping of dependency types.
     *
     * @return array A structure specifying dependency information
     */
    public function get_dependencies() : array
    {
        return [
            'requires' => [],
            'recommends' => [],
            'conflicts_with' => [],
        ];
    }

    /**
     * Explicitly say which icon should be used.
     *
     * @return URLPATH Icon
     */
    public function get_default_icon() : string
    {
        return 'themes/default/images/icons/menu/adminzone/tools/cleanup.svg';
    }

    /**
     * Get a list of files that belong to this addon.
     *
     * @return array List of files
     */
    public function get_file_list() : array
    {
        return [
            'adminzone/pages/modules/admin_broken_urls.php',
            'adminzone/pages/modules/admin_cleanup.php',
            'data/modules/admin_cleanup/.htaccess',
            'data/modules/admin_cleanup/index.html',
            'lang/EN/cleanup.ini',
            'sources/broken_urls.php',
            'sources/hooks/systems/addon_registry/core_cleanup_tools.php',
            'sources/hooks/systems/cleanup/.htaccess',
            'sources/hooks/systems/cleanup/blocks.php',
            'sources/hooks/systems/cleanup/comcode.php',
            'sources/hooks/systems/cleanup/compiled_scripts.php',
            'sources/hooks/systems/cleanup/disposable_values.php',
            'sources/hooks/systems/cleanup/email_bounces.php',
            'sources/hooks/systems/cleanup/http.php',
            'sources/hooks/systems/cleanup/index.html',
            'sources/hooks/systems/cleanup/language.php',
            'sources/hooks/systems/cleanup/lost_disk_content.php',
            'sources/hooks/systems/cleanup/meta_tree_rebuild.php',
            'sources/hooks/systems/cleanup/orphaned_content_lang_strings.php',
            'sources/hooks/systems/cleanup/orphaned_tags.php',
            'sources/hooks/systems/cleanup/orphaned_uploads.php',
            'sources/hooks/systems/cleanup/page_backups.php',
            'sources/hooks/systems/cleanup/reorganise_uploads.php',
            'sources/hooks/systems/cleanup/self_learning.php',
            'sources/hooks/systems/cleanup/sitemap.php',
            'sources/hooks/systems/cleanup/static.php',
            'sources/hooks/systems/cleanup/temp.php',
            'sources/hooks/systems/cleanup/template_usage.php',
            'sources/hooks/systems/cleanup/templates.php',
            'sources/hooks/systems/cleanup/theme_images.php',
            'sources/hooks/systems/cleanup/trusted_sites.php',
            'sources/hooks/systems/cleanup/url_metadata_cache.php',
            'sources/hooks/systems/config/is_on_block_cache.php',
            'sources/hooks/systems/config/is_on_comcode_page_cache.php',
            'sources/hooks/systems/config/is_on_lang_cache.php',
            'sources/hooks/systems/config/is_on_template_cache.php',
            'sources/hooks/systems/reorganise_uploads/.htaccess',
            'sources/hooks/systems/reorganise_uploads/index.html',
            'sources/hooks/systems/tasks/find_broken_urls.php',
            'sources/hooks/systems/tasks/find_orphaned_content_lang_strings.php',
            'sources/hooks/systems/tasks/find_orphaned_uploads.php',
            'sources/hooks/systems/tasks/reorganise_uploads.php',
            'sources_custom/hooks/systems/cleanup/.htaccess',
            'sources_custom/hooks/systems/cleanup/index.html',
            'themes/default/images/icons/menu/adminzone/tools/broken_urls.svg',
            'themes/default/images/icons/menu/adminzone/tools/cleanup.svg',
            'themes/default/images/icons_monochrome/menu/adminzone/tools/broken_urls.svg',
            'themes/default/images/icons_monochrome/menu/adminzone/tools/cleanup.svg',
            'themes/default/templates/BROKEN_CONTENT_LANG_STRINGS.tpl',
            'themes/default/templates/BROKEN_URLS.tpl',
            'themes/default/templates/CLEANUP_COMPLETED_SCREEN.tpl',
            'themes/default/templates/CLEANUP_ORPHANED_UPLOADS.tpl',
        ];
    }

    /**
     * Get mapping between template names and the method of this class that can render a preview of them.
     *
     * @return array The mapping
     */
    public function tpl_previews() : array
    {
        return [
            'templates/CLEANUP_COMPLETED_SCREEN.tpl' => 'administrative__cleanup_completed_screen',
            'templates/CLEANUP_ORPHANED_UPLOADS.tpl' => 'administrative__cleanup_completed_screen',
            'templates/BROKEN_URLS.tpl' => 'administrative__broken_urls_screen',
            'templates/BROKEN_CONTENT_LANG_STRINGS.tpl' => 'administrative__broken_content_lang_strings',
        ];
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__administrative__cleanup_completed_screen() : object
    {
        $urls = [];
        foreach (placeholder_array() as $v) {
            $urls[] = [
                'URL' => placeholder_url(),
                'PATH' => lorem_phrase(),
            ];
        }

        $message = do_lorem_template('CLEANUP_ORPHANED_UPLOADS', [
            'FOUND' => $urls,
        ]);

        return lorem_globalise(do_lorem_template('CLEANUP_COMPLETED_SCREEN', [
            'TITLE' => lorem_screen_title(),
            'MESSAGES' => $message,
        ]), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__administrative__broken_urls_screen() : object
    {
        require_lang('cleanup');

        $urls = [];
        foreach (placeholder_array() as $value) {
            $urls[] = [
                'FULL_URL' => placeholder_url(),
                'TABLE_NAMES' => [placeholder_codename()],
                'FIELD_NAMES' => [placeholder_codename()],
                'IDENTIFIERS' => [['IDENTIFIER' => placeholder_codename(), 'EDIT_URL' => '']],
                'CONTENT_TYPES' => [lorem_phrase()],
                'STATUS' => true,
                'MESSAGE' => '',
            ];
        }

        return lorem_globalise(do_lorem_template('BROKEN_URLS', [
            'URLS' => $urls,
            'DONE' => true,
        ]), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__administrative__broken_content_lang_strings() : object
    {
        require_lang('cleanup');

        return lorem_globalise(do_lorem_template('BROKEN_CONTENT_LANG_STRINGS', [
            'MISSING_CONTENT_LANG_STRINGS' => placeholder_array(),
            'FUSED_CONTENT_LANG_STRINGS' => placeholder_array(),
            'ORPHANED_CONTENT_LANG_STRINGS' => placeholder_array(),
            'MISSING_CONTENT_LANG_STRINGS_ZERO' => placeholder_array(),
        ]), null, '', true);
    }
}
