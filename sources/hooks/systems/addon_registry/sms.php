<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    sms
 */

/**
 * Hook class.
 */
class Hook_addon_registry_sms
{
    /**
     * Get a list of file permissions to set.
     *
     * @param  boolean $runtime Whether to include wildcards represented runtime-created chmoddable files
     * @return array File permissions to set
     */
    public function get_chmod_array(bool $runtime = false) : array
    {
        return [];
    }

    /**
     * Get the current version of this addon (usually software major, software minor, addon build).
     * Put the comment "// addon_version_auto_update" to the right of the return if you want release tools to automatically update this according to software version and find_addon_effective_mtime.
     *
     * @return SHORT_TEXT Version number
     */
    public function get_version() : string
    {
        return '11.0.1'; // addon_version_auto_update 45b7da1f9e12c3a29b2d8ae46b808ab1
    }

    /**
     * Get the minimum required version of the website software needed to use this addon.
     *
     * @return float Minimum required website software version
     */
    public function get_min_cms_version() : float
    {
        return 11.0;
    }

    /**
     * Get the maximum compatible version of the website software to use this addon.
     *
     * @return ?float Maximum compatible website software version (null: no maximum version currently)
     */
    public function get_max_cms_version() : ?float
    {
        return 11.9;
    }

    /**
     * Get the addon category.
     *
     * @return string The category
     */
    public function get_category() : string
    {
        return 'Architecture';
    }

    /**
     * Get the description of the addon.
     *
     * @return string Description of the addon
     */
    public function get_description() : string
    {
        return 'Provides an option for the software to send SMS messages, via the commercial Clickatell web service. By default this is only used by the notifications system.';
    }

    /**
     * Get a list of tutorials that apply to this addon.
     *
     * @return array List of tutorials
     */
    public function get_applicable_tutorials() : array
    {
        return [
            'tut_notifications',
        ];
    }

    /**
     * Get a mapping of dependency types.
     *
     * @return array A structure specifying dependency information
     */
    public function get_dependencies() : array
    {
        return [
            'requires' => [],
            'recommends' => [],
            'conflicts_with' => [],
        ];
    }

    /**
     * Explicitly say which icon should be used.
     *
     * @return URLPATH Icon
     */
    public function get_default_icon() : string
    {
        return 'themes/default/images/icons/admin/component.svg';
    }

    /**
     * Get a list of files that belong to this addon.
     *
     * @return array List of files
     */
    public function get_file_list() : array
    {
        return [
            'data/sms.php',
            'lang/EN/sms.ini',
            'sources/hooks/systems/addon_registry/sms.php',
            'sources/hooks/systems/cns_cpf_filter/sms.php',
            'sources/hooks/systems/config/sms_api_id.php',
            'sources/hooks/systems/config/sms_high_limit.php',
            'sources/hooks/systems/config/sms_high_trigger_limit.php',
            'sources/hooks/systems/config/sms_low_limit.php',
            'sources/hooks/systems/config/sms_low_trigger_limit.php',
            'sources/hooks/systems/config/sms_password.php',
            'sources/hooks/systems/config/sms_username.php',
            'sources/hooks/systems/privacy/sms.php',
            'sources/sms.php',
        ];
    }

    /**
     * Uninstall the addon.
     */
    public function uninstall()
    {
        $tables = [
            'sms_log',
            'confirmed_mobiles',
        ];
        $GLOBALS['SITE_DB']->drop_table_if_exists($tables);

        $privileges = [
            'use_sms',
            'sms_higher_limit',
            'sms_higher_trigger_limit',
        ];
        delete_privilege($privileges);
    }

    /**
     * Install the addon.
     *
     * @param  ?float $upgrade_major_minor From what major/minor version we are upgrading (null: new install)
     * @param  ?integer $upgrade_patch From what patch version of $upgrade_major_minor we are upgrading (null: new install)
     */
    public function install(?float $upgrade_major_minor = null, ?int $upgrade_patch = null)
    {
        if ($upgrade_major_minor === null) {
            $GLOBALS['SITE_DB']->create_table('sms_log', [
                'id' => '*AUTO',
                's_member_id' => 'MEMBER',
                's_time' => 'TIME',
                's_trigger_ip_address' => 'IP',
            ]);
            $GLOBALS['SITE_DB']->create_index('sms_log', 'sms_log_for', ['s_member_id', 's_time']);
            $GLOBALS['SITE_DB']->create_index('sms_log', 'sms_trigger_ip_address', ['s_trigger_ip_address']);
            add_privilege('GENERAL_SETTINGS', 'use_sms', false);
            add_privilege('GENERAL_SETTINGS', 'sms_higher_limit', false);
            add_privilege('GENERAL_SETTINGS', 'sms_higher_trigger_limit', false);

            /* Not currently implemented
            $GLOBALS['SITE_DB']->create_table('confirmed_mobiles', [
                'm_phone_number' => '*SHORT_TEXT',
                'm_member_id' => 'MEMBER',
                'm_time' => 'TIME',
                'm_confirm_code' => 'SHORT_TEXT',
            ]);
            */
            /*$GLOBALS['SITE_DB']->create_index('confirmed_mobiles', 'confirmed_numbers', ['m_confirm_code']);*/
        }

        if (($upgrade_major_minor !== null) && ($upgrade_major_minor < 11.0)) { // LEGACY
            // Database integrity fixes
            $GLOBALS['SITE_DB']->alter_table_field('sms_log', 's_trigger_ip', 'IP', 's_trigger_ip_address');

            $GLOBALS['SITE_DB']->delete_index_if_exists('sms_log', 'sms_trigger_ip');
        }
    }
}
