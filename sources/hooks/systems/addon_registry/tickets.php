<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    tickets
 */

/**
 * Hook class.
 */
class Hook_addon_registry_tickets
{
    /**
     * Get a list of file permissions to set.
     *
     * @param  boolean $runtime Whether to include wildcards represented runtime-created chmoddable files
     * @return array File permissions to set
     */
    public function get_chmod_array(bool $runtime = false) : array
    {
        return [];
    }

    /**
     * Get the current version of this addon (usually software major, software minor, addon build).
     * Put the comment "// addon_version_auto_update" to the right of the return if you want release tools to automatically update this according to software version and find_addon_effective_mtime.
     *
     * @return SHORT_TEXT Version number
     */
    public function get_version() : string
    {
        return '11'; // addon_version_auto_update e4f1a1f8f0927a3bb9e49f3d921d52b8
    }

    /**
     * Get the minimum required version of the website software needed to use this addon.
     *
     * @return float Minimum required website software version
     */
    public function get_min_cms_version() : float
    {
        return 11.0;
    }

    /**
     * Get the maximum compatible version of the website software to use this addon.
     *
     * @return ?float Maximum compatible website software version (null: no maximum version currently)
     */
    public function get_max_cms_version() : ?float
    {
        return 11.9;
    }

    /**
     * Get the addon category.
     *
     * @return string The category
     */
    public function get_category() : string
    {
        return 'New Features';
    }

    /**
     * Get the description of the addon.
     *
     * @return string Description of the addon
     */
    public function get_description() : string
    {
        return 'A support ticket system. Also provides an integrated standalone contact block, and integrated content reporting functionality.';
    }

    /**
     * Get a list of tutorials that apply to this addon.
     *
     * @return array List of tutorials
     */
    public function get_applicable_tutorials() : array
    {
        return [
            'tut_feedback',
            'tut_support_desk',
            'tut_staff',
        ];
    }

    /**
     * Get a mapping of dependency types.
     *
     * @return array A structure specifying dependency information
     */
    public function get_dependencies() : array
    {
        return [
            'requires' => [],
            'recommends' => [],
            'conflicts_with' => [],
        ];
    }

    /**
     * Explicitly say which icon should be used.
     *
     * @return URLPATH Icon
     */
    public function get_default_icon() : string
    {
        return 'themes/default/images/icons/menu/site_meta/tickets.svg';
    }

    /**
     * Get a list of files that belong to this addon.
     *
     * @return array List of files
     */
    public function get_file_list() : array
    {
        return [
            'adminzone/pages/modules/admin_tickets.php',
            'lang/EN/report_content.ini',
            'lang/EN/tickets.ini',
            'site/pages/modules/report_content.php',
            'site/pages/modules/tickets.php',
            'sources/blocks/main_contact_us.php',
            'sources/hooks/blocks/main_staff_checklist/tickets.php',
            'sources/hooks/modules/admin_import_types/tickets.php',
            'sources/hooks/modules/admin_stats/support_tickets.php',
            'sources/hooks/systems/actionlog/tickets.php',
            'sources/hooks/systems/addon_registry/tickets.php',
            'sources/hooks/systems/change_detection/tickets.php',
            'sources/hooks/systems/commandr_fs/ticket_types.php',
            'sources/hooks/systems/commandr_fs_extended_member/ticket_known_emailers.php',
            'sources/hooks/systems/config/reported_times.php',
            'sources/hooks/systems/config/support_operator.php',
            'sources/hooks/systems/config/ticket_auto_assign.php',
            'sources/hooks/systems/config/ticket_forum_name.php',
            'sources/hooks/systems/config/ticket_mail_email_address.php',
            'sources/hooks/systems/config/ticket_mail_folder.php',
            'sources/hooks/systems/config/ticket_mail_nonmatch_policy.php',
            'sources/hooks/systems/config/ticket_mail_on.php',
            'sources/hooks/systems/config/ticket_mail_password.php',
            'sources/hooks/systems/config/ticket_mail_server_host.php',
            'sources/hooks/systems/config/ticket_mail_server_port.php',
            'sources/hooks/systems/config/ticket_mail_server_type.php',
            'sources/hooks/systems/config/ticket_mail_tags.php',
            'sources/hooks/systems/config/ticket_mail_username.php',
            'sources/hooks/systems/config/ticket_text.php',
            'sources/hooks/systems/config/ticket_type_forums.php',
            'sources/hooks/systems/cron/ticket_type_lead_times.php',
            'sources/hooks/systems/cron/tickets_email_integration.php',
            'sources/hooks/systems/notifications/ticket_assigned_staff.php',
            'sources/hooks/systems/notifications/ticket_new_staff.php',
            'sources/hooks/systems/notifications/ticket_reply.php',
            'sources/hooks/systems/notifications/ticket_reply_staff.php',
            'sources/hooks/systems/page_groupings/tickets.php',
            'sources/hooks/systems/preview/ticket.php',
            'sources/hooks/systems/privacy/tickets.php',
            'sources/hooks/systems/resource_meta_aware/ticket_type.php',
            'sources/hooks/systems/rss/tickets.php',
            'sources/report_content.php',
            'sources/tickets.php',
            'sources/tickets2.php',
            'sources/tickets_email_integration.php',
            'themes/default/css/tickets.css',
            'themes/default/images/icons/buttons/add_ticket.svg',
            'themes/default/images/icons/buttons/new_reply_staff_only.svg',
            'themes/default/images/icons/menu/site_meta/contact_us.svg',
            'themes/default/images/icons/menu/site_meta/tickets.svg',
            'themes/default/images/icons_monochrome/buttons/add_ticket.svg',
            'themes/default/images/icons_monochrome/buttons/new_reply_staff_only.svg',
            'themes/default/images/icons_monochrome/menu/site_meta/contact_us.svg',
            'themes/default/images/icons_monochrome/menu/site_meta/tickets.svg',
            'themes/default/javascript/tickets.js',
            'themes/default/templates/BLOCK_MAIN_CONTACT_US.tpl',
            'themes/default/templates/SUPPORT_TICKETS_SCREEN.tpl',
            'themes/default/templates/SUPPORT_TICKETS_SEARCH_SCREEN.tpl',
            'themes/default/templates/SUPPORT_TICKET_LINK.tpl',
            'themes/default/templates/SUPPORT_TICKET_SCREEN.tpl',
            'themes/default/templates/SUPPORT_TICKET_TYPE_SCREEN.tpl',
            'themes/default/text/CNS_REPORTED_POST_FCOMCODE.txt',
            'themes/default/text/REPORTED_CONTENT_FCOMCODE.txt',
        ];
    }

    /**
     * Get mapping between template names and the method of this class that can render a preview of them.
     *
     * @return array The mapping
     */
    public function tpl_previews() : array
    {
        return [
            'templates/SUPPORT_TICKET_LINK.tpl' => 'support_tickets_screen',
            'templates/SUPPORT_TICKETS_SCREEN.tpl' => 'support_tickets_screen',
            'templates/SUPPORT_TICKET_SCREEN.tpl' => 'support_ticket_screen',
            'templates/SUPPORT_TICKETS_SEARCH_SCREEN.tpl' => 'support_tickets_search_screen',
            'templates/SUPPORT_TICKET_TYPE_SCREEN.tpl' => 'support_ticket_type_screen',
            'text/CNS_REPORTED_POST_FCOMCODE.txt' => 'cns_reported_post_fcomcode',
            'text/REPORTED_CONTENT_FCOMCODE.txt' => 'reported_content_fcomcode',
            'templates/BLOCK_MAIN_CONTACT_US.tpl' => 'block_main_contact_us',
        ];
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__support_tickets_screen() : object
    {
        $links = new Tempcode();
        foreach (placeholder_array() as $k => $v) {
            $links->attach(do_lorem_template('SUPPORT_TICKET_LINK', [
                '_NUM_POSTS' => placeholder_number(),
                'NUM_POSTS' => placeholder_number(),
                'CLOSED' => lorem_phrase(),
                'URL' => placeholder_url(),
                'TITLE' => lorem_phrase(),
                'EXTRA_DETAILS' => '',
                'TICKET_TYPE_NAME' => lorem_phrase(),
                'TICKET_TYPE_ID' => placeholder_numeric_id(),
                'FIRST_DATE' => placeholder_date(),
                'FIRST_DATE_RAW' => placeholder_date_raw(),
                'FIRST_POSTER_PROFILE_URL' => placeholder_url(),
                'FIRST_POSTER' => lorem_phrase(),
                'FIRST_POSTER_ID' => placeholder_numeric_id(),
                'LAST_POSTER_PROFILE_URL' => placeholder_url(),
                'LAST_POSTER' => lorem_phrase(),
                'LAST_POSTER_ID' => placeholder_numeric_id(),
                'LAST_DATE' => placeholder_date(),
                'LAST_DATE_RAW' => placeholder_date_raw(),
                'ID' => placeholder_codename(),
                'ASSIGNED' => [],
            ]));
        }

        $types = [];
        foreach (placeholder_array() as $k => $v) {
            $types[] = [
                'NAME' => $v,
                'TICKET_TYPE_ID' => strval($k),
                'SELECTED' => true,
                'LEAD_TIME' => placeholder_number(),
            ];
        }

        return lorem_globalise(do_lorem_template('SUPPORT_TICKETS_SCREEN', [
            'TITLE' => lorem_screen_title(),
            'MESSAGE' => lorem_phrase(),
            'LINKS' => $links,
            'ADD_TICKET_URL' => placeholder_url(),
            'TYPES' => $types,
            'OPEN' => true,
        ]), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__support_ticket_screen() : object
    {
        require_lang('cns');

        require_javascript('editing');

        $comments = new Tempcode();

        $other_tickets = new Tempcode();
        foreach (placeholder_array() as $k => $v) {
            $other_tickets->attach(do_lorem_template('SUPPORT_TICKET_LINK', [
                '_NUM_POSTS' => placeholder_number(),
                'NUM_POSTS' => placeholder_number(),
                'CLOSED' => lorem_phrase(),
                'URL' => placeholder_url(),
                'TITLE' => lorem_phrase(),
                'EXTRA_DETAILS' => '',
                'TICKET_TYPE_NAME' => lorem_phrase(),
                'TICKET_TYPE_ID' => placeholder_numeric_id(),
                'FIRST_DATE' => placeholder_date(),
                'FIRST_DATE_RAW' => placeholder_date_raw(),
                'FIRST_POSTER_PROFILE_URL' => placeholder_url(),
                'FIRST_POSTER' => lorem_phrase(),
                'FIRST_POSTER_ID' => placeholder_numeric_id(),
                'LAST_POSTER_PROFILE_URL' => placeholder_url(),
                'LAST_POSTER' => lorem_phrase(),
                'LAST_POSTER_ID' => placeholder_numeric_id(),
                'LAST_DATE' => placeholder_date(),
                'LAST_DATE_RAW' => placeholder_date_raw(),
                'ID' => placeholder_codename(),
                'ASSIGNED' => [],
            ]));
        }

        $whos_read = [];
        $whos_read[] = [
            'USERNAME' => lorem_word(),
            'MEMBER_ID' => placeholder_numeric_id(),
            'MEMBER_URL' => placeholder_url(),
            'DATE' => placeholder_date(),
        ];

        return lorem_globalise(do_lorem_template('SUPPORT_TICKET_SCREEN', [
            'ID' => placeholder_codename(),
            'TOGGLE_TICKET_CLOSED_URL' => placeholder_url(),
            'CLOSED' => lorem_phrase(),
            'USERNAME' => lorem_word(),
            'PING_URL' => placeholder_url(),
            'WARNING_DETAILS' => '',
            'NEW' => lorem_phrase(),
            'TICKET_TYPE' => null,
            'TICKET_PAGE_TEXT' => lorem_sentence_html(),
            'POST_TEMPLATES' => '',
            'TYPES' => placeholder_array(),
            'STAFF_ONLY' => true,
            'POSTER' => lorem_phrase(),
            'TITLE' => lorem_screen_title(),
            'COMMENTS' => $comments,
            'COMMENT_FORM' => placeholder_comments_form(false, false, 'block', 'expand', false),
            'STAFF_DETAILS' => placeholder_url(),
            'URL' => placeholder_url(),
            'ADD_TICKET_URL' => placeholder_url(),
            'OTHER_TICKETS' => $other_tickets,
            'SET_TICKET_EXTRA_ACCESS_URL' => placeholder_url(),
            'ASSIGNED' => [],
            'EXTRA_DETAILS' => lorem_phrase(),
            'WHOS_READ' => $whos_read,
            'SERIALIZED_OPTIONS' => '',
            'HASH' => '',
        ]), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__support_tickets_search_screen() : object
    {
        return lorem_globalise(do_lorem_template('SUPPORT_TICKETS_SEARCH_SCREEN', [
            'TITLE' => lorem_screen_title(),
            'URL' => placeholder_url(),
            'POST_FIELDS' => '',
            'RESULTS' => lorem_phrase(),
        ]), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__support_ticket_type_screen() : object
    {
        return lorem_globalise(do_lorem_template('SUPPORT_TICKET_TYPE_SCREEN', [
            'TITLE' => lorem_screen_title(),
            'TPL' => placeholder_form(),
            'ADD_FORM' => placeholder_form(),
        ]), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__cns_reported_post_fcomcode() : object
    {
        require_lang('cns');
        require_css('cns');
        return lorem_globalise(do_lorem_template('CNS_REPORTED_POST_FCOMCODE', [
            'POST_ID' => placeholder_numeric_id(),
            'POST_MEMBER_ID' => placeholder_numeric_id(),
            'POST_MEMBER' => lorem_phrase(),
            'TOPIC_TITLE' => lorem_phrase(),
            'POST' => lorem_phrase(),
            'REPORT_POST' => lorem_phrase(),
        ], null, false, null, '.txt', 'text'), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__reported_content_fcomcode() : object
    {
        return lorem_globalise(do_lorem_template('REPORTED_CONTENT_FCOMCODE', [
            'CONTENT_URL' => placeholder_url(),
            'CONTENT_TYPE' => lorem_word(),
            'CONTENT_ID' => placeholder_codename(),
            'CONTENT_MEMBER' => lorem_phrase(),
            'CONTENT_MEMBER_URL' => lorem_phrase(),
            'CONTENT_MEMBER_ID' => placeholder_numeric_id(),
            'CONTENT_TITLE' => lorem_phrase(),
            'CONTENT_RENDERED' => lorem_paragraph_html(),
            'REPORT_POST' => lorem_paragraph(),
        ], null, false, null, '.txt', 'text'), null, '', true);
    }

    /**
     * Get a preview(s) of a (group of) template(s), as a full standalone piece of HTML in Tempcode format.
     * Uses sources/lorem.php functions to place appropriate stock-text. Should not hard-code things, as the code is intended to be declarative.
     * Assumptions: You can assume all Lang/CSS/JavaScript files in this addon have been pre-required.
     *
     * @return Tempcode Preview
     */
    public function tpl_preview__block_main_contact_us() : object
    {
        require_javascript('posting');

        return lorem_globalise(do_lorem_template('BLOCK_MAIN_CONTACT_US', [
            'BLOCK_ID' => lorem_word(),
            'COMMENT_DETAILS' => placeholder_comments_form(false),
            'TYPE' => placeholder_codename(),
        ]), null, '', true);
    }

    /**
     * Uninstall default content.
     */
    public function uninstall_test_content()
    {
        if (get_forum_type() == 'none') {
            return; // Does not support the none forum driver
        }

        require_code('tickets');
        require_code('tickets2');
        require_lang('tickets');

        $to_delete = $GLOBALS['SITE_DB']->query_select('ticket_types', ['id'], [$GLOBALS['SITE_DB']->translate_field_ref('ticket_type_name') => lorem_phrase()]);
        foreach ($to_delete as $record) {
            delete_ticket_type($record['id']);
        }

        // Ticket deletion will be via different hook (as it's a topic); not so important to clean this up either
    }

    /**
     * Install default content.
     */
    public function install_test_content()
    {
        if (get_forum_type() == 'none') {
            return; // Does not support the none forum driver
        }

        require_code('tickets');
        require_code('tickets2');
        require_lang('tickets');

        $ticket_type_id = $GLOBALS['SITE_DB']->query_select_value_if_there('ticket_types', 'MIN(id)');
        if ($ticket_type_id === null) {
            $ticket_type_id = add_ticket_type(lorem_phrase());
        }

        $ticket_id = uniqid('', true);
        ticket_add_post($ticket_id, $ticket_type_id, lorem_phrase(), lorem_chunk(), false, get_member());
    }
}
