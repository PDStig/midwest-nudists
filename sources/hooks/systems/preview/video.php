<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    galleries
 */

/**
 * Hook class.
 */
class Hook_preview_video
{
    /**
     * Find whether this preview hook applies.
     *
     * @return array Triplet: Whether it applies, the attachment ID type (may be null), whether the forum DB is used [optional]
     */
    public function applies() : array
    {
        $applies = (addon_installed('galleries')) && (get_page_name() == 'cms_galleries') && ((get_param_string('type', '') == 'add_other') || (get_param_string('type', '') == '_edit_other'));
        return [$applies, null, false];
    }

    /**
     * Run function for preview hooks.
     *
     * @return array A pair: The preview, the updated post Comcode (may be null)
     */
    public function run() : array
    {
        require_code('galleries');
        require_code('galleries2');

        $cat = post_param_string('cat');

        require_code(_get_module_path(get_module_zone('cms_galleries'), 'cms_galleries'));
        if (class_exists('Mx_' . filter_naughty_harsh('cms_galleries'))) {
            $object = object_factory('Mx_' . filter_naughty_harsh('cms_galleries'));
        } else {
            $object = object_factory('Module_' . filter_naughty_harsh('cms_galleries'));
        }

        list(
            $url,
            $thumb_url,
            $filename,
            $width,
            $height,
            $length,
            $closed_captions_url,
        ) = video_get_defaults__post();

        if ($url == '') {
            if (post_param_integer('id', null) !== null) {
                $rows = $GLOBALS['SITE_DB']->query_select('videos', ['url', 'thumb_url'], ['id' => post_param_integer('id')], '', 1);
                $urls = $rows[0];

                $url = $urls['url'];
                $thumb_url = $urls['thumb_url'];
            } else {
                warn_exit(do_lang_tempcode('IMPROPERLY_FILLED_IN_UPLOAD'));
            }
        }

        $preview = show_gallery_video_media($url, $thumb_url, $width, $height, $length, get_member(), $closed_captions_url);

        return [$preview, null];
    }
}
