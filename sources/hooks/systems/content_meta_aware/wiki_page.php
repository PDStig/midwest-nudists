<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    wiki
 */

/**
 * Hook class.
 */
class Hook_content_meta_aware_wiki_page extends Hook_CMA
{
    /**
     * Get content type details.
     *
     * @param  ?ID_TEXT $zone The zone to link through to (null: autodetect)
     * @param  boolean $get_extended_data Populate additional data that is somewhat costly to compute (add_url, archive_url)
     * @return ?array Map of content-type info (null: disabled)
     */
    public function info(?string $zone = null, bool $get_extended_data = false) : ?array
    {
        if (!addon_installed('wiki')) {
            return null;
        }

        return [
            'support_custom_fields' => true,

            'content_type_label' => 'wiki:_WIKI_PAGE',
            'content_type_universal_label' => 'Wiki+ Page',

            'db' => $GLOBALS['SITE_DB'],
            'table' => 'wiki_pages',
            'id_field' => 'id',
            'id_field_numeric' => true,
            'parent_category_field' => null,
            'parent_category_meta_aware_type' => 'wiki_page',
            'is_category' => true,
            'is_entry' => false,
            'category_field' => 'id', // For category permissions
            'permission_module' => 'wiki_page', // For category permissions
            'parent_spec__table_name' => 'wiki_children',
            'parent_spec__parent_name' => 'parent_id',
            'parent_spec__field_name' => 'child_id',
            'category_is_string' => false,

            'title_field' => 'title',
            'title_field_dereference' => true,
            'description_field' => ['the_description', 'CALL: generate_wiki_page_entry_description'],
            'description_field_dereference' => true,
            'description_field_supports_comcode' => true,
            'image_field' => null,
            'image_field_is_theme_image' => false,
            'alternate_icon_theme_image' => 'icons/menu/rich_content/wiki',

            'view_page_link_pattern' => '_SEARCH:wiki:browse:_WILD',
            'edit_page_link_pattern' => '_SEARCH:cms_wiki:edit_page:_WILD',
            'view_category_page_link_pattern' => '_SEARCH:wiki:browse:_WILD',
            'add_url' => ($get_extended_data && has_submit_permission('cat_low', get_member(), get_ip_address(), 'cms_wiki')) ? (get_module_zone('cms_wiki') . ':cms_wiki:add_page') : null,
            'archive_url' => $get_extended_data ? ((($zone !== null) ? $zone : get_module_zone('wiki')) . ':wiki') : null,

            'support_url_monikers' => false,

            'views_field' => 'wiki_views',
            'order_field' => null,
            'submitter_field' => 'submitter',
            'author_field' => null,
            'add_time_field' => 'add_date',
            'edit_time_field' => 'edit_date',
            'date_field' => 'add_date',
            'validated_field' => null,
            'additional_sort_fields' => null,

            'seo_type_code' => 'wiki_page',

            'feedback_type_code' => null,

            'search_hook' => 'wiki_pages',
            'rss_hook' => 'wiki',
            'attachment_hook' => 'wiki_page',
            'notification_hook' => 'wiki',
            'sitemap_hook' => 'wiki_page',

            'addon_name' => 'wiki',

            'cms_page' => 'wiki',
            'module' => 'wiki',

            'commandr_filesystem_hook' => 'wiki',
            'commandr_filesystem__is_folder' => true,

            'support_revisions' => true,

            'support_privacy' => false,

            'support_content_reviews' => true,

            'support_spam_heuristics' => null,

            'actionlog_regexp' => '\w+_WIKI_PAGE',

            'default_prominence_weight' => PROMINENCE_WEIGHT_MEDIUM,
            'default_prominence_flags' => 0,
            'prominence_custom_sort' => db_function('GREATEST', [db_function('COALESCE', ['edit_date', 0]), db_function('COALESCE', ['(SELECT MAX(date_and_time) FROM ' . get_table_prefix() . 'wiki_posts WHERE page_id=r.id AND validated=1)', 0])]),
            'prominence_custom_sort_dir' => 'DESC',
        ];
    }

    /**
     * Render a content box for a content row.
     *
     * @param  array $row The database row for the content
     * @param  ID_TEXT $zone The zone to display in
     * @param  boolean $give_context Whether to include context (i.e. say WHAT this is, not just show the actual content)
     * @param  boolean $include_breadcrumbs Whether to include breadcrumbs (if there are any)
     * @param  ?ID_TEXT $root Virtual root to use (null: none)
     * @param  boolean $attach_to_url_filter Whether to copy through any filter parameters in the URL, under the basis that they are associated with what this box is browsing
     * @param  ID_TEXT $guid Overridden GUID to send to templates (blank: none)
     * @return Tempcode Results
     */
    public function render_box(array $row, string $zone, bool $give_context = true, bool $include_breadcrumbs = true, ?string $root = null, bool $attach_to_url_filter = false, string $guid = '') : object
    {
        require_code('wiki');

        return render_wiki_page_box($row, $zone, $give_context, $include_breadcrumbs, ($root === null) ? null : intval($root), $guid);
    }

    /**
     * Get the hook name of an AJAX tree selection list.
     *
     * @return ?string Hook name (null: none)
     */
    public function create_selection_tree_list() : ?string
    {
        return 'choose_wiki_page';
    }
}


/**
 * Find an entry description.
 *
 * @param  array $row Database row of entry
 * @param  integer $render_type A FIELD_RENDER_* constant
 * @param  boolean $resource_fs_style Whether to use the content API as resource-fs requires (may be slightly different)
 * @return ?mixed Content description (string or Tempcode, depending on $render_type) (null: could not generate)
 */
function generate_wiki_page_entry_description(array $row, int $render_type = 1, bool $resource_fs_style = false)
{
    if (!addon_installed('wiki')) {
        return null;
    }

    $ret = get_translated_text($row['the_description']);

    // FUDGE: Wiki+ page descriptions may be unreasonably long
    if (strlen($ret) > 200) {
        return '';
    }

    switch ($render_type) {
        case FIELD_RENDER_COMCODE:
            return $ret;

        case FIELD_RENDER_HTML:
            $just_row = db_map_restrict($row, ['id', 'the_description']);
            return get_translated_tempcode('wiki_pages', $just_row, 'the_description');
    }

    return strip_comcode($ret);
}
