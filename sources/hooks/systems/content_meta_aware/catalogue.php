<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    catalogues
 */

/**
 * Hook class.
 */
class Hook_content_meta_aware_catalogue extends Hook_CMA
{
    /**
     * Get content type details.
     *
     * @param  ?ID_TEXT $zone The zone to link through to (null: autodetect)
     * @param  boolean $get_extended_data Populate additional data that is somewhat costly to compute (add_url, archive_url)
     * @return ?array Map of content-type info (null: disabled)
     */
    public function info(?string $zone = null, bool $get_extended_data = false) : ?array
    {
        if (!addon_installed('catalogues')) {
            return null;
        }

        return [
            'support_custom_fields' => false,

            'content_type_label' => 'catalogues:CATALOGUE',
            'content_type_universal_label' => 'Catalogue',
            'content_type_label_override' => 'CALL: generate_catalogue_content_type_label',
            'content_type_universal_label_override' => 'CALL: generate_catalogue_content_type_universal_label',

            'db' => $GLOBALS['SITE_DB'],
            'table' => 'catalogues',
            'id_field' => 'c_name',
            'id_field_numeric' => false,
            'parent_category_field' => null,
            'parent_category_meta_aware_type' => null,
            'is_category' => true,
            'is_entry' => false,
            'category_field' => 'c_name', // For category permissions
            'permission_module' => 'catalogues_catalogue', // For category permissions
            'parent_spec__table_name' => null,
            'parent_spec__parent_name' => null,
            'parent_spec__field_name' => null,
            'category_is_string' => true,

            'title_field' => 'c_title',
            'title_field_dereference' => true,
            'description_field' => 'c_description',
            'description_field_dereference' => true,
            'description_field_supports_comcode' => true,
            'image_field' => null,
            'image_field_is_theme_image' => false,
            'alternate_icon_theme_image' => 'icons/menu/rich_content/catalogues/catalogues',

            'view_page_link_pattern' => '_SEARCH:catalogues:index:_WILD',
            'edit_page_link_pattern' => '_SEARCH:cms_catalogues:_edit_catalogue:_WILD',
            'view_category_page_link_pattern' => null,
            'add_url' => ($get_extended_data && has_submit_permission('mid', get_member(), get_ip_address(), 'cms_catalogues')) ? (get_module_zone('cms_catalogues') . ':cms_catalogues:add_entry') : null,
            'archive_url' => $get_extended_data ? ((($zone !== null) ? $zone : get_module_zone('catalogues')) . ':catalogues') : null,

            'support_url_monikers' => false,

            'views_field' => null,
            'order_field' => null,
            'submitter_field' => null,
            'author_field' => null,
            'add_time_field' => 'c_add_date',
            'edit_time_field' => null,
            'date_field' => 'c_add_date',
            'validated_field' => null,
            'additional_sort_fields' => null,

            'seo_type_code' => null,

            'feedback_type_code' => null,

            'search_hook' => null,
            'rss_hook' => null,
            'attachment_hook' => null,
            'notification_hook' => null,
            'sitemap_hook' => 'catalogue',

            'addon_name' => 'catalogues',

            'cms_page' => 'cms_catalogues',
            'module' => 'catalogues',

            'commandr_filesystem_hook' => 'catalogues',
            'commandr_filesystem__is_folder' => true,

            'support_revisions' => false,

            'support_privacy' => false,

            'support_content_reviews' => true,

            'support_spam_heuristics' => null,

            'actionlog_regexp' => '\w+_CATALOGUE',

            'default_prominence_weight' => PROMINENCE_WEIGHT_NONE,
            'default_prominence_flags' => 0,
        ];
    }

    /**
     * Get headings of special relevant data this content type supports.
     *
     * @return array A map of heading codenames to Tempcode labels
     */
    public function get_special_keymap_headings() : array
    {
        $headings = [];

        $headings['subcategory_count'] = do_lang_tempcode('CATEGORIES');
        $headings['entry_count'] = do_lang_tempcode('ENTRIES');

        return $headings;
    }

    /**
     * Get special relevant data this content type supports.
     *
     * @param  array $row Database row
     * @return array A map of heading codenames to Tempcode values
     */
    public function get_special_keymap(array $row) : array
    {
        $keymap = [];

        $num_children = $GLOBALS['SITE_DB']->query_select_value('catalogue_categories', 'COUNT(*)', ['c_name' => $row['c_name']]);
        $num_entries = $GLOBALS['SITE_DB']->query_select_value('catalogue_entries', 'COUNT(*)', ['c_name' => $row['c_name']]);
        $keymap['subcategory_count'] = escape_html(integer_format($num_children, 0));
        $keymap['entry_count'] = escape_html(integer_format($num_entries, 0));

        return $keymap;
    }

    /**
     * Render a content box for a content row.
     *
     * @param  array $row The database row for the content
     * @param  ID_TEXT $zone The zone to display in
     * @param  boolean $give_context Whether to include context (i.e. say WHAT this is, not just show the actual content)
     * @param  boolean $include_breadcrumbs Whether to include breadcrumbs (if there are any)
     * @param  ?ID_TEXT $root Virtual root to use (null: none)
     * @param  boolean $attach_to_url_filter Whether to copy through any filter parameters in the URL, under the basis that they are associated with what this box is browsing
     * @param  ID_TEXT $guid Overridden GUID to send to templates (blank: none)
     * @return Tempcode Results
     */
    public function render_box(array $row, string $zone, bool $give_context = true, bool $include_breadcrumbs = true, ?string $root = null, bool $attach_to_url_filter = false, string $guid = '') : object
    {
        require_code('catalogues');

        return render_catalogue_box($row, $zone, $give_context, $guid);
    }

    /**
     * Create a selection list.
     *
     * @param  ?string $id The pre-selected ID (null: none selected)
     * @return Tempcode List
     */
    public function create_selection_list(?string $id = null) : object
    {
        require_code('catalogues');

        return create_selection_list_catalogues($id);
    }
}

/**
 * Find an entry content-type language string label.
 *
 * @param  array $row Database row of entry
 * @return Tempcode Label
 */
function generate_catalogue_content_type_label(array $row) : object
{
    if (!addon_installed('catalogues')) {
        return new Tempcode();
    }

    if (!array_key_exists('c_title', $row)) {
        return do_lang_tempcode('catalogues:CATALOGUE');
    }
    return make_string_tempcode(get_translated_text($row['c_title']));
}

/**
 * Find an entry content-type universal label (doesn't depend on language pack).
 *
 * @param  array $row Database row of entry
 * @return string Label
 */
function generate_catalogue_content_type_universal_label(array $row) : string
{
    if (!addon_installed('catalogues')) {
        return 'Catalogue';
    }

    if (!array_key_exists('c_title', $row)) {
        return 'Catalogue';
    }
    return get_translated_text($row['c_title']);
}
