<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    downloads
 */

/**
 * Hook class.
 */
class Hook_content_meta_aware_download_category extends Hook_CMA
{
    /**
     * Get content type details.
     *
     * @param  ?ID_TEXT $zone The zone to link through to (null: autodetect)
     * @param  boolean $get_extended_data Populate additional data that is somewhat costly to compute (add_url, archive_url)
     * @return ?array Map of content-type info (null: disabled)
     */
    public function info(?string $zone = null, bool $get_extended_data = false) : ?array
    {
        if (!addon_installed('downloads')) {
            return null;
        }

        return [
            'support_custom_fields' => true,

            'content_type_label' => 'downloads:DOWNLOAD_CATEGORY',
            'content_type_universal_label' => 'Download category',

            'db' => $GLOBALS['SITE_DB'],
            'table' => 'download_categories',
            'id_field' => 'id',
            'id_field_numeric' => true,
            'parent_category_field' => 'parent_id',
            'parent_category_meta_aware_type' => 'download_category',
            'is_category' => true,
            'is_entry' => false,
            'category_field' => 'parent_id', // For category permissions
            'permission_module' => 'downloads', // For category permissions
            'parent_spec__table_name' => 'download_categories',
            'parent_spec__parent_name' => 'parent_id',
            'parent_spec__field_name' => 'id',
            'category_is_string' => false,

            'title_field' => 'category',
            'title_field_dereference' => true,
            'description_field' => 'the_description',
            'description_field_dereference' => true,
            'description_field_supports_comcode' => true,
            'image_field' => 'rep_image',
            'image_field_is_theme_image' => false,
            'alternate_icon_theme_image' => null,

            'view_page_link_pattern' => '_SEARCH:downloads:browse:_WILD',
            'edit_page_link_pattern' => '_SEARCH:cms_downloads:_edit_category:_WILD',
            'view_category_page_link_pattern' => '_SEARCH:downloads:browse:_WILD',
            'add_url' => ($get_extended_data && has_submit_permission('mid', get_member(), get_ip_address(), 'cms_downloads')) ? (get_module_zone('cms_downloads') . ':cms_downloads:add_category') : null,
            'archive_url' => $get_extended_data ? ((($zone !== null) ? $zone : get_module_zone('downloads')) . ':downloads') : null,

            'support_url_monikers' => true,

            'views_field' => null,
            'order_field' => null,
            'submitter_field' => null,
            'author_field' => null,
            'add_time_field' => 'add_date',
            'edit_time_field' => null,
            'date_field' => 'add_date',
            'validated_field' => null,
            'additional_sort_fields' => null,

            'seo_type_code' => 'downloads_category',

            'feedback_type_code' => null,

            'search_hook' => 'download_categories',
            'rss_hook' => null,
            'attachment_hook' => null,
            'notification_hook' => null,
            'sitemap_hook' => 'download_category',

            'addon_name' => 'downloads',

            'cms_page' => 'cms_downloads',
            'module' => 'downloads',

            'commandr_filesystem_hook' => 'downloads',
            'commandr_filesystem__is_folder' => true,

            'support_revisions' => false,

            'support_privacy' => false,

            'support_content_reviews' => true,

            'support_spam_heuristics' => null,

            'actionlog_regexp' => '\w+_DOWNLOAD_CATEGORY',

            'default_prominence_weight' => PROMINENCE_WEIGHT_NONE,
            'default_prominence_flags' => 0,
            'prominence_custom_sort' => '(SELECT MAX(add_date) FROM ' . get_table_prefix() . 'download_downloads WHERE category_id=r.id AND validated=1)',
            'prominence_custom_sort_dir' => 'DESC',
        ];
    }

    /**
     * Get headings of special relevant data this content type supports.
     *
     * @return array A map of heading codenames to Tempcode labels
     */
    public function get_special_keymap_headings() : array
    {
        $headings = [];

        $headings['subcategory_count'] = do_lang_tempcode('CATEGORIES');
        $headings['entry_count'] = do_lang_tempcode('ENTRIES');

        return $headings;
    }

    /**
     * Get special relevant data this content type supports.
     *
     * @param  array $row Database row
     * @return array A map of heading codenames to Tempcode values
     */
    public function get_special_keymap(array $row) : array
    {
        require_code('downloads');

        $keymap = [];

        $child_counts = count_download_category_children($row['id']);
        $num_children = $child_counts['num_children_children'];
        $num_entries = $child_counts['num_downloads_children'];
        $keymap['subcategory_count'] = escape_html(integer_format($num_children, 0));
        $keymap['entry_count'] = escape_html(integer_format($num_entries, 0));

        return $keymap;
    }

    /**
     * Render a content box for a content row.
     *
     * @param  array $row The database row for the content
     * @param  ID_TEXT $zone The zone to display in
     * @param  boolean $give_context Whether to include context (i.e. say WHAT this is, not just show the actual content)
     * @param  boolean $include_breadcrumbs Whether to include breadcrumbs (if there are any)
     * @param  ?ID_TEXT $root Virtual root to use (null: none)
     * @param  boolean $attach_to_url_filter Whether to copy through any filter parameters in the URL, under the basis that they are associated with what this box is browsing
     * @param  ID_TEXT $guid Overridden GUID to send to templates (blank: none)
     * @return Tempcode Results
     */
    public function render_box(array $row, string $zone, bool $give_context = true, bool $include_breadcrumbs = true, ?string $root = null, bool $attach_to_url_filter = false, string $guid = '') : object
    {
        require_code('downloads');

        return render_download_category_box($row, $zone, $give_context, $include_breadcrumbs, ($root === null) ? null : intval($root), $attach_to_url_filter, $guid);
    }

    /**
     * Get the hook name of an AJAX tree selection list.
     *
     * @return ?string Hook name (null: none)
     */
    public function create_selection_tree_list() : ?string
    {
        return 'choose_download_category';
    }
}
