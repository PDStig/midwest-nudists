<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    cns_forum
 */

/**
 * Hook class.
 */
class Hook_ajax_tree_choose_forum
{
    /**
     * Run function for ajax-tree hooks. Generates XML for a tree list, which is interpreted by JavaScript and expanded on-demand (via new calls).
     *
     * @param  ?ID_TEXT $id The ID to do under (null: root)
     * @param  array $options Options being passed through
     * @param  ?ID_TEXT $default The ID to select by default (null: none)
     * @return string XML in the special category,entry format
     */
    public function run(?string $id, array $options, ?string $default = null) : string
    {
        if (!addon_installed('cns_forum')) {
            return '<result></result>';
        }

        require_code('cns_forums');
        require_code('cns_forums2');

        $compound_list = array_key_exists('compound_list', $options) ? $options['compound_list'] : false;
        $addable_filter = array_key_exists('addable_filter', $options) ? ($options['addable_filter']) : false;
        if ($id === null) {
            $stripped_id = null;
        } else {
            $stripped_id = ($compound_list ? preg_replace('#,.*$#', '', $id) : $id);
        }

        $tree = cns_get_forum_tree(null, ($id === null) ? null : intval($id), '', null, null, $compound_list, 1, true);

        $levels_to_expand = array_key_exists('levels_to_expand', $options) ? ($options['levels_to_expand']) : intval(get_value('levels_to_expand__' . substr(get_class($this), 5), null, true));
        $options['levels_to_expand'] = max(0, $levels_to_expand - 1);

        if (!has_actual_page_access(null, 'forumview')) {
            $tree = $compound_list ? [[], ''] : [];
        }

        $out = '';

        $out .= '<options>' . xmlentities(json_encode($options)) . '</options>' . "\n";

        $categories = collapse_2d_complexity('id', 'c_title', $GLOBALS['FORUM_DB']->query_select('f_forum_groupings', ['id', 'c_title']));

        if ($compound_list) {
            list($tree,) = $tree;
        }

        $second_cats = [];
        foreach ($tree as $t) {
            $second_cats[$t['second_cat']] = true;
        }

        foreach ($tree as $t) {
            if ($compound_list) {
                $_id = $t['compound_list'];
            } else {
                $_id = strval($t['id']);
            }

            if ($stripped_id === strval($t['id'])) {
                continue; // Possible when we look under as a root
            }
            $title = $t['title'];
            $description = array_key_exists($t['group'], $categories) ? $categories[$t['group']] : '';
            $has_children = ($t['child_count'] != 0);
            $selectable = ((!$addable_filter) || cns_may_post_topic($t['id']));

            if ((!empty($t['second_cat'])) && (count($second_cats) > 1)) {
                $title = $t['second_cat'] . ' > ' . $title;
            }

            $tag = 'category'; // category
            $out .= '<' . $tag . ' id="' . xmlentities($_id) . '" title="' . xmlentities($title) . '" description="' . xmlentities($description) . '" has_children="' . ($has_children ? 'true' : 'false') . '" selectable="' . ($selectable ? 'true' : 'false') . '"></' . $tag . '>' . "\n";

            if ($levels_to_expand > 0) {
                $out .= '<expand>' . xmlentities($_id) . '</expand>' . "\n";
            }
        }

        // Mark parent cats for pre-expansion
        if (!cms_empty_safe($default)) {
            if ($compound_list) {
                $_full_tree = cns_get_forum_tree(null, null, '', null, null, $compound_list, null, true);
                list($_full_tree,) = $_full_tree;
                $full_tree = $this->make_tree_map($_full_tree);

                $_default = explode(',', $default);
                $cat = intval($_default[0]);
            } else {
                $cat = intval($default);
            }

            $expansions = [];
            while ($cat !== null) {
                if ($compound_list) {
                    $expansions[] = '<expand>' . $full_tree[$cat] . '</expand>' . "\n";
                } else {
                    $expansions[] = '<expand>' . strval($cat) . '</expand>';
                }
                $cat = $GLOBALS['FORUM_DB']->query_select_value_if_there('f_forums', 'f_parent_forum_id', ['id' => $cat]);
            }
            $expansions = array_reverse($expansions);
            $out .= implode('', $expansions);
        }

        $tag = 'result'; // result
        return '<' . $tag . '>' . $out . '</' . $tag . '>';
    }

    /**
     * Build a map between IDs and compound lists.
     *
     * @param  array $_full_tree Full tree structure
     * @return array The map
     */
    protected function make_tree_map(array $_full_tree) : array
    {
        $out = [];
        foreach ($_full_tree as $t) {
            $out[$t['id']] = $t['compound_list'];
            $out += $this->make_tree_map($t['children']);
        }
        return $out;
    }

    /**
     * Generate a simple selection list for the ajax-tree hook. Returns a normal <select> style <option>-list, for fallback purposes.
     *
     * @param  ?ID_TEXT $id The ID to do under (null: root) - not always supported
     * @param  array $options Options being passed through
     * @param  ?ID_TEXT $it The ID to select by default (null: none)
     * @return Tempcode The nice list
     */
    public function simple(?string $id, array $options, ?string $it = null) : object
    {
        if (!addon_installed('cns_forum')) {
            return new Tempcode();
        }

        require_code('cns_forums2');

        $compound_list = array_key_exists('compound_list', $options) ? $options['compound_list'] : false;
        $addable_filter = array_key_exists('addable_filter', $options) ? ($options['addable_filter']) : false;

        require_code('cns_forums');
        require_code('cns_forums2');

        $tree = create_selection_list_forum_tree(null, null, ($it === null) ? null : [intval($it)], $compound_list);

        return $tree;
    }
}
