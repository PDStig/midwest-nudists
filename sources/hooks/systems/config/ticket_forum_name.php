<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    tickets
 */

/**
 * Hook class.
 */
class Hook_config_ticket_forum_name
{
    /**
     * Gets the details relating to the config option.
     *
     * @return array The details
     */
    public function get_details() : array
    {
        return [
            'human_name' => 'TICKET_FORUM_NAME',
            'type' => 'forum',
            'category' => 'MESSAGES',
            'group' => 'SUPPORT_TICKETS',
            'explanation' => 'CONFIG_OPTION_ticket_forum_name',
            'shared_hosting_restricted' => '0',
            'list_options' => '',
            'order_in_category_group' => 2,
            'required' => true,
            'public' => false,
            'addon' => 'tickets',
        ];
    }

    /**
     * Gets the default value for the config option.
     *
     * @return ?string The default value (null: option is disabled)
     */
    public function get_default() : ?string
    {
        if (!addon_installed('tickets')) {
            return null;
        }

        return do_lang('tickets:TICKET_FORUM_NAME', '', '', '', get_site_default_lang());
    }
}
