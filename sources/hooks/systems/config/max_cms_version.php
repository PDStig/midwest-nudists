<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_configuration
 */

/**
 * Hook class.
 */
class Hook_config_max_cms_version
{
    /**
     * Gets the details relating to the config option.
     *
     * @return array The details
     */
    public function get_details() : array
    {
        return [
            'human_name' => 'MAX_CMS_VERSION',
            'type' => 'float',
            'category' => 'THEME',
            'group' => 'GENERAL',
            'explanation' => 'CONFIG_OPTION_max_cms_version',
            'shared_hosting_restricted' => '0',
            'list_options' => '',
            'theme_override' => true,
            'required' => false,
            'public' => false,
            'addon' => 'core_configuration',
        ];
    }

    /**
     * Gets the default value for the config option.
     *
     * @return ?string The default value (null: option is disabled)
     */
    public function get_default() : ?string
    {
        return '';
    }
}
