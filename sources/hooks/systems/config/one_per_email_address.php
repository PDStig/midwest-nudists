<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Hook class.
 */
class Hook_config_one_per_email_address
{
    /**
     * Gets the details relating to the config option.
     *
     * @return array The details
     */
    public function get_details() : array
    {
        return [
            'human_name' => 'ONE_PER_EMAIL_ADDRESS',
            'type' => 'list',
            'category' => 'USERS',
            'group' => 'JOINING',
            'explanation' => 'CONFIG_OPTION_one_per_email_address',
            'shared_hosting_restricted' => '0',
            'list_options' => '0|1|2',
            'order_in_category_group' => 2,
            'required' => true,
            'public' => false,
            'addon' => 'core_cns',
        ];
    }

    /**
     * Gets the default value for the config option.
     *
     * @return ?string The default value (null: option is disabled)
     */
    public function get_default() : ?string
    {
        return '1';
    }

    /**
     * Code to run before the option is saved.
     * If there is some kind of problem with the new value then we could attach an error message.
     *
     * @param  string $new_value The new value
     * @param  string $old_value The old value
     * @return boolean Whether to allow the save
     */
    public function presave_handler(string $new_value, string $old_value) : bool
    {
        if (($new_value == '2') && ($GLOBALS['FORUM_DRIVER']->get_member_email_address(get_member()) == '')) {
            attach_message(do_lang_tempcode('YOU_ADMIN_NO_EMAIL'), 'warn');
            return false;
        }

        return true;
    }
}
