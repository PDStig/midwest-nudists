<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    ldap
 */

/**
 * Hook class.
 */
class Hook_config_ldap_member_search_qualifier
{
    /**
     * Gets the details relating to the config option.
     *
     * @return array The details
     */
    public function get_details() : array
    {
        return [
            'human_name' => 'LDAP_MEMBER_SEARCH_QUALIFIER',
            'type' => 'line',
            'category' => 'USERS',
            'group' => 'LDAP',
            'explanation' => 'CONFIG_OPTION_ldap_member_search_qualifier',
            'shared_hosting_restricted' => '0',
            'list_options' => '',
            'order_in_category_group' => 9,
            'required' => false,
            'public' => false,
            'addon' => 'ldap',
        ];
    }

    /**
     * Gets the default value for the config option.
     *
     * @return ?string The default value (null: option is disabled)
     */
    public function get_default() : ?string
    {
        if (!addon_installed('ldap')) {
            return null;
        }

        return '';
    }
}
