<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cleanup_tools
 */

/**
 * Hook class.
 */
class Hook_task_find_orphaned_content_lang_strings
{
    /**
     * Run the task hook.
     *
     * @param  ?string $table The table to limit to (null: no limit)
     * @param  boolean $fix Whether to fix issues
     * @return ?array A tuple of at least 2: Return mime-type, content (either Tempcode, or a string, or a filename and file-path pair to a temporary file), map of HTTP headers if transferring immediately, map of ini_set commands if transferring immediately (null: show standard success message)
     */
    public function run(?string $table = null, bool $fix = true) : ?array
    {
        require_lang('cleanup');

        push_db_scope_check(false);

        // When a content language string isn't there
        $missing_content_lang_strings = [];
        $missing_content_lang_strings_zero = [];
        // When a content language string isn't used
        $orphaned_content_lang_strings = [];
        // When a content language string is used more than once
        $fused_content_lang_strings = [];

        $_all_ids = $GLOBALS['SITE_DB']->query_select('translate', ['DISTINCT id']);
        $all_ids = [];
        foreach ($_all_ids as $id) {
            $all_ids[$id['id']] = true;
        }

        $ids_seen_so_far = [];

        $where = [];
        if ($table !== null) {
            $where['m_table'] = $table;
        }

        $all_fields = $GLOBALS['SITE_DB']->query_select('db_meta', ['*'], $where);

        $langidfields = [];
        foreach ($all_fields as $f) {
            if (strpos($f['m_type'], '_TRANS') !== false) {
                $langidfields[] = ['m_name' => $f['m_name'], 'm_table' => $f['m_table']];
            }
        }
        foreach ($langidfields as $iteration => $langidfield) {
            task_log($this, 'Processing table ' . $langidfield['m_table'], $iteration, count($langidfields));

            $select = [$langidfield['m_name']];
            foreach ($all_fields as $f) {
                if ((substr($f['m_type'], 0, 1) == '*') && ($f['m_table'] == $langidfield['m_table'])) {
                    $select[] = $f['m_name'];
                }
            }
            $ofs = $GLOBALS['SITE_DB']->query_select($langidfield['m_table'], $select, [], '', null, 0, false, []);
            foreach ($ofs as $of) {
                $id = $of[$langidfield['m_name']];
                if ($id === null) {
                    continue;
                }

                if (!array_key_exists($id, $all_ids)) {
                    if (($fix) && (($id == 0) || (!array_key_exists($id, $missing_content_lang_strings)/*not fixed already*/))) {
                        $new_id = insert_lang($langidfield['m_name'], '', 2, null, false, $id);
                        if ($id[$langidfield['m_name']] != $new_id) {
                            $GLOBALS['SITE_DB']->query_update($langidfield['m_table'], $new_id, $of, '', 1);
                        }
                    }

                    $missing_content_lang_strings[$id] = true;
                    $missing_content_lang_strings_zero[json_encode($of)] = true;
                } elseif (array_key_exists($id, $ids_seen_so_far)) {
                    $looked_up = get_translated_text($id);
                    if ($fix) {
                        $_of = $GLOBALS['SITE_DB']->query_select($langidfield['m_table'], ['*'], $of, '', 1, 0, false, []);
                        $of = $_of[0];
                        $of_orig = $of;
                        $of = insert_lang($langidfield['m_name'], $looked_up, 2) + $of;
                        $GLOBALS['SITE_DB']->query_update($langidfield['m_table'], $of, $of_orig, '', 1);
                    }
                    $fused_content_lang_strings[$id] = $looked_up;
                }
                if ($langidfield['m_name'] != 't_cache_first_post') { // 'if..!=' is for special exception for one that may be re-used in a cache position
                    $ids_seen_so_far[$id] = true;
                }
            }
        }

        if ($table === null) {
            $orphaned_content_lang_strings = array_diff(array_keys($all_ids), array_keys($ids_seen_so_far));
            if ($fix) {
                foreach ($orphaned_content_lang_strings as $id) {
                    delete_lang($id);
                }
            }
        }

        $ret = do_template('BROKEN_CONTENT_LANG_STRINGS', [
            '_GUID' => '318fcbe81e1e4324350114c3def020dd',
            'MISSING_CONTENT_LANG_STRINGS' => array_keys($missing_content_lang_strings),
            'MISSING_CONTENT_LANG_STRINGS_ZERO' => array_keys($missing_content_lang_strings_zero),
            'FUSED_CONTENT_LANG_STRINGS' => $fused_content_lang_strings,
            'ORPHANED_CONTENT_LANG_STRINGS' => $orphaned_content_lang_strings,
        ]);
        return ['text/html', $ret];
    }
}
