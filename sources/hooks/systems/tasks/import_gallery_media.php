<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    galleries
 */

/**
 * Hook class.
 */
class Hook_task_import_gallery_media
{
    /**
     * Run the task hook.
     *
     * @param  array $files Array of tempURL => filename files to process
     * @param  MEMBER $member_id ID of the member who initiated the mass import
     * @param  ID_TEXT $cat Gallery to import into
     * @param  string $set_title The title to prepend to every image/video
     * @param  integer $allow_rating Post param indicating whether or not ratings should be allowed
     * @param  integer $allow_comments_reviews Post param combination indicating whether to allow comments or reviews
     * @param  integer $allow_trackbacks Post param indicating whether or not to allow trackbacks
     * @param  boolean $watermark Whether or not to apply the gallery's watermarks to the file if it is an image
     * @param  string $notes Staff notes provided for this entry
     * @param  ID_TEXT $privacy_level Level of privacy set for this content
     * @param  array $additional_access Array of additional members who should have access to this content
     * @return ?array A tuple of at least 2: Return mime-type, content (either Tempcode, or a string, or a filename and file-path pair to a temporary file), map of HTTP headers if transferring immediately, map of ini_set commands if transferring immediately (null: show standard success message)
     */
    public function run(array $files, int $member_id, string $cat, string $set_title, int $allow_rating, int $allow_comments_reviews, int $allow_trackbacks, bool $watermark, string $notes, string $privacy_level, array $additional_access) : ?array
    {
        if (!addon_installed('galleries')) {
            return null;
        }

        require_code('images');
        require_code('urls2');
        require_code('galleries');
        require_code('galleries2');
        require_lang('galleries');

        set_mass_import_mode();

        $media_imported = [];

        task_log($this, 'Processing ' . integer_format(count($files)) . ' files for importing into gallery ' . $cat);

        $media_number = 0;
        foreach ($files as $path => $filename) {
            if ((is_image($path, IMAGE_CRITERIA_WEBSAFE, has_privilege($member_id, 'comcode_dangerous'))) || (is_video($path, has_privilege($member_id, 'comcode_dangerous')))) {
                list($new_path, $new_url, $new_filename) = find_unique_path('uploads/galleries', filter_naughty($filename), true);

                // Store on server
                if (rename($path, $new_path) === false) {
                    @unlink($path);
                    intelligent_write_error($new_path);
                }
                fix_permissions($new_path);
                sync_file($new_path);

                // Post-process
                require_code('uploads');
                $test = handle_upload_post_processing(is_image($path, IMAGE_CRITERIA_WEBSAFE, has_privilege($member_id, 'comcode_dangerous')) ? CMS_UPLOAD_IMAGE : CMS_UPLOAD_VIDEO, $new_path, 'uploads/galleries', $filename, OBFUSCATE_NEVER);
                if ($test !== null) {
                    unlink($new_path);
                    sync_file($new_path);
                    $new_url = $test;
                }

                $fallback_title = '';
                if ($set_title == '') {
                    $fallback_title = do_lang('MEDIA_FILE_FALLBACK_TITLE', $cat, ($media_number + 1));
                }

                // Add to database
                $import_details = add_gallery_media_wrap($new_url, $cat, $member_id, $allow_rating, $allow_comments_reviews, $allow_trackbacks, $watermark, $notes, $privacy_level, $additional_access, $new_filename, null, $fallback_title);

                if ($import_details !== null) {
                    $media_number++;
                    $media_imported[] = $import_details;
                }
            } else {
                @unlink($path);
            }

            sync_file($path);
        }

        task_log($this, 'Number of files successfully imported into gallery: ' . integer_format(count($media_imported)));

        if ($set_title != '') {
            foreach ($media_imported as $i => $media_file) {
                list($media_type, $media_id) = $media_file;
                if (count($media_imported) == 1) {
                    $media_title = $set_title;
                } else {
                    $media_title = do_lang('MEDIA_FILE_IN_SET', $set_title, integer_format($i + 1), integer_format(count($media_imported)));
                }
                $lang_value = $GLOBALS['SITE_DB']->query_select_value($media_type . 's', 'title', ['id' => $media_id]);
                $GLOBALS['SITE_DB']->query_update($media_type . 's', lang_remap('title', $lang_value, $media_title), ['id' => $media_id], '', 1);
            }
        }

        // Clear caches
        require_code('caches');
        delete_cache_entry('main_multi_content');
        delete_cache_entry('main_gallery_embed');
        delete_cache_entry('main_gallery_mosaic');

        $ret = do_lang_tempcode('GALLERY_IMPORT_SUCCESS_EDIT_TITLES_INLINE', escape_html(integer_format(count($media_imported))));

        return ['text/html', $ret];
    }
}
