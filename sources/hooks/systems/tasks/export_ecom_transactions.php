<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    ecommerce
 */

/**
 * Hook class.
 */
class Hook_task_export_ecom_transactions
{
    /**
     * Run the task hook.
     *
     * @param  ID_TEXT $filter_username Filter transactions by username (blank: do not filter)
     * @param  ID_TEXT $filter_txn_id Filter by transaction ID (blank: do not filter)
     * @param  SHORT_TEXT $filter_purchase_id Filter by product-specific ID (blank: do not filter)
     * @param  LONG_TEXT $_filter_status Filter by comma-delimited transaction statuses (blank: do not filter)
     * @param  LONG_TEXT $_filter_type_code Filter by comma-delimited product type codes (blank: do not filter)
     * @param  ?TIME $filter_start Filter out all transactions before the given time (null: do not filter)
     * @param  ?TIME $filter_end Filter out all transactions after the given time (null: do not filter)
     * @param  ?string $file_type The file type to export with (null: default)
     * @return ?array A tuple of at least 2: Return mime-type, content (either Tempcode, or a string, or a filename and file-path pair to a temporary file), map of HTTP headers if transferring immediately, map of ini_set commands if transferring immediately (null: show standard success message)
     */
    public function run(string $filter_username = '', string $filter_txn_id = '', string $filter_purchase_id = '', string $_filter_status = '', string $_filter_type_code = '', ?int $filter_start = null, ?int $filter_end = null, ?string $file_type = null) : ?array
    {
        if (!addon_installed('ecommerce')) {
            return null;
        }

        require_code('ecommerce');

        // Build WHERE query and filename
        $where = '1=1';
        $end_filename = '';
        $filter_status = explode(',', $_filter_status);
        $filter_type_code = explode(',', $_filter_type_code);
        if ($filter_username != '') {
            $member_id = $GLOBALS['FORUM_DRIVER']->get_member_from_username($filter_username);
            if ($member_id !== null) {
                $where .= ' AND t_member_id=' . strval($member_id);
                $end_filename .= '_' . $filter_username;
            }
        }
        if ($filter_txn_id != '') {
            $end_filename .= '_' . $filter_txn_id;
            $where .= ' AND (t.id LIKE \'' . db_encode_like('%' . $filter_txn_id . '%') . '\' OR t.t_parent_txn_id LIKE \'' . db_encode_like('%' . $filter_txn_id . '%') . '\')';
        }
        if ($filter_purchase_id != '') {
            $end_filename .= '_' . $filter_purchase_id;
            $where .= ' AND ' . db_string_equal_to('t_purchase_id', $filter_purchase_id);
        }
        if ($_filter_status != '') {
            $end_filename .= '_' . $_filter_status;
            $where .= ' AND (';
            foreach ($filter_status as $key => $status) {
                if ($key > 0) {
                    $where .= ' OR ';
                }
                $where .= db_string_equal_to('t_status', $status);
            }
            $where .= ')';
        }
        if ($_filter_type_code != '') {
            $end_filename .= '_' . $_filter_type_code;
            $where .= ' AND (';
            foreach ($filter_type_code as $key => $product) {
                if ($key > 0) {
                    $where .= ' OR ';
                }
                $where .= db_string_equal_to('t_type_code', $product);
            }
            $where .= ')';
        }
        if ($filter_start !== null) {
            $end_filename .= '_s' . strval($filter_start);
            $where .= ' AND t_time>=' . strval($filter_start);
        }
        if ($filter_end !== null) {
            $end_filename .= '_e' . strval($filter_end);
            $where .= ' AND t_time<=' . strval($filter_end);
        }

        require_code('files_spreadsheets_write');
        if ($file_type === null) {
            $file_type = spreadsheet_write_default();
        }
        $filename = 'transactions_' . strval(time()) . $end_filename . '.' . $file_type;
        $outfile_path = null;
        $sheet_writer = spreadsheet_open_write($outfile_path, $filename);

        $max = 500;
        $start = 0;

        $query = 'FROM ' . get_table_prefix() . 'ecom_transactions t' . $GLOBALS['SITE_DB']->singular_join('ecom_trans_addresses', 'a', 't.id=a.a_txn_id', 'id', 'MIN', 'LEFT JOIN') . ' WHERE ' . $where;

        $max_rows = $GLOBALS['SITE_DB']->query_value_if_there('SELECT COUNT(*) ' . $query);

        do {
            $rows = $GLOBALS['SITE_DB']->query('SELECT t.*,t.id AS t_id,a.* ' . $query . ' ORDER BY t_time', $max, $start);

            foreach ($rows as $i => $_transaction) {
                task_log($this, 'Processing transaction row', $i, $max_rows);

                list($details, $product_object) = find_product_details($_transaction['t_type_code']);
                if ($details !== null) {
                    $item_name = $details['item_name'];
                } else {
                    $item_name = $_transaction['t_type_code'];
                }

                $transaction = [];

                $transaction[do_lang('DATE')] = get_timezoned_date_time($_transaction['t_time'], false);

                $transaction[do_lang('TRANSACTION')] = $_transaction['t_id'];

                $transaction[do_lang('PARENT')] = $_transaction['t_parent_txn_id'];

                $customer = $GLOBALS['FORUM_DRIVER']->get_username($_transaction['t_member_id']);
                $transaction[do_lang('CUSTOMER')] = $customer;

                $member_id = null;
                if ($product_object !== null) {
                    $member_id = method_exists($product_object, 'member_for') ? $product_object->member_for($_transaction['t_type_code'], $_transaction['t_purchase_id']) : null;
                }
                if ($member_id !== null) {
                    $username = $GLOBALS['FORUM_DRIVER']->get_username($member_id);
                } else {
                    $username = do_lang('NA');
                }

                $transaction[do_lang('RELATED_MEMBER')] = $username;

                $transaction[do_lang('PRODUCT')] = $item_name;

                $transaction[do_lang('PURCHASE_ID')] = $_transaction['t_purchase_id'];

                $transaction[do_lang('CURRENCY')] = $_transaction['t_currency'];

                $transaction[do_lang('PRICE')] = float_format($_transaction['t_price']);

                $transaction[do_lang(get_option('tax_system')) . ' (' . do_lang('COUNT_TOTAL') . ')'] = float_format($_transaction['t_tax']);

                $transaction[do_lang(get_option('tax_system')) . ' (' . do_lang('DETAILS') . ')'] = $_transaction['t_tax_derivation'];

                $transaction[do_lang('SHIPPING')] = float_format($_transaction['t_shipping']);

                $transaction[do_lang('STATUS')] = get_transaction_status_string($_transaction['t_status']);

                $transaction[do_lang('REASON')] = trim($_transaction['t_reason'] . '; ' . $_transaction['t_pending_reason'], '; ');

                $transaction[do_lang('NOTES')] = $_transaction['t_memo'];

                $transaction[do_lang('PAYMENT_GATEWAY')] = $_transaction['t_payment_gateway'];

                $transaction[do_lang('OTHER_DETAILS')] = $_transaction['t_invoicing_breakdown'];

                // Put address together
                $address = [];
                if ($_transaction['a_firstname'] !== null) {
                    if ($_transaction['a_firstname'] . $_transaction['a_lastname'] != '') {
                        $address[] = trim($_transaction['a_firstname'] . ' ' . $_transaction['a_lastname']);
                    }
                    if ($_transaction['a_street_address'] != '') {
                        $address[] = $_transaction['a_street_address'];
                    }
                    if ($_transaction['a_city'] != '') {
                        $address[] = $_transaction['a_city'];
                    }
                    if ($_transaction['a_county'] != '') {
                        $address[] = $_transaction['a_county'];
                    }
                    if ($_transaction['a_state'] != '') {
                        $address[] = $_transaction['a_state'];
                    }
                    if ($_transaction['a_post_code'] != '') {
                        $address[] = $_transaction['a_post_code'];
                    }
                    if ($_transaction['a_country'] != '') {
                        $address[] = $_transaction['a_country'];
                    }
                    if ($_transaction['a_email'] != '') {
                        $address[] = do_lang('EMAIL_ADDRESS') . ': ' . $_transaction['a_email'];
                    }
                    if ($_transaction['a_phone'] != '') {
                        $address[] = do_lang('PHONE_NUMBER') . ': ' . $_transaction['a_phone'];
                    }
                }
                $full_address = implode("\n", $address);
                $transaction[do_lang('ADDRESS')] = $full_address;

                $sheet_writer->write_row($transaction);
            }

            $start += $max;
        } while (!empty($rows));

        $sheet_writer->close();

        $headers = [];
        $headers['Content-Type'] = $sheet_writer->get_mime_type();
        $headers['Content-Disposition'] = 'attachment; filename="' . escape_header($filename) . '"';

        $ini_set = [];
        $ini_set['ocproducts.xss_detect'] = '0';

        return [$sheet_writer->get_mime_type(), [$filename, $outfile_path], $headers, $ini_set];
    }
}
