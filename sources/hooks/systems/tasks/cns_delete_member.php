<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Hook class.
 */
class Hook_task_cns_delete_member
{
    /**
     * Run the task hook.
     *
     * @param  MEMBER $member_id The member being deleted
     * @param  MEMBER $member_id_deleting The member doing the deleting
     * @param  boolean $also_purge Whether we are also purging their personal data
     * @return ?array A tuple of at least 2: Return mime-type, content (either Tempcode, or a string, or a filename and file-path pair to a temporary file), map of HTTP headers if transferring immediately, map of ini_set commands if transferring immediately (null: show standard success message)
     */
    public function run(int $member_id, int $member_id_deleting, bool $also_purge = false) : ?array
    {
        // Purge member data if applicable
        if ($also_purge) {
            require_code('privacy');

            // Get criteria
            $username = '';
            $ip_addresses = [];
            $email_address = '';
            fill_in_missing_privacy_criteria($username, $ip_addresses, $member_id, $email_address);

            // Map every privacy table to its default action
            $table_actions = [];
            $hook_obs = find_all_hook_obs('systems', 'privacy', 'Hook_privacy_');
            foreach ($hook_obs as $hook_ob) {
                $details = $hook_ob->info();
                if ($details !== null) {
                    foreach ($details['database_records'] as $table_name => $table_details) {
                        $table_actions[$table_name] = $table_details['removal_default_handle_method'];
                    }
                }
            }

            // Call the privacy_purge task
            $ob = get_hook_ob('systems', 'tasks', 'privacy_purge', 'Hook_task_');
            call_user_func_array([$ob, 'run'], [$table_actions, $username, $ip_addresses, $member_id, $email_address, []]);
        }

        // Actually delete the member
        require_code('cns_members_action');
        require_code('cns_members_action2');
        cns_delete_member($member_id, $member_id_deleting);

        return null;
    }
}
