<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    health_check
 */

/*EXTRA FUNCTIONS: stream_context_set_default*/

/**
 * Hook class.
 */
class Hook_health_check_performance extends Hook_Health_Check
{
    protected $category_label = 'Performance';

    /**
     * Standard hook run function to run this category of health checks.
     *
     * @param  ?array $sections_to_run Which check sections to run (null: all)
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     * @param  boolean $show_unusable_categories Whether to include categories that might not be accessible for some reason
     * @return array A pair: category label, list of results
     */
    public function run(?array $sections_to_run, int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null, bool $show_unusable_categories = false) : array
    {
        $this->process_checks_section('testManualPerformance', 'Manual performance checks', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
        $this->process_checks_section('testCookies', 'Cookies', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
        $this->process_checks_section('testHTTPOptimisation', 'HTTP optimisation', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
        $this->process_checks_section('testPageSpeed', 'Page speed (tests the configured/contextual pages only) (slow)', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
        $this->process_checks_section('testSetup', 'Setup', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
        $this->process_checks_section('testBotFlood', 'Heavy bot activity', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
        $this->process_checks_section('testXdebug', 'Xdebug profiler left enabled', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);

        return [$this->category_label, $this->results];
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testManualPerformance(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        if (!$manual_checks) {
            return;
        }

        // external_health_check (on maintenance sheet)

        $this->stateCheckManual('Check for speed issues on [url="Google PageSpeed"]https://pagespeed.web.dev/[/url], [url="Google Lighthouse"]https://developers.google.com/web/tools/lighthouse/[/url], [url="Pingdom"]https://tools.pingdom.com/[/url], [url="GTmetrix"]https://gtmetrix.com[/url], and [url="WebPageTest"]https://www.webpagetest.org[/url] (take warnings with a pinch of salt, not every suggestion is appropriate)');
    }

    /**
     * Make a URL firewall-safe.
     *
     * @param  URLPATH $url URL
     * @return URLPATH URL that is firewall-safe
     */
    protected function firewallify_url(string $url) : string
    {
        $config_ip_forwarding = get_option('ip_forwarding');

        switch ($config_ip_forwarding) {
            case '':
                return $url;

            case '1':
                $server_ips = get_server_ips();
                $connect_to = $server_ips[0];

                $url = preg_replace('#^(.*://)(.*)(/|:|$)#U', '$1' . $connect_to . '$3', $url);

                break;

            default:
                $protocol_end_pos = strpos($config_ip_forwarding, '://');
                if ($protocol_end_pos !== false) {
                    // Full with protocol
                    $url = preg_replace('#^(.*://)(.*)(/|$)#U', $config_ip_forwarding . '$3', $url);
                } else {
                    // IP address
                    $url = preg_replace('#^(.*://)(.*)(/|:|$)#U', '$1' . $config_ip_forwarding . '$3', $url);
                }

                break;
        }

        $opts = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'SNI_enabled' => true,
                'ciphers' => 'TLSv1',
            ],
        ];
        stream_context_set_default($opts);

        return $url;
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testCookies(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        global $SITE_INFO;
        if ((isset($SITE_INFO['any_guest_cached_too'])) && ($SITE_INFO['any_guest_cached_too'] == '1')) {
            $this->stateCheckSkipped('Skipped; guests are cached');
            return;
        }

        $url = $this->firewallify_url($this->get_page_url());

        require_code('global4');
        require_code('files');

        $headers = @cms_get_headers($url, 1);
        if ($headers === false) {
            $this->stateCheckSkipped('Could not find headers for URL [url="' . $url . '"]' . $url . '[/url]');
            return;
        }

        $found_has_cookies_cookie = false;
        foreach ($headers as $key => $vals) {
            if (!is_string($key)) {
                continue;
            }

            if (cms_strtolower_ascii($key) == cms_strtolower_ascii('Set-Cookie')) {
                if (is_string($vals)) {
                    $vals = [$vals];
                }

                foreach ($vals as $val) {
                    if (preg_match('#^has_cookies=1;#', $val) != 0) {
                        $found_has_cookies_cookie = true;
                    }

                    // Large cookies set
                    $_val = preg_replace('#^.*=#U', '', preg_replace('#; .*$#s', '', $val));
                    $this->assertTrue(strlen($_val) < 100, 'Large cookie @ ' . clean_file_size(strlen($_val)));
                }

                // Too many cookies set
                $this->assertTrue(count($vals) < 8, 'Many cookies are being set which is bad for performance @ ' . integer_format(count($vals)) . ' cookies');
            }
        }

        // Site cookies not set
        $this->assertTrue($found_has_cookies_cookie, 'Cookies not being properly set');
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testHTTPOptimisation(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        //set_option('output_compression', '1');   To test

        if (!php_function_allowed('stream_context_set_default')) {
            $this->stateCheckSkipped('PHP [tt]stream_context_set_default[/tt] function not available');
            return;
        }

        require_code('global4');

        $css_basename = basename(css_enforce('global', 'default'));
        $javascript_basename = basename(javascript_enforce('global', 'default'));
        if (php_function_allowed('usleep')) {
            usleep(1000000);
        }

        $urls = [
            'page' => $this->get_page_url(),
            'css' => get_base_url() . '/themes/default/templates_cached/EN/' . $css_basename,
            'js' => get_base_url() . '/themes/default/templates_cached/EN/' . $javascript_basename,
            'png' => get_base_url() . '/themes/default/images/no_image.png',
        ];

        foreach ($urls as $type => $url) {
            $url = $this->firewallify_url($url);

            stream_context_set_default(['http' => ['header' => 'Accept-Encoding: gzip']]);
            $headers = @cms_get_headers($url, 1);
            if ($headers === false) {
                $this->stateCheckSkipped('Could not find headers for URL [url="' . $url . '"]' . $url . '[/url]');
                continue;
            }

            $is_gzip = false;
            $is_cached = null;
            foreach ($headers as $key => $vals) {
                if (!is_string($key)) {
                    continue;
                }

                if (is_string($vals)) {
                    $vals = [$vals];
                }

                switch (cms_strtolower_ascii($key)) {
                    case 'content-encoding':
                        foreach ($vals as $val) {
                            if ($val == 'gzip') {
                                $is_gzip = true;
                            }
                        }

                        break;

                    case 'expires':
                        $is_cached = (strtotime($vals[0]) > time());
                        break;

                    case 'last-modified':
                        if ($is_cached === null) {
                            $is_cached = (strtotime($vals[0]) < time());
                        }
                        break;
                }
            }
            if ($is_cached === null) {
                $is_cached = false;
            }

            switch ($type) {
                case 'page':
                    $this->assertTrue(!$is_cached, 'Caching should not be given for pages (except for bots, which the software will automatically do if the static cache is enabled). Full headers: ' . serialize($headers));
                    $this->assertTrue($is_gzip, 'Gzip compression is not enabled/working for pages, significantly wasting bandwidth for page loads.');
                    break;

                case 'css':
                case 'js':
                    $this->assertTrue($is_cached, 'Caching should be given for [tt].' . $type . '[/tt] files (the software will automatically make sure edited versions cache under different URLs via automatic timestamp parameters). Full headers: ' . serialize($headers));
                    $this->assertTrue($is_gzip, 'Gzip compression is not enabled/working for [tt].' . $type . '[/tt] files, significantly wasting bandwidth for page loads. Uncomment from [tt]themes/default/templates_cached/.htaccess[/tt]? Full headers: ' . serialize($headers));
                    break;

                case 'png':
                    $this->assertTrue($is_cached, 'Caching should be given for [tt].' . $type . '[/tt] files. Uncomment from [tt]themes/default/templates_cached/.htaccess[/tt]? Full headers: ' . serialize($headers));
                    $this->assertTrue(!$is_gzip, 'Gzip compression should not be given for [tt].' . $type . '[/tt] files, they are already compressed so it is a waste of CPU power. Full headers: ' . serialize($headers));
                    break;
            }
        }
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testPageSpeed(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        $threshold = floatval(get_option('hc_page_speed_threshold'));

        $page_links = $this->process_urls_into_page_links();

        foreach ($page_links as $page_link) {
            $url = page_link_to_url($page_link);

            $time_before = microtime(true);
            $data = http_get_contents($url, ['trigger_error' => false]);
            $time_after = microtime(true);

            $time = ($time_after - $time_before);

            $this->assertTrue($time < $threshold, 'Slow page generation speed for "' . $page_link . '" page @ ' . float_format($time) . ' seconds)');
        }

        if (addon_installed('stats')) {
            $results = $GLOBALS['SITE_DB']->query_select('stats', ['page_link', 'AVG(milliseconds) AS milliseconds'], [], 'GROUP BY page_link');
            foreach ($results as $result) {
                $time = floatval($result['milliseconds']) / 1000.0;
                $this->assertTrue($time < $threshold, 'Slow page generation speed for [tt]' . $result['page_link'] . '[/tt] page @ ' . float_format($time) . ' seconds)');
            }
        }
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testSetup(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        if ((get_option('site_closed') != '0') || ($check_context != CHECK_CONTEXT__LIVE_SITE)) { // We might have intentionally disabled caches on non-live sites
            $this->stateCheckSkipped('Skipped cache checks as the site is currently closed or in test mode.');
            return;
        }

        $options = [
            'is_on_template_cache',
            'is_on_lang_cache',
            'is_on_comcode_page_cache',
            'is_on_block_cache',
        ];
        foreach ($options as $option) {
            $this->assertTrue(get_option($option) == '1', 'Cache option should be enabled to improve performance, ' . $option);
        }

        if ($manual_checks) {
            global $SITE_INFO;
            if (!isset($SITE_INFO['self_learning_cache']) || $SITE_INFO['self_learning_cache'] == '0') {
                $this->stateCheckManual('Consider enabling self-learning cache');
            }

            if ($GLOBALS['PERSISTENT_CACHE'] === null) {
                $this->stateCheckManual('Consider persistent caching');
            }

            if (support_smart_decaching()) {
                $this->stateCheckManual('Consider disabling smart decaching');
            }
        }
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testBotFlood(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        if (!addon_installed('stats')) {
            $this->stateCheckSkipped('[tt]stats[/tt] addon not installed');
            return;
        }

        require_code('files');
        $bots = cms_parse_ini_file_fast(is_file(get_file_base() . '/text_custom/bots.txt') ? (get_file_base() . '/text_custom/bots.txt') : (get_file_base() . '/text/bots.txt'));
        $bots = array_merge($bots, cms_parse_ini_file_fast(is_file(get_file_base() . '/text_custom/uptime_bots.txt') ? (get_file_base() . '/text_custom/uptime_bots.txt') : (get_file_base() . '/text/uptime_bots.txt')));
        $bots_where = '';
        foreach ($bots as $bot) {
            if (trim($bot) != '') {
                $bots_where .= ' AND browser NOT LIKE \'' . db_encode_like('%' . $bot . '%') . '\'';
            }
        }

        $threshold = 200; // Number of hits over 24 hour period. This is every 7 minutes or so, which a guest human would not sustain over that time.

        $where = 'member_id=' . strval($GLOBALS['FORUM_DRIVER']->get_guest_id()) . ' AND date_and_time>' . strval(time() - 60 * 60 * 24) . $bots_where;

        // By IP
        $query = 'SELECT COUNT(*) AS cnt,ip FROM ' . get_table_prefix() . 'stats WHERE ';
        $query .= $where;
        $query .= ' GROUP BY ip HAVING COUNT(*)>=' . strval($threshold) . ' ORDER BY cnt DESC';
        $rows = $GLOBALS['SITE_DB']->query($query);
        foreach ($rows as $row) {
            $browsers = collapse_1d_complexity('browser', $GLOBALS['SITE_DB']->query_select('stats', ['DISTINCT browser'], ['ip' => $row['ip']], 'ORDER BY browser'));
            $this->assertTrue(false, 'Likely bot at ' . $row['ip'] . ' did ' . integer_format($row['cnt']) . ' requests within the last 24 hours. User agents: &ldquo;' . implode('&rdquo;, &ldquo;', $browsers) . '&rdquo;');
        }

        // By User Agent
        $query = 'SELECT COUNT(*) AS cnt,browser FROM ' . get_table_prefix() . 'stats WHERE ';
        $query .= $where;
        $query .= ' AND browser NOT LIKE \'' . db_encode_like('%Mozilla%') . '\''; // This is common to all real web browser user agents; unfortunately some bots also put it in. Realistically we cannot find bots as we cannot maintain an inclusion-list of all web browser user agent patterns
        $query .= ' GROUP BY browser HAVING COUNT(*)>=' . strval($threshold) . ' ORDER BY cnt DESC';
        $rows = $GLOBALS['SITE_DB']->query($query);
        foreach ($rows as $row) {
            $ip_addresses = collapse_1d_complexity('ip', $GLOBALS['SITE_DB']->query_select('stats', ['DISTINCT ip'], ['browser' => $row['browser']], 'ORDER BY ip'));
            $this->assertTrue(false, 'Likely bot &quot;' . $row['browser'] . '&quot; did ' . integer_format($row['cnt']) . ' requests within the last 24 hours. IP addresses: ' . implode(', ', $ip_addresses));
        }
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testXdebug(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        // LEGACY
        $this->assertTrue(ini_get('xdebug.profiler_enable') !== '1', 'Xdebug (v2) profiler was left enabled, use xdebug.profiler_enable_trigger and xdebug.profiler_enable_trigger_value to selectively enable profiling if you want it');
        $this->assertTrue((@strval(ini_get('xdebug.start_with_request')) != 'yes') || (strpos(@strval(ini_get('xdebug.mode')), 'profile') === false), 'Xdebug (v3) profiler was left enabled, use xdebug.start_with_request=trigger and xdebug.trigger_value to selectively enable profiling if you want it');
        $this->assertTrue((@strval(ini_get('xdebug.start_with_request')) != 'yes') || (strpos(@strval(ini_get('xdebug.mode')), 'coverage') === false), 'Xdebug (v3) coverage mode was left enabled');
    }
}
