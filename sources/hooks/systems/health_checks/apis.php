<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    health_check
 */

/**
 * Hook class.
 */
class Hook_health_check_apis extends Hook_Health_Check
{
    protected $category_label = 'API connections';

    /**
     * Standard hook run function to run this category of health checks.
     *
     * @param  ?array $sections_to_run Which check sections to run (null: all)
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     * @param  boolean $show_unusable_categories Whether to include categories that might not be accessible for some reason
     * @return array A pair: category label, list of results
     */
    public function run(?array $sections_to_run, int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null, bool $show_unusable_categories = false) : array
    {
        if (($show_unusable_categories) || ($check_context != CHECK_CONTEXT__INSTALL)) {
            if ((($show_unusable_categories) || (get_option('ipstack_api_key') != ''))) {
                $this->process_checks_section('testIpStackConnection', 'ipstack', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
            }

            require_code('locations_geocoding');
            if (($show_unusable_categories) || (choose_geocoding_service() !== null)) {
                $this->process_checks_section('testGeocodeConnection', 'Geocoding', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
            }

            require_code('translation');
            if (($show_unusable_categories) || (has_translation('EN', 'FR'))) {
                $this->process_checks_section('testTranslationConnection', 'Translation', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
            }

            if ((($show_unusable_categories) || ((get_option('moz_access_id') != '') && (get_option('moz_secret_key') != '')))) {
                $this->process_checks_section('testMozConnection', 'Moz Links', $sections_to_run, $check_context, $manual_checks, $automatic_repair, $use_test_data_for_pass, $urls_or_page_links, $comcode_segments);
            }
        }

        return [$this->category_label, $this->results];
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testIpStackConnection(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        // NB: https requires a paid plan
        $ip_stack_url = 'http://api.ipstack.com/' . rawurlencode('216.58.192.142') . '?access_key=' . urlencode(get_option('ipstack_api_key'));
        $_json = cms_http_request($ip_stack_url, ['ignore_http_status' => true, 'convert_to_internal_encoding' => true, 'trigger_error' => false, 'timeout' => 20.0]);
        $json = @json_decode($_json->data, true);
        if (!is_array($json)) {
            $this->assertTrue(false, 'IP Stack error: ' . $_json->message);
        } else {
            if (array_key_exists('error', $json)) {
                $this->assertTrue(false, 'IP Stack error: ' . $json['error']['info']);
            } else {
                $this->assertTrue($json['country_name'] == 'United States', 'ipstack did not return expected result');
            }
        }
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testGeocodeConnection(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        require_code('locations_geocoding');

        // Forward geocoding
        foreach (['google', 'bing', 'mapquest'] as $service) {
            require_code('hooks/systems/geocoding/' . $service);
            $ob = object_factory('Hook_geocoding_' . $service);
            if ($ob->is_available()) {
                $errormsg = new Tempcode();
                $result = geocode('Berlin, DE', $errormsg, $service);
                if ($errormsg !== null) {
                    $this->assertTrue(false, $service . ' error doing forward geocoding: ' . $errormsg->evaluate());
                } else {
                    $this->assertTrue(($result !== null) && ($result[0] > 52.0) && ($result[0] < 53.0) && ($result[1] > 13.0) && ($result[1] < 14.0), $service . ' returned wrong coordinate');
                }
            }
        }
        // Note if this breaks there's also similar code in locations_catalogues_geoposition and locations_catalogues_geopositioning (non-bundled addons)

        // Reverse geocoding
        foreach (['google', 'bing', 'mapquest'] as $service) {
            require_code('hooks/systems/geocoding/' . $service);
            $ob = object_factory('Hook_geocoding_' . $service);
            if ($ob->is_available(true)) {
                $errormsg = new Tempcode();
                $address = reverse_geocode(52.516667, 13.388889, $errormsg, $service);
                if ($errormsg !== null) {
                    $this->assertTrue(false, $service . ' error doing reverse geocoding of Berlin: ' . $errormsg->evaluate());
                } else {
                    $this->assertTrue($address !== null, $service . ' failed to do reverse geocoding of Berlin');
                    if ($address !== null) {
                        $this->assertTrue($address[2] == 'Berlin', $service . ' returned wrong city' . ', got ' . $address[2] . ', expected Berlin');
                        $this->assertTrue($address[6] == 'DE', $service . ' returned wrong country' . ', got ' . $address[6] . ', expected DE');
                    }
                }

                $errormsg = new Tempcode();
                $address = reverse_geocode(64.133333, -21.933333, $errormsg, $service);
                if ($errormsg !== null) {
                    $this->assertTrue(false, $service . ' error doing reverse geocoding of Raycevick: ' . $errormsg->evaluate());
                } else {
                    $this->assertTrue($address !== null, $service . ' failed to do reverse geocoding of Raycevick');
                    if ($address !== null) {
                        $this->assertTrue(substr($address[2], 0, 3) == 'Rey', $service . ' returned wrong city' . ', got ' . $address[2] . ', expected ~Raycevick'); // Only check first chars due to charset issues
                    }
                }
            }
        }
    }


    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testTranslationConnection(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        $hooks = [
            'bing_translator',
            'google_translate',
        ];

        $from = 'EN';
        $to = 'FR';

        foreach ($hooks as $hook) {
            $translation_object = get_translation_object_for_hook($hook);
            $errormsg = '';
            if (has_translation($from, $to, $translation_object, $errormsg)) {
                $GLOBALS['SITE_DB']->query_delete('translation_cache');

                $from_text = 'Hello';
                $to_text = translate_text($from_text, TRANS_TEXT_CONTEXT__AUTODETECT, $from, $to, $hook, $errormsg);
                if ($errormsg !== null) {
                    $this->assertTrue(false, 'Translation failed from ' . $from . ' to ' . $to . ' (error message is ' . $errormsg . ')');
                } else {
                    $this->assertTrue(($to_text !== null) && (($to_text == 'Bonjour') || ($to_text == 'Salut')), 'Translation failed from ' . $from . ' to ' . $to . ', got ' . $to_text . ' for ' . $from_text . ' (error message is ' . @strval($errormsg) . ')');
                }
            }
        }
    }

    /**
     * Run a section of health checks.
     *
     * @param  integer $check_context The current state of the website (a CHECK_CONTEXT__* constant)
     * @param  boolean $manual_checks Mention manual checks
     * @param  boolean $automatic_repair Do automatic repairs where possible
     * @param  ?boolean $use_test_data_for_pass Should test data be for a pass [if test data supported] (null: no test data)
     * @param  ?array $urls_or_page_links List of URLs and/or page-links to operate on, if applicable (null: those configured)
     * @param  ?array $comcode_segments Map of field names to Comcode segments to operate on, if applicable (null: N/A)
     */
    public function testMozConnection(int $check_context, bool $manual_checks = false, bool $automatic_repair = false, ?bool $use_test_data_for_pass = null, ?array $urls_or_page_links = null, ?array $comcode_segments = null)
    {
        if ($check_context == CHECK_CONTEXT__INSTALL) {
            $this->log('Skipped; we are running from installer.');
            return;
        }
        if ($check_context == CHECK_CONTEXT__SPECIFIC_PAGE_LINKS) {
            $this->log('Skipped; running on specific page links.');
            return;
        }

        if ($use_test_data_for_pass !== null) {
            $url = get_brand_base_url();
        } else {
            $url = get_base_url();
        }

        require_code('broken_urls');
        $ob = new BrokenURLScanner();
        try {
            $urls = $ob->enumerate_moz_backlinks([$url], 1);
            $this->assertTrue((($use_test_data_for_pass === null) || (count($urls) > 0)), 'Error trying to retrieve backlinks');
        } catch (Exception $e) {
            $this->assertTrue(false, 'Backlink error: ' . $e->getMessage());
        }
    }
}
