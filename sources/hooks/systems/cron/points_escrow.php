<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    points
 */

/**
 * Hook class.
 */
class Hook_cron_points_escrow
{
    /**
     * Get info from this hook.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     * @param  ?boolean $calculate_num_queued Calculate the number of items queued, if possible (null: the hook may decide / low priority)
     * @return ?array Return a map of info about the hook (null: disabled)
     */
    public function info(?int $last_run, ?bool $calculate_num_queued) : ?array
    {
        if (!addon_installed('points')) {
            return null;
        }

        require_code('points_escrow');

        if ($calculate_num_queued === null) {
            $calculate_num_queued = true;
        }

        if ($calculate_num_queued) {
            $num_queued = $GLOBALS['SITE_DB']->query_select_value('escrow', 'COUNT(*)', ['status' => ESCROW_STATUS_PENDING], ' AND expiration_time IS NOT NULL AND expiration_time<=' . strval(time()));
        } else {
            $num_queued = null;
        }

        return [
            'label' => 'Cancel / dispute expired points escrow',
            'num_queued' => $num_queued,
            'minutes_between_runs' => 5,
            'enabled_by_default' => true,
        ];
    }

    /**
     * Run function for system scheduler hooks. Searches for things to do. ->info(..., true) must be called before this method.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     */
    public function run(?int $last_run)
    {
        require_code('points_escrow');

        $rows = $GLOBALS['SITE_DB']->query_select('escrow', ['*'], ['status' => ESCROW_STATUS_PENDING], ' AND expiration_time IS NOT NULL AND expiration_time<=' . strval(time()), 100);
        foreach ($rows as $row) {
            if ($row['sender_status'] == 0 && $row['recipient_status'] == 0) { // No one satisfied it
                cancel_escrow($row['id'], $GLOBALS['FORUM_DRIVER']->get_guest_id(), do_lang('ESCROW_EXPIRED'), $row);
            } else {
                dispute_escrow($row['id'], $GLOBALS['FORUM_DRIVER']->get_guest_id(), do_lang('ESCROW_EXPIRED'), $row);
            }
        }
    }
}
