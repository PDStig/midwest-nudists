<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    points
 */

/**
 * Hook class.
 */
class Hook_cron_points_daily
{
    protected $this_points_daily;
    /**
     * Get info from this hook.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     * @param  ?boolean $calculate_num_queued Calculate the number of items queued, if possible (null: the hook may decide / low priority)
     * @return ?array Return a map of info about the hook (null: disabled)
     */
    public function info(?int $last_run, ?bool $calculate_num_queued) : ?array
    {
        if (!addon_installed('points')) {
            return null;
        }

        $points_per_day = intval(get_option('points_per_day'));
        if ($points_per_day <= 0) {
            return null; // Do not run if we are not awarding daily points
        }

        if ($calculate_num_queued === null) {
            $calculate_num_queued = true;
        }

        if ($calculate_num_queued) {
            $this->this_points_daily = date('d/m/Y', tz_time(time(), get_site_timezone()));
            if (get_value('last_points_daily', null, true) !== $this->this_points_daily) {
                $num_queued = $GLOBALS['FORUM_DRIVER']->get_num_members();
            } else {
                $num_queued = 0;
            }
        } else {
            $num_queued = null;
        }

        return [
            'label' => 'Credit daily points',
            'num_queued' => $num_queued,
            'minutes_between_runs' => 60,
            'enabled_by_default' => true,
        ];
    }

    /**
     * Run function for system scheduler hooks. Searches for things to do. ->info(..., true) must be called before this method.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     */
    public function run(?int $last_run)
    {
        if (!addon_installed('points')) {
            return;
        }

        $points_per_day = intval(get_option('points_per_day'));
        if ($points_per_day <= 0) {
            return; // Do not run if we are not awarding daily points
        }

        if (get_value('last_points_daily', null, true) !== $this->this_points_daily) {
            set_value('last_points_daily', $this->this_points_daily, true);

            require_code('points2');
            require_lang('points');

            // Credit daily points to everyone
            $member_id = null;
            do {
                $rows = $GLOBALS['FORUM_DRIVER']->get_next_members($member_id, 100);
                foreach ($rows as $row) {
                    $member_id = $GLOBALS['FORUM_DRIVER']->mrow_member_id($row);

                    points_credit_member($member_id, do_lang('POINTS_PER_DAY'), $points_per_day, 0, null, 0, 'points', 'credit_daily');
                }
            } while (count($rows) > 0);
        }
    }
}
