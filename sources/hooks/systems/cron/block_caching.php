<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/**
 * Hook class.
 */
class Hook_cron_block_caching
{
    /**
     * Get info from this hook.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     * @param  ?boolean $calculate_num_queued Calculate the number of items queued, if possible (null: the hook may decide / low priority)
     * @return ?array Return a map of info about the hook (null: disabled)
     */
    public function info(?int $last_run, ?bool $calculate_num_queued) : ?array
    {
        // Calculate on low priority
        if ($calculate_num_queued === null) {
            $calculate_num_queued = true;
        }

        return [
            'label' => 'Block cache population',
            'num_queued' => $calculate_num_queued ? $GLOBALS['SITE_DB']->query_select_value('cron_caching_requests', 'COUNT(*)') : 0,
            'minutes_between_runs' => 0,
            'enabled_by_default' => true,
        ];
    }

    /**
     * Run function for system scheduler hooks. Searches for things to do. ->info(..., true) must be called before this method.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     */
    public function run(?int $last_run)
    {
        if (get_value('cron_block_caching_careful_contexts') === '1') {
            $request_contexts = $GLOBALS['SITE_DB']->query('SELECT DISTINCT c_theme,c_lang FROM ' . get_table_prefix() . 'cron_caching_requests');
            if (empty($request_contexts)) {
                return;
            }

            $langs = find_all_langs();
            require_code('themes2');
            $themes = find_all_themes();

            // Remove anything tied to a context that no longer exists
            foreach ($request_contexts as $i => $request_context) {
                $lang = $request_context['c_lang'];
                $theme = $request_context['c_theme'];

                if ((!isset($langs[$lang])) || (!isset($themes[$theme]))) {
                    unset($request_contexts[$i]);
                    $GLOBALS['SITE_DB']->query_delete('cron_caching_requests', ['c_theme' => $theme, 'c_lang' => $lang]);
                }
            }

            if ((get_param_string('keep_lang', null) === null) || (get_param_string('keep_theme', null) === null)) {
                // We need to recurse into a new call for each context (language and theme combination)
                $done_something = false;
                $more_to_do = false;
                foreach ($request_contexts as $request_context) {
                    $lang = $request_context['c_lang'];
                    $theme = $request_context['c_theme'];

                    if (($theme != $GLOBALS['FORUM_DRIVER']->get_theme()) || ($lang != user_lang())) {
                        $where = ['c_theme' => $theme, 'c_lang' => $lang];
                        $count = $GLOBALS['SITE_DB']->query_select_value('cron_caching_requests', 'COUNT(*)', $where);
                        if ($count > 0) {
                            $url = get_base_url() . '/data/cron_bridge.php?limit_hook=block_caching&keep_lang=' . urlencode($lang) . '&keep_theme=' . urlencode($theme) . '&force=1';
                            http_get_contents($url, ['trigger_error' => false, 'timeout' => 180.0]);
                            $done_something = true;
                        }
                    } else {
                        $more_to_do = true;
                    }
                }

                // Force re-loading of values that we use to mark progress (as above calls probably resulted in changes happening)
                if ($done_something) {
                    global $VALUE_OPTIONS_CACHE;
                    $VALUE_OPTIONS_CACHE = $GLOBALS['SITE_DB']->query_select('values', ['*']);
                    $VALUE_OPTIONS_CACHE = list_to_map('the_name', $VALUE_OPTIONS_CACHE);
                }

                if (!$more_to_do) {
                    return;
                }
            }

            $where = ['c_theme' => $GLOBALS['FORUM_DRIVER']->get_theme(), 'c_lang' => user_lang()];
        } else {
            $where = [];
        }

        // Execute all in current context...

        global $LANGS_REQUESTED, $LANGS_REQUESTED, $TIMEZONE_MEMBER_CACHE, $JAVASCRIPTS, $CSSS, $REQUIRED_ALL_LANG, $DO_NOT_CACHE_THIS;

        push_query_limiting(false);

        $requests = $GLOBALS['SITE_DB']->query_select('cron_caching_requests', ['*'], $where);
        foreach ($requests as $request) {
            $codename = $request['c_codename'];
            $map = @unserialize($request['c_map']);
            if (!is_array($map)) {
                $map = [];
            }

            list($object, $new_security_scope) = do_block_hunt_file($codename, $map);

            if ($new_security_scope) {
                _solemnly_enter();
            }

            if (is_object($object)) {
                $backup_langs_requested = $LANGS_REQUESTED;
                $backup_required_all_lang = $REQUIRED_ALL_LANG;
                get_users_timezone();
                $backup_timezone = $TIMEZONE_MEMBER_CACHE[get_member()];
                $TIMEZONE_MEMBER_CACHE[get_member()] = $request['c_timezone'];
                $LANGS_REQUESTED = [];
                $REQUIRED_ALL_LANG = [];
                push_output_state(false, true);
                $DO_NOT_CACHE_THIS = false;
                $cache = $object->run($map);
                $TIMEZONE_MEMBER_CACHE[get_member()] = $backup_timezone;
                $cache->handle_symbol_preprocessing();
                if (!$DO_NOT_CACHE_THIS) {
                    if (method_exists($object, 'caching_environment')) {
                        $info = $object->caching_environment();
                        if (!array_key_exists('special_cache_flags', $info)) {
                            $info['special_cache_flags'] = CACHE_AGAINST_DEFAULT;
                        }
                    } else {
                        $info = [];
                        $info['cache_on'] = <<<'PHP'
                        $map
PHP;
                        $info['special_cache_flags'] = CACHE_AGAINST_DEFAULT | CACHE_AGAINST_PERMISSIVE_GROUPS;
                        $info['ttl'] = 60 * 24;
                    }
                    $ttl = $info['ttl'];

                    $cache_identifier = do_block_get_cache_identifier($codename, $info['cache_on'], $map);

                    require_code('caches2');
                    if ($request['c_store_as_tempcode'] == 1) {
                        $cache = make_string_tempcode($cache->evaluate());
                    }
                    set_cache_entry(
                        $codename,
                        $ttl,
                        $cache_identifier,
                        $cache,
                        $info['special_cache_flags'],
                        array_keys($LANGS_REQUESTED),
                        array_keys($JAVASCRIPTS),
                        array_keys($CSSS),
                        true,
                        $request['c_staff_status'],
                        $request['c_member'],
                        $request['c_groups'],
                        $request['c_is_bot'],
                        $request['c_timezone'],
                        $request['c_theme'],
                        $request['c_lang']
                    );
                }
                $LANGS_REQUESTED += $backup_langs_requested;
                $REQUIRED_ALL_LANG += $backup_required_all_lang;
                restore_output_state(false, true);
            }

            if ($new_security_scope) {
                _solemnly_leave();
            }

            $GLOBALS['SITE_DB']->query_delete('cron_caching_requests', $request);
        }
    }
}
