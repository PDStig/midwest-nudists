<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    stats
 */

/**
 * Hook class.
 */
class Hook_cron_stats_preprocess_raw_data
{
    protected const INITIAL_BACK_TIME = 24 * 60 * 60 * 31; // Don't calculate stats older than 31 days ago to prevent server freezes.

    /**
     * Get info from this hook.
     *
     * @param  ?TIME $last_run Last time run (null: never)
     * @param  ?boolean $calculate_num_queued Calculate the number of items queued, if possible (null: the hook may decide / low priority)
     * @return ?array Return a map of info about the hook (null: disabled)
     */
    public function info(?int $last_run, ?bool $calculate_num_queued) : ?array
    {
        if (!addon_installed('stats')) {
            return null;
        }

        return [
            'label' => 'Stats preprocessing',
            'num_queued' => null,
            'minutes_between_runs' => 60 * 24,
            'enabled_by_default' => true,
        ];
    }

    /**
     * Run function for system scheduler hooks. Searches for tasks to perform.
     */
    public function run()
    {
        $server_timezone = get_server_timezone();

        $last_day_processed = get_value('stats__last_day_processed', null, true);
        if ($last_day_processed === null) {
            $start_time = 0;
        } else {
            list($year, $month, $day) = array_map('intval', explode('-', $last_day_processed));
            $start_time = cms_mktime(0, 0, 0, $month, $day, $year);
            $start_time = tz_time($start_time, $server_timezone);
        }

        if ($start_time < (time() - self::INITIAL_BACK_TIME)) {
            $start_time = (time() - self::INITIAL_BACK_TIME);
        }

        $today = cms_date('Y-m-d');
        list($year, $month, $day) = array_map('intval', explode('-', $today));
        $end_time = cms_mktime(0, 0, 0, $month, $day, $year) - 1;
        $end_time = tz_time($end_time, $server_timezone);

        if ($end_time > $start_time) {
            require_code('global3');
            cms_profile_start_for('Hook_cron_stats_preprocess_raw_data');

            push_query_limiting(false);
            disable_php_memory_limit();

            require_code('stats');
            require_lang('stats');

            $hook_obs = find_all_hooks('modules', 'admin_stats');
            foreach (array_keys($hook_obs) as $hook_name) {
                preprocess_raw_data_for($hook_name, $start_time, $end_time);
            }

            set_value('stats__last_day_processed', $today, true);

            // Send KPI notifications...

            send_kpi_notifications();

            pop_query_limiting();

            cms_profile_end_for('Hook_cron_stats_preprocess_raw_data');
        }
    }
}
