<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    shopping
 */

/**
 * Hook class.
 */
class Hook_privacy_shopping extends Hook_privacy_base
{
    /**
     * Find privacy details.
     *
     * @return ?array A map of privacy details in a standardised format (null: disabled)
     */
    public function info() : ?array
    {
        if (!addon_installed('shopping')) {
            return null;
        }

        require_lang('shopping');

        return [
            'label' => 'shopping:SHOPPING',

            'description' => 'shopping:DESCRIPTION_PRIVACY_SHOPPING',

            'cookies' => [
            ],

            'positive' => [
            ],

            'general' => [
                [
                    'heading' => do_lang('INFORMATION_STORAGE'),
                    'action' => do_lang_tempcode('PRIVACY_ACTION_shopping_sessions'),
                    'reason' => do_lang_tempcode('PRIVACY_REASON_shopping_sessions'),
                ],
            ],

            'database_records' => [
                'shopping_logging' => [
                    'timestamp_field' => 'l_date_and_time',
                    'retention_days' => intval(get_option('website_activity_store_time')),
                    'retention_handle_method' => PRIVACY_METHOD__DELETE,
                    'owner_id_field' => 'l_member_id',
                    'additional_member_id_fields' => [],
                    'ip_address_fields' => ['l_ip_address'],
                    'email_fields' => [],
                    'username_fields' => [],
                    'file_fields' => [],
                    'additional_anonymise_fields' => ['l_session_id'],
                    'extra_where' => null,
                    'removal_default_handle_method' => PRIVACY_METHOD__DELETE,
                    'removal_default_handle_method_member_override' => null,
                    'allowed_handle_methods' => PRIVACY_METHOD__DELETE,
                ],
                'shopping_cart' => [
                    'timestamp_field' => 'add_time',
                    'retention_days' => null,
                    'retention_handle_method' => PRIVACY_METHOD__LEAVE,
                    'owner_id_field' => 'ordering_member',
                    'additional_member_id_fields' => [],
                    'ip_address_fields' => [],
                    'email_fields' => [],
                    'username_fields' => [],
                    'file_fields' => [],
                    'additional_anonymise_fields' => ['session_id'],
                    'extra_where' => null,
                    'removal_default_handle_method' => PRIVACY_METHOD__DELETE,
                    'removal_default_handle_method_member_override' => null,
                    'allowed_handle_methods' => PRIVACY_METHOD__ANONYMISE | PRIVACY_METHOD__DELETE,
                ],
                'shopping_orders' => [
                    'timestamp_field' => 'add_date',
                    'retention_days' => null,
                    'retention_handle_method' => PRIVACY_METHOD__LEAVE,
                    'owner_id_field' => 'member_id',
                    'additional_member_id_fields' => [],
                    'ip_address_fields' => [],
                    'email_fields' => [],
                    'username_fields' => [],
                    'file_fields' => [],
                    'additional_anonymise_fields' => ['session_id'],
                    'extra_where' => db_string_not_equal_to('order_status', 'payment_received') . ' AND ' . db_string_not_equal_to('order_status', 'onhold'), // Prevent loss of orders in progress
                    'removal_default_handle_method' => PRIVACY_METHOD__ANONYMISE,
                    'removal_default_handle_method_member_override' => null,
                    'allowed_handle_methods' => PRIVACY_METHOD__ANONYMISE | PRIVACY_METHOD__DELETE,
                ],
            ],
        ];
    }

    /**
     * Serialise a row.
     *
     * @param  ID_TEXT $table_name Table name
     * @param  array $row Row raw from the database
     * @return array Row in a cleanly serialised format
     */
    public function serialise(string $table_name, array $row) : array
    {
        $ret = parent::serialise($table_name, $row);

        switch ($table_name) {
            case 'shopping_orders':
                $ret += [
                    'details' => $GLOBALS['SITE_DB']->query_select('shopping_order_details', ['*'], ['p_order_id' => $row['id']]),
                    'addresses' => $GLOBALS['SITE_DB']->query_select('ecom_trans_addresses', ['*'], ['a_txn_id' => $row['txn_id']]),
                ];
                break;
        }

        return $ret;
    }

    /**
     * Delete a row.
     *
     * @param  ID_TEXT $table_name Table name
     * @param  array $table_details Details of the table from the info function
     * @param  array $row Row raw from the database
     */
    public function delete(string $table_name, array $table_details, array $row)
    {
        switch ($table_name) {
            case 'shopping_orders':
                $GLOBALS['SITE_DB']->query_delete('shopping_order_details', ['p_order_id' => $row['id']]);
                $GLOBALS['SITE_DB']->query_delete('shopping_orders', ['id' => $row['id']], '', 1);
                break;

            default:
                parent::delete($table_name, $table_details, $row);
                break;
        }
    }
}
