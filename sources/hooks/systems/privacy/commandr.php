<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    commandr
 */

/**
 * Hook class.
 */
class Hook_privacy_commandr extends Hook_privacy_base
{
    /**
     * Find privacy details.
     *
     * @return ?array A map of privacy details in a standardised format (null: disabled)
     */
    public function info() : ?array
    {
        if (!addon_installed('commandr')) {
            return null;
        }

        return [
            'label' => 'commandr:COMMANDR',

            'description' => 'NA', // No personal database data

            'cookies' => [
                /*'commandr_*' => $GLOBALS['FORUM_DRIVER']->is_super_admin(get_member()) ? null : [    Silly to include this
                    'reason' => 'Your current Commandr system environment',
                ],*/
            ],

            'positive' => [
            ],

            'general' => [
            ],

            'database_records' => [
            ],
        ];
    }
}
