<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    sms
 */

/**
 * Hook class.
 */
class Hook_privacy_sms extends Hook_privacy_base
{
    /**
     * Find privacy details.
     *
     * @return ?array A map of privacy details in a standardised format (null: disabled)
     */
    public function info() : ?array
    {
        if (!addon_installed('sms')) {
            return null;
        }

        return [
            'label' => 'sms:SMS',

            'description' => 'sms:DESCRIPTION_PRIVACY_SMS',

            'cookies' => [
            ],

            'positive' => [
            ],

            'general' => [
            ],

            'database_records' => [
                'sms_log' => [
                    'timestamp_field' => 's_time',
                    'retention_days' => intval(get_option('email_log_store_time')),
                    'retention_handle_method' => PRIVACY_METHOD__DELETE,
                    'owner_id_field' => 's_member_id',
                    'additional_member_id_fields' => [],
                    'ip_address_fields' => ['s_trigger_ip_address'],
                    'email_fields' => [],
                    'username_fields' => [],
                    'file_fields' => [],
                    'additional_anonymise_fields' => [],
                    'extra_where' => null,
                    'removal_default_handle_method' => PRIVACY_METHOD__DELETE,
                    'removal_default_handle_method_member_override' => null,
                    'allowed_handle_methods' => PRIVACY_METHOD__ANONYMISE | PRIVACY_METHOD__DELETE,
                ],
            ],
        ];
    }
}
