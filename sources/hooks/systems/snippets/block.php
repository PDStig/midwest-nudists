<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/**
 * Hook class.
 */
class Hook_snippet_block
{
    /**
     * Run function for snippet hooks. Generates HTML to insert into a page using AJAX.
     *
     * @return Tempcode The snippet
     */
    public function run() : object
    {
        $sup = get_param_string('block_map_sup', '', INPUT_FILTER_GET_COMPLEX);
        $_map = get_param_string('block_map', false, INPUT_FILTER_GET_COMPLEX);
        if ($sup != '') {
            $_map .= ',' . $sup;
        }

        require_code('blocks');

        $map = comma_list_str_to_arr($_map);

        if (!array_key_exists('block', $map)) {
            return new Tempcode();
        }

        $pass = false;

        $_safelisted_blocks = get_value('safelisted_blocks');
        if (!empty($_safelisted_blocks)) {
            $safelisted_blocks = explode(',', $_safelisted_blocks);
            if (in_array($map['block'], $safelisted_blocks)) {
                $pass = true;
            }
        }

        if (!$pass) {
            $auth_key = get_param_integer('auth_key');

            // Check permissions
            $test = $GLOBALS['SITE_DB']->query_select_value_if_there('temp_block_permissions', 'p_block_constraints', ['p_session_id' => is_guest() ? '' : get_session_id(), 'id' => $auth_key]);
            if (($test === null) || (!block_signature_check(comma_list_str_to_arr($test), $map))) {
                require_lang('permissions');
                return do_template('RED_ALERT', ['_GUID' => '1d0d419810c75eb0b1e6a195e02e24fc', 'TEXT' => do_lang_tempcode('ACCESS_DENIED__ACCESS_DENIED', escape_html($map['block']))]);
            }
        }

        // Cleanup
        if (mt_rand(0, 100) == 1) {
            cms_register_shutdown_function_safe(function () {
                if (!$GLOBALS['SITE_DB']->table_is_locked('temp_block_permissions')) {
                    global $SITE_INFO;

                    $sql = 'DELETE FROM ' . get_table_prefix() . 'temp_block_permissions WHERE ';
                    $expiry_time = 60.0 * 60.0 * floatval(get_option('session_expiry_time'));
                    $sql .= db_string_not_equal_to('p_session_id', '') . ' AND p_time<' . strval(time() - $expiry_time);
                    $sql .= ' OR ';
                    if (!empty($SITE_INFO['static_caching_hours'])) {
                        $expiry_time = floatval($SITE_INFO['static_caching_hours']) * 60.0;
                    }
                    $sql .= db_string_equal_to('p_session_id', '') . ' AND p_time<' . strval(time() - $expiry_time);
                    $sql .= ' AND NOT EXISTS(SELECT * FROM ' . get_table_prefix() . 'sessions WHERE the_session=p_session_id)';
                    $GLOBALS['SITE_DB']->query($sql, 500/*to reduce lock times*/, 0, true); // Errors suppressed in case DB write access broken
                }
            });
        }

        $self_url = get_param_string('self_url', null, INPUT_FILTER_URL_GENERAL);
        if ($self_url !== null) {
            list($zone, $attributes) = page_link_decode(url_to_page_link($self_url));
            list($old_get, $old_zone, $old_current_script) = set_execution_context(
                $attributes,
                $zone,
                'index',
                false,
                null,
                false
            );
        }

        // We need to minimise the dependency stuff that comes out, we don't need any default values
        push_output_state(false, true);

        if (get_param_integer('raw', 0) == 1) {
            $map['raw'] = '1';
        }

        // Cleanup dependencies that will already have been handled
        global $CSSS, $JAVASCRIPTS;
        unset($CSSS['global']);
        unset($CSSS['no_cache']);
        unset($JAVASCRIPTS['global']);
        unset($JAVASCRIPTS['staff']);

        // And, go
        $out = new Tempcode();
        $_eval = do_block($map['block'], $map);
        $eval = $_eval->evaluate();
        if (get_param_integer('no_web_resources', 0) == 0) {
            $out->attach(symbol_tempcode('CSS_TEMPCODE'));
        }
        $out->attach($eval);
        if (get_param_integer('no_web_resources', 0) == 0) {
            $out->attach(symbol_tempcode('JS_TEMPCODE'));
        }

        if ($self_url !== null) {
            set_execution_context(
                $old_get,
                $old_zone,
                $old_current_script,
                false
            );
        }

        return $out;
    }
}
