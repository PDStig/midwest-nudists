<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    stats
 */

/**
 * Hook class.
 */
class Hook_snippet_stats_event
{
    /**
     * Run function for snippet hooks. Generates HTML to insert into a page using AJAX.
     *
     * @return Tempcode The snippet
     */
    public function run() : object
    {
        if (!addon_installed('stats')) {
            warn_exit(do_lang_tempcode('MISSING_ADDON', escape_html('stats')));
        }

        require_code('stats');

        log_stats_event(get_param_string('event', false, INPUT_FILTER_GET_COMPLEX));

        return new Tempcode();
    }
}
