<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    catalogues
 */

/**
 * Hook class.
 */
class Hook_block_ui_renderers_catalogues
{
    /**
     * See if a particular block parameter's UI input can be handled as a hidden field.
     *
     * @param  ID_TEXT $block The block
     * @param  ID_TEXT $parameter The parameter of the block
     * @param  boolean $has_default Whether there is a default value for the field, due to this being an edit
     * @param  string $default Default value for field
     * @return ?Tempcode Rendered field (null: not handled)
     */
    public function render_block_ui_parameter_hidden(string $block, string $parameter, bool $has_default, string $default) : ?object
    {
        if (!addon_installed('catalogues')) {
            return null;
        }

        return null;
    }

    /**
     * See if a particular block parameter's UI input can be rendered by this.
     *
     * @param  ID_TEXT $block The block
     * @param  ID_TEXT $parameter The parameter of the block
     * @param  boolean $has_default Whether there is a default value for the field, due to this being an edit
     * @param  string $default Default value for field
     * @param  string $description Field description
     * @return ?Tempcode Rendered field (null: not handled)
     */
    public function render_block_ui_parameter(string $block, string $parameter, bool $has_default, string $default, string $description) : ?object
    {
        if (!addon_installed('catalogues')) {
            return null;
        }

        if (($parameter == 'param') && (in_array($block, ['main_contact_catalogues', 'main_catalogues_form']))) {
            require_code('catalogues');
            $structured_list = create_selection_list_catalogues($default);

            return form_input_list(titleify($parameter), escape_html($description), $parameter, $structured_list, null, false, false);
        }

        if (($parameter == 'sort') && (in_array($block, ['main_cc_embed']))) {
            $max_field_count = $GLOBALS['SITE_DB']->query_select_value('catalogue_fields', 'COUNT(*) AS cnt', [], 'GROUP BY c_name ORDER BY cnt DESC');

            require_lang('fields');

            $list = new Tempcode();
            $order_by_fields = ['' => do_lang('DEFAULT'), 'add_date' => do_lang('ADDED'), 'compound_rating' => do_lang('POPULARITY'), 'average_rating' => do_lang('RATING'), 'fixed_random' => do_lang('RANDOM')];
            for ($i = 1; $i <= $max_field_count; $i++) {
                $order_by_fields[$i] = do_lang('CATALOGUE_FIELD') . ' #' . strval($i);
            }
            foreach ($order_by_fields as $order_by => $order_by_label) {
                if (!is_string($order_by)) {
                    $order_by = strval($order_by);
                }

                if ($order_by == '') {
                    $selection_value = '';
                    $list->attach(form_input_list_entry($selection_value, $selection_value == $default, $order_by_label));
                } else {
                    foreach (['ASC' => do_lang('ASCENDING'), 'DESC' => do_lang('DESCENDING')] as $order_by_direction => $order_by_direction_label) {
                        $selection_value = $order_by . ' ' . $order_by_direction;
                        $list->attach(form_input_list_entry($selection_value, $selection_value == $default, $order_by_label . ' ' . $order_by_direction_label));
                    }
                }
            }

            return form_input_list(titleify($parameter), escape_html($description), $parameter, $list, null, false, false);
        }

        if ((($default == '') || (is_numeric($default))) && ($parameter == 'param') && (in_array($block, ['main_cc_embed']))) {
            $num_categories = $GLOBALS['SITE_DB']->query_select_value('catalogue_categories', 'COUNT(*)');
            $num_categories_top = $GLOBALS['SITE_DB']->query_select_value('catalogue_categories', 'COUNT(*)', ['cc_parent_id' => null]);
            if (($num_categories_top < 300) && ((!$has_default) || ($num_categories < 300))) { // catalogue category
                $list = new Tempcode();
                $structured_list = new Tempcode();
                $category_map = [];
                $categories = $GLOBALS['SITE_DB']->query_select('catalogue_categories', ['id', 'cc_title', 'c_name'], ($num_categories >= 300) ? ['cc_parent_id' => null] : [], 'ORDER BY ' . $GLOBALS['SITE_DB']->translate_field_ref('cc_title'));
                foreach ($categories as $cat) {
                    if (substr($cat['c_name'], 0, 1) == '_') {
                        continue;
                    }

                    if(!isset($category_map[$cat['c_name']])) {
                        $category_map[$cat['c_name']] = [];
                    }

                    $category_map[$cat['c_name']][] = $cat;
                }
                ksort($category_map);
                foreach ($category_map as $m_catalogue => $m_categories) {
                    $list = new Tempcode();
                    foreach ($m_categories as $m_category) {
                        $list->attach(form_input_list_entry(strval($m_category['id']), $has_default && strval($m_category['id']) == $default, get_translated_text($m_category['cc_title'])));
                    }
                    $structured_list->attach(form_input_list_group($m_catalogue, $list));
                }
                return form_input_list(titleify($parameter), escape_html($description), $parameter, $structured_list, null, false, false);
            }
        }

        return null;
    }
}
