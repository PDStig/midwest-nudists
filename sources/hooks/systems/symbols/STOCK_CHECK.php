<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    shopping
 */

/**
 * Hook class.
 */
class Hook_symbol_STOCK_CHECK
{
    /**
     * Run function for symbol hooks. Searches for tasks to perform.
     *
     * @param  array $param Symbol parameters
     * @return string Result
     */
    public function run(array $param) : string
    {
        if (!addon_installed('shopping')) {
            return '';
        }

        $value = '';

        if (!@cms_empty_safe($param[0])) {
            $type_code = $param[0];

            require_code('ecommerce');

            list(, $product_object) = find_product_details($type_code);
            if (($product_object !== null) && (method_exists($product_object, 'get_available_quantity'))) {
                $available_quantity = $product_object->get_available_quantity($type_code);
            } else {
                $available_quantity = 1;
            }
            $value = ($available_quantity === null) ? '' : strval($available_quantity);
        }

        return $value;
    }
}
