<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    shopping
 */

/**
 * Hook class.
 */
class Hook_symbol_CART_LINK
{
    /**
     * Run function for symbol hooks. Searches for tasks to perform.
     *
     * @param  array $param Symbol parameters
     * @return string Result
     */
    public function run(array $param) : string
    {
        if (!addon_installed('shopping')) {
            return '';
        }

        $value = '';

        require_lang('shopping');

        $cart_url = build_url(['page' => 'shopping', 'type' => 'browse'], get_module_zone('shopping'));

        $where = [];
        if (is_guest()) {
            $where['session_id'] = get_session_id();
        } else {
            $where['ordering_member'] = get_member();
        }
        $item_count = $GLOBALS['SITE_DB']->query_select_value('shopping_cart', 'COUNT(*)', $where);

        if ($item_count > 0) {
            $title = do_lang_tempcode('BUTTON_CART_ITEMS', strval($item_count));
        } else {
            $title = do_lang_tempcode('BUTTON_CART_EMPTY');
        }

        $_value = do_template('ECOM_CART_LINK', ['_GUID' => '46ae00c8a605b84fee1b1c68fc57cd32', 'URL' => $cart_url, 'ITEMS' => strval($item_count), 'TITLE' => $title]);

        $value = $_value->evaluate();
        return $value;
    }
}
