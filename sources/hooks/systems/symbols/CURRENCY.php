<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    ecommerce
 */

/**
 * Hook class.
 */
class Hook_symbol_CURRENCY
{
    /**
     * Run function for symbol hooks. Searches for tasks to perform.
     *
     * @param  array $param Symbol parameters
     * @return string Result
     */
    public function run(array $param) : string
    {
        if (!addon_installed('ecommerce')) {
            return '';
        }

        if (isset($param[0])) {
            require_code('currency');

            $amount = float_unformat($param[0]);
            $from_currency = (!empty($param[1])) ? $param[1] : get_option('currency');
            $to_currency = (!empty($param[2])) ? $param[2] : null;
            $display_method = ((isset($param[3])) && (is_numeric($param[3]))) ? intval($param[3]) : CURRENCY_DISPLAY_TEMPLATED;

            if (!$GLOBALS['STATIC_TEMPLATE_TEST_MODE']) { // Normal operation
                $value = currency_convert($amount, $from_currency, $to_currency, $display_method);
            } else {
                $value = '123';
            }
        } else {
            $value = get_option('currency');
        }

        return $value;
    }
}
