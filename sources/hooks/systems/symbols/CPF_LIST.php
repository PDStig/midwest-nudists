<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Hook class.
 */
class Hook_symbol_CPF_LIST
{
    /**
     * Run function for symbol hooks. Searches for tasks to perform.
     *
     * @param  array $param Symbol parameters
     * @return string Result
     */
    public function run(array $param) : string
    {
        $value = '';

        if ((!@cms_empty_safe($param[0])) && (get_forum_type() == 'cns')) {
            $delimiter = (!empty($param[1])) ? $param[1] : ',';
            $include_label_syntax = isset($param[2]) ? ($param[2] === '1') : true;

            static $cache = [];
            if (isset($cache[$param[0]])) {
                return $cache[$param[0]];
            }

            if (($param[0] == 'usergroup') || ($param[0] == 'm_primary_group|gm_group_id') || ($param[0] == 'm_primary_group') || ($param[0] == 'gm_group_id')) {
                $where = '1=1';
                if (has_privilege(get_member(), 'see_hidden_groups')) {
                    $members_groups = $GLOBALS['CNS_DRIVER']->get_members_groups(get_member());
                    $where .= ' AND (g_hidden=0 OR g.id IN (' . implode(',', array_map('strval', $members_groups)) . '))';
                }

                $group_count = $GLOBALS['FORUM_DB']->query_value_if_there('SELECT COUNT(*) FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_groups g');
                $where_extended = $where;
                if ($group_count > 200) {
                    $where_extended .= ' AND g_is_private_club=0';
                }
                $_m = $GLOBALS['FORUM_DB']->query('SELECT id,g_name,g_order FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_groups g WHERE ' . $where_extended . ' ORDER BY g_order');
                foreach ($_m as $i => $m) {
                    $_m[$i]['text'] = get_translated_text($m['g_name'], $GLOBALS['FORUM_DB']);
                }
                sort_maps_by($_m, 'text', false, true);
                foreach ($_m as $m) {
                    if ($m['id'] == db_get_first_id()) {
                        continue;
                    }

                    if ($value != '') {
                        $value .= $delimiter;
                    }
                    $value .= strval($m['id']) . '=' . $m['text'];
                }

                $cache[$param[0]] = $value;
                return $value;
            }

            require_code('cns_members');
            $cpf_id = find_cpf_field_id($param[0]);
            if ($cpf_id !== null) {
                $test = $GLOBALS['FORUM_DB']->query_select('f_custom_fields', ['cf_default', 'cf_type'], ['id' => $cpf_id]);
                if (isset($test[0])) {
                    switch ($test[0]['cf_type']) {
                        case 'list':
                        case 'list_multi':
                            $bits = explode('|', $test[0]['cf_default']);
                            cms_mb_sort($bits, SORT_FLAG_CASE | SORT_NATURAL);
                            foreach ($bits as $k) {
                                if (trim($k, '-') == '' && $value == '') {
                                    continue;
                                }
                                if ($value != '') {
                                    $value .= $delimiter;
                                }
                                if ($include_label_syntax) {
                                    $value .= $k . '=';
                                }
                                $value .= $k;
                            }
                            break;

                        case 'tick':
                            $value = '=|0=' . do_lang('NO') . '|1=' . do_lang('YES');
                            break;
                    }
                }
            }

            $cache[$param[0]] = $value;
        }

        return $value;
    }
}
