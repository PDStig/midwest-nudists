<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    cns_forum
 */

/**
 * Hook class.
 */
class Hook_rss_cns_topicview
{
    /**
     * Check if the given member has access to view this feed.
     *
     * @param  MEMBER $member_id The member trying to access this feed
     * @return boolean Whether the member has access
     */
    public function has_access(int $member_id) : bool
    {
        if (!addon_installed('cns_forum')) {
            return false;
        }
        if (get_forum_type() != 'cns') {
            return false;
        }

        if (!has_actual_page_access($member_id, 'forumview')) {
            return false;
        }

        return true;
    }

    /**
     * Run function for RSS hooks.
     *
     * @param  string $_filters A list of categories we accept from
     * @param  TIME $cutoff Cutoff time, before which we do not show results from
     * @param  string $prefix Prefix that represents the template set we use
     * @set RSS_ ATOM_
     * @param  string $date_string The standard format of date to use for the syndication type represented in the prefix
     * @param  integer $max The maximum number of entries to return, ordering by date
     * @return ?array A pair: The main syndication section, and a title (null: error)
     */
    public function run(string $_filters, int $cutoff, string $prefix, string $date_string, int $max) : ?array
    {
        if (!$this->has_access(get_member())) {
            return null;
        }

        require_code('cns_topics');

        $filters = selectcode_to_sqlfragment($_filters, 'p_topic_id', 'f_forums', 'f_parent_forum_id', 'p_cache_forum_id', 'id', true, true, $GLOBALS['FORUM_DB']);

        $cutoff = max($cutoff, time() - 60 * 60 * 24 * 60);

        if (!is_guest()) {
            $filters .= ' AND (p_posting_member<>' . strval(get_member()) . ')';
        }

        $rows = $GLOBALS['FORUM_DB']->query('SELECT * FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_posts WHERE p_time>' . strval($cutoff) . (((!has_privilege(get_member(), 'see_not_validated')) && (addon_installed('validation'))) ? ' AND p_validated=1 ' : '') . ' AND ' . $filters . ' ORDER BY p_time DESC,id DESC', $max, 0, false, true);
        $categories = list_to_map('id', $GLOBALS['FORUM_DB']->query('SELECT * FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_topics WHERE t_cache_last_time>' . strval($cutoff), $max));

        $content = new Tempcode();
        foreach ($rows as $row) {
            if (!array_key_exists($row['p_topic_id'], $categories)) {
                continue;
            }
            $category = $categories[$row['p_topic_id']]['t_cache_first_title'];
            if ((cns_may_access_topic($row['p_topic_id'])) && (($row['p_whisper_to_member'] === null) || ($row['p_whisper_to_member'] == get_member()))) {
                $id = strval($row['id']);
                $author = $row['p_poster_name_if_guest'];

                $news_date = date($date_string, $row['p_time']);
                $edit_date = ($row['p_last_edit_time'] === null) ? '' : date($date_string, $row['p_last_edit_time']);
                if ($edit_date == $news_date) {
                    $edit_date = '';
                }

                $news_title = xmlentities($row['p_title']);
                $_summary = get_translated_tempcode('f_posts', $row, 'p_post', $GLOBALS['FORUM_DB']);
                $summary = xmlentities($_summary->evaluate());
                $news = '';

                $category_raw = strval($row['p_topic_id']);

                $view_url = build_url(['page' => 'topicview', 'type' => 'findpost', 'id' => $row['id']], get_module_zone('forumview'), [], false, false, true);

                if ($prefix == 'RSS_') {
                    $if_comments = do_template('RSS_ENTRY_COMMENTS', ['_GUID' => 'ed06bc8f174a5427e1789820666fdd81', 'COMMENT_URL' => $view_url, 'ID' => $id], null, false, null, '.xml', 'xml');
                } else {
                    $if_comments = new Tempcode();
                }

                $content->attach(do_template($prefix . 'ENTRY', ['VIEW_URL' => $view_url, 'SUMMARY' => $summary, 'EDIT_DATE' => $edit_date, 'IF_COMMENTS' => $if_comments, 'TITLE' => $news_title, 'CATEGORY_RAW' => $category_raw, 'CATEGORY' => $category, 'AUTHOR' => $author, 'ID' => $id, 'NEWS' => $news, 'DATE' => $news_date], null, false, null, '.xml', 'xml'));
            }
        }

        require_lang('cns');
        return [$content, do_lang('FORUM_TOPICS')];
    }
}
