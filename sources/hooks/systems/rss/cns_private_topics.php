<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    cns_forum
 */

/**
 * Hook class.
 */
class Hook_rss_cns_private_topics
{
    /**
     * Check if the given member has access to view this feed.
     *
     * @param  MEMBER $member_id The member trying to access this feed
     * @return boolean Whether the member has access
     */
    public function has_access(int $member_id) : bool
    {
        if (!addon_installed('cns_forum')) {
            return false;
        }
        if (get_forum_type() != 'cns') {
            return false;
        }

        if (is_guest($member_id)) {
            return false;
        }
        if (!has_actual_page_access($member_id, 'forumview')) {
            return false;
        }

        return true;
    }

    /**
     * Run function for RSS hooks.
     *
     * @param  string $_filters A list of categories we accept from
     * @param  TIME $cutoff Cutoff time, before which we do not show results from
     * @param  string $prefix Prefix that represents the template set we use
     * @set RSS_ ATOM_
     * @param  string $date_string The standard format of date to use for the syndication type represented in the prefix
     * @param  integer $max The maximum number of entries to return, ordering by date
     * @return ?array A pair: The main syndication section, and a title (null: error)
     */
    public function run(string $_filters, int $cutoff, string $prefix, string $date_string, int $max) : ?array
    {
        if (!$this->has_access(get_member())) {
            return null;
        }

        require_code('cns_notifications');
        $rows = cns_get_pp_rows($max);

        $content = new Tempcode();
        foreach ($rows as $row) {
            $id = strval($row['p_id']);
            $author = $row['t_cache_first_username'];

            $news_date = date($date_string, $row['t_cache_first_time']);
            $edit_date = date($date_string, $row['t_cache_last_time']);
            if ($edit_date == $news_date) {
                $edit_date = '';
            }

            if ($row['t_cache_first_time'] < $cutoff) {
                continue;
            }

            $post_rows = $GLOBALS['FORUM_DB']->query_select('f_posts', ['*'], ['id' => $row['t_cache_first_post_id']], '', 1);
            $row += $post_rows[0];

            $news_title = xmlentities($row['t_cache_first_title']);
            $post_row = db_map_restrict($row, ['id', 'p_post'], ['id' => 't_cache_first_post_id']);
            $_summary = get_translated_tempcode('f_posts', $post_row, 'p_post', $GLOBALS['FORUM_DB']);
            $summary = xmlentities($_summary->evaluate());
            $news = '';

            $category = do_lang('NA');
            $category_raw = '';

            $view_url = build_url(['page' => 'topicview', 'id' => $row['t_id']], get_module_zone('forumview'), [], false, false, false, 'post_' . $id);

            if ($prefix == 'RSS_') {
                $if_comments = do_template('RSS_ENTRY_COMMENTS', ['_GUID' => '448f736ecf0154960177c131dde76125', 'COMMENT_URL' => $view_url, 'ID' => $id], null, false, null, '.xml', 'xml');
            } else {
                $if_comments = new Tempcode();
            }

            $content->attach(do_template($prefix . 'ENTRY', ['VIEW_URL' => $view_url, 'SUMMARY' => $summary, 'EDIT_DATE' => $edit_date, 'IF_COMMENTS' => $if_comments, 'TITLE' => $news_title, 'CATEGORY_RAW' => $category_raw, 'CATEGORY' => $category, 'AUTHOR' => $author, 'ID' => $id, 'NEWS' => $news, 'DATE' => $news_date], null, false, null, '.xml', 'xml'));
        }

        require_lang('cns');
        return [$content, do_lang('PRIVATE_TOPICS')];
    }
}
