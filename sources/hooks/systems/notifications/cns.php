<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Hook class.
 */
class Hook_notification_cns extends Hook_Notification
{
    /**
     * Find whether a handled notification code supports categories.
     * (Content types, for example, will define notifications on specific categories, not just in general. The categories are interpreted by the hook and may be complex. E.g. it might be like a regexp match, or like FORUM:3 or TOPIC:100).
     *
     * @param  ID_TEXT $notification_code Notification code
     * @return boolean Whether it does
     */
    public function supports_categories(string $notification_code) : bool
    {
        if ($notification_code == 'cns_member_joined_group') {
            return true;
        }
        return false;
    }

    /**
     * Standard function to create the standardised category tree.
     *
     * @param  ID_TEXT $notification_code Notification code
     * @param  ?ID_TEXT $id The ID of where we're looking under (null: N/A)
     * @return array Tree structure
     */
    public function create_category_tree(string $notification_code, ?string $id) : array
    {
        $page_links = [];

        if ($notification_code == 'cns_member_joined_group') {
            $where = '1=1';
            if (has_privilege(get_member(), 'see_hidden_groups')) {
                $members_groups = $GLOBALS['CNS_DRIVER']->get_members_groups(get_member());
                $where .= ' AND (g_hidden=0 OR g.id IN (' . implode(',', array_map('strval', $members_groups)) . '))';
            }

            $types = $GLOBALS['FORUM_DB']->query('SELECT id,g_name FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_groups g WHERE ' . $where);
            foreach ($types as $type) {
                $page_links[] = [
                    'id' => $type['id'],
                    'title' => get_translated_text($type['g_name'], $GLOBALS['FORUM_DB']),
                ];
            }
            sort_maps_by($page_links, 'title', false, true);
        }

        return $page_links;
    }

    /**
     * Find a bitmask of settings (e-mail, SMS, etc) a notification code supports for listening on.
     *
     * @param  ID_TEXT $notification_code Notification code
     * @return integer Allowed settings
     */
    public function allowed_settings(string $notification_code) : int
    {
        return A__ALL;
    }

    /**
     * Find the initial setting that members have for a notification code (only applies to the member_could_potentially_enable members).
     *
     * @param  ID_TEXT $notification_code Notification code
     * @param  ?SHORT_TEXT $category The category within the notification code (null: none)
     * @param  MEMBER $member_id The member the notification would be for
     * @return integer Initial setting
     */
    public function get_initial_setting(string $notification_code, ?string $category, int $member_id) : int
    {
        if ($notification_code == 'cns_birthday') {
            return A_NA;
        }
        if ($notification_code == 'cns_member_joined_group') {
            return A_NA;
        }
        if ($notification_code == 'cns_new_member') {
            return A_NA;
        }
        return A__STATISTICAL;
    }

    /**
     * Get a list of all the notification codes this hook can handle.
     * (Addons can define hooks that handle whole sets of codes, so hooks are written so they can take wide authority).
     *
     * @return array List of codes (mapping between code names, and a pair: section and labelling for those codes)
     */
    public function list_handled_codes() : array
    {
        if (get_forum_type() != 'cns') {
            return [];
        }

        $list = [];
        if (get_option('enable_birthdays') != '0') {
            $list['cns_birthday'] = [do_lang('MEMBERS'), do_lang('cns:NOTIFICATION_TYPE_cns_birthday')];
        }
        $list['cns_group_join_request'] = [do_lang('USERGROUPS'), do_lang('cns:NOTIFICATION_TYPE_cns_group_join_request')];
        $list['cns_group_status'] = [do_lang('USERGROUPS'), do_lang('cns:NOTIFICATION_TYPE_cns_group_status')];
        $list['cns_member_joined_group'] = [do_lang('USERGROUPS'), do_lang('cns:NOTIFICATION_TYPE_cns_member_joined_group')];
        $list['cns_new_member'] = [do_lang('MEMBERS'), do_lang('cns:NOTIFICATION_TYPE_cns_new_member')];
        return $list;
    }

    /**
     * Find whether a member could enable this notification (i.e. have permission to).
     *
     * @param  ID_TEXT $notification_code Notification code
     * @param  MEMBER $member_id Member to check against
     * @param  ?SHORT_TEXT $category The category within the notification code (null: none)
     * @return boolean Whether they could
     */
    public function member_could_potentially_enable(string $notification_code, int $member_id, ?string $category = null) : bool
    {
        if (get_forum_type() != 'cns') {
            return false;
        }

        if ($notification_code == 'cns_birthday') {
            // Only members who are a leader of a usergroup should see this notification
            $is_usergroup_leader = $GLOBALS['FORUM_DB']->query_select_value_if_there('f_groups', 'id', ['g_group_lead_member' => $member_id]);
            return ($is_usergroup_leader !== null);
        }

        return parent::member_could_potentially_enable($notification_code, $member_id, $category);
    }

    /**
     * Get a list of members who have enabled this notification (i.e. have permission to AND have chosen to or are defaulted to).
     *
     * @param  ID_TEXT $notification_code Notification code
     * @param  ?SHORT_TEXT $category The category within the notification code (null: none)
     * @param  ?array $to_member_ids List of member IDs we are restricting to (null: no restriction). This effectively works as a intersection set operator against those who have enabled.
     * @param  ?integer $from_member_id The member ID doing the sending. Either a MEMBER or a negative number (e.g. A_FROM_SYSTEM_UNPRIVILEGED) (null: current member)
     * @param  integer $start Start position (for pagination)
     * @param  integer $max Maximum (for pagination)
     * @return array A pair: Map of members to their notification setting, and whether there may be more
     */
    public function list_members_who_have_enabled(string $notification_code, ?string $category = null, ?array $to_member_ids = null, ?int $from_member_id = null, int $start = 0, int $max = 300) : array
    {
        if ($notification_code == 'cns_birthday') {
            $members = $this->_all_members_who_have_enabled($notification_code, $category, $to_member_ids, $start, $max);
            $members = $this->_all_members_who_have_enabled_with_page_access($members, 'members', $notification_code, $category, $to_member_ids, $start, $max);
            return $members;
        }
        if ($notification_code == 'cns_group_join_request') {
            $_members = $this->_all_members_who_have_enabled($notification_code, $category, $to_member_ids, $start, $max);
            $_members = $this->_all_members_who_have_enabled_with_page_access($_members, 'groups', $notification_code, $category, $to_member_ids, $start, $max);

            $members = [];
            foreach ($_members as $member => $notifications) {
                $is_usergroup_leader = $GLOBALS['FORUM_DB']->query_select_value_if_there('f_groups', 'id', ['g_group_lead_member' => $member]);
                if ($is_usergroup_leader !== null) {
                    $members[$member] = $notifications;
                }
            }
            return $members;
        }
        if ($notification_code == 'cns_group_status') {
            $members = $this->_all_members_who_have_enabled($notification_code, $category, $to_member_ids, $start, $max);
            $members = $this->_all_members_who_have_enabled_with_page_access($members, 'groups', $notification_code, $category, $to_member_ids, $start, $max);
            return $members;
        }
        if ($notification_code == 'cns_member_joined_group') {
            list($members, $maybe_more) = $this->_all_members_who_have_enabled($notification_code, $category, $to_member_ids, $start, $max);
            list($members, $maybe_more) = $this->_all_members_who_have_enabled_with_page_access([$members, $maybe_more], 'groups', $notification_code, $category, $to_member_ids, $start, $max);

            if (is_numeric($category)) { // Filter if the group is hidden
                $hidden = $GLOBALS['FORUM_DB']->query_select_value('f_groups', 'g_hidden', ['id' => intval($category)]);

                if ($hidden == 1) {
                    $members_groups = $GLOBALS['CNS_DRIVER']->get_members_groups(get_member());

                    $members_new = [];
                    foreach ($members as $member_id => $setting) {
                        if ((has_privilege($member_id, 'see_hidden_groups')) || (in_array(intval($category), $members_groups))) {
                            $members_new[$member_id] = $setting;
                        }
                    }
                    $members = $members_new;
                }
            }

            return [$members, $maybe_more];
        }
        if ($notification_code == 'cns_new_member') {
            $members = $this->_all_members_who_have_enabled($notification_code, $category, $to_member_ids, $start, $max);
            $members = $this->_all_members_who_have_enabled_with_page_access($members, 'members', $notification_code, $category, $to_member_ids, $start, $max);
            return $members;
        }

        return parent::list_members_who_have_enabled($notification_code, $category, $to_member_ids, $from_member_id, $start, $max);
    }
}
