<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Hook class.
 */
class Hook_profiles_tabs_edit
{
    /**
     * Find whether this hook is active.
     *
     * @param  MEMBER $member_id_of The ID of the member who is being viewed
     * @param  MEMBER $member_id_viewing The ID of the member who is doing the viewing
     * @return boolean Whether this hook is active
     */
    public function is_active(int $member_id_of, int $member_id_viewing) : bool
    {
        if (is_guest($member_id_viewing)) {
            return false;
        }

        if (!(($member_id_of == $member_id_viewing) || (has_privilege($member_id_viewing, 'assume_any_member')) || (has_privilege($member_id_viewing, 'member_maintenance')))) {
            return false;
        }

        $hooks = find_all_hook_obs('systems', 'profiles_tabs_edit', 'Hook_profiles_tabs_edit_');
        foreach ($hooks as $ob) {
            if ($ob->is_active($member_id_of, $member_id_viewing)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Render function for profile tab hooks.
     *
     * @param  MEMBER $member_id_of The ID of the member who is being viewed
     * @param  MEMBER $member_id_viewing The ID of the member who is doing the viewing
     * @param  boolean $leave_to_ajax_if_possible Whether to leave the tab contents null, if this hook supports it, so that AJAX can load it later
     * @return array A tuple: The tab title, the tab contents, the suggested tab order, the icon
     */
    public function render_tab(int $member_id_of, int $member_id_viewing, bool $leave_to_ajax_if_possible = false) : array
    {
        $title = do_lang_tempcode('EDIT');

        require_lang('cns');
        require_css('cns');

        require_code('form_templates');

        push_query_limiting(false);

        global $SKIPPING_LABELS;
        $SKIPPING_LABELS = true;

        $order = 200;

        $only_tab = get_param_string('only_subtab', null);

        if (($leave_to_ajax_if_possible) && ($_SERVER['REQUEST_METHOD'] != 'POST')) {
            return [$title, null, $order, 'buttons/settings'];
        }

        $old_limit = cms_extend_time_limit(TIME_LIMIT_EXTEND__MODEST); // Raise time limit, as can be slow

        $tabs = [];

        $hooks = find_all_hooks('systems', 'profiles_tabs_edit');
        if (isset($hooks['settings'])) { // Editing must go first, so changes reflect in the renders of the tabs
            $hooks = ['settings' => $hooks['settings']] + $hooks;
        }
        foreach (array_keys($hooks) as $hook) {
            if (($only_tab === null) || (preg_match('#(^|,)' . preg_quote($hook, '#') . '(,|$)#', $only_tab) != 0)) {
                send_http_output_ping();

                require_code('hooks/systems/profiles_tabs_edit/' . filter_naughty_harsh($hook));
                $ob = object_factory('Hook_profiles_tabs_edit_' . filter_naughty_harsh($hook));
                if ($ob->is_active($member_id_of, $member_id_viewing)) {
                    $tab = $ob->render_tab($member_id_of, $member_id_viewing, $only_tab !== $hook && $leave_to_ajax_if_possible);

                    if ($tab === null) {
                        continue;
                    }

                    if ($only_tab === $hook) {
                        $title = $tab[0];
                    }

                    if (!array_key_exists(7, $tab)) {
                        $tab[7] = false;
                    }

                    $tab[8] = $hook;

                    $tabs[] = $tab;
                }
            }
        }

        cms_set_time_limit($old_limit);

        // Session ID check, if saving
        if ((!session_considered_confirmed()) && ((post_param_string('edit_password', '', INPUT_FILTER_PASSWORD) != '') || ($member_id_viewing != $member_id_of))) {
            if ((!empty($_POST)) && (!empty($tabs))) {
                access_denied('SESSION', '', true);
            }
        }

        if ($leave_to_ajax_if_possible) {
            return [$title, null, $order, 'buttons/settings'];
        }

        if (get_param_integer('keep_show_loading', 0) == 0) { // We only sort if not doing memory testing, as it makes it harder to debug
            sort_maps_by($tabs, 4, false, true);
        }
        $tabs = array_values($tabs); // Reindex, needed for lastness check

        $js_function_calls = [];

        $hidden = new Tempcode();

        $_tabs = [];
        $tab_first = true;
        foreach ($tabs as $i => $tab) {
            if ($tab === null) {
                continue;
            }

            if (is_array($tab[3])) {
                $js_function_calls = array_merge($js_function_calls, $tab[3]);
            }

            $tab_last = true;
            foreach ($tabs as $j => $tabj) {
                if ($j > $i) {
                    if ($tabj !== null) {
                        $tab_last = false;
                        break;
                    }
                }
            }

            $single_field = $tab[7];

            if (isset($tab[5])) {
                $hidden->attach($tab[5]);
            }

            $_tabs[] = [
                'TAB_TITLE' => $tab[0],
                'TAB_CODE' => $tab[8],
                'TAB_FIELDS' => $tab[1],
                'TAB_ICON' => $tab[6],
                'TAB_TEXT' => $tab[2],
                'TAB_FIRST' => $tab_first,
                'TAB_LAST' => $tab_last,
                'TAB_SINGLE_FIELD' => $single_field,
            ];

            $tab_first = false;
        }

        $url = build_url(['page' => '_SELF'], '_SELF', [], true, false, false/*,'tab--edit'  confusing, esp if was not on settings edit tab initially*/);

        $content = do_template('CNS_MEMBER_PROFILE_EDIT', [
            '_GUID' => '7a3e2cc210583fe4f3097af48b052351',
            'JS_FUNCTION_CALLS' => $js_function_calls,
            'HIDDEN' => $hidden,
            'URL' => $url,
            'SUBMIT_ICON' => 'buttons/save',
            'SUBMIT_NAME' => do_lang_tempcode('SAVE'),
            'SKIP_WEBSTANDARDS' => true,
            'TABS' => $_tabs,
        ]);

        return [$title, $content, $order, 'buttons/settings'];
    }
}
