<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    cns_forum
 */

/**
 * Hook class.
 */
class Hook_commandr_scheduled_publish_topic
{
    /**
     * Get information about this hook.
     *
     * @return ?array Map of hook details (null: addon not available)
     */
    public function info() : ?array
    {
        if (!addon_installed('cns_forum')) {
            return null;
        }

        return [
            'required_parameters' => 0,
        ];
    }

    /**
     * Run Commandr hook.
     *
     * @param  array $options The options with which the command was called
     * @param  string $_id Unspecified identifier for the resource behind this scheduled event
     * @param  array $parameters The json_decoded parameters passed with run_scheduled_action
     * @param  object $commandr_fs A reference to the Commandr filesystem object
     * @return array Array of stdcommand, stdhtml, stdout, and stderr responses
     */
    public function run(array $options, string $_id, array $parameters, object &$commandr_fs) : array
    {
        $id = intval($_id);

        $topic_rows = $GLOBALS['FORUM_DB']->query_select('f_topics t JOIN ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_posts p ON p.id=t.t_cache_first_post_id', ['t.*', 'p.p_posting_member'], ['t.id' => $id], '', 1);
        if (!array_key_exists(0, $topic_rows)) {
            return ['', '', do_lang('MISSING_RESOURCE'), ''];
        }

        $GLOBALS['FORUM_DB']->query_update('f_topics', ['t_cache_first_time' => time(), 't_validated' => 1], ['id' => $id], '', 1);

        $topic_row = $topic_rows[0];

        $forum_id = $topic_row['t_forum_id'];
        $title = $topic_row['t_cache_first_title'];
        $poster = $topic_row['p_posting_member'];

        if ($forum_id !== null) {
            require_code('users2');
            if ((has_actual_page_access(get_modal_user(), 'forumview')) && (has_category_access(get_modal_user(), 'forums', strval($forum_id)))) {
                require_code('syndication');
                syndicate_described_activity('cns:ACTIVITY_ADD_TOPIC', $title, '', '', '_SEARCH:topicview:browse:' . strval($id), '', '', 'cns_forum', 1, $poster);
            }
        }

        return ['', '', do_lang('SUCCESS'), ''];
    }
}
