<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    polls
 */

/**
 * Hook class.
 */
class Hook_page_groupings_polls
{
    /**
     * Run function for do_next_menu hooks. They find links to put on standard navigation menus of the system.
     *
     * @param  ?MEMBER $member_id Member ID to run as (null: current member)
     * @param  boolean $extensive_docs Whether to use extensive documentation tooltips, rather than short summaries
     * @return array List of tuple of links (page grouping, icon, do-next-style linking data), label, help (optional) and/or nulls
     */
    public function run(?int $member_id = null, bool $extensive_docs = false) : array
    {
        if (!addon_installed('polls')) {
            return [];
        }

        return [
            (get_value('hide_polls') === '1') ? null : (has_privilege(get_member(), 'submit_midrange_content', 'cms_polls') ? ['cms', 'menu/social/polls', ['cms_polls', ['type' => 'browse'], get_module_zone('cms_polls')], do_lang_tempcode('ITEMS_HERE', do_lang_tempcode('POLLS'), make_string_tempcode(escape_html(integer_format(intval($GLOBALS['SITE_DB']->query_select_value('poll', 'COUNT(*)')), 0)))), 'polls:DOC_POLLS'] : null),
            ['social', 'menu/social/polls', ['polls', [], get_module_zone('polls')], do_lang_tempcode('POLLS')],
        ];
    }
}
