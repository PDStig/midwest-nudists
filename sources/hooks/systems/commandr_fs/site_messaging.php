<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    site_messaging
 */

require_code('resource_fs');

/**
 * Hook class.
 */
class Hook_commandr_fs_site_messaging extends Resource_fs_base
{
    public $file_resource_type = 'site_message';

    /**
     * Standard Commandr-fs function for seeing how many resources are. Useful for determining whether to do a full rebuild.
     *
     * @param  ID_TEXT $resource_type The resource type
     * @return integer How many resources there are
     */
    public function get_resources_count(string $resource_type) : int
    {
        return $GLOBALS['SITE_DB']->query_select_value('site_messages', 'COUNT(*)');
    }

    /**
     * Standard Commandr-fs function for searching for a resource by label.
     *
     * @param  ID_TEXT $resource_type The resource type
     * @param  LONG_TEXT $label The resource label
     * @return array A list of resource IDs
     */
    public function find_resource_by_label(string $resource_type, string $label) : array
    {
        $_ret = $GLOBALS['SITE_DB']->query_select('site_messages', ['id'], ['title' => $label], 'ORDER BY id');
        $ret = [];
        foreach ($_ret as $r) {
            $ret[] = strval($r['id']);
        }
        return $ret;
    }

    /**
     * Whether the filesystem hook is active.
     *
     * @return boolean Whether it is
     */
    public function is_active() : bool
    {
        return (addon_installed('site_messaging'));
    }

    /**
     * Standard Commandr-fs date fetch function for resource-fs hooks. Defined when getting an edit date is not easy.
     *
     * @param  array $row Resource row (not full, but does contain the ID)
     * @param  ID_TEXT $category Parent category (blank: root / not applicable)
     * @return ?TIME The edit date or add date, whichever is higher (null: could not find one)
     */
    protected function _get_file_edit_date(array $row, string $category = '') : ?int
    {
        $query = 'SELECT MAX(date_and_time) FROM ' . get_table_prefix() . 'actionlogs WHERE ' . db_string_equal_to('param_a', strval($row['id'])) . ' AND  (' . db_string_equal_to('the_type', 'ADD_SITE_MESSAGE') . ' OR ' . db_string_equal_to('the_type', 'EDIT_SITE_MESSAGE') . ')';
        return $GLOBALS['SITE_DB']->query_value_if_there($query);
    }

    /**
     * Standard Commandr-fs add function for resource-fs hooks. Adds some resource with the given label and properties.
     *
     * @param  LONG_TEXT $filename Filename OR Resource label
     * @param  string $path The path (blank: root / not applicable)
     * @param  array $properties Properties (may be empty, properties given are open to interpretation by the hook but generally correspond to database fields)
     * @param  ?ID_TEXT $force_type Resource type to try to force (null: do not force)
     * @return ~ID_TEXT The resource ID (false: error, could not create via these properties / here)
     */
    public function file_add(string $filename, string $path, array $properties, ?string $force_type = null)
    {
        list($properties, $label) = $this->_file_magic_filter($filename, $path, $properties, $this->file_resource_type);

        require_code('site_messaging2');

        $submitter = $this->_default_property_member($properties, 'submitter');
        $title = $this->_default_property_str($properties, 'title');
        $message = $this->_default_property_str($properties, 'message');
        $type = $this->_default_property_str($properties, 'type');
        if ($type == '') {
            $type = 'inform';
        }
        $start_date_time = $this->_default_property_time_null($properties, 'start_date_time');
        $end_date_time = $this->_default_property_time_null($properties, 'end_date_time');
        $validated = $this->_default_property_int_null($properties, 'validated');
        if ($validated === null) {
            $validated = 1;
        }

        $id = add_site_message($title, $message, $type, $validated, $start_date_time, $end_date_time);

        if (isset($properties['groups'])) {
            table_from_portable_rows('site_messages_groups', $properties['groups'], ['message_id' => $id], TABLE_REPLACE_MODE_BY_EXTRA_FIELD_DATA);
        }
        if (isset($properties['pages'])) {
            table_from_portable_rows('site_messages_pages', $properties['pages'], ['message_id' => $id], TABLE_REPLACE_MODE_BY_EXTRA_FIELD_DATA);
        }

        $this->_resource_save_extend($this->file_resource_type, strval($id), $filename, $label, $properties);

        return strval($id);
    }

    /**
     * Standard Commandr-fs load function for resource-fs hooks. Finds the properties for some resource.
     *
     * @param  SHORT_TEXT $filename Filename
     * @param  string $path The path (blank: root / not applicable). It may be a wildcarded path, as the path is used for content-type identification only. Filenames are globally unique across a hook; you can calculate the path using ->search.
     * @return ~array Details of the resource (false: error)
     */
    public function file_load(string $filename, string $path)
    {
        list($resource_type, $resource_id) = $this->file_convert_filename_to_id($filename);

        $rows = $GLOBALS['SITE_DB']->query_select('site_messages', ['*'], ['id' => intval($resource_id)], '', 1);
        if (!array_key_exists(0, $rows)) {
            return false;
        }
        $row = $rows[0];

        $groups = table_to_portable_rows('site_messages_groups', [], ['message_id' => intval($resource_id)]);
        $pages = table_to_portable_rows('site_messages_pages', [], ['message_id' => intval($resource_id)]);

        $properties = [
            'submitter' => remap_resource_id_as_portable('member', $row['m_submitter']),
            'title' => $row['m_title'],
            'message' => get_translated_text($row['m_message']),
            'type' => $row['m_type'],
            'start_date_time' => remap_time_as_portable($row['m_start_date_time']),
            'end_date_time' => remap_time_as_portable($row['m_end_date_time']),
            'validated' => $row['m_validated'],
            'groups' => $groups,
            'pages' => $pages,
        ];
        $this->_resource_load_extend($resource_type, $resource_id, $properties, $filename, $path);
        return $properties;
    }

    /**
     * Standard Commandr-fs edit function for resource-fs hooks. Edits the resource to the given properties.
     *
     * @param  ID_TEXT $filename The filename
     * @param  string $path The path (blank: root / not applicable)
     * @param  array $properties Properties (may be empty, properties given are open to interpretation by the hook but generally correspond to database fields)
     * @param  boolean $explicit_move Whether we are definitely moving (as opposed to possible having it in multiple positions)
     * @return ~ID_TEXT The resource ID (false: error, could not create via these properties / here)
     */
    public function file_edit(string $filename, string $path, array $properties, bool $explicit_move = false)
    {
        list($resource_type, $resource_id) = $this->file_convert_filename_to_id($filename);
        list($properties, $label) = $this->_file_magic_filter($filename, $path, $properties, $this->file_resource_type);

        if ($resource_id === null) {
            return false;
        }

        require_code('site_messaging2');

        $submitter = $this->_default_property_member($properties, 'submitter');
        $title = $this->_default_property_str($properties, 'title');
        $message = $this->_default_property_str($properties, 'message');
        $type = $this->_default_property_str($properties, 'type');
        if ($type == '') {
            $type = 'inform';
        }
        $start_date_time = $this->_default_property_time_null($properties, 'start_date_time');
        $end_date_time = $this->_default_property_time_null($properties, 'end_date_time');
        $validated = $this->_default_property_int_null($properties, 'validated');
        if ($validated === null) {
            $validated = 1;
        }

        edit_site_message(intval($resource_id), $title, $message, $type, $validated, $start_date_time, $end_date_time);

        if (isset($properties['groups'])) {
            table_from_portable_rows('site_messages_groups', $properties['groups'], ['message_id' => intval($resource_id)], TABLE_REPLACE_MODE_BY_EXTRA_FIELD_DATA);
        }
        if (isset($properties['pages'])) {
            table_from_portable_rows('site_messages_pages', $properties['pages'], ['message_id' => intval($resource_id)], TABLE_REPLACE_MODE_BY_EXTRA_FIELD_DATA);
        }

        $this->_resource_save_extend($this->file_resource_type, $resource_id, $filename, $label, $properties);

        return $resource_id;
    }

    /**
     * Standard Commandr-fs delete function for resource-fs hooks. Deletes the resource.
     *
     * @param  ID_TEXT $filename The filename
     * @param  string $path The path (blank: root / not applicable)
     * @return boolean Success status
     */
    public function file_delete(string $filename, string $path) : bool
    {
        list($resource_type, $resource_id) = $this->file_convert_filename_to_id($filename);

        if ($resource_id === null) {
            return false;
        }

        require_code('site_messaging2');

        delete_site_message(intval($resource_id));

        return true;
    }
}
