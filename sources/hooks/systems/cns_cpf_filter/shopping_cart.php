<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    shopping
 */

/**
 * Hook class.
 */
class Hook_cns_cpf_filter_shopping_cart
{
    /**
     * Find which special CPFs to enable.
     *
     * @return array A list of CPFs to enable
     */
    public function to_enable() : array
    {
        if (!addon_installed('shopping')) {
            return [];
        }

        $cpf = [];

        $cpf = array_merge($cpf, ['firstname' => true, 'lastname' => true]);

        // Shipping address
        $cpf = array_merge($cpf, ['street_address' => true, 'city' => true/*Too specific to force on, may be done via config instead, 'county' => true, 'state' => true*/, 'post_code' => true, 'country' => true, 'mobile_phone_number' => true]);

        return $cpf;
    }
}
