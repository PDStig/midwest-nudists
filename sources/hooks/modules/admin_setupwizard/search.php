<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    search
 */

/**
 * Hook class.
 */
class Hook_sw_search
{
    /**
     * Run function for blocks in the setup wizard.
     *
     * @return array A map between block names and pairs (BLOCK_POSITION_* constants for what is supported, then a BLOCK_POSITION_* constant for what is the default)
     */
    public function get_blocks() : array
    {
        if (!addon_installed('search')) {
            return [];
        }

        return ['side_tag_cloud' => [BLOCK_POSITION_PANEL, null], 'main_search' => [BLOCK_POSITION_MAIN, null]];
    }
}
