<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    banners
 */

/**
 * Hook class.
 */
class Hook_admin_stats_banners extends CMSStatsProvider
{
    /**
     * Find metadata about stats graphs that are provided by this stats hook.
     *
     * @param  boolean $for_kpi Whether this is for setting up a KPI
     * @return ?array Map of metadata (null: hook is disabled)
     */
    public function info(bool $for_kpi = false) : ?array
    {
        if (!addon_installed('banners')) {
            return null;
        }

        require_lang('banners');
        require_code('locations');

        $banners = [];
        $rows = $GLOBALS['SITE_DB']->query_select('banners', ['DISTINCT name']);
        foreach ($rows as $row) {
            $banners[$row['name']] = $row['name'];
        }
        asort($banners, SORT_NATURAL | SORT_FLAG_CASE);

        return [
            'banner_clicks' => [
                'label' => do_lang_tempcode('BANNER_CLICKS'),
                'category' => 'economic_activity',
                'filters' => [
                    'banner_clicks__month_range' => new CMSStatsDateMonthRangeFilter('banner_clicks__month_range', do_lang_tempcode('DATE_RANGE'), null, $for_kpi),
                    'banner_clicks__banner' => new CMSStatsListFilter('banner_clicks__banner', do_lang_tempcode('BANNER'), $banners),
                    'banner_clicks__country' => has_geolocation_data() ? new CMSStatsListFilter('banner_clicks__country', do_lang_tempcode('VISITOR_COUNTRY'), find_countries()) : null,
                ],
                'pivot' => new CMSStatsDatePivot('banner_clicks__pivot', $this->get_date_pivots(!$for_kpi)),
                'support_kpis' => self::KPI_HIGH_IS_GOOD,
            ],
        ];
    }

    /**
     * Preprocess raw data in the database into something we can efficiently draw graphs/conclusions from.
     *
     * @param  TIME $start_time Start timestamp
     * @param  TIME $end_time End timestamp
     * @param  array $data_buckets Map of data buckets; a map of bucket name to nested maps with the following maps in sequence: 'month', 'pivot', 'value' (then further map data) ; extended and returned by reference
     */
    public function preprocess_raw_data(int $start_time, int $end_time, array &$data_buckets)
    {
        require_code('locations');
        require_code('temporal');

        $server_timezone = get_server_timezone();

        $date_pivots = $this->get_date_pivots();

        $max = 1000;
        $start = 0;

        $query = 'SELECT c_date_and_time,c_banner_id,c_ip_address FROM ' . get_table_prefix() . 'banner_clicks WHERE ';
        $query .= 'c_date_and_time>=' . strval($start_time) . ' AND ';
        $query .= 'c_date_and_time<=' . strval($end_time);
        $query .= ' ORDER BY c_date_and_time';
        do {
            $rows = $GLOBALS['SITE_DB']->query($query, $max, $start);
            foreach ($rows as $row) {
                $timestamp = $row['c_date_and_time'];
                $timestamp = tz_time($timestamp, $server_timezone);

                $month = to_epoch_interval_index($timestamp, 'months');

                $banner = $row['c_banner_id'];

                $country = geolocate_ip($row['c_ip_address']);
                if ($country === null) {
                    $country = '';
                }

                foreach (array_keys($date_pivots) as $pivot) {
                    $pivot_value = $this->calculate_date_pivot_value($pivot, $timestamp);

                    if (!isset($data_buckets['banner_clicks'][$month][$pivot][$pivot_value][$banner][$country])) {
                        $data_buckets['banner_clicks'][$month][$pivot][$pivot_value][$banner][$country] = 0;
                    }
                    $data_buckets['banner_clicks'][$month][$pivot][$pivot_value][$banner][$country]++;
                }
            }

            $start += $max;
        } while (!empty($rows));
    }

    /**
     * Generate final data from preprocessed data.
     *
     * @param  string $bucket Data bucket we want data for
     * @param  string $pivot Pivot value
     * @param  array $filters Map of filters (including pivot if applicable)
     * @return ?array Final data in standardised map format (null: could not generate)
     */
    public function generate_final_data(string $bucket, string $pivot, array $filters) : ?array
    {
        $range = $this->convert_month_range_filter_to_pair($filters[$bucket . '__month_range']);

        $data = $this->fill_data_by_date_pivots($pivot, $range[0], $range[1]);

        $where = [
            'p_bucket' => $bucket,
            'p_pivot' => $pivot,
        ];
        $extra = '';
        $extra .= ' AND p_month>=' . strval($range[0]);
        $extra .= ' AND p_month<=' . strval($range[1]);
        $data_rows = $GLOBALS['SITE_DB']->query_select('stats_preprocessed', ['p_data'], $where, $extra);
        foreach ($data_rows as $data_row) {
            $_data = @unserialize($data_row['p_data']);
            foreach ($_data as $pivot_value => $__) {
                $pivot_value = $this->make_date_pivot_value_nice($pivot, $pivot_value);

                foreach ($__ as $banner => $___) {
                    if ((!empty($filters[$bucket . '__banner'])) && (!simulated_wildcard_match($filters[$bucket . '__banner'], $banner, true))) {
                        continue;
                    }

                    foreach ($___ as $country => $total_clicks) {
                        if ((!empty($filters[$bucket . '__country'])) && ($filters[$bucket . '__country'] != $country)) {
                            continue;
                        }

                        if (!isset($data[$pivot_value])) {
                            $data[$pivot_value] = 0;
                        }
                        $data[$pivot_value] += $total_clicks;
                    }
                }
            }
        }

        return [
            'type' => null,
            'data' => $data,
            'x_axis_label' => do_lang_tempcode('TIME_IN_TIMEZONE', escape_html(make_nice_timezone_name(get_site_timezone()))),
            'y_axis_label' => do_lang_tempcode('BANNER_CLICKS'),
        ];
    }
}
