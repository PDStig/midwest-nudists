<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    tickets
 */

/**
 * Hook class.
 */
class Hook_admin_stats_support_tickets extends CMSStatsProvider
{
    /**
     * Find metadata about stats graphs that are provided by this stats hook.
     *
     * @param  boolean $for_kpi Whether this is for setting up a KPI
     * @return ?array Map of metadata (null: hook is disabled)
     */
    public function info(bool $for_kpi = false) : ?array
    {
        if (!addon_installed('tickets')) {
            return null;
        }
        if (get_forum_type() != 'cns') {
            return null;
        }

        require_lang('tickets');
        require_code('tickets');

        $forum_id = get_ticket_forum_id();
        if ($forum_id === null) {
            return null;
        }

        return [
            'support_tickets' => [
                'label' => do_lang_tempcode('SUPPORT_TICKETS'),
                'category' => 'feedback_and_engagement',
                'filters' => [
                    'support_tickets__month_range' => new CMSStatsDateMonthRangeFilter('support_tickets__month_range', do_lang_tempcode('DATE_RANGE'), null, $for_kpi),
                ],
                'pivot' => new CMSStatsDatePivot('support_tickets__pivot', $this->get_date_pivots(!$for_kpi)),
                'support_kpis' => self::KPI_HIGH_IS_GOOD,
            ],
        ];
    }

    /**
     * Preprocess raw data in the database into something we can efficiently draw graphs/conclusions from.
     *
     * @param  TIME $start_time Start timestamp
     * @param  TIME $end_time End timestamp
     * @param  array $data_buckets Map of data buckets; a map of bucket name to nested maps with the following maps in sequence: 'month', 'pivot', 'value' (then further map data) ; extended and returned by reference
     */
    public function preprocess_raw_data(int $start_time, int $end_time, array &$data_buckets)
    {
        require_code('temporal');
        $server_timezone = get_server_timezone();

        $max = 1000;
        $start = 0;

        $date_pivots = $this->get_date_pivots();

        $forum_id = get_ticket_forum_id();

        $query = 'SELECT t_cache_first_time FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_topics WHERE ';
        $query .= 't_forum_id=' . strval($forum_id);
        $query .= ' ORDER BY t_cache_first_time';
        do {
            $rows = $GLOBALS['FORUM_DB']->query($query, $max, $start);
            foreach ($rows as $row) {
                $timestamp = $row['t_cache_first_time'];
                if ($timestamp === null) {
                    continue;
                }
                $timestamp = tz_time($timestamp, $server_timezone);

                $month = to_epoch_interval_index($timestamp, 'months');

                foreach (array_keys($date_pivots) as $pivot) {
                    $pivot_value = $this->calculate_date_pivot_value($pivot, $timestamp);

                    if (!isset($data_buckets['support_tickets'][$month][$pivot][$pivot_value])) {
                        $data_buckets['support_tickets'][$month][$pivot][$pivot_value] = 0;
                    }
                    $data_buckets['support_tickets'][$month][$pivot][$pivot_value]++;
                }
            }

            $start += $max;
        } while (!empty($rows));
    }

    /**
     * Generate final data from preprocessed data.
     *
     * @param  string $bucket Data bucket we want data for
     * @param  string $pivot Pivot value
     * @param  array $filters Map of filters (including pivot if applicable)
     * @return ?array Final data in standardised map format (null: could not generate)
     */
    public function generate_final_data(string $bucket, string $pivot, array $filters) : ?array
    {
        $range = $this->convert_month_range_filter_to_pair($filters[$bucket . '__month_range']);

        $data = $this->fill_data_by_date_pivots($pivot, $range[0], $range[1]);

        $where = [
            'p_bucket' => $bucket,
            'p_pivot' => $pivot,
        ];
        $extra = '';
        $extra .= ' AND p_month>=' . strval($range[0]);
        $extra .= ' AND p_month<=' . strval($range[1]);
        $data_rows = $GLOBALS['SITE_DB']->query_select('stats_preprocessed', ['p_data'], $where, $extra);
        foreach ($data_rows as $data_row) {
            $_data = @unserialize($data_row['p_data']);
            foreach ($_data as $pivot_value => $total) {
                $pivot_value = $this->make_date_pivot_value_nice($pivot, $pivot_value);

                $data[$pivot_value] = $total;
            }
        }

        return [
            'type' => null,
            'data' => $data,
            'x_axis_label' => do_lang_tempcode('TIME_IN_TIMEZONE', escape_html(make_nice_timezone_name(get_site_timezone()))),
            'y_axis_label' => do_lang_tempcode('COUNT_NEW'),
        ];
    }
}
