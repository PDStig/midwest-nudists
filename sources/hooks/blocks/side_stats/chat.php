<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    chat
 */

/**
 * Hook class.
 */
class Hook_stats_chat
{
    /**
     * Show a stats section.
     *
     * @return Tempcode The result of execution
     */
    public function run() : object
    {
        if (!addon_installed('chat')) {
            return new Tempcode();
        }

        require_code('chat_stats');
        require_lang('chat');

        $bits = new Tempcode();
        if (get_option('chat_show_stats_count_users') == '1') {
            $num_chatters = get_num_chatters();

            $bits->attach(do_template('BLOCK_SIDE_STATS_SUBLINE', [
                '_GUID' => '904a46b83a84728243f3fd655705cc04',
                'KEY' => do_lang_tempcode('COUNT_CHATTERS'),
                'RAW_VALUE' => strval($num_chatters),
                'VALUE' => integer_format($num_chatters, 0),
            ]));
        }
        if (get_option('chat_show_stats_count_rooms') == '1') {
            $num_chatrooms = get_num_chatrooms();

            $bits->attach(do_template('BLOCK_SIDE_STATS_SUBLINE', [
                '_GUID' => 'adf12b729fd23b6fa7115758a64155c6',
                'KEY' => do_lang_tempcode('CHATROOMS'),
                'RAW_VALUE' => strval($num_chatrooms),
                'VALUE' => integer_format($num_chatrooms, 0),
            ]));
        }
        if (get_option('chat_show_stats_count_messages') == '1') {
            $num_posts = get_num_chatposts();

            $bits->attach(do_template('BLOCK_SIDE_STATS_SUBLINE', [
                '_GUID' => '0e86e89171ddd8225ac41e14b18ecdb0',
                'KEY' => do_lang_tempcode('COUNT_CHATPOSTS'),
                'RAW_VALUE' => strval($num_posts),
                'VALUE' => integer_format($num_posts, 0),
            ]));
        }
        if ($bits->is_empty_shell()) {
            return new Tempcode();
        }

        $chat = do_template('BLOCK_SIDE_STATS_SECTION', ['_GUID' => '4d688c45e01ed34f257fd03100a6be6d', 'SECTION' => do_lang_tempcode('SECTION_CHAT'), 'CONTENT' => $bits]);

        return $chat;
    }
}
