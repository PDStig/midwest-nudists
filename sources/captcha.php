<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    captcha
 */

/*EXTRA FUNCTIONS: srand*/

/**
 * Standard code module initialisation function.
 *
 * @ignore
 */
function init__captcha()
{
    require_lang('captcha');
}

/**
 * Outputs and stores information for a CAPTCHA.
 */
function captcha_script()
{
    if (!addon_installed('captcha')) {
        warn_exit(do_lang_tempcode('MISSING_ADDON', escape_html('captcha')));
    }

    if (get_option('recaptcha_site_key') != '') {
        warn_exit(do_lang_tempcode('INTERNAL_ERROR', escape_html('d66ef5e4a89054d29050a63ad118e7d1')));
    }

    header('X-Robots-Tag: noindex');

    $code_needed = $GLOBALS['SITE_DB']->query_select_value_if_there('captchas', 'si_code', ['si_session_id' => get_session_id(true)]);
    if ($code_needed === null) {
        $code_needed = generate_captcha();
        /*
        set_http_status_code(500);    This would actually be very slightly insecure, as it could be used to probe (binary) login state via rogue sites that check if CAPTCHAs had been generated
        warn_exit(do_lang_tempcode('CAPTCHA_NO_SESSION'));
        */
    }
    mt_srand(crc32($code_needed)); // Important: to stop averaging out of different attempts. This makes the distortion consistent for that particular code.

    cms_ini_set('ocproducts.xss_detect', '0');

    $mode = get_param_string('mode', '');
    $large_mode = ($mode == 'large');

    // Audio version
    if (($mode == 'audio') && (get_option('audio_captcha') === '1')) {
        header('Content-Type: audio/x-wav');
        header('Content-Disposition: inline; filename="captcha.wav"');
        //header('Content-Disposition: attachment; filename="captcha.wav"');  Useful for testing

        if ($_SERVER['REQUEST_METHOD'] == 'HEAD') {
            return;
        }

        $data = captcha_audio($code_needed);

        header('Content-Length: ' . strval(strlen($data)));

        echo $data;

        return;
    }

    list($img, $width, $height) = captcha_image($code_needed);

    // Output using CSS
    if (get_option('css_captcha') === '1') {
        echo '
        <!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>' . do_lang('CONTACT_STAFF_TO_JOIN_IF_IMPAIRED') . '</title>
            <meta name="robots" content="noindex" />
        </head>
        <body style="margin: 0">
        ';
        if (get_option('js_captcha') === '1') {
            echo '<div style="display: none" id="hidden-captcha">';
        }
        echo '<div style="width: ' . ($large_mode ? strval($width * 4) : strval($width)) . 'px; font-size: 0; line-height: 0">';
        if ($mode != 'large') {
            echo '<a target="_blank" data-open-as-overlay="{}" rel="nofollow" href="' . find_script('captcha') . '?cache_break=' . strval(time()) . '&mode=large" title="' . do_lang('captcha:CAPTCHA_CLICK_LARGE') . '" aria-haspopup="dialog">';
        }
        for ($j = 0; $j < $height; $j++) {
            for ($i = 0; $i < $width; $i++) {
                $colour = imagecolorsforindex($img, imagecolorat($img, $i, $j));
                echo '<span style="vertical-align: bottom; overflow: hidden; display: inline-block; -webkit-text-size-adjust: none; text-size-adjust: none; background: rgb(' . strval($colour['red']) . ',' . strval($colour['green']) . ',' . strval($colour['blue']) . '); ' . ($large_mode ? 'width: 4px; height: 4px' : 'width: 1px; height: 1px') . '"></span>';
            }
        }
        if ($mode != 'large') {
            echo '</a>';
        }
        echo '</div>';
        if (get_option('js_captcha') === '1') {
            echo '</div>';
            echo '<script ' . csp_nonce_html() . '>document.getElementById(\'hidden-captcha\').style.display = \'block\';</script>';
        }
        echo '
        </body>
        </html>
        ';
        imagedestroy($img);
        cms_safe_exit_flow();
        return;
    }

    // Output as a PNG
    header('Content-Type: image/png');
    imagepng($img);
    imagedestroy($img);
    cms_safe_exit_flow();
}

/**
 * Create an image CATCHA.
 *
 * @param  string $code_needed The code
 * @return array A tuple: the image CAPTCHA, the width, the height
 */
function captcha_image(string $code_needed) : array
{
    // Write basic, using multiple fonts with random Y-position offsets
    $characters = strlen($code_needed);
    $fonts = [];
    $width = 20;
    for ($i = 0; $i < max(1, $characters); $i++) {
        $font = mt_rand(4, 5); // 1 is too small
        $fonts[] = $font;
        $width += imagefontwidth($font) + 2;
        $height = imagefontheight($font) + 20;
    }
    $img = imagecreate($width, $height);
    $black = imagecolorallocate($img, 0, 0, 0);
    $off_black = imagecolorallocate($img, mt_rand(1, 45), mt_rand(1, 45), mt_rand(1, 45));
    $white = imagecolorallocate($img, 255, 255, 255);
    imagefill($img, 0, 0, $black);
    $x = 10;
    foreach ($fonts as $i => $font) {
        $y_dif = mt_rand(-15, 15);
        imagestring($img, $font, $x, 10 + $y_dif, $code_needed[strlen($code_needed) - $i - 1], $off_black);
        $x += imagefontwidth($font) + 2;
    }
    $x = 10;
    foreach ($fonts as $i => $font) {
        $y_dif = mt_rand(-5, 5);
        imagestring($img, $font, $x, 10 + $y_dif, $code_needed[$i], $white);
        if (get_option('captcha_noise') == '1') {
            imagestring($img, $font, $x + 1, 10 + mt_rand(-1, 1) + $y_dif, $code_needed[$i], $white);
        }
        $x += imagefontwidth($font) + 2;
    }

    // Add some noise
    if (get_option('captcha_noise') == '1') {
        $tricky_remap = [];
        $tricky_remap[$black] = [];
        $tricky_remap[$off_black] = [];
        $tricky_remap[$white] = [];
        for ($i = 0; $i <= 5; $i++) {
            $tricky_remap['!' . strval($black)][] = imagecolorallocate($img, 0 + mt_rand(0, 15), 0 + mt_rand(0, 15), 0 + mt_rand(0, 15));
            $tricky_remap['!' . strval($off_black)][] = $off_black;
            $tricky_remap['!' . strval($white)][] = imagecolorallocate($img, 255 - mt_rand(0, 145), 255 - mt_rand(0, 145), 255 - mt_rand(0, 145));
        }
        $noise_amount = 0.02;//0.04;
        for ($i = 0; $i < intval($width * $height * $noise_amount); $i++) {
            $x = mt_rand(0, $width);
            $y = mt_rand(0, $height);
            if (mt_rand(0, 1) == 0) {
                imagesetpixel($img, $x, $y, $white);
            } else {
                imagesetpixel($img, $x, $y, $black);
            }
        }
        for ($i = 0; $i < $width; $i++) {
            for ($j = 0; $j < $height; $j++) {
                imagesetpixel($img, $i, $j, $tricky_remap['!' . strval(imagecolorat($img, $i, $j))][mt_rand(0, 5)]);
            }
        }
    }

    return [$img, $width, $height];
}

/**
 * Create an audio CATCHA.
 *
 * @param  string $code_needed The code
 * @return string the audio CAPTCHA
 */
function captcha_audio(string $code_needed) : string
{
    $data = '';
    for ($i = 0; $i < strlen($code_needed); $i++) {
        $char = cms_strtolower_ascii($code_needed[$i]);

        $file_path = get_file_base() . '/data_custom/sounds/' . $char . '.wav';
        if (!file_exists($file_path)) {
            $file_path = get_file_base() . '/data/sounds/' . $char . '.wav';
        }
        $myfile = fopen($file_path, 'rb');
        if ($i != 0) {
            fseek($myfile, 44);
        } else {
            $data = fread($myfile, 44);
        }
        $_data = fread($myfile, filesize($file_path));
        for ($j = 0; $j < strlen($_data); $j++) {
            if (get_option('captcha_noise') == '1') {
                $amp_mod = mt_rand(-2, 2);
                $_data[$j] = chr(min(255, max(0, ord($_data[$j]) + $amp_mod)));
            }
            if (get_option('captcha_noise') == '1') {
                if (($j != 0) && (mt_rand(0, 10) == 1)) {
                    $data .= $_data[$j - 1];
                }
            }
            $data .= $_data[$j];
        }
        fclose($myfile);
    }

    // Fix up header
    $data = substr_replace($data, pack('V', strlen($data) - 8), 4, 4);
    $data = substr_replace($data, pack('V', strlen($data) - 44), 40, 4);

    return $data;
}

/**
 * Get a CAPTCHA (aka security code) form field.
 *
 * @param  Tempcode $hidden Hidden fields (will attach to here for non-visible CAPTCHA systems)
 * @return Tempcode The field
 */
function form_input_captcha(object $hidden) : object
{
    $tabindex = get_form_field_tabindex(null);

    if (uses_question_captcha()) {
        require_javascript('captcha');

        $tpl = new Tempcode();

        require_code('form_templates');

        $questions = get_captcha_questions();
        foreach ($questions as $i => $details) {
            list($question, $answer, $wrong_answers) = $details;
            if ($wrong_answers !== null) {
                $answers = array_merge([$answer], $wrong_answers);
                cms_mb_sort($answers, SORT_FLAG_CASE | SORT_NATURAL);
                $_answers = new Tempcode();
                $_answers->attach(form_input_list_entry(''));
                foreach ($answers as $answer) {
                    $_answers->attach(form_input_list_entry($answer));
                }
                $tpl->attach(form_input_list(comcode_to_tempcode($question, null, true), do_lang_tempcode('DESCRIPTION_CAPTCHA_QUESTION_LIST'), 'captcha_' . strval($i), $_answers, null, false, true));
            } else {
                $tpl->attach(form_input_line(comcode_to_tempcode($question, null, true), do_lang_tempcode('DESCRIPTION_CAPTCHA_QUESTION'), 'captcha_' . strval($i), '', true));
            }
        }

        return $tpl;
    }

    // Show template
    $input = do_template('FORM_SCREEN_INPUT_CAPTCHA', ['_GUID' => 'f7452af9b83db36685ae8a86f9762d30', 'TABINDEX' => strval($tabindex)]);
    if (get_option('recaptcha_site_key') != '') {
        $hidden->attach($input);
        return new Tempcode();
    }
    return _form_input('captcha', do_lang_tempcode('SECURITY_IMAGE'), do_lang_tempcode('DESCRIPTION_CAPTCHA'), $input, true, false);
}

/**
 * Find whether CAPTCHA (the security image) should be used if preferred (making this call assumes it is preferred).
 *
 * @return boolean Whether CAPTCHA is used
 */
function use_captcha() : bool
{
    if (get_option('use_captchas') == '0') {
        return false;
    }

    if (running_script('captcha')) {
        return true;
    }

    if (is_guest()) {
        return true;
    }

    // At this point, we know it's a member, who may still get a CAPTCHA depending on various factors...

    if (has_privilege(get_member(), 'avoid_captcha')) {
        return false;
    }

    $days = get_option('captcha_member_days');
    if ((!empty($days)) && ($GLOBALS['FORUM_DRIVER']->get_member_join_timestamp(get_member()) > time() - 60 * 60 * 24 * intval($days))) {
        return true;
    }

    $posts = get_option('captcha_member_posts');
    if (!empty($posts)) {
        $post_count = $GLOBALS['FORUM_DRIVER']->get_post_count(get_member());
        if (get_forum_type() == 'cns') {
            $sql = 'SELECT COUNT(*) FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_posts WHERE p_posting_member=' . strval(get_member());
            $sql .= ' AND (p_cache_forum_id IS NULL OR p_whisper_to_member IS NOT NULL OR p_time>' . strval(time() - 60 * 60 * 24) . ')';
            $post_count -= $GLOBALS['FORUM_DB']->query_value_if_there($sql);
        }
        if ($post_count < intval($posts)) {
            return true;
        }
    }

    return false;
}

/**
 * Find whether the question-CAPTCHA will be used on the current page.
 *
 * @return boolean Whether question-CAPTCHA is used
 */
function uses_question_captcha() : bool
{
    if (empty(get_captcha_questions())) {
        return false;
    }

    if (intval(get_option('captcha_question_total')) <= 0) {
        return false;
    }

    $question_pages = trim(get_option('captcha_question_pages'));
    if ($question_pages == '') {
        return false;
    }

    if (running_script('snippet')) {
        if (get_param_integer('question_captcha', 0) == 0) {
            return false;
        }
    } else {
        if (!match_key_match($question_pages)) {
            return false;
        }
    }

    return true;
}

/**
 * Find question-CAPTCHA questions to use for the CAPTCHA.
 * Applies randomisation as appropriate.
 *
 * @return array A list of tuples: question, answer, multi-choice answers (or null)
 */
function get_captcha_questions() : array
{
    $questions = [];
    $_questions = trim(get_option('captcha_questions'));
    foreach (explode("\n", $_questions) as $_question) {
        $parts = explode('=', $_question);
        if (count($parts) >= 2) {
            $details = [trim($parts[0]), trim($parts[1]), null];
            if (count($parts) > 2) {
                $details[2] = array_slice($parts, 2);
            }
            $questions[] = $details;
        }
    }

    $question_total = intval(get_option('captcha_question_total'));
    if ($question_total > count($questions)) {
        $question_total = count($questions);
    }

    if ($question_total < count($questions)) {
        srand(crc32(get_session_id(true))); // The session ID will seed which questions are picked; consistent across executions

        $_keys = array_rand($questions, $question_total);
        if (!is_array($_keys)) {
            $keys = [$_keys];
        } else {
            $keys = $_keys;
        }

        srand();

        $__questions = [];
        foreach ($keys as $key) {
            $__questions[$key] = $questions[$key];
        }
        $questions = $__questions;
    }

    return $questions;
}

/**
 * Generate a CAPTCHA image.
 *
 * @return ?string The code (null: on CAPTCHA generated)
 */
function generate_captcha() : ?string
{
    if (get_option('recaptcha_site_key') != '') {
        return null;
    }

    global $INVALIDATED_FAST_SPIDER_CACHE;
    $INVALIDATED_FAST_SPIDER_CACHE = true;

    $session = get_session_id(true);
    if ($session == '') {
        if (php_function_allowed('error_log')) {
            error_log(brand_name() . ': WARNING CAPTCHA generated against blank session - static caching is misconfigured');
        }
    }

    // Clear out old codes
    $where = 'si_time<' . strval(time() - 60 * 30) . ' OR ' . db_string_equal_to('si_session_id', $session);
    $rows = $GLOBALS['SITE_DB']->query('SELECT si_session_id FROM ' . get_table_prefix() . 'captchas WHERE ' . $where);
    foreach ($rows as $row) {
        @unlink(get_custom_file_base() . '/uploads/captcha/' . $row['si_session_id'] . '.wav');
    }
    $GLOBALS['SITE_DB']->query('DELETE FROM ' . get_table_prefix() . 'captchas WHERE ' . $where);

    // Create code
    $choices = ['3', '4', '6', '7', '9', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'R', 'T', 'W', 'X', 'Y'];
    $si_code = '';
    for ($i = 0; $i < 6; $i++) {
        $choice = mt_rand(0, count($choices) - 1);
        $si_code .= $choices[$choice]; // NB: In ASCII code all the chars in $choices are 10-99 (i.e. 2 digit)
    }

    // Store code
    $GLOBALS['SITE_DB']->query_insert('captchas', ['si_session_id' => $session, 'si_time' => time(), 'si_code' => $si_code]);

    require_code('files');
    cms_file_put_contents_safe(get_custom_file_base() . '/uploads/captcha/' . $session . '.wav', captcha_audio($si_code), FILE_WRITE_FIX_PERMISSIONS | FILE_WRITE_SYNC_FILE);

    return $si_code;
}

/**
 * Calling this assumes CAPTCHA was needed. Checks that it was done correctly.
 *
 * @param  boolean $regenerate_on_error Whether to possibly regenerate upon error
 */
function enforce_captcha(bool $regenerate_on_error = true)
{
    if (use_captcha()) {
        $error_message = do_lang_tempcode('INVALID_SECURITY_CODE_ENTERED');
        if (!check_captcha(null, $regenerate_on_error, $error_message)) {
            set_http_status_code(500);

            warn_exit($error_message, false, true);
        }
    }
}

/**
 * Normalise a question-CAPTCHA answer, for better comparison.
 *
 * @param  string $answer Answer
 * @return string Normalised answer
 */
function normalise_captcha_question_answer(string $answer) : string
{
    $ret = cms_mb_strtolower($answer);
    $ret = trim($ret);
    return $ret;
}

/**
 * Checks a CAPTCHA.
 *
 * @param  ?string $code_entered CAPTCHA entered (null: read from standard-named parameter)
 * @param  boolean $regenerate_on_error Whether to possibly regenerate upon error
 * @param  ?Tempcode $error_message Error message to write out (null: none)
 * @return boolean Whether it is valid for the current session
 */
function check_captcha(?string $code_entered = null, bool $regenerate_on_error = true, ?object &$error_message = null) : bool
{
    if (!use_captcha()) {
        return true;
    }

    // Question CAPTCHA...

    if (uses_question_captcha()) {
        $questions = get_captcha_questions();

        if ($code_entered === null) {
            $answers = [];
            foreach (array_keys($questions) as $i) {
                $answers[$i] = post_param_string('captcha_' . strval($i));
            }
        } else {
            $answers = explode('||', $code_entered);
            foreach (array_keys($questions) as $i) {
                if (!array_key_exists($i, $answers)) {
                    $answers[$i] = '';
                }
            }
        }

        foreach ($questions as $i => $details) {
            list($question, $answer) = $details;

            if (normalise_captcha_question_answer($answers[$i]) != normalise_captcha_question_answer($answer)) {
                $error_message = do_lang_tempcode('INCORRECT_CAPTCHA_QUESTION_ANSWER', comcode_to_tempcode($question, null, true));

                return false;
            }
        }

        return true;
    }

    // Google CAPTCHA...

    if (get_option('recaptcha_site_key') != '') {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $post_params = [
            'secret' => get_option('recaptcha_server_key'),
            'response' => post_param_string('g-recaptcha-response'),
        ];
        $_response = cms_http_request($url, ['convert_to_internal_encoding' => true, 'post_params' => $post_params, 'ignore_http_status' => true]);

        $response = @json_decode($_response->data, true);

        if (!is_array($response)) {
            $error_message = make_string_tempcode('reCAPTCHA: ERROR ' . $response->message);
            require_code('failure');
            cms_error_log($error_message->evaluate(), 'error_occurred_api');
        } else {
            if (!$response['success']) {
                foreach ($response['error-codes'] as $error_code) {
                    switch ($error_code) {
                        case 'timeout-or-duplicate':
                            $error_message = do_lang_tempcode('RECAPTCHA_ERROR_' . str_replace('-', '_', $error_code));
                            break;

                        case 'missing-input-secret':
                        case 'invalid-input-secret':
                        case 'missing-input-response':
                        case 'invalid-input-response':
                        case 'bad-request':
                            $error_message = do_lang_tempcode('RECAPTCHA_ERROR_' . str_replace('-', '_', $error_code));
                            require_code('failure');
                            cms_error_log('reCAPTCHA: ERROR ' . $error_message->evaluate(), 'error_occurred_api');
                            break;
                    }
                }
            }
        }

        return $response['success'];
    }

    // Regular CAPTCHA...

    if ($code_entered === null) {
        $code_entered = post_param_string('captcha');
    }

    $code_needed = $GLOBALS['SITE_DB']->query_select_value_if_there('captchas', 'si_code', ['si_session_id' => get_session_id(true)]);
    if ($code_needed === null) {
        if (get_option('captcha_single_guess') == '1') {
            generate_captcha();
        }
        attach_message(do_lang_tempcode('NO_SESSION_SECURITY_CODE'), 'warn');
        return false;
    }
    $passes = (cms_strtolower_ascii($code_needed) == cms_strtolower_ascii($code_entered));
    if ($regenerate_on_error) {
        if ($passes) {
            cms_register_shutdown_function_if_available(function () {
                if (get_option('recaptcha_site_key') != '') {
                    return;
                }

                // Delete current CAPTCHA
                if (!running_script('snippet')) {
                    $GLOBALS['SITE_DB']->query_delete('captchas', ['si_session_id' => get_session_id(true)]); // Only allowed to check once
                }
            });
        } else {
            if (get_option('captcha_single_guess') == '1') {
                generate_captcha();
            }
        }
    }
    if (!$passes) {
        $data = serialize($_POST);

        // Log hack-attack
        require_code('antispam');
        if (is_posted_code_alien($data)) {
            log_hack_attack_and_exit('CAPTCHAFAIL_HACK');
        } else {
            log_hack_attack_and_exit('CAPTCHAFAIL');
        }
    }
    return $passes;
}

/**
 * Get code to do an AJAX check of the CAPTCHA.
 * Note we don't use this on the join form, as core_cns.js has its own CAPTCHA check integrated into the validation flow.
 *
 * @return string Function name
 */
function captcha_ajax_check_function() : string
{
    if (!use_captcha()) {
        return '';
    }

    return 'captchaCaptchaAjaxCheck';
}
