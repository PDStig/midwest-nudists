<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_abstract_interfaces
 */

/**
 * Show a tooltip by truncating some text.
 * Note we also have the generate_tooltip_by_truncation_advanced function which is much more powerful and is what drives the $TRUNCATE_* symbols.
 *
 * @param  string $text The text, provided in plain-text format (string or Tempcode)
 * @param  integer $len The length to truncate at
 * @return Tempcode The tooltip (or a null-op if $text is not long enough)
 */
function generate_tooltip_by_truncation(string $text, int $len = 60) : object
{
    if (cms_mb_strlen($text) > $len) {
        $truncated = escape_html(cms_mb_substr($text, 0, $len - 2)) . '&hellip;';
        $tooltip = escape_html($text);

        return tooltip($truncated, $tooltip, false);
    }
    return make_string_tempcode(escape_html($text));
}

/**
 * Show a tooltip, automatic inline/block context detection.
 *
 * @param  mixed $label What the tooltip is on, format depends on $auto_escape (string or Tempcode)
 * @param  mixed $tooltip The tooltip, format depends on $auto_escape (string or Tempcode)
 * @param  boolean $auto_escape Whether to automatically escape each plain-text entry so that it cannot contain HTML (ignored for Tempcode values)
 * @return Tempcode The tooltip
 */
function tooltip($label, $tooltip, bool $auto_escape) : object
{
    $_label = is_object($label) ? $label->evaluate() : $label;
    $is_block = (strpos($_label, '<div') !== false || strpos($_label, '<p') !== false || strpos($_label, '<table') !== false);
    return $is_block ? block_tooltip($label, $tooltip, $auto_escape) : inline_tooltip($label, $tooltip, $auto_escape);
}

/**
 * Show a tooltip on an inline element.
 *
 * @param  mixed $label What the tooltip is on, format depends on $auto_escape (string or Tempcode)
 * @param  mixed $tooltip The tooltip, format depends on $auto_escape (string or Tempcode)
 * @param  boolean $auto_escape Whether to automatically escape each plain-text entry so that it cannot contain HTML (ignored for Tempcode values)
 * @return Tempcode The tooltip
 */
function inline_tooltip($label, $tooltip, bool $auto_escape) : object
{
    if (($auto_escape) && (!is_object($label))) {
        $label = escape_html($label);
    }
    if (($auto_escape) && (!is_object($tooltip))) {
        $tooltip = escape_html($tooltip);
    }

    return do_template('INLINE_TOOLTIP', ['_GUID' => '5e6bb4853dd4bc2064e999b6820a3088', 'LABEL' => $label, 'TOOLTIP' => $tooltip]);
}

/**
 * Show a tooltip on a block element.
 *
 * @param  mixed $label What the tooltip is on, format depends on $auto_escape (string or Tempcode)
 * @param  mixed $tooltip The tooltip, format depends on $auto_escape (string or Tempcode)
 * @param  boolean $auto_escape Whether to automatically escape each plain-text entry so that it cannot contain HTML (ignored for Tempcode values)
 * @return Tempcode The tooltip
 */
function block_tooltip($label, $tooltip, bool $auto_escape) : object
{
    if (($auto_escape) && (!is_object($label))) {
        $label = escape_html($label);
    }
    if (($auto_escape) && (!is_object($tooltip))) {
        $tooltip = escape_html($tooltip);
    }

    return do_template('BLOCK_TOOLTIP', ['_GUID' => '1e22cd77b8432465a95ab2dfb2ce95f0', 'LABEL' => $label, 'TOOLTIP' => $tooltip]);
}
