<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    news
 */

/**
 * Block class.
 */
class Block_side_news_archive
{
    /**
     * Find details of the block.
     *
     * @return ?array Map of block info (null: block is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Chris Graham';
        $info['organisation'] = 'Composr';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 2;
        $info['locked'] = false;
        $info['min_cms_version'] = 11.0;
        $info['addon'] = 'news';
        $info['parameters'] = ['select', 'zone', 'title'];
        return $info;
    }

    /**
     * Find caching details for the block.
     *
     * @return ?array Map of cache details (cache_on and ttl) (null: block is disabled)
     */
    public function caching_environment() : ?array
    {
        $info = [];
        $info['cache_on'] = <<<'PHP'
        [
            array_key_exists('title', $map) ? $map['title'] : do_lang('ARCHIVES'),
            array_key_exists('zone', $map) ? $map['zone'] : get_module_zone('news'),
            array_key_exists('select', $map) ? $map['select'] : '*',
        ]
PHP;
        $info['special_cache_flags'] = CACHE_AGAINST_DEFAULT;
        $info['ttl'] = 15;
        return $info;
    }

    /**
     * Execute the block.
     *
     * @param  array $map A map of parameters
     * @return Tempcode The result of execution
     */
    public function run(array $map) : object
    {
        $error_msg = new Tempcode();
        if (!addon_installed__messaged('news', $error_msg)) {
            return $error_msg;
        }
        if (!addon_installed__messaged('news_shared', $error_msg)) {
            return $error_msg;
        }

        require_code('temporal');
        require_lang('news');

        $block_id = get_block_id($map);

        $zone = array_key_exists('zone', $map) ? $map['zone'] : get_module_zone('news');
        $select = array_key_exists('select', $map) ? $map['select'] : '*';

        if ($select == '*') {
            require_code('selectcode');
            $selects_1 = selectcode_to_sqlfragment($select, 'p.news_category', 'news_categories', null, 'p.news_category', 'id'); // Note that the parameters are fiddled here so that category-set and record-set are the same, yet SQL is returned to deal in an entirely different record-set (entries' record-set)
            $selects_2 = selectcode_to_sqlfragment($select, 'd.news_entry_category', 'news_categories', null, 'd.news_entry_category', 'id'); // Note that the parameters are fiddled here so that category-set and record-set are the same, yet SQL is returned to deal in an entirely different record-set (entries' record-set)
            $q_filter = '(' . $selects_1 . ' OR ' . $selects_2 . ')';
        } else {
            $q_filter = '1=1';
        }

        $rows = $GLOBALS['SITE_DB']->query('SELECT DISTINCT p.id,p.date_and_time FROM ' . get_table_prefix() . 'news p LEFT JOIN ' . get_table_prefix() . 'news_category_entries d ON d.news_entry=p.id WHERE ' . $q_filter . (((!has_privilege(get_member(), 'see_not_validated')) && (addon_installed('validation'))) ? ' AND validated=1' : '') . ' ORDER BY date_and_time DESC');
        $rows = array_reverse($rows);

        if (empty($rows)) {
            return do_template('RED_ALERT', ['_GUID' => '7e78bea91fdd55dea289f66387e3a424', 'TEXT' => do_lang_tempcode('NO_ENTRIES')]);
        }
        $first = $rows[0]['date_and_time'];
        $last = $rows[count($rows) - 1]['date_and_time'];

        $current_month = intval(date('m', utctime_to_usertime($first)));
        $current_year = intval(date('Y', utctime_to_usertime($first)));

        $last_month = intval(date('m', utctime_to_usertime($last)));
        $last_year = intval(date('Y', utctime_to_usertime($last)));

        $years = [];
        $years[$current_year] = ['YEAR' => strval($current_year), 'TIMES' => []];

        require_lang('dates');

        $offset = 0;
        $period_start = $first;

        while (true) {
            $period_start = usertime_to_utctime(cms_mktime(0, 0, 0, $current_month, 0, $current_year));
            $period_end = usertime_to_utctime(cms_mktime(0, 0, 0, $current_month + 1, 0, $current_year)) - 1;

            while ($rows[$offset]['date_and_time'] < $period_start) {
                $offset++;
                if (!isset($rows[$offset]['date_and_time'])) {
                    break 2;
                }
            }

            if ($rows[$offset]['date_and_time'] <= $period_end) {
                while ((isset($rows[$offset]['date_and_time'])) && ($rows[$offset]['date_and_time'] <= $period_end)) {
                    $offset++;
                }
                $offset--;

                $month_string = '';
                switch (strval($current_month)) {
                    case '1':
                        $month_string = do_lang('JANUARY');
                        break;
                    case '2':
                        $month_string = do_lang('FEBRUARY');
                        break;
                    case '3':
                        $month_string = do_lang('MARCH');
                        break;
                    case '4':
                        $month_string = do_lang('APRIL');
                        break;
                    case '5':
                        $month_string = do_lang('MAY');
                        break;
                    case '6':
                        $month_string = do_lang('JUNE');
                        break;
                    case '7':
                        $month_string = do_lang('JULY');
                        break;
                    case '8':
                        $month_string = do_lang('AUGUST');
                        break;
                    case '9':
                        $month_string = do_lang('SEPTEMBER');
                        break;
                    case '10':
                        $month_string = do_lang('OCTOBER');
                        break;
                    case '11':
                        $month_string = do_lang('NOVEMBER');
                        break;
                    case '12':
                        $month_string = do_lang('DECEMBER');
                        break;
                }

                $url = build_url(['page' => 'news', 'type' => 'browse', 'select' => $select, 'module_start' => count($rows) - $offset - 1, 'year' => $current_year, 'month' => $current_month], $zone);

                array_unshift($years[$current_year]['TIMES'], ['URL' => $url, 'MONTH' => strval($current_month), 'MONTH_STRING' => $month_string]);
            }

            if ($current_month != 12) {
                $current_month++;
            } else {
                $current_month = 1;
                $current_year++;
                $years[$current_year] = ['YEAR' => strval($current_year), 'TIMES' => []];
            }
        }

        $years = array_reverse($years);

        $title = array_key_exists('title', $map) ? $map['title'] : do_lang('ARCHIVES');

        return do_template('BLOCK_SIDE_NEWS_ARCHIVE', [
            '_GUID' => '10d6267d943ad77a4025a4e286c41ee7',
            'BLOCK_ID' => $block_id,
            'YEARS' => $years,
            'TITLE' => $title,
        ]);
    }
}
