<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_privacy
 */

/**
 * Block class.
 */
class Block_main_privacy_policy_auto
{
    /**
     * Find details of the block.
     *
     * @return ?array Map of block info (null: block is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Chris Graham';
        $info['organisation'] = 'Composr';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 1;
        $info['locked'] = false;
        $info['min_cms_version'] = 11.0;
        $info['addon'] = 'core_privacy';
        $info['parameters'] = [];
        return $info;
    }

    /**
     * Find caching details for the block.
     *
     * @return ?array Map of cache details (cache_on and ttl) (null: block is disabled)
     */
    public function caching_environment() : ?array
    {
        $info = [];
        $info['cache_on'] = <<<'PHP'
        [
        ]
PHP;
        $info['ttl'] = 60 * 60 * 24 * 365 * 5;
        return $info;
    }

    /**
     * Execute the block.
     *
     * @param  array $map A map of parameters
     * @return Tempcode The result of execution
     */
    public function run(array $map) : object
    {
        require_code('privacy');

        $cookies = [];

        $sections = [];

        $sections[do_lang('COOKIES')] = [
            'HEADING' => do_lang_tempcode('COOKIES'),
            'POSITIVE' => [],
            'GENERAL' => [],
        ];

        $hook_obs = find_all_hook_obs('systems', 'privacy', 'Hook_privacy_');
        foreach ($hook_obs as $hook => $hook_ob) {
            $info = $hook_ob->info();
            if ($info === null) {
                continue;
            }

            foreach ($info['cookies'] as $name => $details) {
                if ($details === null) {
                    continue;
                }

                $cookies[] = [
                    'NAME' => $name,
                    'REASON' => $details['reason'],
                ];
            }

            foreach ($info['positive'] as $details) {
                if ($details === null) {
                    continue;
                }

                $heading = $details['heading'];

                if (!array_key_exists($heading, $sections)) {
                    $sections[$heading] = [
                        'HEADING' => $heading,
                        'POSITIVE' => [],
                        'GENERAL' => [],
                    ];
                }

                $sections[$heading]['POSITIVE'][] = [
                    'EXPLANATION' => $details['explanation'],
                ];
            }

            foreach ($info['general'] as $details) {
                if ($details === null) {
                    continue;
                }

                $heading = $details['heading'];

                if (!array_key_exists($heading, $sections)) {
                    $sections[$heading] = [
                        'HEADING' => $heading,
                        'POSITIVE' => [],
                        'GENERAL' => [],
                    ];
                }

                $sections[$heading]['GENERAL'][] = [
                    'ACTION' => $details['action'],
                    'REASON' => $details['reason'],
                ];
            }
        }

        cms_mb_ksort($sections, SORT_NATURAL | SORT_FLAG_CASE);

        sort_maps_by($cookies, 'NAME');

        $hash = md5(serialize($sections) . serialize($cookies));
        $previous_hash = get_value('privacy_policy_auto__hash', null, true);
        if ($hash !== $previous_hash) {
            set_value('privacy_policy_auto__hash', $hash, true);
            set_value('privacy_policy_auto__time', strval(time()), true);
        }

        return do_template('BLOCK_MAIN_PRIVACY_POLICY_AUTO', [
            '_GUID' => '0abf65878c508bf244836589a8cc45da',
            'SECTIONS' => $sections,
            'COOKIES' => $cookies,
        ]);
    }
}
