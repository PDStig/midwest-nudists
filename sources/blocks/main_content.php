<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/**
 * Block class.
 */
class Block_main_content
{
    /**
     * Find details of the block.
     *
     * @return ?array Map of block info (null: block is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Chris Graham';
        $info['organisation'] = 'Composr';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 2;
        $info['locked'] = false;
        $info['min_cms_version'] = 11.0;
        $info['addon'] = 'core';
        $info['parameters'] = [
            'filter',
            'param',
            'id',
            'select',
            'select_b',
            'title',
            'zone',
            'days',
            'no_links',
            'give_context',
            'include_breadcrumbs',
            'render_if_empty',
            'guid',
            'as_guest',
            'check',
        ];
        return $info;
    }

    /**
     * Find caching details for the block.
     *
     * @return ?array Map of cache details (cache_on and ttl) (null: block is disabled)
     */
    public function caching_environment() : ?array
    {
        $info = [];
        $info['cache_on'] = <<<'PHP'
        (array_key_exists('check', $map) && $map['check'] == '1') ? null : [
            array_key_exists('as_guest', $map) ? ($map['as_guest'] == '1') : false,
            array_key_exists('render_if_empty', $map) ? $map['render_if_empty'] : '1',
            array_key_exists('guid', $map) ? $map['guid'] : '',
            (array_key_exists('give_context', $map) ? $map['give_context'] : '0') == '1',
            (array_key_exists('include_breadcrumbs', $map) ? $map['include_breadcrumbs'] : '0') == '1',
            array_key_exists('filter', $map) ? $map['filter'] : '',
            array_key_exists('no_links', $map) ? $map['no_links'] : 0,
            ((array_key_exists('days', $map)) && ($map['days'] != '')) ? intval($map['days']) : null,
            array_key_exists('title', $map) ? $map['title'] : null,
            array_key_exists('param', $map) ? $map['param'] : 'download',
            array_key_exists('id', $map) ? $map['id'] : '',
            array_key_exists('select', $map) ? $map['select'] : '',
            array_key_exists('select_b', $map) ? $map['select_b'] : '',
            array_key_exists('zone', $map) ? $map['zone'] : '_SEARCH',
        ]
PHP;
        $info['special_cache_flags'] = CACHE_AGAINST_DEFAULT | CACHE_AGAINST_PERMISSIVE_GROUPS;
        if (addon_installed('content_privacy')) {
            $info['special_cache_flags'] |= CACHE_AGAINST_MEMBER;
        }
        $info['ttl'] = 60 * 24; // Intentionally, do randomisation acts as 'of the day'
        return $info;
    }

    /**
     * Execute the block.
     *
     * @param  array $map A map of parameters
     * @return Tempcode The result of execution
     */
    public function run(array $map) : object
    {
        $block_id = get_block_id($map);

        // Determine content type
        require_code('content');
        if (isset($map['param'])) {
            $content_type = $map['param'];
        } else {
            if (addon_installed('downloads')) {
                $content_type = 'download';
            } else {
                $hooks = find_all_hooks('systems', 'content_meta_aware');
                $content_type = key($hooks);
            }
        }

        // Read content ID, if there is one
        $content_id = isset($map['id']) ? $map['id'] : null;
        if (($content_type == 'member') && ($content_id !== null) && (!is_numeric($content_id))) { // FUDGE: For member URL preview boxes to work
            $content_id = @strval($GLOBALS['FORUM_DRIVER']->get_member_from_username($content_id));
        }

        // Read selection parameters
        $check_perms = array_key_exists('check', $map) ? ($map['check'] == '1') : true;
        $randomise = ($content_id === null);
        $select = isset($map['select']) ? $map['select'] : '';
        $select_b = isset($map['select_b']) ? $map['select_b'] : '';
        $filter = isset($map['filter']) ? $map['filter'] : '';
        $days = empty($map['days']) ? null : intval($map['days']);

        // Read display parameters
        $guid = isset($map['guid']) ? $map['guid'] : '';
        $zone = isset($map['zone']) ? $map['zone'] : '_SEARCH';
        $title = isset($map['title']) ? make_string_tempcode(escape_html($map['title'])) : '';
        $give_context = (isset($map['give_context']) ? $map['give_context'] : '0') == '1';
        $include_breadcrumbs = (isset($map['include_breadcrumbs']) ? $map['include_breadcrumbs'] : '0') == '1';

        // Read content object
        $object = get_content_object($content_type);
        $info = ($object === null) ? null : $object->info($zone, true, ($select_b == '') ? null : $select_b);
        if ($info === null) {
            return do_template('RED_ALERT', ['_GUID' => 'd924cfae792e5f73a284731d4d98f216', 'TEXT' => do_lang_tempcode('NO_SUCH_CONTENT_TYPE', escape_html($content_type))]);
        }

        // Moniker?
        if (((!array_key_exists('id_field_numeric', $info)) || ($info['id_field_numeric'])) && ($content_id !== null) && (!is_numeric($content_id))) {
            list(, $resource_page, $resource_type) = explode(':', $info['view_page_link_pattern']);
            $content_id = $info['db']->query_select_value_if_there('url_id_monikers', 'm_resource_id', ['m_resource_page' => $resource_page, 'm_resource_type' => $resource_type, 'm_moniker' => $content_id]);
            if ($content_id === null) {
                return do_template('RED_ALERT', ['_GUID' => 'de3f6b102ce95e42879f51997fc218c9', 'TEXT' => do_lang_tempcode('MISSING_RESOURCE')]);
            }
        }

        // Submit URL
        if ($info['add_url'] !== null) {
            $submit_url = page_link_to_tempcode_url($info['add_url']);
        } else {
            $submit_url = new Tempcode();
        }
        if (!has_actual_page_access(null, $info['cms_page'], null, null)) {
            $submit_url = new Tempcode();
        }

        if ($randomise) { // Randomisation mode
            list($rows) = content_rows_for_type($content_type, $days, '', '', 'random', 0, 1, $select, $select_b, $filter, $check_perms);
        } else { // Select mode
            if ($content_type == 'comcode_page') { // FUDGE
                // Try and force a parse of the page, so it's in the system
                $bits = explode(':', $content_id);
                push_output_state();
                $result = request_page(array_key_exists(1, $bits) ? $bits[1] : get_comcode_zone($bits[0]), false, $bits[0], 'comcode_custom', true);
                restore_output_state();
                if ($result->is_empty()) {
                    return do_template('RED_ALERT', ['_GUID' => '569404aebb825d11a5473b62f6446618', 'TEXT' => do_lang_tempcode('MISSING_RESOURCE')]);
                }
            }

            // Get row
            $lang_fields = find_lang_fields($info['table'], 'r');
            $wherea = get_content_where_for_str_id($content_id, $info, 'r');
            $rows = $info['db']->query_select($info['table'] . ' r', ['r.*'], $wherea, '', 1, 0, false, $lang_fields);
        }

        // Nothing to show?
        if (!array_key_exists(0, $rows)) {
            if ((isset($map['render_if_empty'])) && ($map['render_if_empty'] == '0')) {
                return new Tempcode();
            }

            return do_template('BLOCK_NO_ENTRIES', [
                '_GUID' => ($guid != '') ? $guid : '12d8cdc62cd78480b83c8daaaa68b686',
                'BLOCK_ID' => $block_id,
                'HIGH' => true,
                'TITLE' => $title,
                'MESSAGE' => do_lang_tempcode('MISSING_RESOURCE', $content_type),
                'ADD_NAME' => $object->content_language_string('ADD'),
                'SUBMIT_URL' => $submit_url,
            ]);
        }

        $row = $rows[0];

        // Links...

        $content_id = extract_content_str_id_from_data($row, $info);

        if ($info['archive_url'] !== null) {
            $archive_url = page_link_to_tempcode_url($info['archive_url']);
        } else {
            $archive_url = new Tempcode();
        }

        if ((isset($map['no_links'])) && ($map['no_links'] == '1')) {
            $submit_url = new Tempcode();
            $archive_url = new Tempcode();
        }

        // Render...

        $rendered_content = $object->render_box($row, $zone, $give_context, $include_breadcrumbs, null, false, $guid);

        $raw_date = ($info['date_field'] == '') ? null : $row[$info['date_field']];

        return do_template('BLOCK_MAIN_CONTENT', [
            '_GUID' => ($guid != '') ? $guid : 'fce1eace6008d650afc0283a7be9ec30',
            'BLOCK_ID' => $block_id,
            'TYPE' => $object->get_content_type_label($row),
            'TITLE' => $title,
            'RAW_DATE' => ($raw_date === null) ? '' : strval($raw_date),
            'DATE' => ($raw_date === null) ? new Tempcode() : get_timezoned_date_time_tempcode($raw_date),
            'CONTENT' => $rendered_content,
            'ADD_NAME' => $object->content_language_string('ADD'),
            'SUBMIT_URL' => $submit_url,
            'ARCHIVE_URL' => $archive_url,
        ]);
    }
}
