<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * Add a forum poll.
 *
 * @param  AUTO_LINK $topic_id The ID of the topic to add the poll to
 * @param  SHORT_TEXT $question The question
 * @param  BINARY $is_private Whether the result tallies are kept private until the poll is made non-private
 * @param  BINARY $is_open Whether the poll is open for voting
 * @param  integer $minimum_selections The minimum number of selections that may be made
 * @param  integer $maximum_selections The maximum number of selections that may be made
 * @param  BINARY $requires_reply Whether members must have a post in the topic before they made vote
 * @param  array $answers A list of the potential voteable answers
 * @param  BINARY $view_member_votes Whether others should be able to view individual members' votes in the results
 * @param  BINARY $vote_revocation Whether to allow voters to revoke their vote when the poll's voting is still open
 * @param  BINARY $guests_can_vote Whether guests can vote on the poll without logging in
 * @param  BINARY $point_weighting Whether results for this poll should be weighed based on voter's points
 * @param  boolean $check_permissions Whether to check there are permissions to make the poll
 * @param  ?TIME $poll_closing_time The time voting should close on this poll (null: the poll will not close automatically)
 * @return AUTO_LINK The ID of the newly created forum poll
 */
function cns_make_poll(int $topic_id, string $question, int $is_private, int $is_open, int $minimum_selections, int $maximum_selections, int $requires_reply, array $answers, int $view_member_votes, int $vote_revocation, int $guests_can_vote, int $point_weighting, bool $check_permissions = true, ?int $poll_closing_time = null) : int
{
    require_code('cns_polls');
    require_code('cns_polls_action3');

    if (($check_permissions) && (!cns_may_attach_poll($topic_id))) {
        access_denied('I_ERROR');
    }

    $topic_poll_id = $GLOBALS['FORUM_DB']->query_select_value_if_there('f_topics', 't_poll_id', ['id' => $topic_id]);
    if ($topic_poll_id) {
        warn_exit(do_lang_tempcode('TOPIC_POLL_ALREADY_EXISTS'));
    }

    cns_validate_poll($topic_id, null, $answers, $is_private, $is_open, $minimum_selections, $maximum_selections, $requires_reply, $poll_closing_time, $view_member_votes, $vote_revocation, $guests_can_vote, $point_weighting);

    $poll_id = $GLOBALS['FORUM_DB']->query_insert('f_polls', [
        'po_question' => $question,
        'po_cache_total_votes' => 0,
        'po_cache_voting_power' => 0.0,
        'po_is_private' => $is_private,
        'po_is_open' => $is_open,
        'po_minimum_selections' => $minimum_selections,
        'po_maximum_selections' => $maximum_selections,
        'po_requires_reply' => $requires_reply,
        'po_closing_time' => $poll_closing_time,
        'po_view_member_votes' => $view_member_votes,
        'po_vote_revocation' => $vote_revocation,
        'po_guests_can_vote' => $guests_can_vote,
        'po_point_weighting' => $point_weighting
    ], true);

    foreach ($answers as $i => $answer) {
        $GLOBALS['FORUM_DB']->query_insert('f_poll_answers', [
            'pa_poll_id' => $poll_id,
            'pa_answer' => $answer,
            'pa_cache_num_votes' => 0,
            'pa_cache_voting_power' => 0.0,
            'pa_order' => $i
        ]);
    }

    $map = ['t_poll_id' => $poll_id];

    $GLOBALS['FORUM_DB']->query_update('f_topics', $map, ['id' => $topic_id], '', 1);

    return $poll_id;
}
