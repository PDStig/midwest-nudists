<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/**
 * Standard code module initialisation function.
 *
 * @ignore
 */
function init__wordfilter()
{
    if (!defined('WORDFILTER_MATCH_TYPES')) {
        define('WORDFILTER_MATCH_TYPE_FULL', 'full');
        define('WORDFILTER_MATCH_TYPE_SUBSTRING', 'substring');
        define('WORDFILTER_MATCH_TYPE_PREFIX', 'prefix');

        define('WORDFILTER_MATCH_TYPES', [
            WORDFILTER_MATCH_TYPE_FULL,
            WORDFILTER_MATCH_TYPE_SUBSTRING,
            WORDFILTER_MATCH_TYPE_PREFIX,
        ]);

        define('WORDFILTER_REPLACEMENT_GRAWLIXES', '%GRAWLIXES%');
        // A special value for the `wordfilter.w_replacement` column to replace matching words with grawlixes
    }

    global $WORDFILTERING_ALREADY;
    $WORDFILTERING_ALREADY = false;
}

/**
 * Check the specified text for banned words.
 * If any are found, and the member cannot bypass the word filter, an error message is displayed.
 *
 * @param  string $input The sentence to check
 * @param  ?ID_TEXT $name The name of the parameter this is coming from. Certain parameters are not checked, for reasons of efficiency (avoiding loading whole word check list if not needed) (null: don't know param, do not check to avoid)
 * @param  boolean $exit Whether to die on fully blocked words (useful if importing, for instance)
 * @param  boolean $try_patterns Whether to try pattern matching (this takes more resources)
 * @param  boolean $perm_check Whether to allow permission-based skipping, and length-based skipping
 * @return string "Fixed" version
 */
function check_wordfilter(string $input, ?string $name = null, bool $exit = true, bool $try_patterns = false, bool $perm_check = true) : string
{
    global $WORDFILTERING_ALREADY;
    if ($WORDFILTERING_ALREADY) {
        return $input;
    }

    if ($perm_check) {
        if (strlen($input) < 3) {
            return $input;
        }
        if ((function_exists('has_privilege')) && (!$GLOBALS['MICRO_AJAX_BOOTUP']) && (has_privilege(get_member(), 'bypass_wordfilter'))) {
            return $input;
        }
    }

    // Load filter
    global $WORDS_TO_FILTER_CACHE;
    if ($WORDS_TO_FILTER_CACHE === null) {
        $WORDS_TO_FILTER_CACHE = [];
        if (addon_installed('wordfilter')) {
            $rows = $GLOBALS['SITE_DB']->query_select('wordfilter', ['*']);
            if ($rows !== null) {
                foreach ($rows as $i => $r) {
                    if (($i == 0) && (!array_key_exists('w_replacement', $r))) {
                        return $input; // Safe upgrading
                    }
                    $WORDS_TO_FILTER_CACHE[cms_mb_strtolower($r['word'])] = $r;
                }
            }
        }
    }

    // Find words
    require_code('global4');
    $words = cms_mb_str_word_count($input, 2);

    // Apply filter for complete blocked words
    $changes = [];
    foreach ($words as $pos => $word) {
        $word_lower = cms_mb_strtolower($word);

        $w = isset($WORDS_TO_FILTER_CACHE[$word_lower]) ? $WORDS_TO_FILTER_CACHE[$word_lower] : null;

        // Apply filter for disallowed complete word matches
        if (is_array($w) && ($w['w_match_type'] === WORDFILTER_MATCH_TYPE_FULL)) {
            if (($w['w_replacement'] == '') && ($exit)) {
                warn_exit_wordfilter($name, do_lang_tempcode('WORDFILTER_YOU', escape_html($word))); // In soviet Russia, words filter you
            } else {
                $changes[] = [$pos, $word_lower, $w['w_replacement']];
            }
        }

        // Apply filter for disallowed prefixes, and wildcards
        foreach ($WORDS_TO_FILTER_CACHE as $word2 => $w2) {
            if ($w2['w_match_type'] === WORDFILTER_MATCH_TYPE_PREFIX) {
                if (stripos($word, $word2) === 0) { // if $word starts with $word2
                    if (($w2['w_replacement'] == '') && ($exit)) {
                        warn_exit_wordfilter($name, do_lang_tempcode('WORDFILTER_YOU', escape_html($word))); // In soviet Russia, words filter you
                    } else {
                        $changes[] = [$pos, $word_lower, $w2['w_replacement']];
                    }
                }
            } elseif ($try_patterns && ($w2['w_match_type'] === WORDFILTER_MATCH_TYPE_FULL) && (simulated_wildcard_match($word, $word2, true))) {
                if (($w2['w_replacement'] == '') && ($exit)) {
                    warn_exit_wordfilter($name, do_lang_tempcode('WORDFILTER_YOU', escape_html($word))); // In soviet Russia, words filter you
                } else {
                    $changes[] = [$pos, $word_lower, $w2['w_replacement']];
                }
            }
        }
    }

    // Make changes
    $changes = array_reverse($changes);
    foreach ($changes as $change) {
        $before = substr($input, 0, $change[0]);
        $after = substr($input, $change[0] + strlen($change[1]));
        if ($change[2] === WORDFILTER_REPLACEMENT_GRAWLIXES) {
            $change[2] = generate_grawlixes($change[1]);
        }
        $input = $before . $change[2] . $after;
    }

    // Apply filter for disallowed substrings
    foreach ($WORDS_TO_FILTER_CACHE as $word => $w) {
        if (is_integer($word)) {
            $word = strval($word);
        }

        if (($w['w_match_type'] === WORDFILTER_MATCH_TYPE_SUBSTRING) && (stripos($input, $word) !== false)) {
            $replacement = $w['w_replacement'];
            if (($replacement == '') && ($exit)) {
                warn_exit_wordfilter($name, do_lang_tempcode('WORDFILTER_YOU', escape_html($word)));
            } else {
                if ($replacement === WORDFILTER_REPLACEMENT_GRAWLIXES) {
                    $replacement = generate_grawlixes($word);
                }
                $input = preg_replace('#' . preg_quote($word) . '#i', $replacement, $input);
            }
        }
    }

    return $input;
}

/**
 * Exit with a message about word-filtering.
 *
 * @param  ?ID_TEXT $name The name of the parameter this is coming from. Certain parameters are not checked, for reasons of efficiency (avoiding loading whole word check list if not needed) (null: don't know param, do not check to avoid)
 * @param  Tempcode $message Error message
 * @exits
 */
function warn_exit_wordfilter(?string $name, object $message)
{
    global $WORDFILTERING_ALREADY;
    $WORDFILTERING_ALREADY = true;

    if ($name === null) {
        warn_exit($message);
    }

    require_code('failure');
    if (throwing_errors()) {
        throw new CMSException($message);
    }

    require_code('global3');
    set_http_status_code(400);

    // Output our error / correction form
    cms_ob_end_clean(); // Emergency output, potentially, so kill off any active buffer
    $hidden = build_keep_post_fields([$name]);
    require_code('form_templates');
    $value = post_param_string($name);
    if (strpos($value, "\n") === false) {
        $fields = form_input_line(do_lang_tempcode('CHANGE'), '', $name, $value, true);
    } else {
        $fields = form_input_text(do_lang_tempcode('CHANGE'), '', $name, $value, true, false);
    }
    $post_url = get_self_url();
    $output = do_template('FORM_SCREEN', [
        '_GUID' => 'e644c444027b244ebc382eae66ae23fc',
        'TITLE' => get_screen_title('ERROR_OCCURRED'),
        'TEXT' => $message,
        'URL' => $post_url,
        'HIDDEN' => $hidden,
        'FIELDS' => $fields,
        'SUBMIT_ICON' => 'buttons/proceed',
        'SUBMIT_NAME' => do_lang_tempcode('PROCEED'),
    ]);
    $echo = globalise($output, null, '', true);
    $echo->handle_symbol_preprocessing();
    $echo->evaluate_echo();
    exit();
}

/**
 * Generate grawlixes for a word.
 * The term grawlix refers to the series of typographical symbols (such as @#$%!) used to represent swear words.
 *
 * @param  string $word Word
 * @return string Generated grawlixes
 */
function generate_grawlixes(string $word) : string
{
    static $cache = [];
    if (isset($cache[$word])) {
        return $cache[$word];
    }

    $length = cms_mb_strlen($word);

    $symbols = ['!', '@', '#', '$', '%', /*'&'Could cause issues with HTML and possibly even exploits, */'*'];

    $r_max = count($symbols) - 1;

    $str = '';
    $last_symbol = null;
    for ($i = 0; $i < $length; $i++) {
        do { // Make sure a symbol doesn't appear twice consecutively
            $symbol = $symbols[mt_rand(0, $r_max)];
        } while ($symbol === $last_symbol);

        $str .= $symbol;

        $last_symbol = $symbol;
    }

    $cache[$word] = $str;

    return $str;
}
