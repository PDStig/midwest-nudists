<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    newsletter
 */

/*
 * ---------------------------------------------
 * Managing newsletter subscribers, and settings
 * ---------------------------------------------
 */

/**
 * Script to read in a bounced e-mail.
 */
function incoming_bounced_email_script()
{
    if (!addon_installed('newsletter')) {
        warn_exit(do_lang_tempcode('MISSING_ADDON', escape_html('newsletter')));
    }

    if (!GOOGLE_APPENGINE) {
        return;
    }

    if (!gae_is_admin()) {
        return;
    }

    header('X-Robots-Tag: noindex');

    $bounce_email = file_get_contents('php://input');

    $matches = [];
    if (preg_match('#^From: .*([^ ]+@[^ ]+)#m', $bounce_email, $matches) != 0) {
        $email = $matches[1];

        $id = $GLOBALS['SITE_DB']->query_select_value_if_there('newsletter_subscribers', 'id', ['email' => $email]);
        if ($id !== null) {
            delete_newsletter_subscriber($id);
        }
    }
}

/**
 * Add to the newsletter, in the simplest way.
 * No authorisation support here, checks it works only for non-subscribed or non-confirmed members.
 *
 * @param  EMAIL $email The e-mail address of the subscriber
 * @param  ?LANGUAGE_NAME $language The language (null: users)
 * @param  boolean $get_confirm_mail Whether to require a confirmation mail
 * @param  ?AUTO_LINK $newsletter_id The newsletter to join (null: the first)
 * @param  string $forename Subscribers forename
 * @param  string $surname Subscribers surname
 * @return string Newsletter password
 */
function basic_newsletter_join(string $email, ?string $language = null, bool $get_confirm_mail = false, ?int $newsletter_id = null, string $forename = '', string $surname = '') : string
{
    require_lang('newsletter');

    if ($language === null) {
        $language = user_lang();
    }
    if ($newsletter_id === null) {
        $newsletter_id = db_get_first_id();
    }

    $code_confirm = $GLOBALS['SITE_DB']->query_select_value_if_there('newsletter_subscribers', 'code_confirm', ['email' => $email]);
    if ($code_confirm === null) {
        // New, set their details
        require_code('crypt');
        $password = get_secure_random_password(null, $forename . $surname, $email);
        $salt = get_secure_random_string(32, CRYPT_BASE64);
        $code_confirm = $get_confirm_mail ? get_secure_random_number() : 0;
        add_newsletter_subscriber($email, time(), $code_confirm, ratchet_hash($password, $salt), $salt, $language, $forename, $surname);
    } else {
        if ($code_confirm > 0) {
            // Was not confirmed, allow confirm mail to go again as if this was new, and update their details
            $id = $GLOBALS['SITE_DB']->query_select_value_if_there('newsletter_subscribers', 'id', ['email' => $email]);
            if ($id !== null) {
                edit_newsletter_subscriber($id, $email, time(), null, null, null, $language, $forename, $surname);
            }
            $password = do_lang('NEWSLETTER_PASSWORD_ENCRYPTED');
        } else {
            // Already on newsletter and confirmed so don't allow tampering without authorisation, which this method can't do
            return do_lang('NA');
        }
    }

    // Send confirm e-mail
    if ($get_confirm_mail) {
        $_url = build_url(['page' => 'newsletter', 'type' => 'confirm', 'email' => $email, 'confirm' => $code_confirm], get_module_zone('newsletter'));
        $url = $_url->evaluate();
        $newsletter_url = build_url(['page' => 'newsletter'], get_module_zone('newsletter'));
        $message = do_lang('NEWSLETTER_SIGNUP_TEXT', comcode_escape($url), comcode_escape($password), [$forename, $surname, $email, get_site_name(), $newsletter_url->evaluate()], $language);
        require_code('mail');
        dispatch_mail(do_lang('NEWSLETTER_SIGNUP', null, null, null, $language), $message, do_lang('mail:NO_MAIL_WEB_VERSION__SENSITIVE'), [$email], null, '', '', ['bypass_queue' => true]);
    }

    // Set subscription
    $GLOBALS['SITE_DB']->query_delete('newsletter_subscribe', ['newsletter_id' => $newsletter_id, 'email' => $email], '', 1);
    $GLOBALS['SITE_DB']->query_insert('newsletter_subscribe', ['newsletter_id' => $newsletter_id, 'email' => $email], false, true); // race condition

    return $password;
}

/**
 * Add a newsletter subscriber to the system (not to any particular newsletters though).
 *
 * @param  EMAIL $email The e-mail address of the subscriber
 * @param  TIME $join_time The join time
 * @param  integer $code_confirm Confirm code
 * @param  ID_TEXT $password Newsletter password (hashed)
 * @param  ID_TEXT $salt Newsletter salt
 * @param  LANGUAGE_NAME $language The language
 * @param  string $forename Subscribers forename
 * @param  string $surname Subscribers surname
 * @return AUTO_LINK Subscriber ID
 */
function add_newsletter_subscriber(string $email, int $join_time, int $code_confirm, string $password, string $salt, string $language, string $forename, string $surname) : int
{
    $GLOBALS['SITE_DB']->query_delete('newsletter_subscribers', [
        'email' => $email,
    ]);
    $id = $GLOBALS['SITE_DB']->query_insert('newsletter_subscribers', [
        'email' => $email,
        'join_time' => $join_time,
        'code_confirm' => $code_confirm,
        'the_password' => $password,
        'pass_salt' => $salt,
        'language' => $language,
        'n_forename' => $forename,
        'n_surname' => $surname,
    ], true, true/*race condition*/);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        generate_resource_fs_moniker('newsletter_subscriber', strval($id), null, null, true);
    }

    return $id;
}

/**
 * Edit a newsletter subscriber.
 *
 * @param  AUTO_LINK $id Subscriber ID
 * @param  ?EMAIL $email The e-mail address of the subscriber (null: don't change)
 * @param  ?TIME $join_time The join time (null: don't change)
 * @param  ?integer $code_confirm Confirm code (null: don't change)
 * @param  ?ID_TEXT $password Newsletter password (hashed) (null: don't change)
 * @param  ?ID_TEXT $salt Newsletter salt (null: don't change)
 * @param  ?LANGUAGE_NAME $language The language (null: don't change)
 * @param  ?string $forename Subscribers forename (null: don't change)
 * @param  ?string $surname Subscribers surname (null: don't change)
 */
function edit_newsletter_subscriber(int $id, ?string $email = null, ?int $join_time = null, ?int $code_confirm = null, ?string $password = null, ?string $salt = null, ?string $language = null, ?string $forename = null, ?string $surname = null)
{
    $map = [];
    if ($email !== null) {
        $map['email'] = $email;
    }
    if ($join_time !== null) {
        $map['join_time'] = $join_time;
    }
    if ($code_confirm !== null) {
        $map['code_confirm'] = $code_confirm;
    }
    if ($password !== null) {
        $map['the_password'] = $password;
    }
    if ($salt !== null) {
        $map['pass_salt'] = $salt;
    }
    if ($language !== null) {
        $map['language'] = $language;
    }
    if ($forename !== null) {
        $map['n_forename'] = $forename;
    }
    if ($surname !== null) {
        $map['n_surname'] = $surname;
    }

    $GLOBALS['SITE_DB']->query_update('newsletter_subscribers', $map, ['id' => $id], '', 1);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        generate_resource_fs_moniker('newsletter_subscriber', strval($id));
    }
}

/**
 * Delete a newsletter subscriber.
 *
 * @param  AUTO_LINK $id Subscriber ID
 */
function delete_newsletter_subscriber(int $id)
{
    $GLOBALS['SITE_DB']->query_delete('newsletter_subscribers', ['id' => $id], '', 1);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        expunge_resource_fs_moniker('newsletter_subscriber', strval($id));
    }
}

/**
 * Remove bounced addresses from the newsletter / turn off staff e-mails on member accounts.
 *
 * @param  array $bounces List of e-mail addresses
 */
function remove_email_bounces(array $bounces)
{
    if (empty($bounces)) {
        return;
    }

    $delete_sql = '';
    $delete_sql_members = '';

    foreach ($bounces as $email_address) {
        if ($delete_sql != '') {
            $delete_sql .= ' OR ';
            $delete_sql_members .= ' OR ';
        }
        $delete_sql .= db_string_equal_to('email', $email_address);
        $delete_sql_members .= db_string_equal_to('m_email_address', $email_address);
    }

    $query = 'DELETE FROM ' . get_table_prefix() . 'newsletter_subscribers WHERE ' . $delete_sql;
    $GLOBALS['SITE_DB']->query($query);

    $query = 'DELETE FROM ' . get_table_prefix() . 'newsletter_subscribe WHERE ' . $delete_sql;
    $GLOBALS['SITE_DB']->query($query);

    if (get_forum_type() == 'cns') {
        $query = 'UPDATE ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_members SET m_allow_emails_from_staff=0 WHERE ' . $delete_sql_members;
        $GLOBALS['FORUM_DB']->query($query);
    }
}

/**
 * Make a newsletter.
 *
 * @param  SHORT_TEXT $title The title
 * @param  LONG_TEXT $description The description
 * @return AUTO_LINK The ID
 */
function add_newsletter(string $title, string $description) : int
{
    require_code('global4');
    prevent_double_submit('ADD_NEWSLETTER', null, $title);

    $map = [];
    $map += insert_lang('title', $title, 2);
    $map += insert_lang('the_description', $description, 2);
    $id = $GLOBALS['SITE_DB']->query_insert('newsletters', $map, true);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        generate_resource_fs_moniker('newsletter', strval($id), null, null, true);
    }

    log_it('ADD_NEWSLETTER', strval($id), $title);

    delete_cache_entry('main_newsletter_signup');

    return $id;
}

/**
 * Edit a newsletter.
 *
 * @param  AUTO_LINK $id The ID
 * @param  SHORT_TEXT $title The title
 * @param  LONG_TEXT $description The description
 */
function edit_newsletter(int $id, string $title, string $description)
{
    $rows = $GLOBALS['SITE_DB']->query_select('newsletters', ['*'], ['id' => $id], '', 1);

    if (!array_key_exists(0, $rows)) {
        warn_exit(do_lang_tempcode('MISSING_RESOURCE', 'newsletter'));
    }

    $myrow = $rows[0];
    $_title = $myrow['title'];
    $_description = $myrow['the_description'];
    $map = [];
    $map += lang_remap('title', $_title, $title);
    $map += lang_remap('the_description', $_description, $description);
    $GLOBALS['SITE_DB']->query_update('newsletters', $map, ['id' => $id], '', 1);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        generate_resource_fs_moniker('newsletter', strval($id));
    }

    log_it('EDIT_NEWSLETTER', strval($id), $_title);

    delete_cache_entry('main_newsletter_signup');
}

/**
 * Delete a newsletter.
 *
 * @param  AUTO_LINK $id The ID
 */
function delete_newsletter(int $id)
{
    $rows = $GLOBALS['SITE_DB']->query_select('newsletters', ['*'], ['id' => $id], '', 1);

    if (!array_key_exists(0, $rows)) {
        warn_exit(do_lang_tempcode('INTERNAL_ERROR', escape_html('392b6146d82c5d00ab6b368b967c45de')));
    }

    $myrow = $rows[0];
    $_title = $myrow['title'];
    $_description = $myrow['the_description'];

    $GLOBALS['SITE_DB']->query_delete('newsletters', ['id' => $id], '', 1);
    $GLOBALS['SITE_DB']->query_delete('newsletter_subscribe', ['newsletter_id' => $id]);
    delete_lang($_title);
    delete_lang($_description);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        expunge_resource_fs_moniker('newsletter', strval($id));
    }

    log_it('DELETE_NEWSLETTER', strval($id), get_translated_text($_title));

    delete_cache_entry('main_newsletter_signup');
}

/**
 * Make a periodic newsletter.
 *
 * @param  LONG_TEXT $subject Subject
 * @param  LONG_TEXT $message Message
 * @param  LANGUAGE_NAME $lang Language to send for
 * @param  LONG_TEXT $send_details The data sent in each newsletter
 * @param  BINARY $html_only Whether to send in HTML only
 * @param  SHORT_TEXT $from_email From address
 * @param  SHORT_TEXT $from_name From name
 * @param  SHORT_INTEGER $priority Priority
 * @param  LONG_TEXT $spreadsheet_data Spreadsheet data of who to send to (JSON)
 * @param  SHORT_TEXT $frequency Send frequency
 * @set weekly biweekly monthly
 * @param  SHORT_INTEGER $day Weekday to send on
 * @param  BINARY $in_full Embed full articles
 * @param  ID_TEXT $template Mail template to use, e.g. MAIL
 * @param  ?TIME $last_sent When was last sent (null: now)
 * @return AUTO_LINK The ID
 */
function add_periodic_newsletter(string $subject, string $message, string $lang, string $send_details, int $html_only, string $from_email, string $from_name, int $priority, string $spreadsheet_data, string $frequency, int $day, int $in_full = 0, string $template = 'MAIL', ?int $last_sent = null) : int
{
    require_code('global4');
    prevent_double_submit('ADD_PERIODIC_NEWSLETTER', null, $subject);

    if ($last_sent === null) {
        $last_sent = time();
    }

    $id = $GLOBALS['SITE_DB']->query_insert('newsletter_periodic', [
        'np_subject' => $subject,
        'np_message' => $message,
        'np_lang' => $lang,
        'np_send_details' => $send_details,
        'np_html_only' => $html_only,
        'np_from_email' => $from_email,
        'np_from_name' => $from_name,
        'np_priority' => $priority,
        'np_spreadsheet_data' => $spreadsheet_data,
        'np_frequency' => $frequency,
        'np_day' => $day,
        'np_in_full' => $in_full,
        'np_template' => $template,
        'np_last_sent_time' => $last_sent,
    ], true);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        generate_resource_fs_moniker('periodic_newsletter', strval($id), null, null, true);
    }

    log_it('ADD_PERIODIC_NEWSLETTER', strval($id), $subject);

    return $id;
}

/**
 * Edit a periodic newsletter.
 *
 * @param  AUTO_LINK $id The ID
 * @param  LONG_TEXT $subject Subject
 * @param  LONG_TEXT $message Message
 * @param  LANGUAGE_NAME $lang Language to send for
 * @param  LONG_TEXT $send_details The data sent in each newsletter
 * @param  BINARY $html_only Whether to send in HTML only
 * @param  SHORT_TEXT $from_email From address
 * @param  SHORT_TEXT $from_name From name
 * @param  SHORT_INTEGER $priority Priority
 * @param  LONG_TEXT $spreadsheet_data Spreadsheet data of who to send to (JSON)
 * @param  SHORT_TEXT $frequency Send frequency
 * @set weekly biweekly monthly
 * @param  SHORT_INTEGER $day Weekday to send on
 * @param  BINARY $in_full Embed full articles
 * @param  ID_TEXT $template Mail template to use, e.g. MAIL
 * @param  ?TIME $last_sent When was last sent (null: don't change)
 */
function edit_periodic_newsletter(int $id, string $subject, string $message, string $lang, string $send_details, int $html_only, string $from_email, string $from_name, int $priority, string $spreadsheet_data, string $frequency, int $day, int $in_full, string $template, ?int $last_sent = null)
{
    $rows = $GLOBALS['SITE_DB']->query_select('newsletter_periodic', ['*'], ['id' => $id], '', 1);

    if (!array_key_exists(0, $rows)) {
        warn_exit(do_lang_tempcode('MISSING_RESOURCE'));
    }

    $map = [
        'np_subject' => $subject,
        'np_message' => $message,
        'np_lang' => $lang,
        'np_send_details' => $send_details,
        'np_html_only' => $html_only,
        'np_from_email' => $from_email,
        'np_from_name' => $from_name,
        'np_priority' => $priority,
        'np_spreadsheet_data' => $spreadsheet_data,
        'np_frequency' => $frequency,
        'np_day' => $day,
        'np_in_full' => $in_full,
        'np_template' => $template,
    ];
    if ($last_sent !== null) {
        $map['np_last_sent_time'] = $last_sent;
    }
    $GLOBALS['SITE_DB']->query_update('newsletter_periodic', $map, ['id' => $id]);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        generate_resource_fs_moniker('periodic_newsletter', strval($id));
    }

    log_it('EDIT_PERIODIC_NEWSLETTER', strval($id), $subject);
}

/**
 * Delete a periodic newsletter.
 *
 * @param  AUTO_LINK $id The ID
 */
function delete_periodic_newsletter(int $id)
{
    $rows = $GLOBALS['SITE_DB']->query_select('newsletter_periodic', ['*'], ['id' => $id], '', 1);

    if (!array_key_exists(0, $rows)) {
        warn_exit(do_lang_tempcode('INTERNAL_ERROR', escape_html('846ade3344825beb9de7eea1f54b5985')));
    }

    $subject = $rows[0]['np_subject'];

    $GLOBALS['SITE_DB']->query_delete('newsletter_periodic', ['id' => $id]);

    if ((addon_installed('commandr')) && (!running_script('install')) && (!get_mass_import_mode())) {
        require_code('resource_fs');
        expunge_resource_fs_moniker('periodic_newsletter', strval($id));
    }

    log_it('DELETE_PERIODIC_NEWSLETTER', strval($id), $subject);
}
