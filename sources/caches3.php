<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/**
 * Standard code module initialisation function.
 *
 * @ignore
 */
function init__caches3()
{
    global $ERASED_TEMPLATES_ONCE;
    $ERASED_TEMPLATES_ONCE = false;

    if (!defined('TEMPLATE_DECACHE_BASE')) {
        // Special ways of decaching templates
        define('TEMPLATE_DECACHE_BASE', '\{\+START,INCLUDE');
        // -
        define('TEMPLATE_DECACHE_WITH_LANG', '\{\!|\{\$CHARSET' . '|' . TEMPLATE_DECACHE_BASE);
        define('TEMPLATE_DECACHE_WITH_THEME_IMAGE', '\{\$IMG|\{\$IMG_INLINE' . '|' . TEMPLATE_DECACHE_BASE);
        define('TEMPLATE_DECACHE_WITH_CONFIG', '\{\!|\{\$IMG|\{\$SITE_NAME|\{\$CONFIG_OPTION|\{\$SITE_SCOPE|\{\$SSW|\{\$DOMAIN|\{\$STAFF_ADDRESS|\{\$SHOW_DOCS|\{\$COPYRIGHT|\{\$VALID_FILE_TYPES\{\$BRAND_|\{\$INLINE_STATS' . '|' . TEMPLATE_DECACHE_BASE);
        define('TEMPLATE_DECACHE_WITH_ADDON', '\{\$ADDON_INSTALLED' . '|' . TEMPLATE_DECACHE_WITH_CONFIG);
        // -
        define('TEMPLATE_DECACHE_WITH_ANYTHING_INTERESTING', TEMPLATE_DECACHE_WITH_ADDON); // because TEMPLATE_DECACHE_WITH_ADDON actually does include everything already, via chaining
    }
}

/**
 * Automatically empty caches.
 *
 * @param  boolean $changed_base_url Whether the base URL has just been changed
 */
function auto_decache(bool $changed_base_url)
{
    delete_value('cdn');
    erase_block_cache();
    erase_cached_templates(!$changed_base_url);
    erase_comcode_cache();
    erase_cached_language();
    if (class_exists('Self_learning_cache')) {
        Self_learning_cache::erase_smart_cache();
    }
    erase_persistent_cache();
    if ($changed_base_url) {
        erase_comcode_page_cache();
        set_value('last_base_url', get_base_url());
    }
}

/**
 * Run the specified software cleanup tools.
 *
 * @param  ?array $cleanup_tools The cleanup tools to run (null: all)
 * @return Tempcode Any messages returned
 */
function run_cleanup_tools(?array $cleanup_tools = null) : object
{
    require_lang('cleanup');

    $old_limit = cms_extend_time_limit(TIME_LIMIT_EXTEND__CRAWL);

    $messages = new Tempcode();
    $hooks = find_all_hooks('systems', 'cleanup');
    if ((array_key_exists('cns', $hooks)) && (array_key_exists('cns_topics', $hooks))) {
        // A little reordering
        $temp = $hooks['cns'];
        unset($hooks['cns']);
        $hooks['cns'] = $temp;
    }

    if ($cleanup_tools !== null) {
        foreach ($cleanup_tools as $hook) {
            if (array_key_exists($hook, $hooks)) {
                send_http_output_ping();

                require_code('hooks/systems/cleanup/' . filter_naughty_harsh($hook));
                $object = object_factory('Hook_cleanup_' . filter_naughty_harsh($hook), true);
                if ($object === null) {
                    continue;
                }
                $messages->attach($object->run());

                log_it('CLEANUP_TOOLS', $hook);
            } else {
                $messages->attach(paragraph(do_lang_tempcode('_MISSING_RESOURCE', escape_html($hook))));
            }
        }
    } else {
        foreach (array_keys($hooks) as $hook) {
            send_http_output_ping();

            require_code('hooks/systems/cleanup/' . filter_naughty_harsh($hook));
            $object = object_factory('Hook_cleanup_' . filter_naughty_harsh($hook), true);
            if ($object === null) {
                continue;
            }
            $info = $object->info();
            if ($info['type'] == 'cache') {
                $messages->attach($object->run());

                log_it('CLEANUP_TOOLS', $hook);
            }
        }
    }

    cms_set_time_limit($old_limit);

    return $messages;
}

/**
 * Erase the block cache.
 *
 * @param  boolean $erase_cache_signatures_too Erase cache signatures too
 * @param  ?ID_TEXT $theme Only erase caching for this theme (null: all themes)
 * @param  boolean $persistent_caching_too Erase persistent caching too (recommended in most cases)
 */
function erase_block_cache(bool $erase_cache_signatures_too = false, ?string $theme = null, bool $persistent_caching_too = true)
{
    cms_profile_start_for('erase_tempcode_cache');

    if ($erase_cache_signatures_too) {
        $GLOBALS['SITE_DB']->query_delete('cache_on', [], '', null, 0, true); // May fail because the table might not exist when this is called
    }

    $where_map = [];
    if ($theme !== null) {
        $where_map = ['the_theme' => $theme];
    }
    $GLOBALS['SITE_DB']->query_delete('cache', $where_map);

    if ($persistent_caching_too) {
        erase_persistent_cache(); // Block caching may be directly in here
    }

    cms_profile_end_for('erase_tempcode_cache');
}

/**
 * Regenerate the trusted sites caching.
 */
function regenerate_trusted_sites_cache()
{
    $hook_obs = find_all_hook_obs('systems', 'trusted_sites', 'Hook_trusted_sites_');
    $_ts1 = [];
    $_ts2 = [];
    foreach ($hook_obs as $hook => $ob) {
        $ob->find_trusted_sites_1($_ts1);
        $ob->find_trusted_sites_2($_ts2);
    }
    $ts1 = implode("\n", array_unique($_ts1));
    $ts2 = implode("\n", array_unique($_ts2));
    set_value('trusted_sites_1', $ts1);
    set_value('trusted_sites_2', $ts2);
}

/**
 * Erase the Comcode cache. Warning: This can take a long time on large sites, so is best to avoid.
 */
function erase_comcode_cache()
{
    static $done_once = false; // Useful to stop it running multiple times in admin_cleanup module, as this code takes time
    global $ALLOW_DOUBLE_DECACHE;
    if (!$ALLOW_DOUBLE_DECACHE) {
        if ($done_once) {
            return;
        }
    }

    cms_profile_start_for('erase_comcode_cache');

    reload_lang_fields(true);

    if (multi_lang_content()) {
        $sql = 'UPDATE ' . get_table_prefix() . 'translate' . $GLOBALS['SITE_DB']->prefer_index('translate', 'decache');
        $sql .= ' SET text_parsed=\'\' WHERE ' . db_string_not_equal_to('text_parsed', '')/*this WHERE is so indexing helps*/;
        $GLOBALS['SITE_DB']->query($sql);
    } else {
        global $TABLE_LANG_FIELDS_CACHE;
        foreach ($TABLE_LANG_FIELDS_CACHE as $table => $fields) {
            foreach ($fields as $field => $field_type) {
                if (strpos($field_type, '__COMCODE') !== false) {
                    if ((substr($table, 0, 2) == 'f_') && (get_forum_type() != 'cns') && (get_forum_type() != 'none')) {
                        continue;
                    }

                    $db = get_db_for($table);
                    $db->query('UPDATE ' . $db->get_table_prefix() . $table . ' SET ' . $field . '__text_parsed=\'\' WHERE ' . db_string_not_equal_to($field . '__text_parsed', '')/*this WHERE is so indexing helps*/, null, 0, true/*in case meta DB has an issue*/);
                }
            }
        }
    }

    $done_once = true;

    cms_profile_end_for('erase_comcode_cache');
}

/**
 * Erase the language cache.
 */
function erase_cached_language()
{
    static $done_once = false;
    global $ALLOW_DOUBLE_DECACHE;
    if (!$ALLOW_DOUBLE_DECACHE) {
        if ($done_once) {
            return;
        }
    }
    $done_once = true;

    cms_profile_start_for('erase_cached_language');

    $langs = find_all_langs(true);
    foreach (array_merge(array_keys($langs), ['']) as $lang) {
        $path = get_custom_file_base() . '/caches/lang' . (($lang == '') ? '' : '/') . $lang;
        $_dir = @opendir($path);
        if ($_dir === false) {
            require_code('files2');
            @make_missing_directory($path);
        } else {
            while (false !== ($file = readdir($_dir))) {
                if (substr($file, -4) == '.lcd') {
                    if (running_script('index')) {
                        $key = 'page__' . get_zone_name() . '__' . get_page_name();
                    } else {
                        $key = 'script__' . md5(serialize($_SERVER['SCRIPT_NAME']) . serialize($_GET));
                    }
                    if ($key . '.lcd' == $file) {
                        continue; // Will be open/locked
                    }

                    $i = 0;
                    while ((@unlink($path . '/' . $file) === false) && ($i < 5)) {
                        if (!file_exists($path . '/' . $file)) {
                            break; // Race condition, gone already
                        }
                        if (php_function_allowed('usleep')) {
                            usleep(1000000); // May be race condition, lock
                        }
                        $i++;
                    }
                    if ($i >= 5) {
                        if ((file_exists($path . '/' . $file)) && (substr($file, 0, 5) != 'page_') && (substr($file, 0, 7) != 'script_')) {
                            @unlink($path . '/' . $file) or intelligent_write_error($path . '/' . $file);
                        }
                    }
                }
            }
            closedir($_dir);
        }
    }

    // Re-initialise language stuff
    global $LANGS_REQUESTED;
    $langs_requested_copy = $LANGS_REQUESTED;
    init__lang();
    $LANGS_REQUESTED = $langs_requested_copy;
    require_all_open_lang_files();

    if (class_exists('Self_learning_cache')) {
        Self_learning_cache::erase_smart_cache();
    }

    cms_profile_end_for('erase_cached_language');
}

/**
 * Erase all template caches (caches in all themes).
 *
 * @param  boolean $preserve_some Whether to preserve CSS and JS files that might be linked to between requests
 * @param  ?array $only_templates Only erase specific templates with the following filename, excluding suffix(es) (null: erase all)
 * @param  ?string $raw_file_regexp The original template must contain a match for this regular expression (null: no restriction)
 * @param  boolean $rebuild_some_deleted_files Whether to rebuild some files that are deleted (be very careful about this, it is high-intensity, and may break due to in-memory caches still existing)
 */
function erase_cached_templates(bool $preserve_some = false, ?array $only_templates = null, ?string $raw_file_regexp = null, bool $rebuild_some_deleted_files = false)
{
    if ($only_templates === []) {
        return; // Optimisation
    }

    if ((!$rebuild_some_deleted_files) && ($preserve_some)) {
        static $done_sweeping_decache_already = false;
        if ($done_sweeping_decache_already) {
            return;
        }
        if (($only_templates === null) && ($raw_file_regexp === null)) {
            $done_sweeping_decache_already = true;
        }
    }

    cms_profile_start_for('erase_cached_templates');

    global $ERASED_TEMPLATES_ONCE;
    $ERASED_TEMPLATES_ONCE = true;

    require_code('themes2');
    $themes = array_keys(find_all_themes());
    $langs = find_all_langs(true);

    $relevant_templates_in_cache = [];
    foreach ($themes as $theme) {
        foreach (array_keys($langs) as $lang) {
            $path = get_custom_file_base() . '/themes/' . $theme . '/templates_cached/' . $lang . '/';
            $_dir = @opendir($path);
            if ($_dir === false) {
                require_code('files2');
                @make_missing_directory($path);
            } else {
                $rebuilt = [];

                while (false !== ($file = readdir($_dir))) {
                    // Basic filter
                    if ($file[0] == '.' || $file == 'index.html') {
                        continue;
                    }

                    // $only_templates filter
                    if (($only_templates !== null) && (!in_array(preg_replace('#\..*$#', '', $file), $only_templates))) {
                        continue;
                    }

                    $file_template_name = preg_replace('#(\.tcp|\.gz|\.br|_mobile|_non_minified|_ssl|_non_custom_only)#', '', $file);

                    // $preserve_some filter
                    if (
                        ($preserve_some)
                        &&
                        (
                            (substr($file_template_name, -3) == '.js')
                            ||
                            (substr($file_template_name, -4) == '.css')
                            ||
                            (substr($file_template_name, -3) == '.gz')
                            ||
                            (substr($file_template_name, -3) == '.br')
                        )
                    ) {
                        continue;
                    }

                    if (!isset($relevant_templates_in_cache[$file_template_name])) {
                        $relevant_templates_in_cache[$file_template_name] = [];
                    }
                    $relevant_templates_in_cache[$file_template_name][] = [$file, $path];
                }
                closedir($_dir);
            }
        }
    }

    static $all_template_data = null;
    if (($raw_file_regexp !== null) && ($all_template_data === null)) {
        $all_template_data = [];

        $base_dirs = [get_custom_file_base()];
        if (get_custom_file_base() != get_file_base()) {
            $base_dirs[] = get_file_base();
        }

        $theme_dirs = [
            'css' => '.css',
            'css_custom' => '.css',
            'javascript' => '.js',
            'javascript_custom' => '.js',
            'templates' => '.tpl',
            'templates_custom' => '.tpl',
            'text' => '.txt',
            'text_custom' => '.txt',
            'xml' => '.xml',
            'xml_custom' => '.xml',
        ];

        foreach ($base_dirs as $base_dir) {
            foreach ($themes as $theme) {
                foreach ($theme_dirs as $theme_dir => $ext) {
                    $dir_path = $base_dir . '/themes/' . $theme . '/' . $theme_dir;
                    $_dir = @opendir($dir_path);
                    if ($_dir !== false) {
                        while (false !== ($file = readdir($_dir))) {
                            // Basic filter
                            if (substr($file, -strlen($ext)) != $ext) {
                                continue;
                            }
                            if (!isset($relevant_templates_in_cache[$file])) {
                                continue;
                            }

                            if (!isset($all_template_data[$file])) {
                                $all_template_data[$file] = [];
                            }
                            $contents = @cms_file_get_contents_safe($dir_path . '/' . $file);
                            if ($contents !== false) {
                                $all_template_data[$file][] = $contents;
                            }
                        }
                        closedir($_dir);
                    }
                }
            }
        }
    }

    foreach ($relevant_templates_in_cache as $file_template_name => $_bits_arr) {
        foreach ($_bits_arr as $_bits) {
            list($file, $path) = $_bits;

            // $raw_file_regexp filter
            if ($raw_file_regexp !== null) {
                $may_delete = false;
                if (isset($all_template_data[$file_template_name])) {
                    foreach ($all_template_data[$file_template_name] as $c) {
                        if (preg_match('#' . $raw_file_regexp . '#', $c) != 0) {
                            $may_delete = true;
                            break;
                        }
                    }
                }
                if (!$may_delete) {
                    continue;
                }
            }

            // Do deletion
            $i = 0;
            while ((@unlink($path . $file) === false) && ($i < 5)) {
                if (!file_exists($path . $file)) {
                    break; // Successful delete
                }
                if (php_function_allowed('usleep')) {
                    usleep(1000000); // May be race condition, lock
                }
                $i++;
            }
            if ($i >= 5) {
                if (file_exists($path . $file)) {
                    @unlink($path . $file) or intelligent_write_error($path . $file);
                }
            }
        }

        // Recreate static files right away because of parallelism...
        if ((!$GLOBALS['IN_MINIKERNEL_VERSION']) && (!running_script('upgrader')) && ($rebuild_some_deleted_files)) {
            if ((!$preserve_some) && (!isset($rebuilt[$file_template_name]))) {
                if (/*filter what we'll do due to memory limitation*/in_array($file_template_name, ['_base.css', '_colours.css', 'global.css', 'cns.css', 'forms.css', 'menu__dropdown.css', 'ajax.js', 'editing.js', 'global.js', 'posting.js'])) {
                    if ((isset($GLOBALS['SITE_DB'])) && (function_exists('find_theme_image')) && (!$GLOBALS['IN_MINIKERNEL_VERSION']) && ($GLOBALS['FORUM_DRIVER'] !== null)) {
                        if (substr($file_template_name, -3) == '.js') {
                            require_code('web_resources');
                            javascript_enforce(basename($file_template_name, '.js'), $theme);
                        }

                        if (substr($file_template_name, -4) == '.css') {
                            require_code('web_resources');
                            css_enforce(basename($file_template_name, '.css'), $theme);
                        }

                        $rebuilt[$file_template_name] = true;
                    }
                }
            }
        }
    }

    foreach (array_keys($langs) as $lang) {
        $path = get_custom_file_base() . '/site/pages/html_custom/' . $lang . '/';
        $_dir = is_dir($path) ? opendir($path) : false;
        if ($_dir !== false) {
            while (false !== ($file = readdir($_dir))) {
                if (substr($file, -14) == '_tree_made.htm') {
                    @unlink($path . $file);
                }
            }
            closedir($_dir);
        }
    }

    if (!$GLOBALS['IN_MINIKERNEL_VERSION']) {
        require_code('zones');
        $zones = find_all_zones();
        $values = [];
        foreach ($zones as $zone) {
            $values[] = 'merged__' . $zone . '.css';
            $values[] = 'merged__' . $zone . '.js';
            $values[] = 'merged__' . $zone . '__admin.css';
            $values[] = 'merged__' . $zone . '__admin.js';
        }
        delete_values($values);
    }

    if ((class_exists('Self_learning_cache')) && ($raw_file_regexp === null) && ($only_templates === [])) {
        Self_learning_cache::erase_smart_cache();
    }

    cms_profile_end_for('erase_cached_templates');

    // Rebuild ones needed for this session
    if ((!$preserve_some) && (!$GLOBALS['IN_MINIKERNEL_VERSION']) && (!running_script('upgrader'))) {
        global $JAVASCRIPTS, $CSSS;
        if (is_array($JAVASCRIPTS)) {
            foreach (array_keys($JAVASCRIPTS) as $j) {
                javascript_enforce($j, null, true);
            }
        }
        if (is_array($CSSS)) {
            foreach (array_keys($CSSS) as $c) {
                css_enforce($c, null, true);
            }
        }
    }
}

/**
 * Erase the Comcode page cache.
 */
function erase_comcode_page_cache()
{
    $GLOBALS['COMCODE_PAGE_RUNTIME_CACHE'] = [];

    if (!multi_lang_content()) {
        $GLOBALS['SITE_DB']->query_delete('cached_comcode_pages');
        return;
    }

    push_query_limiting(false);

    do {
        $rows = $GLOBALS['SITE_DB']->query_select('cached_comcode_pages', ['string_index'], [], '', 50, 0, false, []);
        if ($rows === null) {
            $rows = [];
        }
        foreach ($rows as $row) {
            delete_lang($row['string_index']);
            $GLOBALS['SITE_DB']->query_delete('cached_comcode_pages', ['string_index' => $row['string_index']]);
        }
    } while (!empty($rows));
    erase_persistent_cache();

    pop_query_limiting();
}

/**
 * Erase the theme images cache.
 */
function erase_theme_images_cache()
{
    push_query_limiting(false);

    $GLOBALS['SITE_DB']->query('DELETE FROM ' . get_table_prefix() . 'theme_images WHERE url LIKE \'themes/%/images/%\'', null, 0, true/*LEGACY*/);

    Self_learning_cache::erase_smart_cache();

    require_code('themes2');
    $all_themes = find_all_themes();

    $_paths = $GLOBALS['SITE_DB']->query_select('theme_images', ['theme', 'id', 'lang', 'url'], [], '', null, 0, true/*LEGACY*/);
    if ($_paths === null) { // LEGACY: Old field name
        $GLOBALS['SITE_DB']->query('DELETE FROM ' . get_table_prefix() . 'theme_images WHERE path LIKE \'themes/%/images/%\'');

        $_paths = $GLOBALS['SITE_DB']->query_select('theme_images', ['theme', 'id', 'lang', 'path']);
        $legacy = true;
    } else {
        $legacy = false;
    }
    $paths = [];
    foreach ($_paths as $image_details) {
        $image_details_key = ['theme' => $image_details['theme'], 'id' => $image_details['id'], 'lang' => $image_details['lang']];
        $paths[serialize($image_details_key)] = $image_details[$legacy ? 'path' : 'url'];
    }

    foreach ($paths as $_image_details_key => $path) {
        $image_details_key = unserialize($_image_details_key);
        if (!file_exists(get_file_base() . '/themes/' . $image_details_key['theme'])) {
            // Delete: a non-existent theme
            $GLOBALS['SITE_DB']->query_delete('theme_images', $image_details_key, '', 1);
        } elseif ($path == '') {
            // Delete: A blank path, should not be there (actually it is a cache signal for "theme image does not exist")
            $GLOBALS['SITE_DB']->query_delete('theme_images', $image_details_key, '', 1);
        } elseif (preg_match('#^themes/[^/]+/images_custom/#', $path) != 0) {
            if ((!file_exists(get_custom_file_base() . '/' . rawurldecode($path))) && (!file_exists(get_file_base() . '/' . rawurldecode($path)))) {
                // Delete: Custom disk file does not actually exist
                $GLOBALS['SITE_DB']->query_delete('theme_images', $image_details_key, '', 1);
            } else {
                if ($image_details_key['theme'] == 'default') {
                    // Add: Custom images in default theme should be in all themes
                    foreach (array_keys($all_themes) as $theme) {
                        if ($theme != 'default') {
                            $insert_key_map = ['theme' => $theme, 'id' => $image_details_key['id'], 'lang' => $image_details_key['lang']];
                            if (!isset($paths[serialize($insert_key_map)])) {
                                $insert_map = $insert_key_map + [$legacy ? 'path' : 'url' => $path];
                                $GLOBALS['SITE_DB']->query_insert('theme_images', $insert_map, false, true/*In case of race conditions or case sensitivity problems*/);
                            }
                        }
                    }
                }
            }
        }
    }

    pop_query_limiting();
}
