<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core_cns
 */

/**
 * List all the Multi Moderations that may be used in a certain forum.
 *
 * @param  ?AUTO_LINK $forum_id The forum we are listing for (null: private topics)
 * @return array List of multi moderations
 */
function cns_list_multi_moderations(?int $forum_id) : array
{
    if (!addon_installed('cns_multi_moderations')) {
        return [];
    }

    if ($forum_id === null) {
        return [];
    }

    $rows = $GLOBALS['FORUM_DB']->query_select('f_multi_moderations', ['*'], [], 'ORDER BY ' . $GLOBALS['FORUM_DB']->translate_field_ref('mm_name'));
    $out = [];
    if (empty($rows)) {
        return $out;
    }

    $lots_of_forums = $GLOBALS['FORUM_DB']->query_select_value('f_forums', 'COUNT(*)') > 200;
    if (!$lots_of_forums) {
        $all_forums = collapse_2d_complexity('id', 'f_parent_forum_id', $GLOBALS['FORUM_DB']->query_select('f_forums', ['id', 'f_parent_forum_id']));
    }
    foreach ($rows as $row) {
        $row['_mm_name'] = get_translated_text($row['mm_name'], $GLOBALS['FORUM_DB']);

        if ($row['mm_forum_multi_code'] == '*') {
            $out[$row['id']] = $row['_mm_name'];
            continue;
        }

        require_code('selectcode');
        if ($lots_of_forums) {
            $sql = selectcode_to_sqlfragment($row['mm_forum_multi_code'], 'id', 'f_forums', 'f_parent_forum_id', 'f_parent_forum_id', 'id', true, true, $GLOBALS['FORUM_DB']);
            if ($GLOBALS['FORUM_DB']->query_value_if_there('SELECT id FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_forums WHERE id=' . strval($forum_id) . ' AND (' . $sql . ')', false, true) !== null) {
                $out[$row['id']] = $row['_mm_name'];
            }
        } else {
            $idlist = selectcode_to_idlist_using_memory($row['mm_forum_multi_code'], $all_forums, 'f_forums', 'f_parent_forum_id', 'f_parent_forum_id', 'id', true, true, $GLOBALS['FORUM_DB']);
            if (in_array($forum_id, $idlist)) {
                $out[$row['id']] = $row['_mm_name'];
            }
        }
    }
    return $out;
}

/**
 * Whether a certain member may perform Multi Moderations in a certain forum.
 *
 * @param  ?AUTO_LINK $forum_id The forum (null: private topics)
 * @param  ?MEMBER $member_id The member (null: current member)
 * @return boolean Answer
 */
function cns_may_perform_multi_moderation(?int $forum_id, ?int $member_id = null) : bool
{
    if ($member_id === null) {
        $member_id = get_member();
    }

    if (!cns_may_moderate_forum($forum_id, $member_id)) {
        return false;
    }

    return has_privilege($member_id, 'run_multi_moderations');
}

/**
 * Whether a certain member may give formal warnings to other members.
 *
 * @param  ?MEMBER $member_id The member (null: current member)
 * @return boolean Answer
 */
function cns_may_warn_members(?int $member_id = null) : bool
{
    if ($member_id === null) {
        $member_id = get_member();
    }

    return has_privilege($member_id, 'warn_members');
}

/**
 * Get all the warning and warnings_punitive rows for a certain member.
 *
 * @param  MEMBER $member_id The member
 * @return array The warning rows
 */
function cns_get_warnings(int $member_id) : array
{
    if (!addon_installed('cns_warnings')) {
        return [];
    }

    // Get warnings
    $warnings = $GLOBALS['FORUM_DB']->query_select('f_warnings', ['*'], ['w_member_id' => $member_id, 'w_is_warning' => 1], 'ORDER BY w_time');

    // Load in punitive actions via the 'punitive' key of each warning
    foreach ($warnings as $i => $warning) {
        $warnings[$i]['punitive'] = $GLOBALS['FORUM_DB']->query_select('f_warnings_punitive', ['*'], ['p_warning_id' => $warning['id']], '');
    }

    return $warnings;
}
