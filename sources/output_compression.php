<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.


 NOTE TO PROGRAMMERS:
   Do not edit this file. If you need to make changes, save your changed file to the appropriate *_custom folder
   **** If you ignore this advice, then your website upgrades (e.g. for bug fixes) will likely kill your changes ****

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    core
 */

/**
 * Standard code module initialisation function.
 *
 * @ignore
 */
function init__output_compression()
{
    reinit_output_compression();
}

/**
 * (Re)initialise output compression.
 */
function reinit_output_compression()
{
    $setting = intval(get_option('output_compression'));
    if ($setting == 0) {
        disable_output_compression();
    } else {
        enable_output_compression();
    }
}

/**
 * Enable output compression, as per configuration.
 */
function enable_output_compression()
{
    if (headers_sent()) {
        return; // Not safe to change
    }

    disable_output_compression(true); // Start from blank slate

    $page = get_param_string('page', ''); // Not get_page_name for bootstrap order reasons
    if ((in_safe_mode()) || ($page == 'admin_config')) {
        // So can fix broken sites, changing the option back
        return;
    }

    $use_cmd_line = false;
    if (supports_brotli(true, $use_cmd_line)) {
        cms_ini_set('brotli.output_compression', 'On');
        cms_ini_set('brotli.output_compression_level', '11'); // Recommended. Default for Brotli
    } elseif (supports_gzip(true, $use_cmd_line)) {
        cms_ini_set('zlib.output_compression', '4096');
        cms_ini_set('zlib.output_compression_level', '2'); // Recommended. Compression doesn't get much better after this, but performance drop
    }
}

/**
 * Disable output compression.
 *
 * @param  boolean $during_init Whether this is just during initialisation (useful for debugging)
 */
function disable_output_compression(bool $during_init = false)
{
    if (headers_sent()) {
        return; // Not safe to change
    }

    foreach (_disable_output_compression() as $key => $val) {
        cms_ini_set($key, $val);
    }

    @header_remove('Content-Encoding');
}

/**
 * Get PHP .ini settings that need to be changed to disable output compression.
 *
 * @return array Map of PHP .ini settings
 */
function _disable_output_compression() : array
{
    return [
        'zlib.output_compression' => 'Off',
        'brotli.output_compression' => 'Off',
    ];
}

/**
 * Find whether Brotli is supported.
 *
 * @param  boolean $live Check the current web client can support it
 * @param  boolean $use_cmd_line Whether a command line call is needed to do the compression (returned by reference)
 * @return boolean Whether it is
 */
function supports_brotli(bool $live = true, bool &$use_cmd_line = false) : bool
{
    $use_cmd_line = false;

    if ($live) {
        static $live_ok = null;
        if ($live_ok === false) {
            return false;
        }
        if ($live_ok !== true) {
            if (get_option('output_compression') != '2') {
                $live_ok = false;
                return false;
            }

            if (!accepts_encoding('br')) {
                $live_ok = false;
                return false;
            }

            if (!tacit_https()) {
                $live_ok = false;
                return false; // No Brotli on HTTP due to non-compliant proxy caches potentially getting polluted
            }

            if ((!php_function_allowed('brotli_compress')) || (ini_get('output_handler') != '')) {
                $live_ok = false;
                return false; // We have to use a real output handler as otherwise realistically we get chaos
            }
        }
    }

    static $ok = null, $ok_use_cmd_line = null;
    if ($ok !== null) {
        $use_cmd_line = $ok_use_cmd_line;
        return $ok;
    }

    if (php_function_allowed('brotli_compress')) {
        $use_cmd_line = false;
        $ok_use_cmd_line = false;
        $ok = true;
        return true;
    }

    if (php_function_allowed('shell_exec')) {
        if (strpos(shell_exec('brotli -h 2>&1'), 'Usage:') !== false) {
            $use_cmd_line = true;
            $ok_use_cmd_line = true;
            $ok = true;
            return true;
        }
    }

    $use_cmd_line = false;
    $ok_use_cmd_line = false;
    $ok = false;
    return false;
}

/**
 * Compress via Brotli. Assumes support_brotli has been called to check in advance.
 *
 * @param  string $in Data to compress
 * @param  integer $level Compression level
 * @range 0 11
 * @return ~string Compressed data (false: error)
 */
function cms_brotli_compress(string $in, int $level)
{
    if (php_function_allowed('brotli_compress')) {
        return brotli_compress($in, $level);
    }

    if ((php_function_allowed('shell_exec')) && (php_function_allowed('escapeshellarg'))) {
        $tmp_name = cms_tempnam('brotli');
        $tmp_name_out = cms_tempnam('brotli_out');
        file_put_contents($tmp_name, $in);
        $result = shell_exec('brotli -q ' . strval($level) . ' -f -o ' . cms_escapeshellarg($tmp_name_out) . ' ' . cms_escapeshellarg($tmp_name) . ' 2>&1');
        $out = file_get_contents($tmp_name_out);
        @unlink($tmp_name);
        @unlink($tmp_name_out);
        if ($out == $in) {
            return false;
        }
        return $out;
    }
    return false;
}

/**
 * Uncompress via Brotli. Assumes support_brotli has been called to check in advance.
 *
 * @param  string $in Data to uncompress
 * @return ~string Uncompressed data (false: error)
 */
function cms_brotli_uncompress(string $in)
{
    if (php_function_allowed('brotli_uncompress')) {
        return brotli_uncompress($in);
    }

    if ((php_function_allowed('shell_exec')) && (php_function_allowed('escapeshellarg'))) {
        $tmp_name = cms_tempnam('brotli');
        $tmp_name_out = cms_tempnam('brotli_out');
        file_put_contents($tmp_name, $in);
        shell_exec('brotli -d -f -o ' . cms_escapeshellarg($tmp_name_out) . ' ' . cms_escapeshellarg($tmp_name));
        $out = file_get_contents($tmp_name_out);
        @unlink($tmp_name);
        @unlink($tmp_name_out);
        if ($out == $in) {
            return false;
        }
        return $out;
    }
    return false;
}

/**
 * Find whether Gzip is supported.
 *
 * @param  boolean $live Check the current web client can support it
 * @param  boolean $use_cmd_line Whether a command line call is needed to do the compression (returned by reference)
 * @return boolean Whether it is
 */
function supports_gzip(bool $live = true, bool &$use_cmd_line = false) : bool
{
    $use_cmd_line = false;

    if ($live) {
        static $live_ok = null;
        if ($live_ok === false) {
            return false;
        }
        if ($live_ok !== true) {
            if (get_option('output_compression') == '0') {
                $live_ok = false;
                return false;
            }

            if (!accepts_encoding('gzip')) {
                $live_ok = false;
                return false;
            }

            if ((!php_function_allowed('gzencode')) || (ini_get('output_handler') != '')) {
                $live_ok = false;
                return false; // We have to use a real output handler as otherwise realistically we get chaos
            }
        }
    }

    static $ok = null, $ok_use_cmd_line = null;
    if ($ok !== null) {
        $use_cmd_line = $ok_use_cmd_line;
        return $ok;
    }

    if (php_function_allowed('gzencode')) {
        $use_cmd_line = false;
        $ok_use_cmd_line = false;
        $ok = true;
        return true;
    }

    if (php_function_allowed('shell_exec')) {
        if (strpos(shell_exec('gzip -h 2>&1'), 'Usage:') !== false) {
            $use_cmd_line = true;
            $ok_use_cmd_line = true;
            $ok = true;
            return true;
        }
    }

    $use_cmd_line = false;
    $ok_use_cmd_line = false;
    $ok = false;
    return false;
}

/**
 * Compress via Gzip. Assumes support_gzip has been called to check in advance.
 *
 * @param  string $in Data to compress
 * @param  integer $level Compression level
 * @range 0 9
 * @return ~string Compressed data (false: error)
 */
function cms_gzencode(string $in, int $level)
{
    if (php_function_allowed('gzencode')) {
        return gzencode($in, $level);
    }

    if ((php_function_allowed('shell_exec')) && (php_function_allowed('escapeshellarg'))) {
        $tmp_name = cms_tempnam('gzip');
        $tmp_name_out = cms_tempnam('gzip_out');
        file_put_contents($tmp_name, $in);
        $result = shell_exec('gzip -' . strval($level) . ' -f -o ' . cms_escapeshellarg($tmp_name_out) . ' ' . cms_escapeshellarg($tmp_name) . ' 2>&1');
        $out = file_get_contents($tmp_name_out);
        @unlink($tmp_name);
        @unlink($tmp_name_out);
        if ($out == $in) {
            return false;
        }
        return $out;
    }
    return false;
}

/**
 * Uncompress via Gzip. Assumes support_brotli has been called to check in advance.
 *
 * @param  string $in Data to uncompress
 * @return ~string Uncompressed data (false: error)
 */
function cms_gzdecode(string $in)
{
    if (php_function_allowed('gzdecode')) {
        return gzdecode($in);
    }

    if ((php_function_allowed('shell_exec')) && (php_function_allowed('escapeshellarg'))) {
        $tmp_name = cms_tempnam('gzip');
        $tmp_name_out = cms_tempnam('gzip_out');
        file_put_contents($tmp_name, $in);
        shell_exec('gzip -d -f -o ' . cms_escapeshellarg($tmp_name) . ' ' . cms_escapeshellarg($tmp_name_out));
        $out = file_get_contents($tmp_name_out);
        @unlink($tmp_name);
        @unlink($tmp_name_out);
        if ($out == $in) {
            return false;
        }
        return $out;
    }
    return false;
}

/**
 * Find whether the current web client supports an output compression encoding.
 *
 * @param  string $encoding Encoding to check
 * @return boolean Whether it does
 */
function accepts_encoding(string $encoding) : bool
{
    $encodings = isset($_SERVER['HTTP_ACCEPT_ENCODING']) ? $_SERVER['HTTP_ACCEPT_ENCODING'] : '';
    foreach (explode(',', $encodings) as $accepted_encoding) {
        if (preg_match('#^\s*' . preg_quote($encoding, '#') . '(;|\s|$)#', $accepted_encoding) != 0) {
            return true;
        }
    }
    return false;
}
