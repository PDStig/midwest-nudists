<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    mwn_misc
 */

/**
 * Hook class.
 */
class Hook_addon_registry_mwn_misc
{
    /**
     * Get a list of file permissions to set.
     *
     * @param  boolean $runtime Whether to include wildcards represented runtime-created chmoddable files
     * @return array File permissions to set
     */
    public function get_chmod_array(bool $runtime = false) : array
    {
        return [];
    }

    /**
     * Get the version of Composr this addon is for.
     *
     * @return float Version number
     */
    public function get_version() : float
    {
        return cms_version_number();
    }

    /**
     * Get the minimum required version of the website software needed to use this addon.
     *
     * @return float Minimum required website software version
     */
    public function get_min_cms_version() : float
    {
        return 11.0;
    }

    /**
     * Get the maximum compatible version of the website software to use this addon.
     *
     * @return ?float Maximum compatible website software version (null: no maximum version currently)
     */
    public function get_max_cms_version() : ?float
    {
        return null;
    }

    /**
     * Get the addon category.
     *
     * @return string The category
     */
    public function get_category() : string
    {
        return 'Development';
    }

    /**
     * Get the addon author.
     *
     * @return string The author
     */
    public function get_author() : string
    {
        return 'Patrick Schmalstig';
    }

    /**
     * Find other authors.
     *
     * @return array A list of co-authors that should be attributed
     */
    public function get_copyright_attribution() : array
    {
        return [];
    }

    /**
     * Get the addon licence (one-line summary only).
     *
     * @return string The licence
     */
    public function get_licence() : string
    {
        return 'GNU General Public License v3.0';
    }

    /**
     * Get the description of the addon.
     *
     * @return string Description of the addon
     */
    public function get_description() : string
    {
        return 'Misc small customizations for the Midwest Nudists website which are too small for their own addon';
    }

    /**
     * Get a list of tutorials that apply to this addon.
     *
     * @return array List of tutorials
     */
    public function get_applicable_tutorials() : array
    {
        return [];
    }

    /**
     * Get a mapping of dependency types.
     *
     * @return array A structure specifying dependency information
     */
    public function get_dependencies() : array
    {
        return [
            'requires' => [],
            'recommends' => [],
            'conflicts_with' => [],
        ];
    }

    /**
     * Explicitly say which icon should be used.
     *
     * @return URLPATH Icon
     */
    public function get_default_icon() : string
    {
        return 'themes/default/images/icons/admin/component.svg';
    }

    /**
     * Get a list of files that belong to this addon.
     *
     * @return array List of files
     */
    public function get_file_list() : array
    {
        return [
            // Hooks
            'sources_custom/hooks/systems/addon_registry/mwn_misc.php',
            'sources_custom/hooks/systems/contentious_overrides/mwn_misc.php',
            'sources_custom/hooks/systems/cns_implicit_usergroups/mwn_youngadult.php',

            // Theme customizations
            'themes/default/templates_custom/CNS_JOIN_EMAIL_BLACKLISTS.tpl',
            'themes/admin/css_custom/_colours.css',
            'themes/admin/css_custom/adminzone.css',
            'themes/default/templates_custom/BLOCK_TOP_LOGIN.tpl',

            // Language files
            'lang_custom/EN/mwn_misc.ini',
            'lang_custom/EN/notifications.ini',

            // Modules
            'pages/minimodules_custom/contact.php',

            // API overrides
            'sources_custom/ecommerce.php',

            // Weather
            'sources_custom/weather2.php',
            'sources_custom/hooks/systems/fields/weather.php',
            'sources_custom/hooks/systems/contentious_overrides/weather.php',
            'sources_custom/hooks/systems/weather/nws.php',
            'lang_custom/EN/weather2.ini',
            'themes/default/templates_custom/WEATHER.tpl',

            // Rank
            'sources_custom/hooks/systems/profiles_tabs/rank.php',
            'themes/default/css_custom/stepper.css',
        ];
    }
}
