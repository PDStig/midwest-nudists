<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2023

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    delete_blacklist
 */

/**
 * Hook class.
 */
class Hook_privacy_delete_blacklist extends Hook_privacy_base
{
    /**
     * Find privacy details.
     *
     * @return ?array A map of privacy details in a standardised format (null: disabled)
     */
    public function info() : ?array
    {
        if (!addon_installed('delete_blacklist') || !addon_installed('cns_warnings')) {
            return null;
        }

        return [
            'label' => 'delete_blacklist:DELETE_BLACKLIST',

            'description' => 'delete_blacklist:DESCRIPTION_PRIVACY_DELETE_BLACKLIST',

            'cookies' => [
            ],

            'positive' => [
            ],

            'general' => [
                [
                    'heading' => do_lang('INFORMATION_DISCLOSURE'),
                    'action' => do_lang_tempcode('delete_blacklist:PRIVACY_ACTION_BLACKLIST'),
                    'reason' => do_lang_tempcode('delete_blacklist:PRIVACY_REASON_BLACKLIST'),
                ],
            ],

            'database_records' => [
                'blacklist' => [
                    'timestamp_field' => 'b_time',
                    'retention_days' => null,
                    'retention_handle_method' => PRIVACY_METHOD__LEAVE,
                    'owner_id_field' => null,
                    'additional_member_id_fields' => [],
                    'ip_address_fields' => ['b_ip_address'],
                    'email_fields' => ['b_email_address'],
                    'username_fields' => ['b_username'],
                    'additional_anonymise_fields' => [],
                    'extra_where' => null,
                    'removal_default_handle_method' => PRIVACY_METHOD__LEAVE, // We do not want them deleting when a member deletes their account either
                    'removal_default_handle_method_member_override' => PRIVACY_METHOD__LEAVE,
                    'allowed_handle_methods' => PRIVACY_METHOD__DELETE,
                ],
            ],
        ];
    }
}
