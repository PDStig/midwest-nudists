<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    mwn_misc
 */

/**
 * Hook class.
 */
class Hook_implicit_usergroups_mwn_youngadult
{
    /**
     * Finds the group IDs it is bound to.
     *
     * @return array A list of usergroup IDs
     */
    public function get_bound_group_ids() : array
    {
        return [14];
    }

    protected function _where()
    {
        $eago = intval(date('Y')) - 35;
        $eago_2 = intval(date('Y')) - 18;
        $ret = '(m_dob_year>' . strval($eago) . ' OR m_dob_year=' . strval($eago) . ' AND (m_dob_month>' . date('m') . ' OR m_dob_month=' . date('m') . ' AND m_dob_day>' . date('d') . '))';
        $ret .= ' AND (m_dob_year<' . strval($eago_2) . ' OR m_dob_year=' . strval($eago_2) . ' AND (m_dob_month<' . date('m') . ' OR m_dob_month=' . date('m') . ' AND m_dob_day<=' . date('d') . '))';
        return $ret;
    }

    /**
     * Finds all members in the group.
     *
     * @param  GROUP $group_id The group ID to check (if only one group supported by the hook, can be ignored)
     * @param  ?integer $max Return up to this many entries for members (null: no limit)
     * @param  integer $start Return members after this offset
     * @return ?array The list of members as a map between member ID and member row (null: unsupported by hook)
     */
    public function get_member_list(int $group_id, ?int $max = null, int $start = 0) : ?array
    {
        if (!addon_installed('mwn_misc')) {
            return [];
        }

        return list_to_map('id', $GLOBALS['FORUM_DB']->query('SELECT * FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_members WHERE ' . $this->_where(), $max, $start));
    }

    /**
     * Finds a count of the members in the group.
     *
     * @param  GROUP $group_id The group ID to check (if only one group supported by the hook, can be ignored)
     * @return integer count
     */
    public function get_member_list_count(int $group_id) : int
    {
        if (!addon_installed('mwn_misc')) {
            return 0;
        }

        return $GLOBALS['FORUM_DB']->query_value_if_there('SELECT COUNT(*) FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_members WHERE ' . $this->_where());
    }

    /**
     * Finds whether the member is within the implicit usergroup.
     *
     * @param  MEMBER $member_id The member ID
     * @param  GROUP $group_id The group ID to check (if only one group supported by the hook, can be ignored)
     * @param  ?boolean $is_exclusive Return-by-reference if the member should *only* be in this usergroup (null: initially unset)
     * @return boolean Whether they are
     */
    public function is_member_within(int $member_id, int $group_id, ?bool &$is_exclusive = null) : bool
    {
        if (!addon_installed('mwn_misc')) {
            return false;
        }

        if ($member_id == get_member()) {
            // IDEA: #3830 Support timezones, decide age based on user's own timezone

            $eago = intval(date('Y')) - 35;
            $eago_2 = intval(date('Y')) - 18;
            $row = $GLOBALS['FORUM_DRIVER']->get_member_row($member_id);
            $dob_year = $row['m_dob_year'];
            $dob_month = $row['m_dob_month'];
            $dob_day = $row['m_dob_day'];
            $pass_a = $dob_year > $eago || $dob_year == $eago && ($dob_month > intval(date('m')) || $dob_month == intval(date('m')) && $dob_day > intval(date('d')));
            $pass_b = $dob_year < $eago_2 || $dob_year == $eago_2 && ($dob_month < intval(date('m')) || $dob_month == intval(date('m')) && $dob_day <= intval(date('d')));
            return $pass_a && $pass_b;
        }

        $sql = 'SELECT id FROM ' . $GLOBALS['FORUM_DB']->get_table_prefix() . 'f_members WHERE (' . $this->_where() . ') AND id=' . strval($member_id);
        return ($GLOBALS['FORUM_DB']->query_value_if_there($sql) !== null);
    }
}
