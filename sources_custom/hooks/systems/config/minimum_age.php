<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    minimum_age
 */

/**
 * Hook class.
 */
class Hook_config_minimum_age
{
    /**
     * Gets the details relating to the config option.
     *
     * @return ?array The details (null: disabled)
     */
    public function get_details() : ?array
    {
        return [
            'human_name' => 'MINIMUM_AGE',
            'type' => 'integer',
            'category' => 'USERS',
            'group' => '_LOGIN',
            'explanation' => 'CONFIG_OPTION_minimum_age',
            'shared_hosting_restricted' => '0',
            'list_options' => '',
            'required' => true,
            'public' => false,
            'addon' => 'minimum_age',
        ];
    }

    /**
     * Gets the default value for the config option.
     *
     * @return ?string The default value (null: option is disabled)
     */
    public function get_default() : ?string
    {
        if (!addon_installed('minimum_age')) {
            return null;
        }

        return '0';
    }
}
