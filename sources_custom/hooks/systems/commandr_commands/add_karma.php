<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    karma
 */

/**
 * Hook class.
 */
class Hook_commandr_command_add_karma
{
    /**
     * Run function for Commandr hooks.
     *
     * @param  array $options The options with which the command was called
     * @param  array $parameters The parameters with which the command was called
     * @param  object $commandr_fs A reference to the Commandr filesystem object
     * @return array Array of stdcommand, stdhtml, stdout, and stderr responses
     */
    public function run(array $options, array $parameters, object &$commandr_fs) : array
    {
        $err = new Tempcode();
        if (!addon_installed__messaged('karma', $err)) {
            return ['', '', '', $err->evaluate()];
        }

        require_lang('karma');

        if ((array_key_exists('h', $options)) || (array_key_exists('help', $options))) {
            return ['', do_command_help('add_karma', ['h'], [true, true, true, true, true]), '', ''];
        }

        if (!has_privilege(get_member(), 'moderate_karma')) {
            require_lang('permissions');
            return ['', '', '', do_lang('ACCESS_DENIED__PRIVILEGE', $GLOBALS['FORUM_DRIVER']->get_username(get_member()), 'moderate_karma')];
        }

        if (!array_key_exists(0, $parameters)) {
            return ['', '', '', do_lang('MISSING_PARAM', '1', 'add_karma')];
        }
        if (!array_key_exists(1, $parameters)) {
            return ['', '', '', do_lang('MISSING_PARAM', '2', 'add_karma')];
        }
        if (!array_key_exists(2, $parameters)) {
            return ['', '', '', do_lang('MISSING_PARAM', '3', 'add_karma')];
        }
        if (!array_key_exists(3, $parameters)) {
            return ['', '', '', do_lang('MISSING_PARAM', '4', 'add_karma')];
        }

        $karma_type = $parameters[0];
        if (!in_array($parameters[0], ['good', 'bad'])) {
            return ['', '', '', do_lang('CMD_ADD_KARMA_PARAM_0_INVALID')];
        }

        $member_to = $GLOBALS['FORUM_DRIVER']->get_member_from_username($parameters[1]);
        if (($member_to === null) || (is_guest($member_to))) {
            return ['', '', '', do_lang('MEMBER_NO_EXIST')];
        }

        $amount = intval($parameters[2]);
        $reason = escape_html($parameters[3]);

        $content_id = '';
        if (array_key_exists(4, $parameters)) {
            $content_id = escape_html($parameters[4]);
        }

        require_code('karma2');

        add_karma($karma_type, get_member(), $member_to, $amount, $reason, 'commandr_command', $content_id);

        return ['', '', do_lang('SUCCESS'), ''];
    }
}
