<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    weather
 */

/**
 * Hook class.
 */
class Hook_weather_nws
{
    // @see https://www.weather.gov/documentation/services-web-api

    protected const INCHES_PER_MM = 0.03937008;
    protected const SERVER = "https://api.weather.gov";

    /**
     * Lookup weather for a location.
     *
     * @param  ?string $location_search Location (null: use $latitude and $longitude)
     * @param  ?float $latitude Latitude (null: use $location_search)
     * @param  ?float $longitude Longitude (null: use $location_search)
     * @param  string $units Units to use
     * @set imperial metric
     * @param  ?integer $max_days Maximum number of days to return if supported (null: no limit)
     * @return ?array A pair: Weather API current conditions in standardised simple format, Weather API forecast in standardised simple format (null: not available)
     */
    public function lookup(?string $location_search = null, ?float $latitude = null, ?float $longitude = null, string $units = 'metric', ?int $max_days = null) : ?array
    {
        if (!addon_installed('weather')) {
            return null;
        }

        // NWS does not support location search. Try our geocoding services to convert to a latitude and longitude.
        if (($latitude === null) || ($longitude === null)) {
            if ($location_search === null) {
                return null;
            } else {
                require_code('locations_geocoding');
                $location = geocode($location_search);
                if (!is_array($location)) {
                    return null;
                }
                $latitude = $location[0];
                $longitude = $location[1];
            }
        }

        require_code('http');
        require_code('locations');

        // Current conditions...
        $current_conditions = $this->lookup_observations($latitude, $longitude);
        if ($current_conditions === null) {
            return null;
        }

        // Forecast...
        $forecast = $this->lookup_forecast($latitude, $longitude, $max_days);
        if ($forecast === null) {
            return null;
        }

        // ---

        $result = [$current_conditions, $forecast];

        return $result;
    }

    /**
     * Find the gridpoint / forecast area of a given latitude and longitude.
     *
     * @param  float $latitude Latitude
     * @param  float $longitude Longitude
     * @return ?array Array of [gridId, gridX, gridY, city, state] (null: Location not found or not within the NWS jurisdiction)
     */
    protected function lookup_gridpoint(float $latitude, float $longitude) : ?array
    {
        $response = cache_and_carry('cms_http_request', [self::SERVER . '/points/' . float_to_raw_string($latitude) . ',' . float_to_raw_string($longitude), ['convert_to_internal_encoding' => true, 'trigger_error' => false, 'ignore_http_status' => true, 'timeout' => 10.0]], 60 * 24, true);
        list($data, , , , $http_message) = $response;

        $result = @json_decode($data, true);
        if (!is_array($result)) {
            $errormsg = do_lang('WEATHER_ERROR', 'NWS', $http_message . ' error, ' . $data);
            throw new Exception($errormsg);
        }

        if (!isset($result['properties'])) {
            return null;
        }

        return [
            $result['properties']['gridId'],
            $result['properties']['gridX'],
            $result['properties']['gridY'],
            $result['properties']['relativeLocation']['properties']['city'],
            $result['properties']['relativeLocation']['properties']['state'],
        ];
    }

    /**
     * Find up to 5 observation stations for a given latitude / longitude.
     *
     * @param  float $latitude Latitude
     * @param  float $longitude Longitude
     * @return ?array Array of [observation station IDs[], city, state] (null: not found)
     */
    protected function lookup_stations(float $latitude, float $longitude) : ?array
    {
        $gridpoint = $this->lookup_gridpoint($latitude, $longitude);
        if (!is_array($gridpoint)) {
            return null;
        }

        $response = cache_and_carry('cms_http_request', [self::SERVER . '/gridpoints/' . $gridpoint[0] . '/' . strval($gridpoint[1]) . ',' . strval($gridpoint[2]) . '/stations?limit=5', ['convert_to_internal_encoding' => true, 'trigger_error' => false, 'ignore_http_status' => true, 'timeout' => 10.0]], 60 * 24);
        list($data, , , , $http_message) = $response;

        $result = @json_decode($data, true);
        if (!is_array($result)) {
            $errormsg = do_lang('WEATHER_ERROR', 'NWS', $http_message . ' error, ' . $data);
            throw new Exception($errormsg);
        }

        if (!isset($result['features'])) {
            return null;
        }

        $stations = [];

        foreach ($result['features'] as $station) {
            $stations[] = $station['properties']['stationIdentifier'];
        }

        return [
            $stations,
            $gridpoint[3],
            $gridpoint[4],
        ];
    }

    /**
     * Find the current observations of a given latitude / longitude.
     *
     * @param  float $latitude Latitude
     * @param  float $longitude Longitude
     * @return ?array Array of current condition properties (null: not found)
     */
    protected function lookup_observations(float $latitude, float $longitude) : ?array
    {
        $wind_directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N']; // Have to repeat 'N' as we've seen degrees set as 360 (lol)

        $stations = $this->lookup_stations($latitude, $longitude);
        if (!is_array($stations)) {
            return null;
        }

        require_code('temporal');

        foreach ($stations[0] as $station) {
            $response = cache_and_carry('cms_http_request', [self::SERVER . '/stations/' . $station . '/observations/latest', ['convert_to_internal_encoding' => true, 'trigger_error' => false, 'ignore_http_status' => true, 'timeout' => 10.0]], 15);
            list($data, , , , $http_message) = $response;

            $result = @json_decode($data, true);
            if (!is_array($result)) {
                continue;
            }

            if (!isset($result['properties'])) {
                continue;
            }
            if (!isset($result['properties']['timestamp'])) {
                continue;
            }

            $weather = $result['properties'];

            return [
                'LOCATION' => $stations[1] . ', ' . $stations[2],
                'TIMESTAMP' => get_timezoned_date_time(strtotime($weather['timestamp']), false),
                'ICON_URL' => $weather['icon'],
                'UNITS' => 'metric', // No way to specify units in NWS for observations
                'VALUES' => [
                    'CONDITIONS' => $weather['textDescription'],
                    'TEMPERATURE_DEGREES' => $weather['temperature']['value'],
                    'HEAT_INDEX_DEGREES' => $weather['heatIndex']['value'],
                    'WIND_CHILL_DEGREES' => $weather['windChill']['value'],
                    'HUMIDITY_PERCENT' => $weather['relativeHumidity']['value'],
                    'DEW_POINT_DEGREES' => $weather['dewpoint']['value'],
                    'WIND_SPEED_DISTANCEPH' => $weather['windSpeed']['value'],
                    'WIND_DIRECTION' => $wind_directions[intval(round(8.0 * floatval($weather['windDirection']['value']) / 360.0))],
                    'WIND_GUST_DISTANCEPH' => $weather['windGust']['value'],
                    'VISIBILITY_DISTANCE' => $weather['visibility']['value'],
                    'BAROMETRIC_PRESSURE_HG' => $weather['barometricPressure']['value'],
                ]
            ];
        }

        return null;
    }

    /**
     * Find the forecast of a given latitude / longitude.
     *
     * @param  float $latitude Latitude
     * @param  float $longitude Longitude
     * @param  ?integer $max_days Maximum number of days to return(null: no limit)
     * @return ?array Array of forecasts (null: not found)
     */
    protected function lookup_forecast(float $latitude, float $longitude, ?int $max_days = null) : ?array
    {
        $gridpoint = $this->lookup_gridpoint($latitude, $longitude);
        if (!is_array($gridpoint)) {
            return null;
        }

        // Force using metric units for consistency with observations, which does not support units
        $response = cache_and_carry('cms_http_request', [self::SERVER . '/gridpoints/' . $gridpoint[0] . '/' . strval($gridpoint[1]) . ',' . strval($gridpoint[2]) . '/forecast?units=si', ['convert_to_internal_encoding' => true, 'trigger_error' => false, 'ignore_http_status' => true, 'timeout' => 10.0]], 60);
        list($data, , , , $http_message) = $response;

        $result = @json_decode($data, true);
        if (!is_array($result)) {
            $errormsg = do_lang('WEATHER_ERROR', 'NWS', $http_message . ' error, ' . $data);
            throw new Exception($errormsg);
        }

        if (!isset($result['properties'])) {
            return null;
        }
        if (!isset($result['properties']['periods'])) {
            return null;
        }

        require_code('temporal');

        $days_processed = 0.0;
        $forecast = [];
        foreach ($result['properties']['periods'] as $period) {
            $forecast[] = [
                'TIMESTAMP_START' => get_timezoned_date_time(strtotime($period['startTime']), false),
                'TIMESTAMP_END' => get_timezoned_date_time(strtotime($period['endTime']), false),
                'FORECAST_DAY' => $period['name'],
                'LOCATION' => $gridpoint[3] . ', ' . $gridpoint[4],
                'ICON_URL' => $period['icon'],
                'UNITS' => 'metric',
                'VALUES' => [
                    'FORECAST' => $period['detailedForecast'],
                    'TEMPERATURE_HIGH_DEGREES' => $period['isDaytime'] ? $period['temperature'] : null,
                    'TEMPERATURE_LOW_DEGREES' => $period['isDaytime'] ? null : $period['temperature'],
                    'PRECIPITATION_PROBABILITY_PERCENT' => $period['probabilityOfPrecipitation']['value'],

                    'HUMIDITY_PERCENT' => $period['relativeHumidity']['value'],

                    'WIND_SPEED' => $period['windSpeed'],
                    'WIND_DIRECTION' => $period['windDirection'],
                ],
            ];

            $days_processed += 0.5;
            if ($days_processed >= floatval($max_days)) {
                return $forecast;
            }
        }

        return $forecast;
    }
}
