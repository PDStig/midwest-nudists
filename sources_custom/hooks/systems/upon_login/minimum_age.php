<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    minimum_age
 */

/**
 * Hook class.
 */
class Hook_upon_login_minimum_age
{
    /**
     * Standard upon login hook.
     *
     * @param  boolean $new_attempt Whether it is a new login attempt
     * @param  string $username Username
     * @param  ?MEMBER $member_id Member ID of already-confirmed login
     */
    public function run(bool $new_attempt, string $username, ?int $member_id)
    {
        if (!addon_installed('minimum_age')) {
            return;
        }

        if (get_forum_type() != 'cns') {
            return;
        }

        if (get_option('minimum_age') == '0') {
            return;
        }

        if (($member_id === null) && is_guest($member_id)) {
            return;
        }

        $row = $GLOBALS['FORUM_DRIVER']->get_member_row($member_id);

        // Allow if member does not have date of birth set
        if (($row['m_dob_year'] === null) || ($row['m_dob_month'] === null) || ($row['m_dob_day'] === null)) {
            return;
        }

        require_code('temporal');

        // Allow if member is old enough
        if (intval(floor(utctime_to_usertime(time() - mktime(0, 0, 0, $row['m_dob_month'], $row['m_dob_day'], $row['m_dob_year'])) / 31536000.0)) >= intval(get_option('minimum_age'))) {
            return;
        }

        // By now, member is not old enough. Log them out, send them to the home page, and show an error about it.
        require_code('users_active_actions');
        require_code('site2');
        require_lang('minimum_age');

        handle_active_logout();
        redirect_exit(build_url(['page' => '']), do_lang_tempcode('MINIMUM_AGE_VIOLATION_TITLE'), do_lang_tempcode('MINIMUM_AGE_VIOLATION_TEXT', get_option('minimum_age')), false, 'warn');
    }
}
