<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    mwn_verification
 */

/**
 * Hook class.
 */
class Hook_cns_cpf_filter_mwn_verification
{
    /**
     * Find which special CPFs to enable.
     *
     * @return array A list of CPFs to enable
     */
    public function to_enable() : array
    {
        if (!addon_installed('mwn_verification')) {
            return [];
        }
        if (!addon_installed('tickets')) {
            return [];
        }

        require_lang('mwn_verification');

        return ['verified_until' => true];
    }
}
