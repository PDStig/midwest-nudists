<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    mwn_misc
 */

/**
 * Hook class.
 */
class Hook_contentious_overrides_mwn_misc
{
    public function require_lang_compile(&$load_target, $codename, $lang)
    {
        if (!addon_installed('mwn_misc')) {
            return;
        }

        if ($lang != 'EN') {
            return;
        }

        if ($codename == 'cns') {
            $load_target['DESCRIPTION_I_AGREE_RULES'] = 'Before you may create an account, you must read and agree to our legally-binding Terms and Conditions. Please read through the Terms and Conditions, check the declarations at the bottom, then click either agree &amp; proceed, or disagree.';
            $load_target['SUBMIT_UNVALIDATED_FORUM_TOPICS'] = 'Thank you for your submission. It will require review by the staff before it is validated / goes live. The staff have been notified.';
            $load_target['SUBMIT_UNVALIDATED_FORUM_POSTS'] = 'Thank you for your submission. It will require review by the staff before it is validated / goes live. The staff have been notified.';
        }

        if ($codename == 'cns_warnings') {
            $load_target['NEW_WARNING_TO_YOU'] = 'Terms and Conditions: Notice of Violation (warning)';
        }

        if ($codename == 'report_content') {
            $load_target['DESCRIPTION_REPORT_POST'] = 'You are making a report on the post &ldquo;{1}&rdquo;. Only report a post if it violates the {2}. Please do not report posts for any other reason (such as grammar errors); Contact Us instead in the menu under Help > Contact Us. <br /><br />In the box below, explain why you believe this post violates the rules. Your report will be submitted as a confidential support ticket between you and staff with a copy of the post made in the ticket. Be aware if you file the report as a guest{3}, staff cannot further communicate with you if the report is inconclusive (which could result in no action being taken).';
        }

        if ($codename == 'global') {
            $load_target['SUBMIT_UNVALIDATED'] = 'Thank you for your submission. It will require review by the staff before it is validated / goes live. The staff have been notified.';
        }
    }

    public function compile_template(&$data, $template_name, $theme, $lang, $suffix, $directory)
    {
        if (!addon_installed('mwn_misc')) {
            return;
        }

        // Inject list of e-mails blacklisted / known to not work with Midwest Nudists
        if (in_array($template_name, ['CNS_JOIN_STEP2_SCREEN', 'ECOM_PURCHASE_STAGE_GUEST', 'BOOKING_JOIN_OR_LOGIN_SCREEN'])) {
            if (strpos($data, '{+START,INCLUDE,CNS_JOIN_STEP2_SCREEN}') === false) { // Hybridauth workaround
                $data = override_str_replace_exactly(
                    '{!ENTER_PROFILE_DETAILS}',
                    <<<TEMPCODE
                        <ditto>
                        </p>
                        <p>
                            {+START,INCLUDE,CNS_JOIN_EMAIL_BLACKLISTS}{+END}
                        </p>
                    TEMPCODE,
                    $data
                );
            }
        }

        // Inject custom message if a Wiki page is blank
        if (in_array($template_name, ['WIKI_PAGE_SCREEN'])) {
            $data = override_str_replace_exactly(
                '{+START,IF_NON_EMPTY,{DESCRIPTION}}',
                <<<TEMPCODE
                    {+START,IF_EMPTY,{DESCRIPTION}}
                        {!mwn_misc:MWN_EMPTY_WIKI}
                    {+END}
                    <ditto>
                TEMPCODE,
                $data
            );
        }
    }
}
