<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    minimum_age
 */

/**
 * Hook class.
 */
class Hook_contentious_overrides_minimum_age
{
    public function require_lang_compile(&$load_target, $codename, $lang)
    {
        if (!addon_installed('minimum_age')) {
            return;
        }

        if ($lang != 'EN') {
            return;
        }

        if ($codename == 'cns') {
            $load_target['DATE_OF_BIRTH_NO_SELF_EDIT'] = '<strong>Make sure you put in your date of birth correctly; it cannot be later edited, and you cannot request for it to be changed.</strong> If you specify the wrong date of birth, then you cannot verify your age should you wish to upload media to the site.';
        }

        if ($codename == 'privacy') {
            $load_target['PRIVACY_EXPLANATION_COPPA'] = 'Those under the age of {1} may not create an account on our website. This is due to the gray, slippery laws of the United States surrounding child pornography. Any media, whether sexual or not, containing a minor who is partially or fully nude can be classified as child pornography under U.S. law. Furthermore, there are severe ramifications allowing a minor independent access to our website. For these reasons, we do not allow those under the age of {1} to create their own account.\n\nHowever, those under the age of {1} can still view or use the site through the proxy of a parent or legal guardian who is {1} years or over. The parent or legal guardian must have an account of their own, must agree to the Terms and Conditions, and must be controlling / guiding all actions the child wishes to perform on the website.';
        }
    }
}
