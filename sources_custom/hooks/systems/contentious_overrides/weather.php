<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    weather
 */

/**
 * Hook class.
 */
class Hook_contentious_overrides_weather
{
    public function require_lang_compile(&$load_target, $codename, $lang)
    {
        if ($lang != 'EN') {
            return;
        }

        if ($codename == 'fields') {
            $load_target['FIELD_TYPE_weather'] = 'Weather conditions / forecast for a given location or latitude,longitude';
        }
    }
}
