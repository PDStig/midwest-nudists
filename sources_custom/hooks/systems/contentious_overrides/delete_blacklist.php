<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    delete_blacklist
 */

/**
 * Hook class.
 */
class Hook_contentious_overrides_delete_blacklist
{
    public function call_included_code($path, $codename, &$code)
    {
        if (!addon_installed('delete_blacklist') || !addon_installed('cns_warnings')) {
            return;
        }

        if ($codename == 'cns_members_action2') {
            if ($code === null) {
                $code = clean_php_file_for_eval(file_get_contents($path));
            }

            require_code('override_api');

            insert_code_before__by_command(
                $code,
                'cns_check_name_valid',
                'return null;',
                "
                require_lang('delete_blacklist');

                // Check IP blacklist
                \$blacklisted = \$GLOBALS['SITE_DB']->query_select_value_if_there('blacklist', 'id', ['b_ip_address' => get_ip_address()]);
                if (\$blacklisted !== null) {
                    \$error = do_lang_tempcode('IP_ADDRESS_BLACKLISTED');
                    if (\$return_errors) {
                        return \$error;
                    }
                    warn_exit(\$error);
                }

                // Check username blacklist
                if (\$username !== null) {
                    \$blacklisted = \$GLOBALS['SITE_DB']->query_select_value_if_there('blacklist', 'id', ['b_username' => \$username]);
                    if (\$blacklisted !== null) {
                        \$error = do_lang_tempcode('USERNAME_BLACKLISTED');
                        if (\$return_errors) {
                            return \$error;
                        }
                        warn_exit(\$error);
                    }
                }

                // Check email address blacklist
                if ((\$email_address !== null) && (\$email_address != '')) {
                    \$blacklisted = \$GLOBALS['SITE_DB']->query_select_value_if_there('blacklist', 'id', ['b_email_address' => \$email_address]);
                    if (\$blacklisted !== null) {
                        \$error = do_lang_tempcode('EMAIL_ADDRESS_BLACKLISTED');
                        if (\$return_errors) {
                            return \$error;
                        }
                        warn_exit(\$error);
                    }
                }"
            );
        }

        if ($codename == 'hooks/systems/profiles_tabs_edit/delete') {
            if ($code === null) {
                $code = clean_php_file_for_eval(file_get_contents($path));
            }

            require_code('override_api');

            insert_code_before__by_command(
                $code,
                'render_tab',
                '// Queue the task',
                "
                require_code('cns_members_action');
            require_code('cns_members_action2');

            // Blacklist the member if they have any warnings on their account to prevent a moderation loophole
            if ((get_forum_type() == 'cns') && addon_installed('delete_blacklist') && addon_installed('cns_warnings')) {
                require_code('cns_moderation');
                require_lang('delete_blacklist');
                if (count(cns_get_warnings(\$member_id_of)) > 0) {
                    \$banned_ip = \$GLOBALS['FORUM_DRIVER']->get_member_row_field(\$member_id_of, 'm_ip_address');
                    \$GLOBALS['SITE_DB']->query_insert('blacklist', [
                        'b_username' => \$GLOBALS['FORUM_DRIVER']->get_username(\$member_id_of, false, USERNAME_DEFAULT_BLANK),
                        'b_email_address' => \$GLOBALS['FORUM_DRIVER']->get_member_email_address(\$member_id_of),
                        'b_ip_address' => \$banned_ip,
                        'b_time' => time()
                    ]);
                }
            }"
            );

            insert_code_before__by_command(
                $code,
                'render_tab',
                'return [$title, $fields, $text, $js_function_calls, $order, null, \'tabs/member_account/edit/delete\'];',
                "
                \$active_warnings = \$GLOBALS['FORUM_DB']->query_select_value('f_warnings', 'COUNT(*)', ['w_member_id' => \$member_id_of, 'w_is_warning' => 1], '');
                if ((\$active_warnings !== null) && (\$active_warnings > 0)) {
                    require_lang('delete_blacklist');
                    \$text->attach(do_template('MESSAGE', [
                        'TYPE' => 'warn',
                        'MESSAGE' => do_lang_tempcode('MEMBER_DELETE_WITH_WARNINGS'),
                    ]));
                }"
            );
        }
    }
}
