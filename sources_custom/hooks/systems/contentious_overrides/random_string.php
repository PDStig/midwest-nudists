<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    random_string
 */

/**
 * Hook class.
 */
class Hook_contentious_overrides_random_string
{
    public function require_lang_compile(&$load_target, $codename, $lang)
    {
        if (!addon_installed('random_string')) {
            return;
        }

        if ($lang != 'EN') {
            return;
        }

        if ($codename == 'fields') {
            $load_target['FIELD_TYPE_random_string'] = 'A cryptographically-secure random string of 13 characters';
        }
    }

    public function call_included_code($path, $codename, &$code)
    {
        // Add random string field code
        if ($codename == 'form_templates') {
            require_code('form_templates__random_string');
        }
    }
}
