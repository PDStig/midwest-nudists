<?php /*

 Composr
 Copyright (c) ocProducts, 2004-2022

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  ocProducts Ltd
 * @package    mwn_verification
 */

if (!function_exists('has_privilege')) {

    /**
     * Find if a member has a specified privilege.
     *
     * @param  MEMBER $member_id The member being checked whether to have the privilege
     * @param  ID_TEXT $privilege The privilege being checked for
     * @param  ?ID_TEXT $page The page being checked (null: current page)
     * @param  ?mixed $cats A list of cat details to require access to (c-type-1,c-id-1,c-type-2,c-d-2,...), or a string of the cat type if you will accept overrides in any matching cat (null: N/A)
     * @return boolean Whether the member has the privilege
     */
    function has_privilege(int $member_id, string $privilege, ?string $page = null, $cats = null) : bool
    {
        // Skip if addon not installed
        if (!addon_installed('mwn_verification')) {
            return non_overridden__has_privilege($member_id, $privilege, $page, $cats);
        }

        // No need to verify guests
        if (is_guest($member_id)) {
            return non_overridden__has_privilege($member_id, $privilege, $page, $cats);
        }

        if ($page === null) {
            $page = str_replace('-', '_', get_param_string('page', '')); // Not get_page_name for bootstrap order reasons
        }

        // List of privileges that should be denied if the member is not verified (page independent)
        $mwn_verification_privileges = [
            'exceed_filesize_limit',
            'mass_import',
            'own_avatars',
            'upload_anything_filedump',
            'upload_filedump',
        ];

        // List of privileges that should be denied if the member is not verified (page dependent, key as privilege and value as array of pages)
        $mwn_verification_privileges_page = [
            'submit_midrange_content' => [
                'cms_downloads',
                'cms_galleries',
                'cms_banners',
            ],
            'edit_own_midrange_content' => [
                'cms_downloads',
                'cms_galleries',
                'cms_banners',
            ],
            'edit_midrange_content' => [
                'cms_downloads',
                'cms_galleries',
                'cms_banners',
            ]
        ];

        // If privilege is not in the list, go back to original code.
        if (!in_array($privilege, $mwn_verification_privileges) && (!array_key_exists($privilege, $mwn_verification_privileges_page) || !in_array($page, $mwn_verification_privileges_page[$privilege]))) {
            return non_overridden__has_privilege($member_id, $privilege, $page, $cats);
        }

        require_lang('mwn_verification');
        require_code('mwn_verification');

        // Deny if current member is not verified
        if (!mwn_can_upload_content(get_member(), get_zone_name() . ':' . $page)) {
            mwn_show_not_verified_message(get_member());
            return false;
        }

        // Member is verified
        return non_overridden__has_privilege($member_id, $privilege, $page, $cats);
    }
}
