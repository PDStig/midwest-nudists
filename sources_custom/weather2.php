<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    weather
 */

/**
 * Standard code module initialisation function.
 *
 * @ignore
 */
function init__weather2()
{
    require_lang('weather');
    require_lang('weather2');
}

/**
 * Return the WEATHER template having formatted the weather data.
 *
 * @param  array $current_conditions Current weather observations
 * @param  array $forecast Weather forecast
 * @param  ID_TEXT $units The units to use
 * @set metric imperial
 * @return Tempcode WEATHER
 */
function do_weather_template(array $current_conditions, array $forecasts, string $units = 'metric') : object
{
    $err = new Tempcode();
    if (!addon_installed__messaged('weather', $err)) {
        return $err;
    }

    // Process our weather properties
    foreach ($current_conditions['VALUES'] as $codename => $value) {
        if ($current_conditions['VALUES'][$codename] === null) {
            unset($current_conditions['VALUES'][$codename]);
        } else {
            $current_conditions['VALUES'][$codename] = process_weather_value($codename, $value, (isset($current_conditions['UNITS']) ? $current_conditions['UNITS'] : $units));
        }
    }
    $current_conditions['VALUES'] = array_values($current_conditions['VALUES']);

    foreach ($forecasts as $i => $forecast) {
        foreach ($forecast['VALUES'] as $codename => $value) {
            if ($forecasts[$i]['VALUES'][$codename] === null) {
                unset($forecasts[$i]['VALUES'][$codename]);
            } else {
                $forecasts[$i]['VALUES'][$codename] = process_weather_value($codename, $value, isset($forecasts[$i]['UNITS']) ? $forecasts[$i]['UNITS'] : $units);
            }
        }

        $forecasts[$i]['VALUES'] = array_values($forecasts[$i]['VALUES']);
    }

    $tpl_map = [
        'LOCATION' => isset($current_conditions['LOCATION']) ? $current_conditions['LOCATION'] : null,

        'CURRENT_CONDITIONS' => [$current_conditions],
        'FORECASTS' => $forecasts,
    ];

    return do_template('WEATHER', $tpl_map);
}

/**
 * Process a given weather property into a Tempcode string.
 *
 * @param  ID_TEXT $codename The weather language string (it should accept $value as the first parameter and unit identifier as second parameter when applicable)
 * @param  mixed $value The value of this property
 * @param  ID_TEXT $units The units to use
 * @set metric imperial
 * @return Tempcode A string representation of the weather property
 */
function process_weather_value(string $codename, $_value, string $units = 'metric') : object
{
    if ($_value === null) {
        if ($codename == '') {
            return comcode_to_tempcode(do_lang('NA'));
        }
        return do_lang_tempcode($codename, do_lang('NA'), escape_html(''));
    }

    // Determine our unit of measurement
    $unit = '';
    if ($units == 'metric') {
        if (substr_compare($codename, '_PERCENT', -8) == 0) {
            $unit = '%';
        } elseif (substr_compare($codename, '_DEGREES', -8) == 0) {
            $unit = 'ºC';
        } elseif (substr_compare($codename, '_DISTANCEPH', -11) == 0) {
            $unit = 'km/h';
        } elseif (substr_compare($codename, '_DISTANCE', -9) == 0) {
            $unit = 'm';
        } elseif (substr_compare($codename, '_HG', -3) == 0) {
            $unit = 'Pa';
        } elseif (substr_compare($codename, '_DEPTH', -6) == 0) {
            $unit = 'mm';
        } elseif (substr_compare($codename, '_DEPTH_RATE', -11) == 0) {
            $unit = 'mm/h';
        }
    } elseif ($units == 'imperial') {
        if (substr_compare($codename, '_PERCENT', -8) == 0) {
            $unit = '%';
        } elseif (substr_compare($codename, '_DEGREES', -8) == 0) {
            $unit = 'ºF';
        } elseif (substr_compare($codename, '_DISTANCEPH', -11) == 0) {
            $unit = 'mph';
        } elseif (substr_compare($codename, '_DISTANCE', -9) == 0) {
            $unit = 'ft';
        } elseif (substr_compare($codename, '_HG', -3) == 0) {
            $unit = 'in/Hg';
        } elseif (substr_compare($codename, '_DEPTH', -6) == 0) {
            $unit = 'in';
        } elseif (substr_compare($codename, '_DEPTH_RATE', -11) == 0) {
            $unit = 'in/h';
        }
    }

    // Determine what to do with our value
    if (is_numeric($_value)) {
        $value = integer_format(intval(round(floatval($_value))));
    } else {
        $value = strval($_value);
    }

    if ($codename == '') {
        return comcode_to_tempcode(comcode_escape($value) . comcode_escape($unit));
    }
    return do_lang_tempcode($codename, escape_html($value), escape_html($unit));
}
