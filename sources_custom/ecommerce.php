<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    community_billboard
 */

/**
 * Find the 'discounted' price for a product (check the return values carefully).
 *
 * @param  array $details Product details
 * @param  ?MEMBER $member_id The member who this is for (null: current member)
 * @return array A tuple: Discounted price (null is no discount), Discounted price tax code, Points required to get discount (null is no discount), Whether this is a discount
 */
function get_discounted_price(array $details, ?int $member_id = null) : array
{
    // No discount for guests (because guests do not use points) or if points is not installed
    if ((!addon_installed('points')) || (is_guest())) {
        return [
            null,
            $details['tax_code'],
            null,
            false,
        ];
    }

    if ($member_id === null) {
        $member_id = get_member();
    }

    if (($details['price'] === null/*has to be points as no monetary-price*/) || (($details['discount_points__price_reduction'] !== null) && (float_to_raw_string($details['discount_points__price_reduction'], 2) == float_to_raw_string($details['price'])))) {
        return [
            0.00,
            '0.00',
            $details['price_points'],
            false,
        ];
    }

    // OVERRIDDEN code for Midwest Nudists below

    require_code('points');

    $discount_rates = [ // Must be in reverse order
        1.0,
        0.75,
        0.5,
        0.25
    ];

    if (($details['discount_points__num_points'] !== null) && ($details['discount_points__price_reduction'] !== null)) {
        foreach ($discount_rates as $discount_rate) {
            $details['discount_points__num_points'] = intval(round(floatval($details['price_points']) * $discount_rate));
            $details['discount_points__price_reduction'] = $details['price'] * $discount_rate;

            if ((points_balance($member_id) >= $details['discount_points__num_points'])) {
                $discounted_price = max(0.00, $details['price'] - $details['discount_points__price_reduction']);
                return [
                    $discounted_price,
                    tax_multiplier($details['tax_code'], ($discounted_price / $details['price'])),
                    $details['discount_points__num_points'],
                    true,
                ];
            }
        }
    }

    return [
        null,
        $details['tax_code'],
        null,
        false,
    ];
}
