<?php /*

 Composr
 Copyright (c) Christopher Graham, 2004-2024

 See docs/LICENSE.md for full licensing information.

*/

/**
 * @license    http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
 * @copyright  Christopher Graham
 * @package    cns_warnings_standing
 */

/**
 * Block class.
 */
class Block_main_account_standing
{
    /**
     * Find details of the block.
     *
     * @return ?array Map of block info (null: block is disabled)
     */
    public function info() : ?array
    {
        $info = [];
        $info['author'] = 'Patrick Schmalstig';
        $info['organisation'] = 'PDStig, LLC';
        $info['hacked_by'] = null;
        $info['hack_version'] = null;
        $info['version'] = 1;
        $info['locked'] = false;
        $info['parameters'] = ['param'];
        $info['addon'] = 'cns_warnings_standing';
        $info['min_cms_version'] = 11.0;
        return $info;
    }

    public function caching_environment()
    {
        $info = [];
        $info['cache_on'] = <<<'PHP'
            [
                $map,
                $GLOBALS['SITE_DB']->query_select_value('f_warnings', 'COUNT(*)', ['w_member_id' => $map['param']]),
                $GLOBALS['SITE_DB']->query_select_value('f_warnings_punitive', 'COUNT(*)', ['p_member_id' => $map['param']]),
            ]
         PHP;
        $info['ttl'] = 15;
        return $info;
    }

    /**
     * Execute the block.
     *
     * @param  array $map A map of parameters
     * @return Tempcode The result of execution
     */
    public function run(array $map) : object
    {
        i_solemnly_declare(I_UNDERSTAND_SQL_INJECTION | I_UNDERSTAND_XSS | I_UNDERSTAND_PATH_INJECTION);

        require_code('temporal');

        $block_id = get_block_id($map);
        $member_id = intval($map['param']);

        $criteria = [
            'formal_warnings' => 0,
            'bad_karma' => 0,
            'previously_banned' => 0,
            'spam_syndications' => 0,
            'points_charged' => 0,
            'probation_total' => 0, // Days
            'probation_remaining' => 0, // Seconds
            'silence_total' => 0, // Count
            'silence_topics_now' => [],
            'silence_forums_now' => [],
            'privileges_revoked_total' => 0,
            'privileges_revoked_now' => [],
            'access_revoked_total' => 0,
            'access_revoked_now' => [],
        ];

        // Calculate our criteria for account standing based on punitive actions and warnings
        $warnings = $GLOBALS['SITE_DB']->query_select('f_warnings', ['id', 'w_member_id', 'w_is_warning'], ['w_member_id' => $member_id, 'w_is_warning' => 1]);
        foreach ($warnings as $warning) {
            $criteria['formal_warnings']++;

            $punitive_actions = $GLOBALS['SITE_DB']->query_select('f_warnings_punitive', ['*'], ['p_warning_id' => $warning['id'], 'p_reversed' => 0]);
            foreach ($punitive_actions as $action) {
                if (($action['p_action'] == '_PUNITIVE_BAD_KARMA') && addon_installed('karma')) {
                    $criteria['bad_karma'] += intval($action['p_param_a']);
                }
                if (($action['p_action'] == '_PUNITIVE_IP_BANNED') || ($action['p_action'] == '_PUNITIVE_BAN_ACCOUNT')) {
                    $criteria['previously_banned']++;
                }
                if ($action['p_action'] == '_PUNITIVE_STOP_FORUM_SPAM') {
                    $criteria['spam_syndications']++;
                }
                if ($action['p_action'] == '_PUNITIVE_PROBATION') {
                    $criteria['probation_total'] += intval($action['p_param_a']);
                }
                if (($action['p_action'] == '_PUNITIVE_SILENCE_FROM_TOPIC') || ($action['p_action'] == '_PUNITIVE_SILENCE_FROM_FORUM')) {
                    $criteria['silence_total']++;
                }
                if ($action['p_action'] == '_PUNITIVE_REVOKE_PRIVILEGE') {
                    $criteria['privileges_revoked_total']++;
                }
                if (($action['p_action'] == '_PUNITIVE_REVOKE_CATEGORY_ACCESS') || ($action['p_action'] == '_PUNITIVE_REVOKE_PAGE_ACCESS') || ($action['p_action'] == '_PUNITIVE_REVOKE_ZONE_ACCESS')) {
                    $criteria['access_revoked_total']++;
                }
            }
        }

        // Calculate our criteria for account standing based on current restrictions
        $probation = $GLOBALS['FORUM_DRIVER']->get_member_row_field($member_id, 'm_on_probation_until');
        if (($probation !== null) && ($probation > time())) {
            $criteria['probation_remaining'] = intval($probation) - time();
        }

        $privilege_restrictions = $GLOBALS['SITE_DB']->query_parameterised("SELECT * FROM {prefix}member_privileges WHERE member_id={member_id} AND the_value=0 AND active_until>{current_time}", ['member_id' => $member_id, 'current_time' => time()]);
        foreach ($privilege_restrictions as $restriction) {
            if (($restriction['privilege'] == 'submit_lowrange_content') && ($restriction['module_the_name'] == 'topics')) {
                $forum_id = $GLOBALS['FORUM_DB']->query_select_value('f_topics', 't_forum_id', ['id' => $restriction['category_name']]);
                $criteria['silence_topics_now'][] = [
                    'TOPIC_NAME' => $GLOBALS['FORUM_DB']->query_select_value('f_topics', 't_cache_first_title', ['id' => $restriction['category_name']]),
                    'FORUM_NAME' => $GLOBALS['FORUM_DB']->query_select_value('f_forums', 'f_name', ['id' => $forum_id]),
                    'TIME_LEFT' => ($restriction['active_until'] !== null) ? display_time_period($restriction['active_until'] - time()) : do_lang('INDEFINITE_TIME'),
                ];
            } elseif (($restriction['privilege'] == 'submit_lowrange_content') && ($restriction['module_the_name'] == 'forums')) { // We ignore midrange entry to prevent duplicates
                $criteria['silence_forums_now'][] = [
                    'FORUM_NAME' => $GLOBALS['FORUM_DB']->query_select_value('f_forums', 'f_name', ['id' => $restriction['category_name']]),
                    'TIME_LEFT' => ($restriction['active_until'] !== null) ? display_time_period($restriction['active_until'] - time()) : do_lang('INDEFINITE_TIME'),
                ];
            } else {
                if (($restriction['privilege'] == 'submit_midrange_content') && ($restriction['module_the_name'] == 'forums')) {
                    continue; // No duplicates for forum silencing
                }
                $criteria['privileges_revoked_now'][] = [
                    'PRIVILEGE' => $restriction['privilege'],
                    'THE_PAGE' => $restriction['the_page'],
                    'MODULE_THE_NAME' => $restriction['module_the_name'],
                    'CATEGORY_NAME' => strval($restriction['category_name']),
                    'TIME_LEFT' => ($restriction['active_until'] !== null) ? display_time_period($restriction['active_until'] - time()) : do_lang('INDEFINITE_TIME'),
                ];
            }
        }

        // TODO: zone/page/category access restrictions when they are supported

        if ($criteria['previously_banned'] > 0) { // Start of danger criteria
            $standing = '1';
        } elseif ($criteria['spam_syndications'] > 0) {
            $standing = '1';
        } elseif ($criteria['bad_karma'] >= 250) {
            $standing = '1';
        } elseif ($criteria['formal_warnings'] >= 6) {
            $standing = '1';
        } elseif ($criteria['probation_total'] >= 14) {
            $standing = '1';
        } elseif ($criteria['probation_remaining'] > 0) { // Start of probation criteria
            $standing = '2';
        } elseif ((count($criteria['silence_topics_now']) + count($criteria['silence_forums_now'])) > 0) { // Start of restricted criteria
            $standing = '3';
        } elseif (count($criteria['privileges_revoked_now']) > 0) {
            $standing = '3';
        } elseif (count($criteria['access_revoked_now']) > 0) {
            $standing = '3';
        } elseif ($criteria['bad_karma'] >= 100) { // Start of at risk criteria
            $standing = '4';
        } elseif ($criteria['formal_warnings'] >= 3) {
            $standing = '4';
        } elseif ($criteria['probation_total'] > 0) {
            $standing = '4';
        } elseif ($criteria['privileges_revoked_total'] > 0) {
            $standing = '4';
        } elseif ($criteria['access_revoked_total'] > 0) {
            $standing = '4';
        } elseif ($criteria['formal_warnings'] >= 1) { // Start of caution criteria
            $standing = '5';
        } else {
            $standing = '6';
        }

        $summary = do_lang_tempcode('DESCRIPTION_ACCOUNT_STANDING_' . $standing);

        return do_template('BLOCK_MAIN_ACCOUNT_STANDING', [
            '_GUID' => 'dfvewhduiorb43bfjhcbkddfv',
            'BLOCK_ID' => $block_id,
            'STANDING' => $standing,
            'SUMMARY' => $summary,
            'PREVIOUSLY_BANNED' => strval($criteria['previously_banned']),
            'SPAM_SYNDICATION' => strval($criteria['spam_syndications']),
            'BAD_KARMA' => strval($criteria['bad_karma']),
            'FORMAL_WARNINGS' => strval($criteria['formal_warnings']),
            'PROBATION_TOTAL' => strval($criteria['probation_total']),
            'PROBATION_REMAINING' => display_time_period($criteria['probation_remaining']),
            '_PROBATION_REMAINING' => strval($criteria['probation_remaining']),
            'SILENCE_TOTAL' => strval($criteria['silence_total']),
            'SILENCE_FORUMS_NOW' => $criteria['silence_forums_now'],
            'SILENCE_TOPICS_NOW' => $criteria['silence_topics_now'],
            'PRIVILEGES_REVOKED_NOW' => $criteria['privileges_revoked_now'],
            'ACCESS_REVOKED_NOW' => $criteria['access_revoked_now'],
            'PRIVILEGES_REVOKED_TOTAL' => strval($criteria['privileges_revoked_total']),
            'ACCESS_REVOKED_TOTAL' => strval($criteria['access_revoked_total']),
        ]);
    }
}
