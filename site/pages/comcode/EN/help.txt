[title="1" sub="{$SITE_NAME*} help"]Help[/title]

{+START,IF,{$HAS_FORUM,1}}[title="2"]{$?,{$IS_GUEST},Logging-in,Your login}[/title]

{$?,{$IS_GUEST},{$COMCODE,You need to [page=":login"]log in[/page] to {$SITE_NAME*} in order to},With your login you} get full access to our features, to interact with other members, to manage your profile, and to be able to manage{+START,IF,{$ADDON_INSTALLED,points}} (and get points for){+END} the content you submit.

{+START,IF,{$ADDON_INSTALLED,points}}[title="2"]Points[/title]

Every logged-in member can gain points as they contribute to our community, in ways like...
 - Posting on the forums
 - Contributing content to {$SITE_NAME*}, such as gallery images

{+START,IF,{$ADDON_INSTALLED,ecommerce}}As you gain points, you can spend them on various fun or useful things. The [page="_SEARCH:purchase" ignore_if_hidden="1"]purchasing[/page] page contains all the items you can buy using your points, such as:
 - Pinning of topics on the forum
 - Highlighting of a member's name on the forum
 - Banner advertisements, and upgrades. Through this you may advertise something of your own that ordinarily the staff would not allocate advertising space for.
 - E-mail addresses. You may be able to purchase forwarding and POP3 e-mail addresses

Products that require manual setup will usually only be charged for upon activation, not at point of sale.{+END}
The points system extends further than this though, as members have the ability to give each-other points.{+START,IF,{$EQ,{$CONFIG_OPTION,enable_gift_points},1}} Members can only give each other points via gift points. Gift points are given out to all our members at a rate of 1 per day for every day they are a member but are useless until they are given to people who you feel have done you or the community a good service. It's like a quota of "thank you"s that you can give out.{+END} For example, somebody might release some works to the community and then members might give them a whole load of points for it, giving them credit for their work.
To send points to a member you visit the points tab of their profile. The point gift will then show on that tab and will also count towards their point total (which they can use to buy things with).
{+START,IF,{$EQ,{$CONFIG_OPTION,enable_gift_points},1}}Occasionally upon giving a gift, you will be awarded points yourself, as a random award for your consideration.{+END}

Members of staff have the ability to give any number of points they like to people, including huge numbers, or even negative numbers. Please the staff and you could be awarded massively in points; abuse our systems and you could be penalised.

Please have fun with the system and use your points well.{+END}
{+START,IF,{$AND,{$ADDON_INSTALLED,news},{$HAS_ACTUAL_PAGE_ACCESS,cms_news}}}[title="2"]News/Blogs[/title]

{$ANCHOR,ADD_NEWS}[page="_SEARCH:cms_news:add" ignore_if_hidden="1"]The add news page[/page] allows you to add your news to {$SITE_NAME*}. For most members, this will put the news into a validation queue and inform the staff of the submission.
{+START,IF,{$HAS_PRIVILEGE,have_personal_category,cms_news}}You also have the opportunity to submit to your own personal news category; this is your own blog on the website.{+END}
{+END}
{+START,IF,{$AND,{$ADDON_INSTALLED,galleries},{$HAS_ACTUAL_PAGE_ACCESS,cms_galleries}}}[title="2"]Galleries[/title]

{$ANCHOR,ADD_MEDIA}The [page="_SEARCH:cms_galleries:add" ignore_if_hidden="1"]add image[/page]/[page="_SEARCH:cms_galleries:add_other" ignore_if_hidden="1"]add video[/page] pages allow you to add your media to {$SITE_NAME*}. For most members, this will put the media into a validation queue and inform the staff of the submission.
{+END}{+END}{+START,IF,{$ADDON_INSTALLED,wiki}}[title="2"]Wiki+[/title]

Wiki+ is a system that allows users to post content into a database of posts. This system is similar to that of a forum, however rather than having a structure of forums and user created topics, it has a staff-managed hierarchical structure of pages.

The system can be used for all kinds of things, such as posting reference material indexed hierarchically by category, organising ideas indexed by topic, or discussing TV programmes indexed by episode name.

The staff will make clear how the Wiki+ system is employed on this website.{+END}

[title="2"]Other things to do[/title]

Other features we have include:
[list]
{+START,IF,{$ADDON_INSTALLED,calendar}}[*] a calendar[/*]{+END}
{+START,IF,{$ADDON_INSTALLED,chat}}[*] chatrooms[/*]{+END}
{+START,IF,{$ADDON_INSTALLED,downloads}}[*] downloads[/*]{+END}
{+START,IF,{$ADDON_INSTALLED,cns_forum}}[*] forums[/*][*] private topics (for private messaging)[/*]{+END}
{+START,IF,{$ADDON_INSTALLED,quizzes}}[*] quizzes[/*]{+END}
[/list]

You may see submission links on the menus, and in other places. Submitting any kind of entry will be a similar process to submitting news: your entry will likely be put in a validation queue.

[block]main_comcode_page_children[/block]
